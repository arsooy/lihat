#include "imageutils.h"

#include "cmimetyperesolver.h"

#include <QByteArray>
#include <QCache>
#include <QFileInfo>
#include <QIcon>
#include <QImageReader>
#include <QList>
#include <QString>


using namespace Lihat;
using namespace Lihat::Utils;

typedef QCache< QString, QIcon > TMIMEIconsCache;
static const unsigned char CACHED_MIME_ICONS_MAX = 25;
Q_GLOBAL_STATIC_WITH_ARGS(TMIMEIconsCache, glMIMEIconsCache, (CACHED_MIME_ICONS_MAX))

QPixmap* Image::createMIMEIconPixmapFromName(const QString& name, const QSize& requestsize)
{
    QIcon mimeicon;

    QFileInfo finfo(name);
    QString fname = finfo.fileName();

    if (!fname.isEmpty())
    {
        if (glMIMEIconsCache()->contains(fname))
            mimeicon = *(glMIMEIconsCache()->object(fname));
        else
        {
            mimeicon = Utils::Image::getMIMEIcon(fname);
            if (!mimeicon.isNull())
            {
                QIcon* pmicon = new QIcon(mimeicon);
                glMIMEIconsCache()->insert(fname, pmicon);
            }
        }
    }

    if (!mimeicon.isNull())
        return new QPixmap(mimeicon.pixmap(requestsize));

    return 0;
}

QStringList Image::getExtensions(const QString& filters)
{
    QStringList ret;
    QStringList wfilters = filters.split(";;");
    if ((wfilters.count() == 1) && (wfilters.at(0) == filters))
        wfilters.clear();

    for (int i = 0; i < wfilters.size(); ++i)
    {
        QString filter = wfilters.at(i);
        filter = filter.section(QRegExp("(\\(|\\))"), 1);
        filter = filter.replace(")", "");
        filter = filter.replace("*.", "");
        ret << filter.split(" ");
    }
    ret.removeDuplicates();

    return ret;
}

QIcon Image::getMIMEIcon(const QString& url)
{
    CMIMETypeResolver* mimetyperesolver = getMIMETypeResolver();

    SMIMEType* mimetype;
    QString mimetypestr;

    mimetypestr = mimetyperesolver->typeNameFromFilename(url);
    mimetype = mimetyperesolver->fromTypeName(mimetypestr);
    if (mimetype)
    {
        QIcon result = QIcon::fromTheme(mimetype->iconName());
        if (result.isNull())
            result = QIcon::fromTheme(mimetype->iconName(true));

        if (!result.isNull())
            return result;
    }

    // try non-subclass mimetype
    mimetypestr = mimetyperesolver->typeNameFromFilename(url, true);
    mimetype = mimetyperesolver->fromTypeName(mimetypestr);
    if (mimetype)
    {
        QIcon result = QIcon::fromTheme(mimetype->iconName());
        if (result.isNull())
            result = QIcon::fromTheme(mimetype->iconName(true));

        if (!result.isNull())
            return result;
    }

    return QIcon();
}

QString Image::getOpenFileFilters()
{
    QList< QByteArray > formats = QImageReader::supportedImageFormats();
    QStringList temp;

    if (formats.contains("jpg"))
        temp << "JPEG (*.jpg *.jpeg)";
    if (formats.contains("gif"))
        temp << "Graphic Interchange Format (*.gif)";
    if (formats.contains("png"))
        temp << "Portable Network Graphics (*.png)";
    if (formats.contains("bmp"))
        temp << "Bitmap (*.bmp)";
    if (formats.contains("pbm"))
        temp << "Portable Bitmap (*.pbm)";
    if (formats.contains("pgm"))
        temp << "Portable Graymap (*.pgm)";
    if (formats.contains("ppm"))
        temp << "Portable Pixmap (*.ppm)";
    if (formats.contains("xbm"))
        temp << "X11 Bitmap (*.xbm)";
    if (formats.contains("xpm"))
        temp << "X11 Pixmap (*.xpm)";

    QStringList exts = getExtensions(temp.join(";;"));
    for (int i = 0; i < formats.size(); ++i)
    {
        if (!exts.contains(QString(formats.at(i))))
            temp << "Image Format (*." + QString(formats.at(i)) + ")";
    }

    // put one all-encompasing filter at top
    exts = getExtensions(temp.join(";;"));
    for (int i = 0; i < exts.size(); ++i)
        exts.replace(i, "*." + exts.at(i));
    temp.insert(0, "All supported formats (" + exts.join(" ") + ")");

    return temp.join(";;");
}
