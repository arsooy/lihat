#ifndef FILEUTILS_H
#define FILEUTILS_H

#include <QString>
#include <QStringList>

namespace Lihat
{
    namespace Utils
    {
        namespace File
        {
            bool     copyFilesTo(const QStringList& files, const QString& destination);
            bool     copyFilesToClipboard(const QStringList& files);
            bool     cutFilesToClipboard(const QStringList& files);
            bool     deleteFiles(const QStringList& files);
            bool     fileIsText(const QString& fname);
            QString  fileSizeToString(quint64 filesize);
            bool     moveFilesTo(const QStringList& files, const QString& destination);
            bool     pasteFilesFromClipboardTo(const QString& destination);
        } // namespace File
    } // namespace Utils
} // namespace Lihat

#endif // FILEUTILS_H
