#ifndef CMAINWINDOW_H
#define CMAINWINDOW_H

#include "config_lihat.h"

#include <QMainWindow>
#include <QMap>
#include <QString>
#include <QUrl>


class QWidget;

namespace Lihat
{
    class CMainWindowPrivate;

    class CMainWindow:
        public QMainWindow
    {
        Q_OBJECT
    public:
        explicit CMainWindow(QWidget* parent = 0);
        virtual ~CMainWindow();

        bool applyCommandArguments(const QStringList& args);

    public slots:
        void goNext();
        void goPrevious();
        void goUp();

    private:
        Q_DECLARE_PRIVATE(CMainWindow)
        CMainWindowPrivate* const  d_ptr;

    private slots:
        void slotEditMenuAboutToShow();
        void slotFileDeleteHovered();
        void slotFileDeleteTriggered();
        void slotFileMenuAboutToShow();
        void slotGenericMenuAboutToShow();
        void slotGoMenuAboutToShow();
        void slotToolsMenuAboutToShow();
        void slotNextHovered();
        void slotOpenTriggered();
        void slotPreviousHovered();
        void slotViewMenuAboutToShow();
        void slotViewWidgetActiveWidgetChanged();
        void slotViewWidgetCurrentChanged();
        void slotViewWidgetImageViewed(const QUrl& imgurl);
        void slotViewWidgetPathChanged(const QUrl& pathurl);

#ifdef FILEOPS_API_NONE
        void slotCopyToHovered();
        void slotCopyToTriggered();
        void slotFileOperationsTriggered();
        void slotMoveToHovered();
        void slotMoveToTriggered();
#endif

    };

    CMainWindow*  getMainWindow();

} // namespace Lihat

#endif // CMAINWINDOW_H
