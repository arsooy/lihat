#include "pathutils.h"

#include <QDir>


using namespace Lihat::Utils;

QString Path::join(const QString& path, const QString& name)
{
    return toClean(path) + QDir::separator() + name;
}

bool Path::parentOfPath(const QString& path, QString& outparent)
{
    QFileInfo finfo(path);
    if (finfo.exists())
        outparent = finfo.absolutePath();
    else
        outparent = QString();

    return !outparent.isEmpty();
}

bool Path::parentOfPath(const QUrl& path, QString& outparent)
{
    return parentOfPath(path.toString(QUrl::StripTrailingSlash), outparent);
}

QString Path::toClean(const QString& path)
{
    QString cpath = toNative(path);
#ifdef Q_OS_WIN32
    if (cpath.endsWith(QDir::separator()))
        cpath.chop(1);
#endif
#ifdef Q_OS_LINUX
    if ((cpath != "/") && cpath.endsWith(QDir::separator()))
        cpath.chop(1);
#endif

    return cpath;
}

QString Path::toNative(const QString& path)
{
    return QDir::toNativeSeparators(path);
}

QStringList Path::urlsToStringList(const QList< QUrl >& urls)
{
    QStringList result;
    for (int i = 0; i < urls.count(); ++i)
    {
        QUrl url = urls.at(i);
        if (url.isLocalFile())
            result << toClean(url.toLocalFile());
    }

    return result;
}
