#include "cbasethumbnailview.h"

#include "../persistenceutils.h"
#include "cbasescheduledtask.h"
#include "cbasethumbnailview_p.h"
#include "ithumbnailsloader.h"
#include "models/cbasethumbnailmodel.h"
#include "sthumbnailitem.h"

#include <limits.h>

#include <QAction>
#include <QDropEvent>
#include <QMimeData>
#include <QPainter>
#include <QPixmap>
#include <QMutex>
#include <QScrollBar>
#include <QString>
#include <QTextOption>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::Thumbnails;

static const QString NO_THUMBNAIL_STR(QListView::tr("Thumbnail not available"));

CBaseThumbnailView::CBaseThumbnailView(QWidget* parent):
    QListView(parent),
    d_ptr(new CBaseThumbnailViewPrivate(this))
{
    Q_D(CBaseThumbnailView);

    d->initActions();
    d->initUserInterface();
}

CBaseThumbnailView::CBaseThumbnailView(CBaseThumbnailViewPrivate& private_d, QWidget* parent):
    QListView(parent),
    d_ptr(&private_d)
{
    Q_D(CBaseThumbnailView);

    d->initActions();
    d->initUserInterface();
}

CBaseThumbnailView::~CBaseThumbnailView()
{
    Q_D(CBaseThumbnailView);

    d->serialize();
    delete d;
}

void CBaseThumbnailView::slotCopyHovered()
{
    Q_D(CBaseThumbnailView);

    QAction* acCopy = d->getAction("acCopy");
    if (acCopy)
        acCopy->setEnabled(!selectedIndexes().isEmpty());
}

void CBaseThumbnailView::slotCopyTriggered()
{
    QModelIndexList indexes = selectedIndexes();
    if (indexes.isEmpty())
        return;

    QModelIndexList siftedindexes;
    for (int i = 0; i < indexes.count(); ++i)
    {
        QModelIndex index = indexes.at(i);
        if (siftIndexForItemOperation(index, IO_COPY))
            siftedindexes << index;
    }

    emit itemsWantCopy(siftedindexes);
}

void CBaseThumbnailView::slotCutHovered()
{
    Q_D(CBaseThumbnailView);

    QAction* acCut = d->getAction("acCut");
    if (acCut)
        acCut->setEnabled(!selectedIndexes().isEmpty());
}

void CBaseThumbnailView::slotCutTriggered()
{
    QModelIndexList indexes = selectedIndexes();
    if (indexes.isEmpty())
        return;

    QModelIndexList siftedindexes;
    for (int i = 0; i < indexes.count(); ++i)
    {
        QModelIndex index = indexes.at(i);
        if (siftIndexForItemOperation(index, IO_CUT))
            siftedindexes << index;
    }

    emit itemsWantCut(siftedindexes);
}

void CBaseThumbnailView::slotDeleteHovered()
{
    Q_D(CBaseThumbnailView);

    QAction* acDelete = d->getAction("acDelete");
    if (acDelete)
        acDelete->setEnabled(!selectedIndexes().isEmpty());
}

void CBaseThumbnailView::slotDeleteTriggered()
{
    QModelIndexList indexes = selectedIndexes();
    if (indexes.isEmpty())
        return;

    QModelIndexList siftedindexes;
    for (int i = 0; i < indexes.count(); ++i)
    {
        QModelIndex index = indexes.at(i);
        if (siftIndexForItemOperation(index, IO_DELETE))
            siftedindexes << index;
    }

    emit itemsWantDelete(siftedindexes);
}

void CBaseThumbnailView::slotPasteHovered()
{
    Q_D(CBaseThumbnailView);

    QAction* acPaste = d->getAction("acPaste");
    if (acPaste)
    {
        // paste-to can be done when selection is at one item or
        // no items at all
        acPaste->setEnabled(selectedIndexes().count() <= 1);
    }
}

void CBaseThumbnailView::slotPasteTriggered()
{
    QModelIndexList indexes = selectedIndexes();

    QModelIndexList siftedindexes;
    for (int i = 0; i < indexes.count(); ++i)
    {
        QModelIndex index = indexes.at(i);
        if (siftIndexForItemOperation(index, IO_PASTE))
            siftedindexes << index;
    }

    emit itemsGetPasted(siftedindexes);
}

bool CBaseThumbnailView::adjustCurrentThumbnailByOffset(const int offset)
{
    Q_D(CBaseThumbnailView);

    QModelIndex sibling = d->currentThumbnailSiblingByOffset(offset);
    if (sibling.isValid())
    {
        setCurrentIndex(sibling);
        return true;
    }

    return false;
}

void CBaseThumbnailView::beginUpdate()
{
    Q_D(CBaseThumbnailView);

    d->InsideUpdateCount++;
    if (d->InsideUpdateCount > 1)
        return;

    d->IgnoreViewportChange = true;
    d->StorePivotIndex      = false;
    d->PivotIndex           = QModelIndex();
    if (d->ClearLocalDataOnBeginUpdate)
        clearThumbnailLocalData();
    d->ThumbnailItemToIndexCache.clear();

    emit updateStarted();
}

void CBaseThumbnailView::clearThumbnailLocalData()
{
    Q_D(CBaseThumbnailView);

    d->clearLocalData();
}

void CBaseThumbnailView::currentChanged(const QModelIndex& current, const QModelIndex& previous)
{
    Q_D(CBaseThumbnailView);

    QListView::currentChanged(current, previous);

    if (d->StorePivotIndex && current.isValid())
        d->PivotIndex = current;
}

void CBaseThumbnailView::dropEvent(QDropEvent* event)
{
    Q_D(CBaseThumbnailView);

    QListView::dropEvent(event);

    if (event->mimeData()->hasUrls())
    {
        d->applyDragKeyModifiers(event);
        processDroppedURLs(event->mimeData()->urls(), event->dropAction(), event->pos());
    }
}

void CBaseThumbnailView::endUpdate()
{
    Q_D(CBaseThumbnailView);

    d->InsideUpdateCount--;
    if (d->InsideUpdateCount > 0)
        return;

    // run tasks scheduled for this point
    QList< CBaseScheduledTask* > tasks = d->ScheduledTasks.values("EndThumbnailsUpdate");
    while (tasks.count() > 0)
    {
        CBaseScheduledTask* task = tasks.takeFirst();
        d->ScheduledTasks.remove("EndThumbnailsUpdate", task);

        switch (task->taskIdent())
        {
            case CScheduledSetCurrentThumbnail::IdSetCurrentThumbnail:
            {
                CScheduledSetCurrentThumbnail* scttask = dynamic_cast< CScheduledSetCurrentThumbnail* >(task);
                if (scttask)
                    scttask->runTask(this);

                break;
            }

            default:
                ;
        }

        delete task;
    }
    d->ScheduledTasks.remove("EndThumbnailsUpdate");

    d->IgnoreViewportChange = false;
    if (isVisible())
    {
        viewportChanged();
        d->StorePivotIndex = true;
    }

    emit updateFinished();
}

QPixmap* CBaseThumbnailView::fetchThumbnailViewPixmap(int pixmapcode)
{
    Q_D(CBaseThumbnailView);

    switch (pixmapcode)
    {
        case (int) SThumbnailItem::TI_FILE:
            return d->CorePixmaps.File;

        case (int) SThumbnailItem::TI_FOLDER:
            return d->CorePixmaps.Folder;

        case (int) SThumbnailItem::TI_INVALID:
            return d->CorePixmaps.NoThumbnail;

        default:
            return 0;
    }
}

QModelIndex CBaseThumbnailView::indexOfThumbnailItem(SThumbnailItem* thumbnailitem)
{
    Q_D(CBaseThumbnailView);

    QModelIndex* cacheindex = d->ThumbnailItemToIndexCache.object(thumbnailitem);
    if (cacheindex && cacheindex->isValid())
        return *cacheindex;

    QAbstractItemModel* mmodel = model();
    Q_ASSERT(mmodel);

    for (int i = 0; i < mmodel->rowCount(); ++i)
    {
        QModelIndex index = mmodel->index(i, 0);
        SThumbnailItem* titem = thumbnailItemAt(index);
        if (titem == thumbnailitem)
        {
            // put in cache for next lookup
            cacheindex = new QModelIndex(index);
            d->ThumbnailItemToIndexCache.insert(titem, cacheindex);

            return index;
        }
    }

    return QModelIndex();
}

SThumbnailLocalData* CBaseThumbnailView::thumbnailLocalData(SThumbnailItem* key, bool autocreate)
{
    Q_D(CBaseThumbnailView);

    SThumbnailLocalData* result = d->ThumbnailLocalData.value(key, 0);
    if (!result && autocreate)
    {
        result = new SThumbnailLocalData();
        d->ThumbnailLocalData.insert(key, result);
    }

    return result;
}

void CBaseThumbnailView::slotHorizontalScrollBarValueChanged(int positionatcall)
{
    Q_D(CBaseThumbnailView);

    if (d->ScrollBarsSettledTimer.Horizontal.isActive())
        d->ScrollBarsSettledTimer.Horizontal.stop();

    if (positionatcall == -1)
        d->LastScrollBarsPosition.Horizontal = horizontalScrollBar()->value();
    else
        d->LastScrollBarsPosition.Horizontal = positionatcall;

    d->ScrollBarsSettledTimer.Horizontal.setSingleShot(true);
    d->ScrollBarsSettledTimer.Horizontal.setInterval(d->SCROLLBAR_SETTLED_TIMEOUT);
    d->ScrollBarsSettledTimer.Horizontal.start();
}

void CBaseThumbnailView::slotVerticalScrollBarValueChanged(int positionatcall)
{
    Q_D(CBaseThumbnailView);

    if (d->ScrollBarsSettledTimer.Vertical.isActive())
        d->ScrollBarsSettledTimer.Vertical.stop();

    if (positionatcall == -1)
        d->LastScrollBarsPosition.Vertical = verticalScrollBar()->value();
    else
        d->LastScrollBarsPosition.Vertical = positionatcall;

    d->ScrollBarsSettledTimer.Vertical.setSingleShot(true);
    d->ScrollBarsSettledTimer.Vertical.setInterval(d->SCROLLBAR_SETTLED_TIMEOUT);
    d->ScrollBarsSettledTimer.Vertical.start();
}

void CBaseThumbnailView::processDroppedURLs(const QList< QUrl >& urls, Qt::DropAction dropaction, const QPoint& pos)
{
    Q_UNUSED(urls);
    Q_UNUSED(dropaction);
    Q_UNUSED(pos);

    // TODO: implement in subclass
}

void CBaseThumbnailView::processIndexesFoundOnViewport(const QModelIndexList& indexes)
{
    Q_D(CBaseThumbnailView);

    bool pivotindexstored = false;
    QModelIndex pivotindex;

    QModelIndex currentindex = currentIndex();
    const SThumbnailItem* currentindextitem = thumbnailItemAt(currentindex);

    int loadcount = 0;
    for (int i = 0; i < indexes.count(); ++i)
    {
        QModelIndex index = indexes.at(i);
        SThumbnailItem* titem = thumbnailItemAt(index);

        if (d->StorePivotIndex && !pivotindexstored &&
            index.isValid() && currentindex.isValid())
        {
            // if current index is in viewport, remember it
            if (currentindextitem && (currentindextitem == titem))
            {
                pivotindex = index;
                pivotindexstored = true;
            }
        }

        if (d->ThumbnailLoader && siftLoadItemThumbnail(index))
            ++loadcount;
    }

    // remember what can be seen at current viewport, this will be used
    // when widget resized to make the view set at around what was seen
    // before the resize.
    if (d->StorePivotIndex)
    {
        if (pivotindex.isValid())
            d->PivotIndex = pivotindex;
        else
            d->PivotIndex = QModelIndex();
    }

    // start loading thumbnails
    if (d->ThumbnailLoader && (loadcount > 0))
        d->ThumbnailLoader->startImageLoadQueue(this);
}

void CBaseThumbnailView::resizeEvent(QResizeEvent* event)
{
    QListView::resizeEvent(event);

    thumbnailViewResized();
}

void CBaseThumbnailView::scrollToIndex(const QModelIndex& index, bool center)
{
    Q_D(CBaseThumbnailView);

    if (!index.isValid())
    {
        // scroll to beginning of view
        if (d->Orientation == Qt::Vertical)
            verticalScrollBar()->setValue(0);
        else if (d->Orientation == Qt::Horizontal)
            horizontalScrollBar()->setValue(0);

        return;
    }
    else
    {
        QListView::ScrollHint scrollhint = QListView::EnsureVisible;
        if (center)
            scrollhint = QListView::PositionAtCenter;
        scrollTo(index, scrollhint);

        if (horizontalScrollBar()->isVisibleTo(this))
            d->LastScrollBarsPosition.Horizontal = horizontalScrollBar()->value();
        if (verticalScrollBar()->isVisibleTo(this))
            d->LastScrollBarsPosition.Vertical = verticalScrollBar()->value();
    }
}

void CBaseThumbnailView::selectionChanged(const QItemSelection& selected, const QItemSelection& deselected)
{
    QListView::selectionChanged(selected, deselected);

    emit itemsSelected(selectedIndexes());
}

bool CBaseThumbnailView::setCurrentThumbnail(const int index, bool installAtEndUpdate)
{
    Q_D(CBaseThumbnailView);

    if (installAtEndUpdate)
    {
        CScheduledSetCurrentThumbnail* task = new CScheduledSetCurrentThumbnail(index);
        return d->addSetCurrentThumbnailScheduledTask("EndThumbnailsUpdate", task);
    }
    else
    {
        QModelIndex mindex = model()->index(index, 0);
        if (mindex.isValid())
        {
            setCurrentIndex(mindex);

            // as this might change the location of viewport if widget is not
            // shown make sure next time it is shown to automatically call
            // viewportChanged to start creating thumbnails
            d->ViewportChangedOnShow = !isVisible();
            return true;
        }
    }

    return false;
}

void CBaseThumbnailView::setThumbnailLoader(IThumbnailsLoader* tloader)
{
    Q_D(CBaseThumbnailView);

    d->ThumbnailLoader = tloader;
}

void CBaseThumbnailView::setThumbnailSize(const QSize& tsize)
{
    Q_D(CBaseThumbnailView);

    if (d->ThumbnailSize != tsize)
    {
        d->ThumbnailSize = tsize;
        thumbnailSizeChanged();
    }

    if (isVisible() && !d->isUpdating())
        viewportChanged();
}

void CBaseThumbnailView::showEvent(QShowEvent* event)
{
    Q_D(CBaseThumbnailView);

    QWidget::showEvent(event);

    if (d->ViewportChangedOnShow)
        viewportChanged();

    d->ViewportChangedOnResize = isVisible();
}

bool CBaseThumbnailView::siftIndexForItemOperation(const QModelIndex& index, EItemOperation itemoper)
{
    Q_UNUSED(index);
    Q_UNUSED(itemoper);
    return true;

    // TODO: implement this in subclass
}

bool CBaseThumbnailView::siftLoadItemThumbnail(const QModelIndex& index)
{
    Q_D(CBaseThumbnailView);

    SThumbnailItem* titem = thumbnailItemAt(index);

    // skip folder items, we'll use CorePixmap.Folder for these items
    if (titem->Type == SThumbnailItem::TI_FOLDER)
        return false;

    SThumbnailLocalData* tlocaldata = thumbnailLocalData(titem, false);
    if (tlocaldata && ((tlocaldata->LoadStatus == TLS_PENDING) || (tlocaldata->LoadStatus == TLS_RELOADNEEDED)))
        return d->ThumbnailLoader->createImageLoadQueueJob(this, index);

    return false;
}

bool CBaseThumbnailView::siftVisibleIndex(const QModelIndex& index)
{
    Q_UNUSED(index);
    return true;

    // TODO: implement this in subclass
}

void CBaseThumbnailView::slotDelayedViewportUpdaterTimeout()
{
    Q_D(CBaseThumbnailView);

    bool oldignoreviewportchange = d->IgnoreViewportChange;
    bool ok = false;
    d->IgnoreViewportChange = false;

    if (d->Orientation == Qt::Horizontal)
        ok = (d->LateUpdaterLastScrollBarsPosition.Horizontal == horizontalScrollBar()->value());
    else if (d->Orientation == Qt::Vertical)
        ok = (d->LateUpdaterLastScrollBarsPosition.Vertical == verticalScrollBar()->value());

    if (ok)
    {
        qDebug() << "CBaseThumbnailView::slotDelayedViewportUpdaterTimeout:" <<
                    "Updating viewport by delayedViewportUpdater.";
        viewportChanged();
    }

    d->IgnoreViewportChange = oldignoreviewportchange;
}

void CBaseThumbnailView::slotHorizontalScrollBarSettledTimeout()
{
    Q_D(CBaseThumbnailView);

    if (horizontalScrollBar()->value() == d->LastScrollBarsPosition.Horizontal)
    {
        qDebug() << "CBaseThumbnailView::slotHorizontalScrollBarSettledTimeout:" <<
                    "Horizontal scrollBar settled at position" << d->LastScrollBarsPosition.Horizontal;
        viewportChanged();
    }
}

void CBaseThumbnailView::slotResizeSettledTimeout()
{
    thumbnailViewResized();
}

void CBaseThumbnailView::slotVerticalScrollBarSettledTimeout()
{
    Q_D(CBaseThumbnailView);

    if (verticalScrollBar()->value() == d->LastScrollBarsPosition.Vertical)
    {
        qDebug() << "CBaseThumbnailView::slotVerticalScrollBarSettledTimeout:" <<
                    "Vertical scrollBar settled at position" << d->LastScrollBarsPosition.Vertical;
        viewportChanged();
    }
}

SThumbnailItem* CBaseThumbnailView::thumbnailItemAt(const QModelIndex& index)
{
    if (!index.isValid())
        return 0;

    CBaseThumbnailModel* ipmodel = qobject_cast< CBaseThumbnailModel* >(model());
    if (ipmodel)
    {
        SThumbnailItem* result = ipmodel->thumbnailItem(index.row());
        return result;
    }

    qDebug() << "CThumbnailView::thumbnailItemAt:" <<
                "Unable to get STumbnailItem because index is not part of this thumbnail view.";
    return 0;
}

void CBaseThumbnailView::thumbnailSizeChanged()
{
    Q_D(CBaseThumbnailView);

    d->rebuildCorePixmaps();
    // TODO: implement this in subclass
}

QSize CBaseThumbnailView::thumbnailSize() const
{
    Q_D(const CBaseThumbnailView);

    return d->ThumbnailSize;
}

QMutex* CBaseThumbnailView::mutex()
{
    Q_D(CBaseThumbnailView);

    return &d->Mutex;
}

void CBaseThumbnailView::thumbnailsUpdate()
{
    scrollToIndex(currentIndex());
    viewportChanged();
}

void CBaseThumbnailView::thumbnailViewResized()
{
    Q_D(CBaseThumbnailView);

    // Here we make visible thumbnails since last viewport change shown
    // after resize. We need to do this because when resizing Qt will
    // re-layout items and to avoid confusion (because I hate to ask myself
    // "where was that item again?") there must be at least one
    // item from previous view that can be relied on to be visible in the new
    // view.
    if (d->PivotIndex.isValid())
    {
        bool oldignoreviewportchange = d->IgnoreViewportChange;
        d->IgnoreViewportChange = false;


        // Since scrollToThumbnail() will cause scrollbars position to
        // change, which in turn should cause install*ScrollBarSettledTimer
        // to start countdown and trigger *ScrollBarSettledTimer_timeout
        // that may call viewportChanged() which will overwrite a new
        // IndexToBeInView and defeat the whole purpose of this.
        //
        // Basically, just put the call to scrollToThumbnail() between two
        // FindIndexToBeInView toggle.
        d->StorePivotIndex = false;
        scrollToIndex(d->PivotIndex, true);
        d->StorePivotIndex = true;

        d->IgnoreViewportChange = oldignoreviewportchange;
    }

    if (d->ViewportChangedOnResize && isVisible())
        viewportChanged();
}

void CBaseThumbnailView::viewportChanged()
{
    Q_D(CBaseThumbnailView);

    if (d->IgnoreViewportChange)
        return;

    // get QModelIndexes for items can be seen on viewport
    QList< QModelIndex > vindexes = visibleIndexes();

    // process these indexes
    processIndexesFoundOnViewport(vindexes);
}

QModelIndexList CBaseThumbnailView::visibleIndexes()
{
    Q_D(CBaseThumbnailView);

    QModelIndexList result;
    if (!isVisible())
        return result;
    if (!model() || (model() && model()->rowCount() < 1))
        return result;

    int itemspacing = spacing();

    QModelIndex vindex = indexAt(QPoint(itemspacing + 1, 0));
    if (!vindex.isValid())
        vindex = indexAt(QPoint(itemspacing + 1, (itemspacing * 2) + 1));

    if (!vindex.isValid())
        return result;

    QRect currect = visualRect(vindex);
    while (d->isIndexVisible(vindex))
    {
        if (siftVisibleIndex(vindex))
            result << vindex;

        // get next in row
        vindex = indexAt(QPoint(currect.right() + (itemspacing * 2) + 1, currect.top()));
        if (!vindex.isValid())
        {
            // go to next row
            vindex = indexAt(QPoint(itemspacing, currect.bottom() + (itemspacing * 2) + 1));
            if (!vindex.isValid())
                break;
        }

        currect = visualRect(vindex);
    }

    return result;
}


CBaseThumbnailViewPrivate::CBaseThumbnailViewPrivate(CBaseThumbnailView* btv):
    q_ptr(btv),
    ClearLocalDataOnBeginUpdate(true),
    IgnoreViewportChange(false),
    InsideUpdateCount(0),
    ItemSpacing(2),
    Mutex(QMutex::Recursive),
    Orientation(Qt::Vertical),
    StorePivotIndex(true),
    ThumbnailLoader(0),
    ThumbnailSize(96, 96),
    ViewportChangedOnShow(true),
    ViewportChangedOnResize(false)
{
    deserialize();
}

CBaseThumbnailViewPrivate:: ~CBaseThumbnailViewPrivate()
{
    clearScheduledTasks();
    clearLocalData();
}

bool CBaseThumbnailViewPrivate::addSetCurrentThumbnailScheduledTask(const QString& atname, CBaseScheduledTask* task)
{
    CScheduledSetCurrentThumbnail* ttask = dynamic_cast< CScheduledSetCurrentThumbnail* >(task);
    if (ttask)
    {
        ScheduledTasks.insert(atname, ttask);
        return true;
    }

    return false;
}

bool CBaseThumbnailViewPrivate::applyDragKeyModifiers(QEvent* event) const
{
    QDropEvent* dropevent = dynamic_cast< QDropEvent* >(event);
    if (!dropevent)
        return false;

    Qt::KeyboardModifiers keymods = dropevent->keyboardModifiers();
    if (keymods.testFlag(Qt::ControlModifier))
    {
        dropevent->setDropAction(Qt::CopyAction);
        dropevent->accept();
        return true;
    }
    else if (keymods.testFlag(Qt::ShiftModifier))
    {
        dropevent->setDropAction(Qt::MoveAction);
        dropevent->accept();
        return true;
    }
    else
        dropevent->setDropAction(Qt::IgnoreAction);

    return false;
}

void CBaseThumbnailViewPrivate::clearLocalData()
{
    QHash< SThumbnailItem*, SThumbnailLocalData* >::iterator iter = ThumbnailLocalData.begin();
    while (iter != ThumbnailLocalData.end())
    {
        delete iter.value();

        iter = ThumbnailLocalData.erase(iter);
    }
}

void CBaseThumbnailViewPrivate::clearScheduledTasks()
{
    QStringList keys = ScheduledTasks.keys();
    keys.removeDuplicates();

    for (int i = 0; i < keys.count(); ++i)
    {
        QList< CBaseScheduledTask* >  tasks = ScheduledTasks.values(keys.at(i));
        while (tasks.count() > 0)
            delete tasks.takeFirst();
    }
    ScheduledTasks.clear();
}

QModelIndex CBaseThumbnailViewPrivate::currentThumbnailSiblingByOffset(const int offset) const
{
    Q_Q(const CBaseThumbnailView);

    QModelIndex currentindex = q->currentIndex();
    if (currentindex.isValid() && q->model())
        return q->model()->index(currentindex.row() + offset, 0);

    return QModelIndex();
}

QAction* CBaseThumbnailViewPrivate::getAction(const QString& name, bool mustexists)
{
    Q_Q(CBaseThumbnailView);

    QAction* result = q->findChild< QAction* >(name);
    if (mustexists)
        Q_ASSERT(result);

    return result;
}

void CBaseThumbnailViewPrivate::deserialize()
{
    BackgroundColor = QColor(Utils::Persistence::getConfig("ThumbnailView/BackgroundColor", QVariant("#888888")).toString());
    ItemSpacing = qBound< unsigned char >(0, Utils::Persistence::getConfig("ThumbnailView/ItemSpacing", QVariant(2)).toUInt(),
                                          UCHAR_MAX);
}

void CBaseThumbnailViewPrivate::initActions()
{
    Q_Q(CBaseThumbnailView);

    QAction* act;

    act = new QAction(q->tr("&Copy"), q);
    act->setObjectName("acCopy");
    act->setIcon(QIcon::fromTheme("edit-copy", QIcon(":lihat/glyph/edit-copy.png")));
    act->setShortcut(QKeySequence::Copy);
    QObject::connect(act, SIGNAL(hovered()),
                     q, SLOT(slotCopyHovered()));
    QObject::connect(act, SIGNAL(triggered(bool)),
                     q, SLOT(slotCopyTriggered()));
    q->addAction(act);

    act = new QAction(q->tr("C&ut"), q);
    act->setObjectName("acCut");
    act->setIcon(QIcon::fromTheme("edit-cut", QIcon(":lihat/glyph/edit-cut.png")));
    act->setShortcut(QKeySequence::Cut);
    QObject::connect(act, SIGNAL(hovered()), q, SLOT(slotCutHovered()));
    QObject::connect(act, SIGNAL(triggered(bool)),
                     q, SLOT(slotCutTriggered()));
    q->addAction(act);

    act = new QAction(q->tr("&Paste"), q);
    act->setObjectName("acPaste");
    act->setIcon(QIcon::fromTheme("edit-paste", QIcon(":lihat/glyph/edit-paste.png")));
    act->setShortcut(QKeySequence::Paste);
    QObject::connect(act, SIGNAL(hovered()), q, SLOT(slotPasteHovered()));
    QObject::connect(act, SIGNAL(triggered(bool)),
                     q, SLOT(slotPasteTriggered()));
    q->addAction(act);

    act = new QAction(q->tr("&Delete"), q);
    act->setObjectName("acDelete");
    act->setIcon(QIcon::fromTheme("edit-delete", QIcon(":lihat/glyph/edit-delete.png")));
    act->setShortcut(QKeySequence::Delete);
    QObject::connect(act, SIGNAL(hovered()), q, SLOT(slotDeleteHovered()));
    QObject::connect(act, SIGNAL(triggered(bool)),
                     q, SLOT(slotDeleteTriggered()));
    q->addAction(act);
}

void CBaseThumbnailViewPrivate::initUserInterface()
{
    Q_Q(CBaseThumbnailView);

    QPalette pal(q->palette());
    pal.setColor(QPalette::Base, BackgroundColor);
    q->setPalette(pal);

    q->setResizeMode(QListView::Adjust);
    q->setSelectionMode(QListView::ExtendedSelection);
    q->setUniformItemSizes(true);
    q->setViewMode(QListView::IconMode);
    q->setSpacing(ItemSpacing);
    q->setBatchSize(1 << 8);

    q->setAcceptDrops(true);
    q->setDragDropMode(QListView::InternalMove);
    q->setDragEnabled(true);
    q->setDropIndicatorShown(true);

    q->setHorizontalScrollMode(QListView::ScrollPerPixel);
    q->setVerticalScrollMode(QListView::ScrollPerPixel);

    if (Orientation == Qt::Horizontal)
    {
        q->setHorizontalScrollBarPolicy(Qt::ScrollBarAsNeeded);
        q->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    }
    else if (Orientation == Qt::Vertical)
    {
        q->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        q->setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    }

    QObject::connect(&ResizeSettledTimer, SIGNAL(timeout()),
                     q, SLOT(slotResizeSettledTimeout()));
    QObject::connect(q->horizontalScrollBar(), SIGNAL(valueChanged(int)),
                     q, SLOT(slotHorizontalScrollBarValueChanged(int)));
    QObject::connect(q->verticalScrollBar(), SIGNAL(valueChanged(int)),
                     q, SLOT(slotVerticalScrollBarValueChanged(int)));
    QObject::connect(&ScrollBarsSettledTimer.Horizontal, SIGNAL(timeout()),
                     q, SLOT(slotHorizontalScrollBarSettledTimeout()));
    QObject::connect(&ScrollBarsSettledTimer.Vertical, SIGNAL(timeout()),
                     q, SLOT(slotVerticalScrollBarSettledTimeout()));
    rebuildCorePixmaps();
}

void CBaseThumbnailViewPrivate::installDelayedViewportUpdater(unsigned int mseconds)
{
    Q_Q(CBaseThumbnailView);

    // this will trigger viewportChanged if user stay on current position in
    // viewport for some msecs.
    if (q->verticalScrollBar()->isVisibleTo(q))
        LateUpdaterLastScrollBarsPosition.Vertical = q->verticalScrollBar()->value();

    if (q->horizontalScrollBar()->isVisibleTo(q))
        LateUpdaterLastScrollBarsPosition.Horizontal = q->horizontalScrollBar()->value();

    QTimer::singleShot(mseconds, q, SLOT(slotDelayedViewportUpdaterTimeout()));
}

void CBaseThumbnailViewPrivate::installResizeSettledTimer(unsigned int mseconds)
{
    if (ResizeSettledTimer.isActive())
        ResizeSettledTimer.stop();

    ResizeSettledTimer.setSingleShot(true);
    ResizeSettledTimer.start(mseconds);
}

bool CBaseThumbnailViewPrivate::isIndexVisible(const QModelIndex& index) const
{
    Q_Q(const CBaseThumbnailView);

    QRect visiblerect = q->viewport()->rect();
    return visiblerect.intersects(q->visualRect(index));
}

bool CBaseThumbnailViewPrivate::isUpdating()
{
    return InsideUpdateCount > 0;
}

void CBaseThumbnailViewPrivate::rebuildCorePixmaps()
{
    Q_Q(CBaseThumbnailView);
    CorePixmaps.clear();

    QStyle* tvstyle = q->style();
    QPalette pal = q->palette();
    QFont fnt = QFont("sans", qMax< int >(7, (int) ThumbnailSize.height() / 9.6));
    fnt.setStyleHint(QFont::SansSerif);
    fnt.setBold(true);

    QTextOption textopt;
    textopt.setAlignment(Qt::AlignCenter);
    textopt.setWrapMode(QTextOption::WordWrap);

    QIcon diricon = QIcon::fromTheme("folder",
                                     QIcon(":/lihat/glyph/folder.png"));
    CorePixmaps.Folder = new QPixmap(diricon.pixmap(ThumbnailSize));


    QIcon fileicon = QIcon::fromTheme("image-x-generic",
                                      QIcon(":/lihat/glyph/image-x-generic.png"));
    CorePixmaps.File = new QPixmap(fileicon.pixmap(ThumbnailSize));

    // create 'invalid' thumbnail
    {
        CorePixmaps.NoThumbnail = new QPixmap(ThumbnailSize);
        CorePixmaps.NoThumbnail->fill(QColor(255, 255, 255, 0));

        QPainter painter(CorePixmaps.NoThumbnail);
        painter.save();
        painter.setOpacity(0.25);
        tvstyle->drawItemPixmap(&painter, CorePixmaps.NoThumbnail->rect(),
                                Qt::AlignCenter, *(CorePixmaps.File));
        painter.setOpacity(1.0);
        painter.setPen(pal.color(QPalette::Text));
        painter.setFont(fnt);
        painter.drawText(CorePixmaps.NoThumbnail->rect(), NO_THUMBNAIL_STR, textopt);
        painter.restore();
    }
}

void CBaseThumbnailViewPrivate::serialize()
{
    Utils::Persistence::setConfig("ThumbnailView/BackgroundColor", QVariant(BackgroundColor.name()));
    Utils::Persistence::setConfig("ThumbnailView/ItemSpacing", QVariant((unsigned int) ItemSpacing));
}

QList< QUrl > CBaseThumbnailViewPrivate::siftDroppedURLs(const QList< QUrl >& urls)
{
    QList< QUrl > result;

    for (int i = 0; i < urls.count(); ++i)
    {
        if (urls.at(i).isValid())
            result << urls.at(i);
    }

    return result;
}


CScheduledSetCurrentThumbnail::CScheduledSetCurrentThumbnail(const int index):
    Index(index)
{

}

int CScheduledSetCurrentThumbnail::runTask(CBaseThumbnailView* btv)
{
    if (Index != -1)
        btv->setCurrentThumbnail(Index);

    return 0;
}
