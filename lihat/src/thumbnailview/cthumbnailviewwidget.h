#ifndef CTHUMBNAILVIEWWIDGET_H
#define CTHUMBNAILVIEWWIDGET_H

#include "ithumbnailsloader.h"

#include <QFutureWatcher>
#include <QModelIndex>
#include <QTimer>
#include <QUrl>
#include <QWidget>


class QAction;
class QFocusEvent;
class QStatusBar;

namespace Lihat
{
    namespace Image
    {

        class CImageViewWidget;

    } // namespace Image

    namespace ImageLoad
    {

        class CBaseLoadJob;

    } // namespace ImageLoad

    namespace Thumbnails
    {

        class CBaseThumbnailView;
        class CBaseThumbnailModel;
        class CFoldersTreeView;
        class CThumbnailView;
        class CViewFiltersWidget;
        struct SThumbnailItem;
        struct SDroppedURLsContext;

        class CThumbnailViewWidget:
            public QWidget,
            public IThumbnailsLoader
        {
            Q_OBJECT
        public:
            explicit CThumbnailViewWidget(QWidget* parent = 0);
            virtual ~CThumbnailViewWidget();

            bool goToCurrentParent();
            bool goToPath(const QUrl& url, bool autoselectfirstitem = true);

            bool hasNextThumbnail();
            bool hasPreviousThumbnail();
            bool setCurrentThumbnail(const QUrl& url, bool allowchangepath = true, bool forceinstallendupdate = false);

            QList< QUrl > selectedURLs() const;

            QUrl currentThumbnailURL() const;
            QUrl currentThumbnailParentURL() const;

            CViewFiltersWidget* filtersWidget() const;
            void                setImageViewWidget(Image::CImageViewWidget* imageviewwidget);

            CThumbnailView* thumbnailView() const;

            void addThumbnailView(CBaseThumbnailView* view);
            void removeThumbnailView(CBaseThumbnailView* view);

            QUrl modelURL() const;

            void startThumbnailLoaders();

        public slots:
            bool nextThumbnail();
            bool previousThumbnail();
            bool reload();

        signals:
            void currentChanged(const QUrl& url);
            void pathChanged(const QUrl& path);
            void viewRequested(const QUrl& url);

        protected:
            bool                                       m_canmodifylocationshistory;
            QTimer                                     m_delayedfolderstocurrentindextimer;
            CViewFiltersWidget*                        m_filterswidget;
            CFoldersTreeView*                          m_folderstreeView;
            bool                                       m_ignorefolderscurrentindexchanged;
            bool                                       m_ignorelocationcurrentindexchanged;
            Image::CImageViewWidget*                   m_linkedimageviewwidget;
            QList< ImageLoad::CBaseLoadJob* >          m_loadjobs;
            int                                        m_locationpos;
            QList< QUrl >                              m_locationshistory;
            char                                       m_locationshistorymax;
            QList< CBaseThumbnailView* >               m_managedviews;
            CBaseThumbnailModel*                       m_model;
            bool                                       m_processloadjobresult;
            QFutureWatcher< ImageLoad::CBaseLoadJob* > m_thumbloadfuturewatcher;
            CThumbnailView*                            m_thumbnailview;
            bool                                       m_waitingthumbnailloadthreadtostart;

            // IThumbnailsLoader
            virtual bool addImageLoadQueueJob(CBaseThumbnailView* view, ImageLoad::CBaseLoadJob* job);
            virtual void clearImageLoadQueueJobs(CBaseThumbnailView* view);
            virtual bool createImageLoadQueueJob(CBaseThumbnailView* view, const QModelIndex& index);
            virtual void handleImageLoadQueueLoadResult(ImageLoad::CBaseLoadJob* job);
            virtual void startImageLoadQueue(CBaseThumbnailView* requester);

            // QWidget
            virtual void focusInEvent(QFocusEvent* event);
            virtual void hideEvent(QHideEvent* event);
            virtual void showEvent(QShowEvent* event);

            void itemsProviderPathUpdated();

            void cleanupThumbnailViews();
            QList< QUrl > thumbnailViewIndexesToURLs(CBaseThumbnailView* tv, const QModelIndexList& indexes) const;


            void readPersistence();
            void writePersistence();

            int  addToLocationsHistory(const QUrl& url, bool deserialize = false);
            void populateLocationsHistory();

            void attachToMainWindowStatusBar(QStatusBar* statusbar);
            void detachFromMainWindowStatusBar(QStatusBar* statusbar);

            bool setStatusText(const QString& stext, int index = 0);
            bool updateStatusTexts();

            void initActions();

            QAction* getAction(const QString& name, bool mustexists = true);

            void setThumbnailsSortedBy(int sortby);
            void setThumbnailsSortAscending(bool asc);

            bool updateFoldersTreeviewSelection(const QString& path);
            void updateBackForwardButton();
            void updateFiltersButtonText();
            void updateGridSize(short linenum = -1);

            void dataModelRequired();
            void installDelayedFoldersTreeViewScrollToCurrentIndex(int delaymsecs = 500);

        private slots:
            void slotBackHovered();
            void slotBackTriggered();
            void slotDelayedFoldersTreeViewScrollToCurrentIndexTimeout();
            void slotFiltersHovered();
            void slotFiltersToggled(bool checked);
            void slotFiltersWidgetEscapePressed();
            void slotFiltersWidgetFilterChanged();
            void slotFoldersHovered();
            void slotFoldersToggled(bool visible);
            void slotFoldersTreeviewCurrentIndexChanged(const QModelIndex& current, const QModelIndex& previous);
            void slotForwardHovered();
            void slotForwardTriggered();
            void slotGoTriggered();
            void slotGoUpHovered();
            void slotGoUpRequested();
            void slotLocationCurrentIndexChanged(int index);
            void slotLocationReturnPressed();
            void slotSortByDateTriggered();
            void slotSortByNameTriggered();
            void slotSortBySizeTriggered();
            void slotShowDimensionTriggered(bool checked);
            void slotShowDimensionHovered();
            void slotShowSizeHovered();
            void slotShowSizeTriggered(bool checked);
            void slotSortOrderAscendingTriggered(bool checked);
            void slotThumbnailViewCurrentItemChanged(const QModelIndex& current, const QModelIndex&);
            void slotThumbnailViewItemExecuted(const QModelIndex& index);
            void slotThumbnailViewItemsSelected(const QModelIndexList& indexes);
            void slotThumbnailViewUpdateFinished();

            void slotLoadWatcherResultReadyAt(int index);
            void slotLoadWatcherStarted();
            void slotThumbnailViewClearLoadJobs();
            void slotThumbnailViewItemThumbnailWanted(const QModelIndex& index);
            void slotThumbnailViewItemsGetPasted(const QModelIndexList& indexes);
            void slotThumbnailViewItemsWantCopy(const QModelIndexList& indexes);
            void slotThumbnailViewItemsWantCut(const QModelIndexList& indexes);
            void slotThumbnailViewItemsWantDelete(const QModelIndexList& indexes);
            void slotThumbnailViewURLsDropped(const SDroppedURLsContext* context);

        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CTHUMBNAILVIEWWIDGET_H
