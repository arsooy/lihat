#ifndef CBASETHUMBNAILVIEW_H
#define CBASETHUMBNAILVIEW_H

#include "ipixmapprovider.h"

#include <QList>
#include <QListView>
#include <QModelIndex>
#include <QModelIndexList>
#include <QSize>
#include <QString>
#include <QUrl>
#include <QWidget>


class QAction;
class QDragEnterEvent;
class QDragMoveEvent;
class QDropEvent;
class QKeyEvent;
class QResizeEvent;
class QShowEvent;
class QMutex;

namespace Lihat
{
    namespace Thumbnails
    {

        class  CBaseThumbnailView;
        class  CBaseThumbnailViewPrivate;
        class  CThumbnailViewPrivate;
        class  IThumbnailsLoader;
        struct SThumbnailItem;
        struct SThumbnailLocalData;


        class CBaseThumbnailView:
            public QListView,
            protected Thumbnails::IPixmapProvider
        {
            Q_OBJECT
            Q_PROPERTY(QSize thumbnailSize READ thumbnailSize WRITE setThumbnailSize)
        public:
            enum EItemOperation
            {
                IO_DELETE = 1,
                IO_CUT    = 2,
                IO_COPY   = 3,
                IO_LINK   = 4,
                IO_PASTE  = 5
            };


            enum EThumbnailLoadStatus
            {
                TLS_INVALID         =  0,
                TLS_NOTAVAILABLE    =  1,
                TLS_PENDING         =  2,
                TLS_RELOADNEEDED    =  3,
                TLS_MIMETYPEPIXMAP  = 98,
                TLS_LOADED          = 99,
            };


            explicit CBaseThumbnailView(QWidget* parent = 0);
            virtual ~CBaseThumbnailView();

            bool adjustCurrentThumbnailByOffset(const int offset);
            bool setCurrentThumbnail(const int index, bool installAtEndUpdate = false);

            void scrollToIndex(const QModelIndex& index, bool center = false);
            QModelIndexList visibleIndexes();

            virtual QModelIndex     indexOfThumbnailItem(SThumbnailItem* thumbnailitem);
            virtual SThumbnailItem* thumbnailItemAt(const QModelIndex& index);

            void clearThumbnailLocalData();
            SThumbnailLocalData* thumbnailLocalData(SThumbnailItem* key, bool autocreate = true);

            void setThumbnailLoader(IThumbnailsLoader* tloader);
            void  setThumbnailSize(const QSize& tsize);
            QSize thumbnailSize() const;

            QMutex* mutex();

        signals:
            void itemsGetPasted(const QModelIndexList& indexes);
            void itemsSelected(const QModelIndexList& indexes);
            void itemsWantCopy(const QModelIndexList& indexes);
            void itemsWantCut(const QModelIndexList& indexes);
            void itemsWantDelete(const QModelIndexList& indexes);

            void thumbnailClearLoadJobs();
            void thumbnailClearLoadResults();

            void updateFinished();
            void updateStarted();

        public slots:
            virtual void beginUpdate();
            virtual void endUpdate();
            virtual void thumbnailsUpdate();
            virtual void viewportChanged();

        protected:
            Q_DECLARE_PRIVATE(CBaseThumbnailView)
            CBaseThumbnailViewPrivate* const  d_ptr;

            explicit CBaseThumbnailView(CBaseThumbnailViewPrivate& private_d, QWidget* parent = 0);

            // QListView
            virtual void currentChanged(const QModelIndex& current, const QModelIndex& previous);
            virtual void dropEvent(QDropEvent* event);
            virtual void resizeEvent(QResizeEvent* event);
            virtual void showEvent(QShowEvent* event);

            // IThumbnailViewPixmapProvider
            virtual QPixmap*  fetchThumbnailViewPixmap(int pixmapcode);

            virtual void initItemDelegate() = 0;
            virtual void processDroppedURLs(const QList< QUrl >& urls, Qt::DropAction dropaction, const QPoint& pos = QPoint());
            virtual void processIndexesFoundOnViewport(const QModelIndexList& indexes);
            virtual void selectionChanged(const QItemSelection& selected, const QItemSelection& deselected);
            virtual bool siftIndexForItemOperation(const QModelIndex& index, EItemOperation itemoper);
            virtual bool siftLoadItemThumbnail(const QModelIndex& index);
            virtual bool siftVisibleIndex(const QModelIndex& index);
            virtual void thumbnailSizeChanged();
            virtual void thumbnailViewResized();

        private slots:
            void slotCopyHovered();
            void slotCopyTriggered();
            void slotCutHovered();
            void slotCutTriggered();
            void slotDelayedViewportUpdaterTimeout();
            void slotDeleteHovered();
            void slotDeleteTriggered();
            void slotHorizontalScrollBarSettledTimeout();
            void slotHorizontalScrollBarValueChanged(int positionatcall = -1);
            void slotPasteHovered();
            void slotPasteTriggered();
            void slotResizeSettledTimeout();
            void slotVerticalScrollBarSettledTimeout();
            void slotVerticalScrollBarValueChanged(int positionatcall = -1);

        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CBASETHUMBNAILVIEW_H
