#include "cthumbnailview.h"

#include "../persistenceutils.h"
#include "cbasescheduledtask.h"
#include "citemdelegate.h"
#include "cthumbnailview_p.h"
#include "models/cbasethumbnailmodel.h"
#include "models/cproxymodel.h"
#include "sthumbnailitem.h"

#include <QDrag>
#include <QDragEnterEvent>
#include <QDragMoveEvent>
#include <QDropEvent>
#include <QKeyEvent>
#include <QMenu>
#include <QMimeData>
#include <QPainter>
#include <QPoint>
#include <QResizeEvent>
#include <QShowEvent>
#include <QUrl>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::Thumbnails;

class CScheduledSetCurrentThumbnailV2:
    public CScheduledSetCurrentThumbnail
{
public:
    QUrl const URL;

    explicit CScheduledSetCurrentThumbnailV2(const QUrl& url);

    virtual int runTask(CBaseThumbnailView* btv);
};

CThumbnailView::CThumbnailView(QWidget* parent):
    CBaseThumbnailView(*new CThumbnailViewPrivate(this), parent)
{
    Q_D(CThumbnailView);

    d->initActions();
    d->initUserInterface();

    initItemDelegate();
}

CThumbnailView::CThumbnailView(CBaseThumbnailViewPrivate& private_d, QWidget* parent):
    CBaseThumbnailView(private_d, parent)
{
    Q_D(CThumbnailView);

    d->initActions();
    d->initUserInterface();

    initItemDelegate();
}

CThumbnailView::~CThumbnailView()
{

}

void CThumbnailView::beginUpdate()
{
    Q_D(CThumbnailView);

    if (d->InsideUpdateCount < 1)
    {
        setCursor(Qt::BusyCursor);
        // remove pending load jobs, after endUpdate we might have different
        // thumbnails to load anyway
        emit thumbnailClearLoadJobs();
    }

    CBaseThumbnailView::beginUpdate();
}

void CThumbnailView::dragEnterEvent(QDragEnterEvent* event)
{
    Q_D(CThumbnailView);

    if (!event->mimeData()->hasUrls())
        return;

    if (!d->applyDragKeyModifiers(event))
        event->acceptProposedAction();
}

void CThumbnailView::dragMoveEvent(QDragMoveEvent* event)
{
    Q_D(CThumbnailView);

    if (!event->mimeData()->hasUrls())
        return;

    if (!d->applyDragKeyModifiers(event))
        event->acceptProposedAction();
}

void CThumbnailView::endUpdate()
{
    Q_D(CThumbnailView);

    if (d->InsideUpdateCount == 1)
    {
        // this is the last closing endUpdate call, re-apply sort and
        // filtering on view
        CProxyModel* proxymodel = qobject_cast< CProxyModel* >(model());
        if (proxymodel)
            proxymodel->invalidate();
    }

    CBaseThumbnailView::endUpdate();

    if (d->InsideUpdateCount < 1)
        setCursor(QCursor(Qt::ArrowCursor));
}

bool CThumbnailView::executeIndex(const QModelIndex& index)
{
    if (index.isValid() && (index.model() == model()))
    {
        emit itemExecuted(index);
        return true;
    }

    return false;
}

void CThumbnailView::initItemDelegate()
{
    CItemDelegate* idel = new CItemDelegate(this);
    idel->setThumbnailViewPixmapProvider(this);

    setItemDelegate(idel);
}

void CThumbnailView::mouseDoubleClickEvent(QMouseEvent* event)
{
    QAbstractItemView::mouseDoubleClickEvent(event);

    QModelIndex index = indexAt(event->pos());
    if (index.isValid())
        emit itemExecuted(index);
}

void CThumbnailView::keyReleaseEvent(QKeyEvent* event)
{
    switch (event->key())
    {
        case Qt::Key_Return:
        {
            QModelIndexList selidxs = selectedIndexes();
            if (selidxs.count() > 0)
            {
                QModelIndex index = currentIndex();
                if (index.isValid())
                    emit itemExecuted(index);
            }

            break;
        }

        default:
            ;
    }

    QListView::keyReleaseEvent(event);
}

void CThumbnailView::processDroppedURLs(const QList< QUrl >& urls, Qt::DropAction dropaction, const QPoint& pos)
{
    Q_D(CThumbnailView);

    CBaseThumbnailView::processDroppedURLs(urls, dropaction, pos);

    d->dropDroppedURLsContext();

    if (dropaction == Qt::IgnoreAction)
    {
        d->DroppedURLsContext = new SDroppedURLsContext(dropaction, urls);
        d->DropPopupMenu->popup(mapToGlobal(pos), d->getAction("acDropCancel"));
        return;
    }

    SDroppedURLsContext dropctx(dropaction, urls);
    emit urlsDropped(&dropctx);
}

bool CThumbnailView::setCurrentThumbnail(const int index, bool installAtEndUpdate)
{
    return CBaseThumbnailView::setCurrentThumbnail(index, installAtEndUpdate);
}

bool CThumbnailView::setCurrentThumbnail(const QUrl& url, bool installAtEndUpdate)
{
    Q_D(CThumbnailView);

    if (installAtEndUpdate)
    {
        CScheduledSetCurrentThumbnailV2* task = new CScheduledSetCurrentThumbnailV2(url);
        return d->addSetCurrentThumbnailScheduledTask("EndThumbnailsUpdate", task);
    }
    else
    {
        SThumbnailItem* titem = 0;
        QModelIndex visualidx = currentIndex();
        if (visualidx.isValid())
        {
            titem = thumbnailItemAt(visualidx);
            if (titem && (titem->loadURL() == url))
                return true;
        }

        for (int i = 0; i < model()->rowCount(); ++i)
        {
            QModelIndex index = model()->index(i, 0);
            if (!index.isValid())
                continue;

            titem = thumbnailItemAt(index);
            if (titem && (titem->loadURL() == url))
            {
                scrollToIndex(index);
                setCurrentIndex(index);

                qDebug() << "CThumbnailView::setCurrentThumbnail:" <<
                            "Current thumbnail set to" << visualidx;

                d->ViewportChangedOnShow = !isVisible();
                return true;
            }
        }
    }

    return false;
}

void CThumbnailView::setThumbnailViewModel(QAbstractItemModel* model)
{
    Q_D(CThumbnailView);

    if (d->ProxyModel)
    {
        emit modelDetached(d->ProxyModel);
        delete d->ProxyModel;
    }

    CBaseThumbnailModel* ipmodel = qobject_cast< CBaseThumbnailModel* >(model);
    if (ipmodel)
    {
        d->ProxyModel = new CProxyModel(this);
        d->ProxyModel->setSourceModel(ipmodel);
        d->ProxyModel->sort(0);

        QListView::setModel(d->ProxyModel);

        connect(ipmodel, SIGNAL(itemsProviderCollectStarted()),
                this, SLOT(beginUpdate()));
        connect(ipmodel, SIGNAL(itemsProviderCollectFinished()),
                this, SLOT(endUpdate()));
        connect(ipmodel, SIGNAL(itemsProviderNewItemsDetected(QList< SThumbnailItem* >)),
                this, SLOT(slotItemsProviderNewItemsDetected(QList< SThumbnailItem* >)));
        connect(d->ProxyModel, SIGNAL(sortChanged()),
                this, SLOT(viewportChanged()));
    }
    else
        QListView::setModel(model);
}

bool CThumbnailView::isAutoClearLocalData() const
{
    Q_D(const CThumbnailView);

    return d->ClearLocalDataOnBeginUpdate;
}

void CThumbnailView::setAutoClearLocalData(bool autoclear)
{
    Q_D(CThumbnailView);

    d->ClearLocalDataOnBeginUpdate = autoclear;
}

bool CThumbnailView::siftIndexForItemOperation(const QModelIndex& index, CBaseThumbnailView::EItemOperation itemoper)
{
    if (CBaseThumbnailView::siftIndexForItemOperation(index, itemoper))
    {
        switch (itemoper)
        {
            case IO_DELETE:
            case IO_COPY:
            case IO_CUT:
            case IO_LINK:
            {
                SThumbnailItem* titem = thumbnailItemAt(index);
                return (bool) titem;

                break;
            }

            case IO_PASTE:
            {
                // paste can only be done on no index (which means paste on
                // whatever directory user is looking at) and folder items
                if (!index.isValid())
                    return true;
                else
                {
                    SThumbnailItem* titem = thumbnailItemAt(index);
                    return (titem->Type == SThumbnailItem::TI_FOLDER);
                }

                break;
            }

            default:
                ;
        }
    }

    return false;
}

bool CThumbnailView::siftVisibleIndex(const QModelIndex& index)
{
    if (CBaseThumbnailView::siftVisibleIndex(index))
    {
        SThumbnailItem* titem = thumbnailItemAt(index);
        return (bool) titem;
    }

    return false;
}

void CThumbnailView::slotDropCancelTriggered()
{
    Q_D(CThumbnailView);
    d->dropDroppedURLsContext();
}

void CThumbnailView::slotDropCopyTriggered()
{
    Q_D(CThumbnailView);

    Q_ASSERT(d->DroppedURLsContext);
    d->DroppedURLsContext->Action = Qt::CopyAction;

    emit urlsDropped(d->DroppedURLsContext);
}

void CThumbnailView::slotDropMoveTriggered()
{
    Q_D(CThumbnailView);

    Q_ASSERT(d->DroppedURLsContext);
    d->DroppedURLsContext->Action = Qt::MoveAction;

    emit urlsDropped(d->DroppedURLsContext);
}

void CThumbnailView::slotItemsProviderNewItemsDetected(QList< SThumbnailItem* > items)
{
    CProxyModel* proxmodel = qobject_cast< CProxyModel* >(model());
    if (proxmodel && !items.isEmpty())
        proxmodel->invalidate();

    // see if any of the items can be seen at the moment
    QModelIndexList vindexes = visibleIndexes();
    for (int i = 0; i < items.count(); ++i)
    {
        QModelIndex index = indexOfThumbnailItem(items.at(i));
        if (!index.isValid())
            continue;

        if (vindexes.contains(index))
        {
            // this item can be seen, request for thumbnail generation
            viewportChanged();
            break;
        }
    }
}

void CThumbnailView::startDrag(Qt::DropActions /* supportedActions */)
{
    QModelIndexList selindexes = selectedIndexes();
    if (selindexes.empty())
        return;

    QList< QUrl > urls;
    for (int i = 0; i <  selindexes.count(); ++i)
    {
        SThumbnailItem* titem = thumbnailItemAt(selindexes.at(i));
        if (titem && titem->isValid())
            urls << titem->loadURL();
    }

    if (!urls.isEmpty())
    {
        QDrag* drag = new QDrag(this);
        QMimeData* mimedata = new QMimeData();
        mimedata->setUrls(urls);

        drag->setMimeData(mimedata);
        drag->exec(Qt::MoveAction | Qt::CopyAction, Qt::CopyAction);
    }
}

SThumbnailItem* CThumbnailView::thumbnailItemAt(const QModelIndex& index)
{
    CProxyModel* proxymodel = qobject_cast< CProxyModel* >(model());
    Q_ASSERT(proxymodel);

    CBaseThumbnailModel* thumbnailitemsprovider = dynamic_cast< CBaseThumbnailModel* >(proxymodel->sourceModel());
    Q_ASSERT(thumbnailitemsprovider);

    QModelIndex realindex = proxymodel->mapToSource(index);
    if (realindex.isValid())
        return thumbnailitemsprovider->thumbnailItem(realindex.row());

    return CBaseThumbnailView::thumbnailItemAt(index);
}

void CThumbnailView::thumbnailSizeChanged()
{
    emit thumbnailClearLoadJobs();
    emit thumbnailClearLoadResults();

    CBaseThumbnailView::thumbnailSizeChanged();
}


CThumbnailViewPrivate::CThumbnailViewPrivate(CThumbnailView* thumbnailview):
    CBaseThumbnailViewPrivate(thumbnailview),
    DropPopupMenu(0),
    DroppedURLsContext(0),
    ProxyModel(0)
{

}

CThumbnailViewPrivate::~CThumbnailViewPrivate()
{
    dropDroppedURLsContext();
}

bool CThumbnailViewPrivate::addSetCurrentThumbnailScheduledTask(const QString& atname, CBaseScheduledTask* task)
{
    CScheduledSetCurrentThumbnail* ttask = dynamic_cast< CScheduledSetCurrentThumbnail* >(task);
    if (ttask)
    {
        ScheduledTasks.insert(atname, ttask);
        return true;
    }

    return false;
}

void CThumbnailViewPrivate::dropDroppedURLsContext()
{
    if (DroppedURLsContext)
        delete DroppedURLsContext;
    DroppedURLsContext = 0;
}

void CThumbnailViewPrivate::initActions()
{
    Q_Q(CThumbnailView);

    QAction* act;
    act = new QAction(q->tr("Copy\tCtrl"), q);
    act->setObjectName("acDropCopy");
    act->setIcon(QIcon::fromTheme("edit-copy", QIcon(":lihat/glyph/edit-copy.png")));
    QObject::connect(act, SIGNAL(triggered(bool)),
                     q, SLOT(slotDropCopyTriggered()));

    act = new QAction(q->tr("Move\tShift"), q);
    act->setObjectName("acDropMove");
    act->setIcon(QIcon::fromTheme("go-jump", QIcon(":lihat/glyph/go-jump.png")));
    QObject::connect(act, SIGNAL(triggered(bool)),
                     q, SLOT(slotDropMoveTriggered()));

    act = new QAction(q->tr("Cancel"), q);
    act->setObjectName("acDropCancel");
    act->setIcon(QIcon::fromTheme("process-stop", QIcon(":lihat/glyph/process-stop.png")));
    act->setShortcut(QKeySequence(Qt::Key_Escape));
    QObject::connect(act, SIGNAL(triggered(bool)),
                     q, SLOT(slotDropCancelTriggered()));
}

void CThumbnailViewPrivate::initUserInterface()
{
    Q_Q(CThumbnailView);

    DropPopupMenu = new QMenu(q);
    DropPopupMenu->addAction(getAction("acDropCopy"));
    DropPopupMenu->addAction(getAction("acDropMove"));
    DropPopupMenu->addSeparator();
    DropPopupMenu->addAction(getAction("acDropCancel"));
}


SDroppedURLsContext::SDroppedURLsContext(Qt::DropAction action, const QList< QUrl >& urls):
    Action(action)
{
    for (int i = 0; i < urls.count(); ++i)
        URLs << QUrl(urls.at(i));
}

SDroppedURLsContext::SDroppedURLsContext(const QDropEvent* dropevent)
{
    if (!dropevent)
        return;

    Action = dropevent->dropAction();
    if (dropevent->mimeData()->hasUrls())
    {
        for (int i = 0; i < dropevent->mimeData()->urls().count(); ++i)
            URLs << QUrl(dropevent->mimeData()->urls().at(i));
    }
}

SDroppedURLsContext::~SDroppedURLsContext()
{
    URLs.clear();
}


CScheduledSetCurrentThumbnailV2::CScheduledSetCurrentThumbnailV2(const QUrl& url):
    CScheduledSetCurrentThumbnail(-1),
    URL(url)
{

}

int CScheduledSetCurrentThumbnailV2::runTask(CBaseThumbnailView* btv)
{
    CThumbnailView* thumbnailview = qobject_cast< CThumbnailView* >(btv);
    if (thumbnailview)
    {
        if (!URL.isEmpty())
            thumbnailview->setCurrentThumbnail(URL);
    }

    return 0;
}
