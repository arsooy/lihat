#ifndef CBASETHUMBNAILVIEWPRIVATE_H
#define CBASETHUMBNAILVIEWPRIVATE_H

#include "scorepixmaps.h"

#include <QCache>
#include <QColor>
#include <QHash>
#include <QList>
#include <QModelIndex>
#include <QMultiHash>
#include <QMutex>
#include <QSize>
#include <QTimer>
#include <QUrl>

class QAction;
class QPixmap;


namespace Lihat
{
    namespace Thumbnails
    {

        class  CBaseThumbnailView;
        class  CBaseScheduledTask;
        class  IThumbnailsLoader;
        struct SThumbnailItem;
        struct SThumbnailLocalData;

        class CBaseThumbnailViewPrivate
        {
        public:
            struct SScrollBarsPosition
            {
                int  Horizontal;
                int  Vertical;

                SScrollBarsPosition():
                    Horizontal(0),
                    Vertical(0)
                {

                }
            };


            struct SOrientationTimers
            {
                QTimer  Horizontal;
                QTimer  Vertical;
            };


            typedef QCache< SThumbnailItem*, QModelIndex >                   TThumbnailIndexCache;
            typedef QMultiHash< QString, CBaseScheduledTask* >  TScheduledTasksMap;
            typedef QHash< SThumbnailItem*, SThumbnailLocalData* >           TThumbnailLocalData;


            static const unsigned short  SCROLLBAR_SETTLED_TIMEOUT = 350;


            Q_DECLARE_PUBLIC(CBaseThumbnailView)
            CBaseThumbnailView*             q_ptr;

            QColor                          BackgroundColor;
            bool                            ClearLocalDataOnBeginUpdate;
            SCorePixmaps                    CorePixmaps;
            bool                            IgnoreViewportChange;
            int                             InsideUpdateCount;
            unsigned char                   ItemSpacing;
            SScrollBarsPosition             LastScrollBarsPosition;
            SScrollBarsPosition             LateUpdaterLastScrollBarsPosition;
            QMutex                          Mutex;
            Qt::Orientation                 Orientation;
            QModelIndex                     PivotIndex;
            bool                            StorePivotIndex;
            IThumbnailsLoader*  ThumbnailLoader;
            TThumbnailLocalData             ThumbnailLocalData;
            TThumbnailIndexCache            ThumbnailItemToIndexCache;
            QSize                           ThumbnailSize;
            bool                            ViewportChangedOnShow;
            bool                            ViewportChangedOnResize;
            SOrientationTimers              ScrollBarsSettledTimer;
            QTimer                          ResizeSettledTimer;
            TScheduledTasksMap              ScheduledTasks;

            explicit CBaseThumbnailViewPrivate(CBaseThumbnailView* btv);
            virtual ~CBaseThumbnailViewPrivate();

            bool           addSetCurrentThumbnailScheduledTask(const QString& atname, CBaseScheduledTask* task);
            void           clearScheduledTasks();

            bool           applyDragKeyModifiers(QEvent* event) const;
            void           clearLocalData();
            QModelIndex    currentThumbnailSiblingByOffset(const int offset) const;
            void           deserialize();
            QAction*       getAction(const QString& name, bool mustexists = true);
            void           initActions();
            void           initUserInterface();
            void           installDelayedViewportUpdater(unsigned int mseconds = 750);
            void           installResizeSettledTimer(unsigned int mseconds = 100);
            bool           isIndexVisible(const QModelIndex& index) const;
            bool           isUpdating();
            void           rebuildCorePixmaps();
            void           serialize();
            QList< QUrl >  siftDroppedURLs(const QList< QUrl >& urls);
        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CBASETHUMBNAILVIEWPRIVATE_H
