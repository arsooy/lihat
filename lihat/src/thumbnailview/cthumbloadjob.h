#ifndef CTHUMBLOADJOB_H
#define CTHUMBLOADJOB_H

#include "../imageload/curlloadjob.h"
#include "../imageload/iresizejob.h"

#include <QSize>
#include <QString>


class QImageReader;

namespace Lihat
{
    namespace Thumbnails
    {

        class  CBaseThumbnailView;
        struct SThumbnailItem;

        class CThumbLoadJob:
            public ImageLoad::CURLLoadJob,
            public ImageLoad::IResizeJob
        {
        public:
            CBaseThumbnailView* const Requester;
            SThumbnailItem* const     ThumbnailItem;

            explicit CThumbLoadJob(CBaseThumbnailView* requester, SThumbnailItem* titem, const QSize& loadsize);
            explicit CThumbLoadJob(const CThumbLoadJob& other);
            virtual ~CThumbLoadJob();

            bool operator==(const CThumbLoadJob& other);

            // CURLLoadJob
            virtual ImageLoad::CURLLoadJob::EImageLoadType imageLoadType() const;
            virtual bool                                   isValid() const;
            virtual bool                                   load();

            // IResizeJob
            virtual void                   setRequestedSize(const QSizeF& rsize);
            virtual QSizeF                 requestedSize() const;
            virtual void                   setResizeTransformation(Qt::TransformationMode tm);
            virtual Qt::TransformationMode resizeTransformation() const;

        protected:
            QSizeF m_requestsize;
            QSize  m_foundsize;
            Qt::TransformationMode m_resizetrans;

            virtual bool loadImageReader(QImageReader* ir);
            virtual bool applyResizeToImageReader(QImageReader* ir);
            virtual bool applySubImageToImageReader(QImageReader* ir);

        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CTHUMBLOADJOB_H
