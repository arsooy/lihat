#ifndef CBASESCHEDULEDTASK_H
#define CBASESCHEDULEDTASK_H

namespace Lihat
{
    namespace Thumbnails
    {

        class CBaseThumbnailView;

        class CBaseScheduledTask
        {
        public:
            virtual ~CBaseScheduledTask()
            {

            }

            virtual int taskIdent() const = 0;
            virtual int runTask(CBaseThumbnailView* btv) = 0;
        };


        class CScheduledSetCurrentThumbnail:
            public CBaseScheduledTask
        {
        public:
            static const int IdSetCurrentThumbnail = 11;

            int const Index;

            explicit CScheduledSetCurrentThumbnail(const int index);

            virtual int taskIdent() const { return IdSetCurrentThumbnail; }
            virtual int runTask(CBaseThumbnailView* btv);
        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CBASESCHEDULEDTASK_H
