#include "sthumbnailitem.h"

#include "iitemsprovider.h"

#include <QPixmap>


using namespace Lihat;
using namespace Lihat::Thumbnails;

SThumbnailItem::SThumbnailItem(IItemsProvider* provider):
    Identifier(-1),
    Type(TI_INVALID),
    ItemsProviderData(0),
    Provider(provider)
{

}

SThumbnailItem::~SThumbnailItem()
{

}

bool SThumbnailItem::isValid() const
{
    return (Identifier != -1) && (Type != TI_INVALID);
}

QUrl SThumbnailItem::loadURL() const
{
    Q_ASSERT(Provider);

    return Provider->thumbnailItemURL(const_cast< SThumbnailItem* >(this));
}


SThumbnailLocalData::SThumbnailLocalData():
    LoadStatus(SThumbnailItem::ILS_PENDING),
    Pixmap(0)
{

}

SThumbnailLocalData::~SThumbnailLocalData()
{
    deletePixmap();
}

void SThumbnailLocalData::deletePixmap()
{
    if (Pixmap)
        delete Pixmap;
    Pixmap = 0;
}
