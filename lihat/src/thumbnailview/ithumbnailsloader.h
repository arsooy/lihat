#ifndef ITHUMBNAILSLOADER_H
#define ITHUMBNAILSLOADER_H

#include <QModelIndex>


namespace Lihat
{
    namespace ImageLoad
    {

        class  CBaseLoadJob;
        class  CBaseLoadResult;

    } // namespace ImageLoad

    namespace Thumbnails
    {
        class  CBaseThumbnailView;
        struct SThumbnailItem;

        class IThumbnailsLoader
        {
        public:
            virtual ~IThumbnailsLoader()
            {

            }

            virtual bool  addImageLoadQueueJob(CBaseThumbnailView* requester, ImageLoad::CBaseLoadJob* job) = 0;
            virtual void  clearImageLoadQueueJobs(CBaseThumbnailView* requester) = 0;
            virtual bool  createImageLoadQueueJob(CBaseThumbnailView* requester, const QModelIndex& index) = 0;
            virtual void  handleImageLoadQueueLoadResult(ImageLoad::CBaseLoadJob* job) = 0;
            virtual void  startImageLoadQueue(CBaseThumbnailView* requester) = 0;
        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // ITHUMBNAILSLOADER_H
