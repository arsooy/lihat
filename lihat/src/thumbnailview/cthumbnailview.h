#ifndef CTHUMBNAILVIEW_H
#define CTHUMBNAILVIEW_H

#include "cbasethumbnailview.h"

#include <QList>
#include <QListView>
#include <QModelIndex>
#include <QModelIndexList>
#include <QResizeEvent>
#include <QSize>
#include <QString>
#include <QUrl>
#include <QWidget>


class QAction;
class QDragEnterEvent;
class QDragMoveEvent;
class QDropEvent;
class QKeyEvent;
class QResizeEvent;
class QShowEvent;

namespace Lihat
{
    namespace Thumbnails
    {

        struct SDroppedURLsContext
        {
            Qt::DropAction  Action;
            QList< QUrl >   URLs;

            explicit SDroppedURLsContext(Qt::DropAction action, const QList< QUrl >& urls);
            explicit SDroppedURLsContext(const QDropEvent* dropevent);
            virtual ~SDroppedURLsContext();
        };


        class CThumbnailView:
            public CBaseThumbnailView
        {
            Q_OBJECT
        public:
            explicit CThumbnailView(QWidget* parent = 0);
            virtual ~CThumbnailView();

            virtual bool            executeIndex(const QModelIndex& index);
            virtual SThumbnailItem* thumbnailItemAt(const QModelIndex& index);

            bool setCurrentThumbnail(const int index, bool installAtEndUpdate = false);
            bool setCurrentThumbnail(const QUrl& url, bool installAtEndUpdate = false);
            void setThumbnailViewModel(QAbstractItemModel* model);

            bool isAutoClearLocalData() const;
            void setAutoClearLocalData(bool autoclear);

        signals:
            void goToCurrentParentPathRequested();
            void itemExecuted(const QModelIndex& index);
            void modelDetached(QAbstractItemModel* model);
            void urlsDropped(const SDroppedURLsContext* context);

        public slots:
            virtual void beginUpdate();
            virtual void endUpdate();

        protected:
            explicit CThumbnailView(CBaseThumbnailViewPrivate& private_d, QWidget* parent = 0);

            // QListView
            virtual void dragEnterEvent(QDragEnterEvent* event);
            virtual void dragMoveEvent(QDragMoveEvent* event);
            virtual void keyReleaseEvent(QKeyEvent* event);
            virtual void mouseDoubleClickEvent(QMouseEvent* event);
            virtual void startDrag(Qt::DropActions supportedActions);

            virtual void initItemDelegate();
            virtual void processDroppedURLs(const QList< QUrl >& urls, Qt::DropAction dropaction, const QPoint& pos = QPoint());
            virtual bool siftIndexForItemOperation(const QModelIndex& index, EItemOperation itemoper);
            virtual bool siftVisibleIndex(const QModelIndex& index);
            virtual void thumbnailSizeChanged();

        private:
            Q_DECLARE_PRIVATE(CThumbnailView)

        private slots:
            void slotDropCancelTriggered();
            void slotDropCopyTriggered();
            void slotDropMoveTriggered();
            void slotItemsProviderNewItemsDetected(QList< SThumbnailItem* > items);
        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CTHUMBNAILVIEW_H
