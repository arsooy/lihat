#include "cfolderstreeview.h"

#include "../config_lihat.h"
#include "models/cfilesystemmodel.h"

#include <QDir>
#include <QHeaderView>
#include <QScrollBar>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::Thumbnails;

CFoldersTreeView::CFoldersTreeView(QWidget* parent):
    QTreeView(parent),
    m_model(new CFilesystemModel(this)),
    m_modelinitialized(false)
{
#if LIHAT_QT == 4
    header()->setResizeMode(QHeaderView::ResizeToContents);
#elif LIHAT_QT == 5
    header()->setSectionResizeMode(QHeaderView::ResizeToContents);
#endif
    header()->setStretchLastSection(false);
    setHeaderHidden(true);

    setAutoScroll(false);
    setSelectionBehavior(QTreeView::SelectRows);
    setUniformRowHeights(true);
    setVerticalScrollMode(QAbstractItemView::ScrollPerPixel);

    m_model->setFilter(QDir::Drives | QDir::AllDirs | QDir::NoDotAndDotDot);
    setModel(m_model);

    QObject::connect(m_model, SIGNAL(directoryLoaded(QString)),
                     this, SIGNAL(directoryAvailable(QString)));
}

CFoldersTreeView::~CFoldersTreeView()
{

}

void CFoldersTreeView::currentChanged(const QModelIndex& current, const QModelIndex& previous)
{
    QTreeView::currentChanged(current, previous);

    emit  currentIndexChanged(current, previous);
}

bool CFoldersTreeView::indexAtCenterArea(const QModelIndex& index)
{
    QRect viewrect = viewport()->rect();

    unsigned int horzcutmargin = qBound< unsigned int >(0, int(0.05 * viewrect.width()), int(0.3 * viewrect.width()));
    unsigned int vertcutmargin = qBound< unsigned int >(0, int(0.2 * viewrect.width()), int(0.5 * viewrect.width()));

    QRect centerviewrect = viewrect;
    centerviewrect.adjust(horzcutmargin, vertcutmargin, -horzcutmargin, -vertcutmargin);

    QRect indexrect = visualRect(index);
    QRect insectrect = centerviewrect & indexrect;

    unsigned int indexarea = indexrect.width() * indexrect.height();
    unsigned int insectarea = insectrect.width() * insectrect.height();
    qreal minimumarea = 0.5 * qreal(indexarea);

    return insectarea >= minimumarea;
}

QModelIndex CFoldersTreeView::indexOfPath(const QString& path)
{
    QModelIndex result = m_model->index(path);

    return result;
}

void CFoldersTreeView::scrollToWhenNecessary(const QModelIndex& index, bool center)
{
    if (index.model() != model())
        return;

    if (!indexAtCenterArea(index))
    {
        if (center)
            scrollTo(index, PositionAtCenter);
        else
            scrollTo(index, EnsureVisible);
    }
}

void CFoldersTreeView::showEvent(QShowEvent* event)
{
    QWidget::showEvent(event);

    if (isVisible() && !m_modelinitialized)
    {
        m_model->setRootPath("/");
        m_modelinitialized = true;
    }
}
