#ifndef CDIRECTORYREADER_H
#define CDIRECTORYREADER_H

#include <QFileSystemWatcher>
#include <QMutex>
#include <QObject>
#include <QString>
#include <QStringList>


namespace Lihat
{
    namespace Thumbnails
    {

        class CDirectoryReader:
            public QObject
        {
            Q_OBJECT
        public:
            explicit CDirectoryReader(QObject* parent = 0);
            virtual ~CDirectoryReader();

            void clear();

            bool hasEntry(const QString& name);
            void* entryData(const QString& name, bool* found);

            QString path() const;
            void    setPath(const QString& path);

        signals:
            void directoryChanged();
            void entryFound(const QString& entryname, void* data);
            void entryDeleted(const QString& formerentryname);

            void finished();
            void started();

        public slots:
            void read();
            void stop();

        private:
            QStringList        m_entries;
            QString            m_path;
            bool               m_stopped;
            QFileSystemWatcher m_watcher;
            QMutex             m_mutex;

            void clearWatcher();
            bool installWatcher();

            void readDirectory();

        private slots:
            void slotWatcherDirectoryChanged(const QString& path);

        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CDIRECTORYREADER_H
