#ifndef CBASETHUMBNAILMODEL_H
#define CBASETHUMBNAILMODEL_H

#include "../iitemsprovider.h"

#include <QAbstractListModel>


namespace Lihat
{
    namespace Thumbnails
    {

        class CBaseThumbnailModel:
            public QAbstractListModel,
            public IItemsProvider
        {
            Q_OBJECT
        public:
            explicit CBaseThumbnailModel(QObject* parent = 0):
                QAbstractListModel(parent)
            {

            }

            virtual ~CBaseThumbnailModel()
            {

            }

        signals:
            /**
             * @brief To be emitted when items provider start to enumerate its items.
             *
             * @return void
             */
            void itemsProviderCollectStarted();
            /**
             * @brief To be emitted when items provider finish enumerating items.
             *
             * @return void
             */
            void itemsProviderCollectFinished();
            /**
             * @brief To be emitted when new items detected.
             *
             * @param items List of items not found before.
             * @return void
             */
            void itemsProviderNewItemsDetected(QList< SThumbnailItem* > items);
        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CBASETHUMBNAILMODEL_H
