#include "cfilesystemmodel.h"


using namespace Lihat;
using namespace Lihat::Thumbnails;

CFilesystemModel::CFilesystemModel(QObject* parent):
    QFileSystemModel(parent)
{

}

CFilesystemModel::~CFilesystemModel()
{

}

int CFilesystemModel::columnCount(const QModelIndex& parent) const
{
    Q_UNUSED(parent);

    return 1;
}
