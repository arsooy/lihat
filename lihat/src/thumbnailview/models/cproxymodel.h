#ifndef CPROXYMODEL_H
#define CPROXYMODEL_H

#include "../filters/citemsfilter.h"

#include <QList>
#include <QModelIndex>
#include <QSortFilterProxyModel>


namespace Lihat
{
    namespace Thumbnails
    {

        class CProxyModel:
            public QSortFilterProxyModel
        {
            Q_OBJECT
        public:
            enum EItemSortingBy
            {
                ISB_NAME = 0,
                ISB_SIZE = 1,
                ISB_DATE = 2
            };


            explicit CProxyModel(QObject* parent = 0);
            virtual ~CProxyModel();

            // QSortFilterProxyModel
            virtual bool filterAcceptsRow(int source_row, const QModelIndex&) const;
            virtual bool lessThan(const QModelIndex& left, const QModelIndex& right) const;

            void          clearItemsFilter();
            bool          deleteItemsFilter(CItemsFilter* filter);
            bool          deleteItemsFilterAt(int filterindex);
            int           itemsFilterCount() const;
            CItemsFilter* newItemsFilter(CItemsFilter::EItemsFilterType type);

            void setSortedBy(Lihat::Thumbnails::CProxyModel::EItemSortingBy sortby);
            EItemSortingBy sortedBy() const;

            void setSortOrder(Qt::SortOrder order);
            Qt::SortOrder sortOrder() const;

        public slots:
            void filterChanged();

        signals:
            void itemsFilterDeleted(const CItemsFilter* filter);
            void itemsFilterAdded(CItemsFilter* filter);
            void sortChanged();

        private:
            QList< CItemsFilter* >  m_itemsfilters;
            bool                    m_sortascending;
            EItemSortingBy          m_sortby;

        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CPROXYMODEL_H
