#ifndef CDIRECTORYMODEL_H
#define CDIRECTORYMODEL_H

#include "cdirectoryreader.h"
#include "cbasethumbnailmodel.h"

#include <QAbstractListModel>
#include <QDate>
#include <QThread>
#include <QTimer>
#include <QStringList>


namespace Lihat
{
    namespace Thumbnails
    {

        class CDirectoryModel:
            public CBaseThumbnailModel
        {
            Q_OBJECT
        public:
            enum EItemTypeBit
            {
                ITB_FILE        = 1 << 0,
                ITB_SYMLINK     = 1 << 1,
                ITB_EXECUTABLE  = 1 << 2,
                ITB_HIDDEN      = 1 << 3,
                ITB_READABLE    = 1 << 4,
                ITB_WRITABLE    = 1 << 5
            };

            struct ItemData
            {
                QDate  CreationDate;
                QDate  LastAccessDate;
                QDate  LastModificationDate;
                qint64 Size;
                char   Type;
            };


            explicit CDirectoryModel(QObject* parent = 0);
            virtual ~CDirectoryModel();

            // QAbstractListModel
            virtual QVariant      data(const QModelIndex& index, int role = Qt::DisplayRole) const;
            virtual Qt::ItemFlags flags(const QModelIndex& index) const;
            virtual QModelIndex   index(int row, int column, const QModelIndex& parent = QModelIndex()) const;
            virtual QMimeData*    mimeData(const QModelIndexList& indexes) const;
            virtual bool          removeRows(int row, int count, const QModelIndex& = QModelIndex());
            virtual int           rowCount(const QModelIndex& = QModelIndex()) const;

            // IItemsProvider
            virtual void            disposeThumbnailItem(SThumbnailItem* titem);
            virtual int             indexOfThumbnailItem(SThumbnailItem* titem);
            virtual SThumbnailItem* thumbnailItem(int idx);
            virtual void            thumbnailItemRemove(int idx);
            virtual QUrl            thumbnailItemURL(SThumbnailItem* titem);
            virtual int             thumbnailItemsCount() const;
            virtual void            thumbnailItemsClear();
            virtual EProviderType   thumbnailItemsProviderType() const;

            virtual QString path() const;
            virtual bool    setPath(const QString& path);

            int indexOfItemName(const QString& name) const;

            static ItemData* modelItemData(SThumbnailItem* titem, bool* ok, bool autocreate = false);
            static bool loadModelItemData(const QUrl& fileurl, ItemData** pidata);

        protected:
            QString                  m_currentpath;
            QTimer                   m_delayedupdatetimer;
            bool                     m_inwatchmode;
            QList< SThumbnailItem* > m_items;
            QList< SThumbnailItem* > m_newitemsplaceholder;
            QMutex                   m_mutex;
            CDirectoryReader         m_reader;
            QStringList              m_readernames;
            QThread                  m_readerthread;

            void installDelayedUpdate();

        protected slots:
            void slotReaderStart();
            void slotReaderFinished();
            void slotReaderDirectoryChanged();
            void slotReaderEntryDeleted(QString fename);
            void slotReaderEntryFound(QString name, void* data);
            void update();

        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CDIRECTORYMODEL_H
