#include "cproxymodel.h"

#include "../filters/citemsdatefilter.h"
#include "../filters/citemsfilter.h"
#include "../filters/citemsnamefilter.h"
#include "../filters/citemssizefilter.h"
#include "../models/cbasethumbnailmodel.h"
#include "../sthumbnailitem.h"
#include "cdirectorymodel.h"

#include <QtDebug>


using namespace Lihat;
using namespace Lihat::Thumbnails;

CProxyModel::CProxyModel(QObject* parent):
    QSortFilterProxyModel(parent),
    m_sortascending(true),
    m_sortby(ISB_NAME)
{

}

CProxyModel::~CProxyModel()
{
    clearItemsFilter();
}

void CProxyModel::clearItemsFilter()
{
    while (m_itemsfilters.count() > 0)
        deleteItemsFilterAt(m_itemsfilters.count() - 1);
}

bool CProxyModel::deleteItemsFilter(CItemsFilter* filter)
{
    int index = m_itemsfilters.indexOf(filter);
    if (index != -1)
        return deleteItemsFilterAt(index);

    return false;
}

bool CProxyModel::deleteItemsFilterAt(int filterindex)
{
    CItemsFilter* filter = m_itemsfilters.takeAt(filterindex);
    if (filter)
    {
        delete filter;
        emit itemsFilterDeleted(filter);
        return true;
    }

    return false;
}

bool CProxyModel::filterAcceptsRow(int source_row, const QModelIndex&) const
{
    if (itemsFilterCount() < 1)
        return true;

    CBaseThumbnailModel* ipsourcemodel = qobject_cast< CBaseThumbnailModel* >(sourceModel());
    if (!ipsourcemodel)
    {
        qWarning() << "CProxyModel::filterAcceptsRow:" <<
                      "Expecting a CBaseThumbnailViewItemProviderModel instance at source model, disabling filter.";
        return true;
    }

    for (int i = 0; i < m_itemsfilters.count(); ++i)
    {
        CItemsFilter* filter = m_itemsfilters.at(i);
        if (!filter->itemSatisfyFilter(source_row, ipsourcemodel))
            return false;
    }

    return true;
}

void CProxyModel::filterChanged()
{
    invalidateFilter();
}

int CProxyModel::itemsFilterCount() const
{
    return m_itemsfilters.count();
}

bool CProxyModel::lessThan(const QModelIndex& left, const QModelIndex& right) const
{
    const SThumbnailItem* l_titem = static_cast< const SThumbnailItem* >(left.internalPointer());
    const SThumbnailItem* r_titem = static_cast< const SThumbnailItem* >(right.internalPointer());

    if (l_titem->Type != r_titem->Type)
    {
        // force folder before files
        if (m_sortascending)
            return l_titem->Type == SThumbnailItem::TI_FOLDER;
        else
            return l_titem->Type != SThumbnailItem::TI_FOLDER;
    }
    else
    {
        CBaseThumbnailModel* srcmodel = qobject_cast< CBaseThumbnailModel* >(sourceModel());

        if (srcmodel && ((m_sortby == ISB_DATE) || (m_sortby == ISB_SIZE)))
        {
            switch (srcmodel->thumbnailItemsProviderType())
            {
                case IItemsProvider::PT_FILESYSTEM:
                {
                    CDirectoryModel::ItemData* ldata = reinterpret_cast< CDirectoryModel::ItemData* >(l_titem->ItemsProviderData);
                    CDirectoryModel::ItemData* rdata = reinterpret_cast< CDirectoryModel::ItemData* >(r_titem->ItemsProviderData);

                    if (ldata && rdata)
                    {
                        // if both data is not the same, do the data comparison
                        // otherwise this will go to default comparison
                        if ((m_sortby == ISB_DATE) && (ldata->LastModificationDate != rdata->LastModificationDate))
                            return ldata->LastModificationDate < rdata->LastModificationDate;
                        else if ((m_sortby == ISB_SIZE) && (ldata->Size != rdata->Size))
                            return ldata->Size < rdata->Size;
                    }

                    // do not break here, we will fall through using default
                    // comparison
                }

                default:
                    ;
            }
        }
    }

    // this is the default comparison, by comparing item 'caption'
    return QString::localeAwareCompare(l_titem->Caption, r_titem->Caption) < 0;
}

CItemsFilter* CProxyModel::newItemsFilter(CItemsFilter::EItemsFilterType type)
{
    CItemsFilter* filter = 0;

    switch (type)
    {
        case CItemsFilter::IFT_DATE:
        {
            filter = new CItemsDateFilter();
            break;
        }

        case CItemsFilter::IFT_NAME:
        {
            filter = new CItemsNameFilter();
            break;
        }

        case CItemsFilter::IFT_SIZE:
        {
            filter = new CItemsSizeFilter();
            break;
        }

        default:
            filter = 0;
    }

    if (filter)
    {
        m_itemsfilters << filter;
        emit itemsFilterAdded(filter);
    }

    return filter;
}

void CProxyModel::setSortedBy(CProxyModel::EItemSortingBy sortby)
{
    if (m_sortby == sortby)
        return;

    m_sortby = sortby;
    sort(0, sortOrder());
    emit sortChanged();
}

CProxyModel::EItemSortingBy CProxyModel::sortedBy() const
{
    return m_sortby;
}

void CProxyModel::setSortOrder(Qt::SortOrder order)
{
    if (order == sortOrder())
        return;

    m_sortascending = order == Qt::AscendingOrder ? true : false;
    sort(0, sortOrder());
    emit sortChanged();
}

Qt::SortOrder CProxyModel::sortOrder() const
{
    return m_sortascending ? Qt::AscendingOrder : Qt::DescendingOrder;
}
