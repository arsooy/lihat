#include "cdirectoryreader.h"

#include "../../pathutils.h"
#include "cdirectorymodel.h"

#include <QDir>
#include <QDirIterator>


using namespace Lihat;
using namespace Lihat::Thumbnails;

CDirectoryReader::CDirectoryReader(QObject* parent):
    QObject(parent),
    m_stopped(false)
{
    connect(&m_watcher, SIGNAL(directoryChanged(QString)),
            this, SLOT(slotWatcherDirectoryChanged(QString)));
}

CDirectoryReader::~CDirectoryReader()
{
    clearWatcher();
    clear();
}

void CDirectoryReader::clear()
{
    m_entries.clear();
}

void CDirectoryReader::clearWatcher()
{
    QStringList entries = m_watcher.files();
    if (!entries.isEmpty())
        m_watcher.removePaths(entries);

    entries = m_watcher.directories();
    if (!entries.isEmpty())
        m_watcher.removePaths(entries);
}

bool CDirectoryReader::hasEntry(const QString& name)
{
    return m_entries.contains(name);
}

bool CDirectoryReader::installWatcher()
{
    if (m_path.isEmpty())
        return false;

    clearWatcher();
    m_watcher.addPath(m_path);

    return true;
}

void CDirectoryReader::readDirectory()
{

    QDirIterator diriterator(m_path, QDir::NoDotAndDotDot | QDir::AllEntries);
    if (m_entries.isEmpty() && !diriterator.hasNext())
        return;

    QStringList latestnames;
    while (!m_stopped && diriterator.hasNext())
    {
        diriterator.next();
        latestnames << diriterator.fileName();
    }
    if (m_stopped)
        return;

    // filter names against m_entries
    int curidx = 0;
    while (!m_stopped && (curidx < m_entries.count()))
    {
        QString name = m_entries.at(curidx);
        int idxatlatestnames = latestnames.indexOf(name);

        if (idxatlatestnames == -1)
        {
            // cant be found in the latest reading of directory, this entry
            // must have been deleted since last collect
            m_entries.removeAll(name);

            emit entryDeleted(name);
            continue;
        }
        else
        {
            // can be found in the latest reading of directory, this entry
            // remains so remove the duplicate at latestnames
            latestnames.removeAt(idxatlatestnames);
        }

        ++curidx;
    }
    if (m_stopped)
        return;

    // by now latestnames is filtered to items not found in m_entries, they are
    // new entries
    for (int i = 0; i < latestnames.count(); ++i)
    {
        if (m_stopped)
            return;

        QString name = latestnames.at(i);

        // fill in model data now since we have the time ..
        m_mutex.lock();
        CDirectoryModel::ItemData* idata = 0;
        CDirectoryModel::loadModelItemData(QUrl::fromLocalFile(Utils::Path::join(m_path, name)),
                                           &idata);
        m_entries << name;
        m_mutex.unlock();

        emit entryFound(name, idata);
    }
}

QString CDirectoryReader::path() const
{
    return m_path;
}

void CDirectoryReader::setPath(const QString& path)
{
    m_path = path;
    clear();
    installWatcher();
}

void CDirectoryReader::read()
{
    m_stopped = false;

    emit started();
    readDirectory();
    emit finished();
}

void CDirectoryReader::stop()
{
    m_stopped = true;
}

void CDirectoryReader::slotWatcherDirectoryChanged(const QString& path)
{
    if (!m_path.isEmpty() && (path == m_path))
        emit directoryChanged();
}
