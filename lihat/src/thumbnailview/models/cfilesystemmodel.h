#ifndef CFILESYSTEMMODEL_H
#define CFILESYSTEMMODEL_H

#include <QFileSystemModel>


namespace Lihat
{
    namespace Thumbnails
    {

        class CFilesystemModel:
            public QFileSystemModel
        {
            Q_OBJECT
        public:
            explicit CFilesystemModel(QObject* parent = 0);
            virtual ~CFilesystemModel();

        protected:
            virtual int columnCount(const QModelIndex& parent = QModelIndex()) const;
        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CFILESYSTEMMODEL_H
