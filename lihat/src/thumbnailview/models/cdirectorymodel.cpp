#include "cdirectorymodel.h"

#include "../../pathutils.h"
#include "../sthumbnailitem.h"

#include <QFileInfo>
#include <QMimeData>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::Thumbnails;

static const unsigned short DELAYED_REFRESH_INTERVAL = 200;

CDirectoryModel::CDirectoryModel(QObject* parent):
    CBaseThumbnailModel(parent),
    m_inwatchmode(false),
    m_reader(0)
{
    m_reader.moveToThread(&m_readerthread);

    connect(&m_reader, SIGNAL(directoryChanged()),
            this, SLOT(slotReaderDirectoryChanged()));
    connect(&m_reader, SIGNAL(entryFound(QString, void*)),
            this, SLOT(slotReaderEntryFound(QString, void*)));
    connect(&m_reader, SIGNAL(entryDeleted(QString)),
            this, SLOT(slotReaderEntryDeleted(QString)));

    connect(&m_reader, SIGNAL(started()),
            this, SLOT(slotReaderStart()), Qt::BlockingQueuedConnection);
    connect(&m_reader, SIGNAL(finished()),
            this, SLOT(slotReaderFinished()));

    connect(&m_delayedupdatetimer, SIGNAL(timeout()),
            this, SLOT(update()));

    connect(&m_readerthread, SIGNAL(started()),
            &m_reader, SLOT(read()));
}

CDirectoryModel::~CDirectoryModel()
{
    m_readerthread.quit();
    m_readerthread.wait();
    thumbnailItemsClear();
}

QVariant CDirectoryModel::data(const QModelIndex& index, int role) const
{
    if (!index.isValid())
        return QVariant();

    switch (role)
    {
        case Qt::DisplayRole:
        {
            const SThumbnailItem* titem = m_items.at(index.row());
            if (titem)
                return m_readernames.at(titem->Identifier);

            break;
        }

        case Qt::ToolTipRole:
        {
            const SThumbnailItem* titem = m_items.at(index.row());
            if (titem)
                return QVariant(Utils::Path::toClean(titem->loadURL().toLocalFile()));

            break;
        }

        default:
            break;
    }

    return QVariant();
}

Qt::ItemFlags CDirectoryModel::flags(const QModelIndex& index) const
{
    if (!index.isValid())
        return 0;

    Qt::ItemFlags result = QAbstractItemModel::flags(index);

    const SThumbnailItem* titem = m_items.at(index.row());
    if (!titem)
        return result;

    QUrl itemurl = titem->loadURL();
    if (!itemurl.isEmpty())
    {
        result = result | Qt::ItemIsDragEnabled;
        if (titem->Type == SThumbnailItem::TI_FOLDER)
            result = result | Qt::ItemIsDropEnabled;
    }

    return result;
}

QModelIndex CDirectoryModel::index(int row, int column, const QModelIndex& parent) const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    return createIndex(row, column, m_items.at(row));
}

int CDirectoryModel::indexOfThumbnailItem(SThumbnailItem* titem)
{
    QMutexLocker locker(&m_mutex);
    return m_items.indexOf(titem);
}

QMimeData* CDirectoryModel::mimeData(const QModelIndexList& indexes) const
{
    QMimeData* mimedata = new QMimeData();
    QList< QUrl > urls;

    for (int i = 0; i < indexes.count(); ++i)
    {
        QModelIndex index = indexes.at(i);
        const SThumbnailItem* titem = m_items.at(index.row());

        urls << titem->loadURL();
    }

    mimedata->setUrls(urls);
    return mimedata;
}

bool CDirectoryModel::removeRows(int row, int count, const QModelIndex& )
{
    QMutexLocker locker(&m_mutex);
    while (count > 0)
    {
        m_items.removeAt(row);
        count--;
    }

    return count == 0;
}

int CDirectoryModel::rowCount(const QModelIndex& ) const
{
    return m_items.count();
}

void CDirectoryModel::slotReaderFinished()
{
    if (m_inwatchmode)
    {
        // emit a notification that we have new item(s) on the list
        QList< SThumbnailItem* >::iterator iter = m_newitemsplaceholder.begin();
        while (iter != m_newitemsplaceholder.end())
        {
            SThumbnailItem* titem = *iter;
            if (m_items.indexOf(titem) == -1)
            {
                iter = m_newitemsplaceholder.erase(iter);
                continue;
            }

            ++iter;
        }

        if (!m_newitemsplaceholder.isEmpty())
            emit itemsProviderNewItemsDetected(m_newitemsplaceholder);
    }
    else
    {
        emit itemsProviderCollectFinished();

        // we got here through an explicit setPath() call, which starts the
        // directory entries collection process, now that it is done we move to
        // 'watch for changes' mode to detect changes in that directory
        m_inwatchmode = true;
    }
}

void CDirectoryModel::slotReaderStart()
{
    if (!m_inwatchmode)
    {
        m_newitemsplaceholder.clear();
        emit itemsProviderCollectStarted();
    }
}

void CDirectoryModel::slotReaderDirectoryChanged()
{
    installDelayedUpdate();
}

void CDirectoryModel::slotReaderEntryDeleted(QString fename)
{
    qDebug() << "CDirectoryModel::slotReaderEntryDeleted:" <<
                fename;
    // remove from m_items
    int itemindex = indexOfItemName(fename);
    if (itemindex != -1)
        thumbnailItemRemove(itemindex);
}

void CDirectoryModel::slotReaderEntryFound(QString name, void* data)
{
    ItemData* idata = (ItemData*) data;

    qDebug() << "CDirectoryModel::slotReaderEntryFound:" <<
                name;

    // add name to identifier lookup list
    m_readernames << name;

    // insert to m_items
    QMutexLocker locker(&m_mutex);
    SThumbnailItem* titem = new SThumbnailItem(this);
    titem->Identifier = m_readernames.indexOf(name);
    titem->Caption = name;
    if (idata->Type & ITB_FILE)
        titem->Type = SThumbnailItem::TI_FILE;
    else
        titem->Type = SThumbnailItem::TI_FOLDER;
    titem->ItemsProviderData = idata;

    int newidx = m_items.count();
    beginInsertRows(QModelIndex(), newidx, newidx);
    m_items << titem;
    endInsertRows();

    if (m_inwatchmode)
    {
        // add to the list of items we'll send later at the end of reader's
        // enumeration (convenience for model users to see which items are new)
        m_newitemsplaceholder << titem;
    }
}

SThumbnailItem* CDirectoryModel::thumbnailItem(int idx)
{
    return m_items.at(idx);
}

int CDirectoryModel::thumbnailItemsCount() const
{
    return m_items.count();
}

void CDirectoryModel::disposeThumbnailItem(SThumbnailItem* titem)
{
    if (titem)
    {
        if (titem->ItemsProviderData)
        {
            // NOTE: this skip proper internal datatype check(s), this method
            // assume THAT YOU KNOW WHAT YOU ARE DOING, that you are feeding a
            // SThumbnailItem* alloc'd by an instance of this class, if that
            // is not the case then expect A SIGSEGV.
            //
            // So if YOU, that is most likely future me, get a SIGSEGV that
            // points HERE, see if the item passed to this function ACTUALLY
            // CREATED BY AN INSTANCE OF THIS CLASS.
            delete (ItemData*) titem->ItemsProviderData;
            titem->ItemsProviderData = 0;
        }

        delete titem;
    }
}

void CDirectoryModel::thumbnailItemRemove(int idx)
{
    QMutexLocker locker(&m_mutex);

    beginRemoveRows(QModelIndex(), idx, idx);
    disposeThumbnailItem(m_items.takeAt(idx));
    endRemoveRows();
}

QUrl CDirectoryModel::thumbnailItemURL(SThumbnailItem* titem)
{
    return QUrl::fromLocalFile(Utils::Path::join(path(), titem->Caption));
}

void CDirectoryModel::thumbnailItemsClear()
{
    int idx;
    while (m_items.count() > 0)
    {
        idx = m_items.count() - 1;
        thumbnailItemRemove(idx);
    }
}

IItemsProvider::EProviderType CDirectoryModel::thumbnailItemsProviderType() const
{
    return IItemsProvider::PT_FILESYSTEM;
}

int CDirectoryModel::indexOfItemName(const QString& name) const
{
    for (int i = 0; i < m_items.count(); ++i)
    {
        SThumbnailItem* cur = m_items.at(i);
        if (cur->Caption == name)
            return i;
    }

    return -1;
}

CDirectoryModel::ItemData* CDirectoryModel::modelItemData(SThumbnailItem* titem, bool* ok, bool autocreate)
{
    *ok = false;
    if (!titem)
        return 0;
    if (!titem->Provider)
        return 0;
    if (titem->Provider->thumbnailItemsProviderType() != PT_FILESYSTEM)
        return 0;

    ItemData* idata = (ItemData*) titem->ItemsProviderData;
    if (!idata)
    {
        if (autocreate)
        {
            loadModelItemData(titem->loadURL(), &idata);
            titem->ItemsProviderData = idata;
        }
        else
            return 0;
    }

    *ok = true;
    return idata;
}

bool CDirectoryModel::loadModelItemData(const QUrl& fileurl, ItemData** pidata)
{
    ItemData* idata = *pidata;
    bool dataincomplete = idata && (idata->CreationDate.isNull() ||
                                    idata->LastAccessDate.isNull() ||
                                    idata->LastModificationDate.isNull());
    if ((!idata || dataincomplete) && fileurl.isValid())
    {
        QFileInfo finfo(fileurl.toLocalFile());
        if (!finfo.exists())
            return false;

        // fill data from QFileInfo
        if (!idata)
            idata = new ItemData();
        idata->CreationDate = finfo.created().date();
        idata->LastAccessDate = finfo.lastRead().date();
        idata->LastModificationDate = finfo.lastModified().date();
        idata->Size = finfo.size();

        idata->Type = 0;
        if (finfo.isFile())
            idata->Type |= ITB_FILE;
        if (finfo.isSymLink())
            idata->Type |= ITB_SYMLINK;
        if (finfo.isExecutable())
            idata->Type |= ITB_EXECUTABLE;
        if (finfo.isHidden())
            idata->Type |= ITB_HIDDEN;
        if (finfo.isReadable())
            idata->Type |= ITB_READABLE;
        if (finfo.isWritable())
            idata->Type |= ITB_WRITABLE;

        *pidata = idata;
        return true;
    }

    return false;
}


QString CDirectoryModel::path() const
{
    return m_currentpath;
}

bool CDirectoryModel::setPath(const QString& path)
{
    QFileInfo pathfinfo(path);
    if (!(pathfinfo.isReadable() && pathfinfo.isDir()))
        return false;

    // dispose all that we have
    thumbnailItemsClear();
    m_readernames.clear();
    m_inwatchmode = false;

    // set new path and start collecting
    m_currentpath = Utils::Path::toClean(pathfinfo.absoluteFilePath());
    m_reader.setPath(m_currentpath);
    update();

    return true;
}

void CDirectoryModel::installDelayedUpdate()
{
    if (m_delayedupdatetimer.isActive())
        m_delayedupdatetimer.stop();

    // start the timer
    m_delayedupdatetimer.setSingleShot(true);
    m_delayedupdatetimer.start(DELAYED_REFRESH_INTERVAL);
}

void CDirectoryModel::update()
{
    // ask currently running reader to finish
    m_reader.stop();
    m_readerthread.quit();
    m_readerthread.wait();

    m_readerthread.start();
}
