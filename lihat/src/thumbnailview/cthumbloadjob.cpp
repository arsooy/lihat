#include "cthumbloadjob.h"

#include "../icoutils.h"
#include "../imageload/cimageloadresult.h"
#include "sthumbnailitem.h"


using namespace Lihat;
using namespace Lihat::Thumbnails;

CThumbLoadJob::CThumbLoadJob(CBaseThumbnailView* requester, SThumbnailItem* titem, const QSize& loadsize):
    ImageLoad::CURLLoadJob(titem->loadURL()),
    Requester(requester),
    ThumbnailItem(titem)
{
    setRequestedSize(loadsize);
    setResizeTransformation(Qt::SmoothTransformation);
}

CThumbLoadJob::CThumbLoadJob(const CThumbLoadJob& other):
    ImageLoad::CURLLoadJob(other.URL),
    Requester(other.Requester),
    ThumbnailItem(other.ThumbnailItem)
{
    setRequestedSize(other.requestedSize());
    setResizeTransformation(other.resizeTransformation());
}

CThumbLoadJob::~CThumbLoadJob()
{

}

bool CThumbLoadJob::operator==(const CThumbLoadJob& other)
{
    return CURLLoadJob::operator==(other) &&
           (Requester == other.Requester) &&
           (ThumbnailItem == other.ThumbnailItem) &&
           (requestedSize() == other.requestedSize()) &&
           (resizeTransformation() == other.resizeTransformation());
}

ImageLoad::CURLLoadJob::EImageLoadType CThumbLoadJob::imageLoadType() const
{
    return ILT_THUMBNAILVIEW;
}

bool CThumbLoadJob::isValid() const
{
    return CURLLoadJob::isValid() &&
           (Requester && ThumbnailItem && ThumbnailItem->isValid());
}

bool CThumbLoadJob::load()
{
    dropResult();

    if (!URL.isLocalFile())
    {
        // for now we only load local files
        setResult(new ImageLoad::CImageLoadResult(ImageLoad::CImageLoadResult::ILRC_FAIL));

        return false;
    }

    return loadFile(URL.toLocalFile());
}

bool CThumbLoadJob::loadImageReader(QImageReader* ir)
{
    QSize thumbsize = requestedSize().toSize();

    applySubImageToImageReader(ir);
    applyResizeToImageReader(ir);

    QImage* img = new QImage();
    if (ir->read(img))
    {
        QSize loadedsize = img->size();
        if ((m_parambits & ImageLoad::IResizeJob::InBit) &&
            ((loadedsize.width()  > thumbsize.width()) || (loadedsize.height() > thumbsize.height())))
        {
            // not resized by read(), use scaled() into size to force it
            QImage scaled = img->scaled(thumbsize, Qt::KeepAspectRatio, resizeTransformation());
            scaled.swap(*img);
        }

        ImageLoad::CImageLoadResult* result = new ImageLoad::CImageLoadResult(ImageLoad::CImageLoadResult::ILRC_OK);
        result->Image = img;
        result->FullSize = m_foundsize;

        setResult(result);

        return true;
    }
    else
    {
        delete img;

        setResult(new ImageLoad::CImageLoadResult(ImageLoad::CImageLoadResult::ILRC_FAIL));
    }

    return false;
}

bool CThumbLoadJob::applyResizeToImageReader(QImageReader* ir)
{
    if (!(m_parambits & ImageLoad::IResizeJob::InBit))
        return false;

    QSize thumbsize = requestedSize().toSize();
    Q_ASSERT(thumbsize.isValid() && !thumbsize.isEmpty());

    if (ir->supportsOption(QImageIOHandler::ScaledSize))
    {
        QSize readsize = m_foundsize = ir->size();
        if (readsize.isValid() &&
            ((readsize.width() > thumbsize.width()) || (readsize.height() > thumbsize.height())))
        {
            QSize downsize = QSize(readsize);
            downsize.scale(thumbsize, Qt::KeepAspectRatio);
            ir->setScaledSize(downsize);

            return true;
        }
    }

    return false;
}

bool CThumbLoadJob::applySubImageToImageReader(QImageReader* ir)
{
    QSize thumbsize = requestedSize().toSize();
    if (QString(ir->format()) == Utils::ICO::FORMAT_STR)
    {
        // if we are dealing with icon, look for best looking sub image
        return Utils::ICO::locateNicerIcon(ir, thumbsize) == Utils::ICO::NILR_SUCCESS;
    }

    return false;
}

QSizeF CThumbLoadJob::requestedSize() const
{
    return m_requestsize;
}

Qt::TransformationMode CThumbLoadJob::resizeTransformation() const
{
    return m_resizetrans;
}

void CThumbLoadJob::setRequestedSize(const QSizeF& rsize)
{
    m_requestsize = rsize;
    if (m_requestsize.isValid() && !m_requestsize.isEmpty())
        m_parambits |= ImageLoad::IResizeJob::InBit;
}

void CThumbLoadJob::setResizeTransformation(Qt::TransformationMode tm)
{
    m_resizetrans = tm;
}
