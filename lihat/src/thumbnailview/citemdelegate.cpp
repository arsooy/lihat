#include "citemdelegate.h"

#include "../fileutils.h"
#include "../persistenceutils.h"
#include "cthumbnailview.h"
#include "cthumbnailview_p.h"
#include "iitemsprovider.h"
#include "ipixmapprovider.h"
#include "models/cdirectorymodel.h"
#include "models/cproxymodel.h"
#include "sthumbnailitem.h"

#include <QPainter>
#include <QPixmap>
#include <QRect>
#include <QStyleOptionViewItem>
#include <QTextLayout>
#include <QtDebug>
#include <qmath.h>


using namespace Lihat;
using namespace Lihat::Thumbnails;

static const QString SIZE_DIM_FORMAT = QString("%1 x %2");

CItemDelegate::CItemDelegate(QObject* parent):
    QStyledItemDelegate(parent),
    m_cellpadding(4),
    m_drawdimension(Utils::Persistence::getConfig("ThumbnailView/ShowDimension", QVariant(false)).toBool()),
    m_drawfilesize(Utils::Persistence::getConfig("ThumbnailView/ShowFileSize", QVariant(false)).toBool()),
    m_infotextpaintopacity(0.75),
    m_infotextpointpercent(0.9),
    m_namelines(2)
{

}

CItemDelegate::~CItemDelegate()
{

}

unsigned short CItemDelegate::cellPadding() const
{
    return m_cellpadding;
}

unsigned char CItemDelegate::maxLinesForName() const
{
    return m_namelines;
}

bool CItemDelegate::isDrawingFileSize() const
{
    return m_drawfilesize;
}

void CItemDelegate::setDrawFileSize(bool draw)
{
    m_drawfilesize = draw;
}

bool CItemDelegate::isDrawingImageDimension() const
{
    return m_drawdimension;
}

void CItemDelegate::setDrawImageDimension(bool draw)
{
    m_drawdimension = draw;
}

void CItemDelegate::paint(QPainter* painter,
                          const QStyleOptionViewItem& soption,
                          const QModelIndex& index) const
{
    Q_ASSERT(index.isValid());
    Q_ASSERT(m_pixmapprovider);

    QStyleOptionViewItemV4 option(soption);
    CThumbnailView* view = qobject_cast< CThumbnailView* >(const_cast< QWidget* >(option.widget));
    if (!view)
        return;

    SThumbnailItem* titem = view->thumbnailItemAt(index);
    if (!titem)
        return;

    QSize thumbsize = view->thumbnailSize();
    QRect cellrect = option.rect;
    QRect pixrect = QRect(cellrect.topLeft() + QPoint(m_cellpadding, m_cellpadding),
                          thumbsize);
    QRect caprect = QRect(pixrect.left(), pixrect.bottom() + m_cellpadding,
                          pixrect.width(), cellrect.bottom() - pixrect.bottom() - m_cellpadding);

    int thumbnailalignment = Qt::AlignBottom | Qt::AlignHCenter;

    QPixmap* tpixmap = 0;
    SThumbnailLocalData* viewlocaldata = view->thumbnailLocalData(titem);

    CBaseThumbnailView::EThumbnailLoadStatus loadstatus = (CBaseThumbnailView::EThumbnailLoadStatus) viewlocaldata->LoadStatus;
    switch (loadstatus)
    {
        case CBaseThumbnailView::TLS_NOTAVAILABLE:
        {
            tpixmap = m_pixmapprovider->fetchThumbnailViewPixmap((int) SThumbnailItem::TI_INVALID);
            break;
        }

        case CBaseThumbnailView::TLS_PENDING:
        {
            tpixmap = m_pixmapprovider->fetchThumbnailViewPixmap((int) SThumbnailItem::TI_FILE);
            break;
        }

        case CBaseThumbnailView::TLS_MIMETYPEPIXMAP:
        case CBaseThumbnailView::TLS_LOADED:
        {
            tpixmap = viewlocaldata->Pixmap;
            break;
        }

        default:
            tpixmap = m_pixmapprovider->fetchThumbnailViewPixmap((int) SThumbnailItem::TI_FILE);
    }

    if (titem->Type == SThumbnailItem::TI_FOLDER)
        tpixmap = m_pixmapprovider->fetchThumbnailViewPixmap((int) SThumbnailItem::TI_FOLDER);

    QStyle* style = view->style();
    QString dimensionstr;
    QString fsizestr;
    QRect dimensionrect;
    QRect fsizerect;
    int bottommost = 0;

    // draw background
    painter->save();
    {
        // paint picture box
        //painter->fillRect(pixrect, Qt::blue);

        // paint caption box
        //painter->fillRect(caprect, Qt::cyan);

        // calculate item name rect
        QRect namerect(caprect);
        // calculate minimum height, unless the name is too big then we use the
        // default (ie. max. number of lines * font height)
        namerect.setHeight(qMin< int >(option.fontMetrics.height() * m_namelines,
                                       option.fontMetrics.boundingRect(namerect, Qt::TextWordWrap, titem->Caption).height()));

        bottommost = namerect.bottom();
        bool drawadinfo = false;
        if ((titem->Type == SThumbnailItem::TI_FILE) && (m_drawdimension || m_drawfilesize))
        {
            if (m_drawdimension && titem->FullSize.isValid())
            {
                if (!drawadinfo) // first info gets padding from name rect
                    bottommost += m_cellpadding;

                dimensionstr = SIZE_DIM_FORMAT.arg(titem->FullSize.width()).arg(titem->FullSize.height());
                dimensionrect = QRect(namerect.left(), bottommost,
                                      namerect.width(), option.fontMetrics.height());
                drawadinfo = dimensionrect.isValid();

                bottommost += dimensionrect.height();
            }

            if (m_drawfilesize && titem->Provider)
            {
                switch (titem->Provider->thumbnailItemsProviderType())
                {
                    case IItemsProvider::PT_FILESYSTEM:
                    {
                        bool idataok = false;
                        CDirectoryModel::ItemData* idata = CDirectoryModel::modelItemData(titem, &idataok);
                        if (idataok && idata)
                        {
                            if (!drawadinfo) // first info gets padding from name rect
                                bottommost += m_cellpadding;

                            fsizestr = Utils::File::fileSizeToString(idata->Size);
                            fsizerect = QRect(namerect.left(), bottommost,
                                            namerect.width(), option.fontMetrics.height());

                            drawadinfo = fsizerect.isValid();
                        }
                    }

                    default:
                        ;
                }

                bottommost += fsizerect.height();
            }
        }

        QRect mincellrect(cellrect);
        mincellrect.setHeight(bottommost - cellrect.top() + m_cellpadding);

        // draw list item
        QStyleOptionViewItemV4 sviopt(option);
        sviopt.features = QStyleOptionViewItemV4::WrapText | QStyleOptionViewItemV4::HasDecoration |
                          QStyleOptionViewItemV4::HasDisplay;
        sviopt.displayAlignment = Qt::AlignCenter;
        sviopt.showDecorationSelected = true;
        sviopt.decorationSize = thumbsize;
        sviopt.decorationPosition = QStyleOptionViewItem::Top;
        sviopt.rect = mincellrect;
        bool isSelected = option.state & QStyle::State_Selected;
        bool isCurrent = view->selectionModel()->currentIndex() == index;

        painter->setOpacity(1.0);
        if (isSelected && !isCurrent)
            painter->setOpacity(0.75);

        style->drawControl(QStyle::CE_ItemViewItem, &sviopt, painter, view);
        painter->setOpacity(1.0);

        // calculate width used by item caption
        int linescount = 0;
        int lineswidth = 0;
        // the use of QTextLayout is to exhaust the caption by laying down as
        // many text as possible for each line bounded by a certain width
        QTextLayout textlyt(titem->Caption, painter->font());
        textlyt.beginLayout();
        while (linescount < m_namelines -1)
        {
            QTextLine line = textlyt.createLine();
            if (!line.isValid())
                break;

            line.setLineWidth(namerect.width());
            lineswidth += line.naturalTextWidth();
            ++linescount;
        }
        textlyt.endLayout();

        if ((linescount < m_namelines) && (lineswidth > namerect.width()))
        {
            // since we are unable to put down name without breaking the
            // allocated width for a line it seems the name starts with/is one
            // unwrappable line, in that case we will force the text to be
            // elided as a single line
            lineswidth = namerect.width();
        }
        else
            lineswidth += namerect.width();

        if (isSelected)
            painter->setPen(option.palette.highlightedText().color());

        // paint caption-name box
        //painter->fillRect(namerect, Qt::red);
        // draw item caption
        painter->drawText(namerect, Qt::AlignTop | Qt::AlignHCenter | Qt::TextWordWrap,
                          option.fontMetrics.elidedText(titem->Caption, Qt::ElideRight, lineswidth));

        if (drawadinfo)
        {
            painter->setOpacity(m_infotextpaintopacity);
            QFont infofont(option.font);
            infofont.setPointSizeF(infofont.pointSize() * m_infotextpointpercent);
            painter->setFont(infofont);
            if (!dimensionstr.isNull() && dimensionrect.isValid())
            {
                // paint caption-dimension box
                //painter->fillRect(dimensionrect, Qt::yellow);
                painter->drawText(dimensionrect, dimensionstr, QTextOption(Qt::AlignCenter));
            }
            if (!fsizestr.isNull() && fsizerect.isValid())
            {
                // paint caption-size box
                //painter->fillRect(fsizerect, Qt::gray);
                painter->drawText(fsizerect, fsizestr, QTextOption(Qt::AlignCenter));
            }
        }
    }
    painter->restore();

    if ((loadstatus == CBaseThumbnailView::TLS_MIMETYPEPIXMAP) ||
        (loadstatus == CBaseThumbnailView::TLS_PENDING) ||
        (titem->Type == SThumbnailItem::TI_FOLDER) ||
        (titem->Type == SThumbnailItem::TI_INVALID))
    {
        // "normal" icons aligned to center
        thumbnailalignment = Qt::AlignBottom | Qt::AlignHCenter;
    }

    // draw thumbnail
    Q_ASSERT(tpixmap);
    style->drawItemPixmap(painter, pixrect, thumbnailalignment, *tpixmap);
}

bool CItemDelegate::setThumbnailViewPixmapProvider(IPixmapProvider* provider)
{
    m_pixmapprovider = provider;

    return m_pixmapprovider == provider;
}

QSize CItemDelegate::sizeHint(const QStyleOptionViewItem& soption,
                              const QModelIndex& /* index */) const
{
    QStyleOptionViewItemV4 option(soption);
    const CThumbnailView* thumbv = dynamic_cast< const CThumbnailView* >(option.widget);
    if (!thumbv)
        return QSize();

    QSize thumbsize = thumbv->thumbnailSize();
    int width  = thumbsize.width()  + (m_cellpadding * 2);
    int height = thumbsize.height() + (m_cellpadding * 4);

    char linenum = m_namelines;
    height += option.fontMetrics.height() * linenum;

    qreal inheight = 0;
    if (isDrawingFileSize())
        inheight += option.fontMetrics.height() * m_infotextpointpercent;
    if (isDrawingImageDimension())
        inheight += option.fontMetrics.height() * m_infotextpointpercent;
    if (inheight != 0)
        inheight += m_cellpadding;

    return QSize(width, height + qCeil(inheight));
}
