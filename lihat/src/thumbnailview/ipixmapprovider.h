#ifndef IPIXMAPPROVIDER_H
#define IPIXMAPPROVIDER_H

class QPixmap;

namespace Lihat
{
    namespace Thumbnails
    {

        class IPixmapProvider
        {
        public:
            virtual ~IPixmapProvider()
            {

            }

            virtual QPixmap*  fetchThumbnailViewPixmap(int pixmapcode) = 0;
        };

    }
} // namespace Lihat

#endif // IPIXMAPPROVIDER_H
