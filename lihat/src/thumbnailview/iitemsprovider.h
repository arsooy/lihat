#ifndef IITEMSPROVIDER_H
#define IITEMSPROVIDER_H

#include <QUrl>


namespace Lihat
{
    namespace Thumbnails
    {

        struct SThumbnailItem;

        class IItemsProvider
        {
        public:
            enum EProviderType
            {
                PT_INVALID    = 0,
                PT_FILESYSTEM = 1,
                PT_SUBIMAGE   = 2
            };


            virtual ~IItemsProvider()
            {

            }


            virtual void            disposeThumbnailItem(SThumbnailItem* titem) = 0;
            virtual int             indexOfThumbnailItem(SThumbnailItem* titem) = 0;
            virtual SThumbnailItem* thumbnailItem(int providerindex) = 0;
            virtual void            thumbnailItemRemove(int idx) = 0;
            virtual QUrl            thumbnailItemURL(SThumbnailItem* titem) = 0;
            virtual void            thumbnailItemsClear() = 0;
            virtual int             thumbnailItemsCount() const = 0;
            virtual EProviderType   thumbnailItemsProviderType() const = 0;
        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // IITEMSPROVIDER_H
