#ifndef CTHUMBNAILVIEW_P_H
#define CTHUMBNAILVIEW_P_H

#include "cbasethumbnailview_p.h"

#include <QString>


class QMenu;
class QEvent;

namespace Lihat
{
    namespace Thumbnails
    {

        class  CBaseScheduledTask;
        class  CThumbnailView;
        class  CProxyModel;
        struct SDroppedURLsContext;
        struct SUpdateCurrentThumbnailParam;

        class CThumbnailViewPrivate:
            public CBaseThumbnailViewPrivate
        {
        public:
            Q_DECLARE_PUBLIC(CThumbnailView)

            QMenu*               DropPopupMenu;
            SDroppedURLsContext* DroppedURLsContext;
            CProxyModel*         ProxyModel;

            explicit CThumbnailViewPrivate(CThumbnailView* thumbnailview);
            virtual ~CThumbnailViewPrivate();

            bool  addSetCurrentThumbnailScheduledTask(const QString& atname, CBaseScheduledTask* task);
            void  dropDroppedURLsContext();
            void  initActions();
            void  initUserInterface();
        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CTHUMBNAILVIEW_P_H
