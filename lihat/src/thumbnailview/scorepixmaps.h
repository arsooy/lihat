#ifndef SCOREPIXMAPS_H
#define SCOREPIXMAPS_H


class QPixmap;

namespace Lihat
{
    namespace Thumbnails
    {

        struct SCorePixmaps
        {
            QPixmap*  File;
            QPixmap*  Folder;
            QPixmap*  NoThumbnail;

            SCorePixmaps();
            ~SCorePixmaps();

            void  clear();
        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // SCOREPIXMAPS_H
