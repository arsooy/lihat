#include "cfilterwidget.h"

#include <QHBoxLayout>
#include <QStyle>
#include <QToolButton>


using namespace Lihat;
using namespace Lihat::Thumbnails;

CFilterWidget::CFilterWidget(CItemsFilter* filter, QWidget* parent):
    QWidget(parent),
    m_filter(filter)
{

}

CFilterWidget::~CFilterWidget()
{

}

void CFilterWidget::initUserInterface()
{
    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setMargin(0);
    layout->setSpacing(4);
    layout->setSizeConstraint(QLayout::SetFixedSize);

    QToolButton* deletebutton = new QToolButton(this);
    deletebutton->setObjectName("tbtnDelete");
    deletebutton->setAutoRaise(true);
    deletebutton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    deletebutton->setIcon(style()->standardIcon(QStyle::SP_DialogDiscardButton));
    deletebutton->setIconSize(QSize(16, 16));
    connect(deletebutton, SIGNAL(clicked()),
            this, SIGNAL(filterDiscarded()));

    layout->addWidget(createFilterWidget(), 1);
    layout->addWidget(deletebutton, 0, Qt::AlignTop);
}

CItemsFilter* CFilterWidget::itemsFilter() const
{
    return m_filter;
}
