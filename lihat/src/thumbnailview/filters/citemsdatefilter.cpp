#include "citemsdatefilter.h"

#include "../iitemsprovider.h"
#include "../models/cdirectorymodel.h"
#include "../sthumbnailitem.h"

#include <QFileInfo>


using namespace Lihat;
using namespace Lihat::Thumbnails;

CItemsDateFilter::CItemsDateFilter():
    CItemsFilter(),
    m_datefilterdate(QDate::currentDate()),
    m_datefiltermode(DFM_ON),
    m_datefilterwhich(DFW_CREATION)
{

}

CItemsDateFilter::~CItemsDateFilter()
{

}

CItemsFilter::EItemsFilterType CItemsDateFilter::filterType() const
{
    return CItemsFilter::IFT_DATE;
}

bool CItemsDateFilter::itemSatisfyFilter(int itemsproviderindex, IItemsProvider* itemsprovider)
{
    if (!m_datefilterdate.isValid() || m_datefilterdate.isNull())
        return false;

    QUrl itemurl;
    SThumbnailItem* titem = itemsprovider->thumbnailItem(itemsproviderindex);
    if (titem)
    {
        itemurl = titem->loadURL();
        if (!itemurl.isValid())
            return false;

        if (!itemurl.isLocalFile())
            return false;
    }
    Q_ASSERT(titem);

    QDate itemdate;
    switch (itemsprovider->thumbnailItemsProviderType())
    {
        case IItemsProvider::PT_FILESYSTEM:
        {
            CDirectoryModel::ItemData* itemdata = (CDirectoryModel::ItemData*) titem->ItemsProviderData;
            // split to two lines for readibility sake
            CDirectoryModel::loadModelItemData(titem->loadURL(), &itemdata);
            titem->ItemsProviderData = itemdata;

            switch (m_datefilterwhich)
            {
                case DFW_CREATION:
                {
                    itemdate = itemdata->CreationDate;
                    break;
                }

                case DFW_MODIFICATION:
                {
                    itemdate = itemdata->LastModificationDate;
                    break;
                }

                case DFW_ACCESS:
                {
                    itemdate = itemdata->LastAccessDate;
                    break;
                }

                default:
                    ;
            }

            break;
        }

        default:
            return false;
    }

    if (itemdate.isNull())
        return false;

    switch (m_datefiltermode)
    {
        case DFM_ON:
            return itemdate == m_datefilterdate;
        case DFM_NOTON:
            return itemdate != m_datefilterdate;
        case DFM_BEFORE:
            return itemdate < m_datefilterdate;
        case DFM_BEFOREORON:
            return itemdate <= m_datefilterdate;
        case DFM_AFTER:
            return itemdate > m_datefilterdate;
        case DFM_AFTERORON:
            return itemdate >= m_datefilterdate;
        default:
            return false;
    }

    return false;
}

QDate CItemsDateFilter::dateFilterDate() const
{
    return m_datefilterdate;
}

CItemsDateFilter::EDateFilterMode CItemsDateFilter::dateFilterMode() const
{
    return m_datefiltermode;
}

CItemsDateFilter::EDateFilterWhich CItemsDateFilter::dateFilterWhich() const
{
    return m_datefilterwhich;
}

void CItemsDateFilter::setDateFilterDate(const QDate& date)
{
    m_datefilterdate = date;
}

void CItemsDateFilter::setDateFilterMode(const CItemsDateFilter::EDateFilterMode mode)
{
    m_datefiltermode = mode;
}

void CItemsDateFilter::setDateFilterWhich(const CItemsDateFilter::EDateFilterWhich which)
{
    m_datefilterwhich = which;
}
