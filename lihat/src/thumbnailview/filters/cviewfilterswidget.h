#ifndef CVIEWFILTERSWIDGET_H
#define CVIEWFILTERSWIDGET_H

#include "citemsfilter.h"

#include <QList>
#include <QWidget>


namespace Lihat
{
    namespace Thumbnails
    {

        class CFilterWidget;
        class CProxyModel;

        class CViewFiltersWidget:
            public QWidget
        {
            Q_OBJECT
        public:
            explicit CViewFiltersWidget(QWidget* parent = 0);
            virtual ~CViewFiltersWidget();

            void newFilter(CItemsFilter::EItemsFilterType type);
            void setProxyModel(CProxyModel* proxymodel);

        signals:
            void filterChanged();

        protected:
            QWidget*     m_filterwidgetscontainer;
            CProxyModel* m_proxymodel;

            void initActions();

            CFilterWidget* createFilterWidget(CItemsFilter* filter);
            CFilterWidget* getFilterWidget(const CItemsFilter* filter) const;

        private slots:
            void slotFilterDiscarded();
            void slotNewFilterClicked();
            void slotProxyModelItemsFilterAdded(CItemsFilter* filter);
            void slotProxyModelItemsFilterDeleted(const CItemsFilter* filter);
            void slotNewFilterByDateTriggered();
            void slotNewFilterByNameHovered();
            void slotNewFilterByNameTriggered();
            void slotNewFilterBySizeTriggered();

        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CVIEWFILTERSWIDGET_H
