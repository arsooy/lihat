#ifndef CITEMSNAMEFILTER_H
#define CITEMSNAMEFILTER_H

#include <QString>

#include "citemsfilter.h"


namespace Lihat
{
    namespace Thumbnails
    {

        class CItemsNameFilter:
            public CItemsFilter
        {
        public:
            enum ENameFilterMode
            {
                NFM_CONTAINS     = 0,
                NFM_NOTCONTAINS  = 1
            };


            explicit CItemsNameFilter();
            virtual ~CItemsNameFilter();

            // CItemsFilter
            virtual EItemsFilterType filterType() const;
            virtual bool             itemSatisfyFilter(int itemsproviderindex, IItemsProvider* itemsprovider);

            bool            nameFilterIsCaseSensitive() const;
            ENameFilterMode nameFilterMode() const;
            QString         nameFilterText() const;
            void            setNameFilterIsCaseSensitive(const bool casesensitive);
            void            setNameFilterMode(const ENameFilterMode ftype);
            void            setNameFilterText(const QString& text);

        private:
            bool            m_namefiltercasesensitive;
            ENameFilterMode m_namefiltermode;
            QString         m_namefiltertext;

        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CITEMSNAMEFILTER_H
