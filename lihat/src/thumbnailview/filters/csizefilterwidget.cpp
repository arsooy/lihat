#include "csizefilterwidget.h"

#include <QComboBox>
#include <QLineEdit>
#include <QVBoxLayout>
#include <QWidget>


using namespace Lihat;
using namespace Lihat::Thumbnails;

CSizeFilterWidget::CSizeFilterWidget(CItemsSizeFilter* filter, QWidget* parent):
    CFilterWidget(filter, parent)
{
    initUserInterface();
}

CSizeFilterWidget::~CSizeFilterWidget()
{

}

QWidget* CSizeFilterWidget::createFilterWidget()
{
    CItemsSizeFilter* filter = static_cast< CItemsSizeFilter* >(itemsFilter());
    Q_ASSERT(filter);

    QWidget* ret = new QWidget(this);
    ret->setMinimumWidth(200);

    QVBoxLayout* layout = new QVBoxLayout(ret);
    layout->setMargin(0);
    layout->setSizeConstraint(QLayout::SetMinimumSize);

    // comparison widget
    QComboBox* cbMode = new QComboBox(ret);
    cbMode->setObjectName("cbMode");
    cbMode->addItem(tr("Equals to (=)"),
                    QVariant(int(CItemsSizeFilter::SFM_EQUALS)));
    cbMode->addItem(tr("Not equals to (!=)"),
                    QVariant(int(CItemsSizeFilter::SFM_NOTEQUALS)));
    cbMode->addItem(tr("Greater than (>)"),
                    QVariant(int(CItemsSizeFilter::SFM_GREATER)));
    cbMode->addItem(tr("Greater or equals to (>=)"),
                    QVariant(int(CItemsSizeFilter::SFM_GREATEROREQUALS)));
    cbMode->addItem(tr("Lesser than (<)"),
                    QVariant(int(CItemsSizeFilter::SFM_LESSER)));
    cbMode->addItem(tr("Lesser or equals to (<=)"),
                    QVariant(int(CItemsSizeFilter::SFM_LESSEROREQUALS)));
    cbMode->setCurrentIndex(cbMode->findData(QVariant(int(filter->sizeFilterMode()))));
    connect(cbMode, SIGNAL(currentIndexChanged(int)),
            this, SLOT(slotModeCurrentIndexChanged(int)));
    layout->addWidget(cbMode);

    // amount input widget
    QLineEdit* leSizeAmount = new QLineEdit(ret);
    leSizeAmount->setObjectName("leSizeAmount");
    leSizeAmount->setText(QString("%1").arg(filter->sizeFilterAmount()));
    QIntValidator* amountvalidator = new QIntValidator(leSizeAmount);
    amountvalidator->setBottom(0);
    amountvalidator->setTop((unsigned short) - 1);
    leSizeAmount->setValidator(amountvalidator);
    connect(leSizeAmount, SIGNAL(editingFinished()),
            this, SLOT(slotSizeAmountEditingFinished()));
    layout->addWidget(leSizeAmount);

    // amount scale
    QComboBox* cbScale = new QComboBox(ret);
    cbScale->setObjectName("cbScale");
    cbScale->addItem(tr("Byte"),
                     QVariant(int(CItemsSizeFilter::SFS_BYTES)));
    cbScale->addItem(tr("Kilobyte"),
                     QVariant(int(CItemsSizeFilter::SFS_KILOBYTES)));
    cbScale->addItem(tr("Megabyte"),
                     QVariant(int(CItemsSizeFilter::SFS_MEGABYTES)));
    cbScale->setCurrentIndex(cbScale->findData(QVariant(int(filter->sizeFilterScale()))));
    connect(cbScale, SIGNAL(currentIndexChanged(int)),
            this, SLOT(slotScaleCurrentIndexChanged(int)));
    layout->addWidget(cbScale);

    return ret;
}

void CSizeFilterWidget::slotModeCurrentIndexChanged(int index)
{
    QComboBox* cbMode = findChild< QComboBox* >("cbMode");
    Q_ASSERT(cbMode);

    if (index == -1)
        return;

    Q_ASSERT(itemsFilter()->filterType() == CItemsFilter::IFT_SIZE);
    CItemsSizeFilter* filter = static_cast< CItemsSizeFilter* >(itemsFilter());

    CItemsSizeFilter::ESizeFilterMode newmode = (CItemsSizeFilter::ESizeFilterMode) cbMode->itemData(index).toInt();
    if (filter->sizeFilterMode() != newmode)
    {
        filter->setSizeFilterMode(newmode);
        emit filterChanged();
    }
}

void CSizeFilterWidget::slotScaleCurrentIndexChanged(int index)
{
    QComboBox* cbScale = findChild< QComboBox* >("cbScale");
    Q_ASSERT(cbScale);

    if (index == -1)
        return;

    Q_ASSERT(itemsFilter()->filterType() == CItemsFilter::IFT_SIZE);
    CItemsSizeFilter* filter = static_cast< CItemsSizeFilter* >(itemsFilter());

    CItemsSizeFilter::ESizeFilterScale newscale = (CItemsSizeFilter::ESizeFilterScale) cbScale->itemData(index).toInt();
    if (filter->sizeFilterScale() != newscale)
    {
        filter->setSizeFilterScale(newscale);
        emit filterChanged();
    }
}

void CSizeFilterWidget::slotSizeAmountEditingFinished()
{
    QLineEdit* leSizeAmount = findChild< QLineEdit* >("leSizeAmount");
    Q_ASSERT(leSizeAmount);

    Q_ASSERT(itemsFilter()->filterType() == CItemsFilter::IFT_SIZE);
    CItemsSizeFilter* filter = static_cast< CItemsSizeFilter* >(itemsFilter());

    bool convertok = false;
    QString amountstring = leSizeAmount->text().trimmed();
    unsigned short newamount = amountstring.toUShort(&convertok);
    if (convertok && (filter->sizeFilterAmount() != newamount))
    {
        filter->setSizeFilterAmount(newamount);
        emit filterChanged();
    }
}

void CSizeFilterWidget::showEvent(QShowEvent* event)
{
    QWidget::showEvent(event);

    QLineEdit* leSizeAmount = findChild< QLineEdit* >("leSizeAmount");
    Q_ASSERT(leSizeAmount);
    leSizeAmount->setFocus();
    leSizeAmount->selectAll();
}
