#ifndef CFILTERWIDGET_H
#define CFILTERWIDGET_H

#include <QWidget>


namespace Lihat
{
    namespace Thumbnails
    {

        class CItemsFilter;

        class CFilterWidget:
            public QWidget
        {
            Q_OBJECT
        public:
            explicit CFilterWidget(CItemsFilter* filter,  QWidget* parent = 0);
            virtual ~CFilterWidget();

            void                           initUserInterface();
            virtual CItemsFilter* itemsFilter() const;

        protected:
            CItemsFilter* const m_filter;

            virtual QWidget* createFilterWidget() = 0;

        signals:
            void filterDiscarded();
            void filterChanged();

        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CFILTERWIDGET_H
