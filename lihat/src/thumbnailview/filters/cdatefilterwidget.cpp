#include "cdatefilterwidget.h"

#include <QComboBox>
#include <QDateEdit>
#include <QVBoxLayout>
#include <QWidget>


using namespace Lihat;
using namespace Lihat::Thumbnails;

CDateFilterWidget::CDateFilterWidget(CItemsDateFilter* filter, QWidget* parent):
    CFilterWidget(filter, parent)
{
    initUserInterface();
}

CDateFilterWidget::~CDateFilterWidget()
{

}

QWidget* CDateFilterWidget::createFilterWidget()
{
    CItemsDateFilter* filter = static_cast< CItemsDateFilter* >(itemsFilter());
    Q_ASSERT(filter);

    QWidget* ret = new QWidget(this);
    ret->setMinimumWidth(200);

    QVBoxLayout* layout = new QVBoxLayout(ret);
    layout->setMargin(0);
    layout->setSizeConstraint(QLayout::SetMinimumSize);

    // date selection widget
    QComboBox* cbWhich = new QComboBox(ret);
    cbWhich->setObjectName("cbWhich");
    cbWhich->addItem(tr("Created"),
                     QVariant(int(CItemsDateFilter::DFW_CREATION)));
    cbWhich->addItem(tr("Modified"),
                     QVariant(int(CItemsDateFilter::DFW_MODIFICATION)));
    connect(cbWhich, SIGNAL(currentIndexChanged(int)),
            this, SLOT(slotWhichDateCurrentIndexChanged(int)));
    layout->addWidget(cbWhich);

    // comparison widget
    QComboBox* cbMode = new QComboBox(ret);
    cbMode->setObjectName("cbMode");
    cbMode->addItem(tr("On (=)"),
                    QVariant(int(CItemsDateFilter::DFM_ON)));
    cbMode->addItem(tr("Not on (!=)"),
                    QVariant(int(CItemsDateFilter::DFM_NOTON)));
    cbMode->addItem(tr("After (>)"),
                    QVariant(int(CItemsDateFilter::DFM_AFTER)));
    cbMode->addItem(tr("After or on (>=)"),
                    QVariant(int(CItemsDateFilter::DFM_AFTERORON)));
    cbMode->addItem(tr("Before (<)"),
                    QVariant(int(CItemsDateFilter::DFM_BEFORE)));
    cbMode->addItem(tr("Before or on (<=)"),
                    QVariant(int(CItemsDateFilter::DFM_BEFOREORON)));
    connect(cbMode, SIGNAL(currentIndexChanged(int)),
            this, SLOT(slotModeCurrentIndexChanged(int)));
    layout->addWidget(cbMode);

    // date input widget
    QDateEdit* deDate = new QDateEdit(ret);
    deDate->setObjectName("deDate");
    deDate->setCalendarPopup(true);
    deDate->setDate(filter->dateFilterDate());
    deDate->setDisplayFormat("dd MMMM yyyy");
    connect(deDate, SIGNAL(editingFinished()),
            this, SLOT(slotDateEditingFinished()));
    layout->addWidget(deDate);

    return ret;
}

void CDateFilterWidget::slotModeCurrentIndexChanged(int index)
{
    QComboBox* cbMode = findChild< QComboBox* >("cbMode");
    Q_ASSERT(cbMode);

    if (index == -1)
        return;

    Q_ASSERT(itemsFilter()->filterType() == CItemsFilter::IFT_DATE);
    CItemsDateFilter* filter = static_cast< CItemsDateFilter* >(itemsFilter());

    CItemsDateFilter::EDateFilterMode newmode = (CItemsDateFilter::EDateFilterMode) cbMode->itemData(index).toInt();
    if (filter->dateFilterMode() != newmode)
    {
        filter->setDateFilterMode(newmode);
        emit filterChanged();
    }
}

void CDateFilterWidget::slotWhichDateCurrentIndexChanged(int index)
{
    QComboBox* cbWhich = findChild< QComboBox* >("cbWhich");
    Q_ASSERT(cbWhich);

    if (index == -1)
        return;

    Q_ASSERT(itemsFilter()->filterType() == CItemsFilter::IFT_DATE);
    CItemsDateFilter* filter = static_cast< CItemsDateFilter* >(itemsFilter());

    CItemsDateFilter::EDateFilterWhich newwhich = (CItemsDateFilter::EDateFilterWhich) cbWhich->itemData(index).toInt();
    if (filter->dateFilterWhich() != newwhich)
    {
        filter->setDateFilterWhich(newwhich);
        emit filterChanged();
    }
}

void CDateFilterWidget::slotDateEditingFinished()
{
    QDateEdit* deDate = findChild< QDateEdit* >("deDate");
    Q_ASSERT(deDate);

    Q_ASSERT(itemsFilter()->filterType() == CItemsFilter::IFT_DATE);
    CItemsDateFilter* filter = static_cast< CItemsDateFilter* >(itemsFilter());

    QDate newdate = deDate->date();
    if (newdate.isValid() && (filter->dateFilterDate() != newdate))
    {
        filter->setDateFilterDate(newdate);
        emit filterChanged();
    }
}

void CDateFilterWidget::showEvent(QShowEvent* event)
{
    QWidget::showEvent(event);

    QDateEdit* deDate = findChild< QDateEdit* >("deDate");
    Q_ASSERT(deDate);
    deDate->setFocus();
}
