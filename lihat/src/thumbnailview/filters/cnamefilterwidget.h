#ifndef CNAMEFILTERWIDGET_H
#define CNAMEFILTERWIDGET_H

#include "cfilterwidget.h"
#include "citemsnamefilter.h"

class QWidget;

namespace Lihat
{
    namespace Thumbnails
    {

        class CNameFilterWidget:
            public CFilterWidget
        {
            Q_OBJECT
        public:
            explicit CNameFilterWidget(CItemsNameFilter* filter, QWidget* parent = 0);
            virtual ~CNameFilterWidget();

        protected:
            virtual QWidget* createFilterWidget();
            virtual void     showEvent(QShowEvent* event);

        private slots:
            void slotMatchCaseToggled();
            void slotModeCurrentIndexChanged(int index);
            void slotNameFilterEditingFinished();

        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CNAMEFILTERWIDGET_H
