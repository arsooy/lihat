#include "cnamefilterwidget.h"

#include <QCheckBox>
#include <QComboBox>
#include <QFormLayout>
#include <QLineEdit>
#include <QWidget>


using namespace Lihat;
using namespace Lihat::Thumbnails;

CNameFilterWidget::CNameFilterWidget(CItemsNameFilter* filter, QWidget* parent):
    CFilterWidget(filter, parent)
{
    initUserInterface();
}

CNameFilterWidget::~CNameFilterWidget()
{

}

QWidget* CNameFilterWidget::createFilterWidget()
{
    CItemsNameFilter* filter = static_cast< CItemsNameFilter* >(itemsFilter());
    Q_ASSERT(filter);

    QWidget* ret = new QWidget(this);
    ret->setMinimumWidth(200);

    QVBoxLayout* layout = new QVBoxLayout(ret);
    layout->setMargin(0);
    layout->setSizeConstraint(QLayout::SetMinimumSize);

    // mode selection widget
    QComboBox* cbMode = new QComboBox(ret);
    cbMode->setObjectName("cbMode");
    cbMode->addItem(tr("Name contains"),
                    QVariant(int(CItemsNameFilter::NFM_CONTAINS)));
    cbMode->addItem(tr("Name does not contains"),
                    QVariant(int(CItemsNameFilter::NFM_NOTCONTAINS)));
    cbMode->setCurrentIndex(cbMode->findData(QVariant(int(filter->nameFilterMode()))));
    connect(cbMode, SIGNAL(currentIndexChanged(int)),
            this, SLOT(slotModeCurrentIndexChanged(int)));
    layout->addWidget(cbMode);

    // name input widget
    QLineEdit* leNameFilter = new QLineEdit(ret);
    leNameFilter->setObjectName("leNameFilter");
    connect(leNameFilter, SIGNAL(editingFinished()),
            this, SLOT(slotNameFilterEditingFinished()));
    layout->addWidget(leNameFilter);

    // case sensitivity widget
    QCheckBox* ckbMatchCase = new QCheckBox(tr("Match case"), this);
    ckbMatchCase->setObjectName("ckbMatchCase");
    ckbMatchCase->setCheckable(true);
    ckbMatchCase->setChecked(filter->nameFilterIsCaseSensitive());
    connect(ckbMatchCase, SIGNAL(toggled(bool)),
            this, SLOT(slotMatchCaseToggled()));
    layout->addWidget(ckbMatchCase);
    layout->addStretch();

    return ret;
}

void CNameFilterWidget::slotModeCurrentIndexChanged(int index)
{
    QComboBox* cbMode = findChild< QComboBox* >("cbMode");
    Q_ASSERT(cbMode);

    if (index == -1)
        return;

    Q_ASSERT(itemsFilter()->filterType() == CItemsFilter::IFT_NAME);
    CItemsNameFilter* filter = static_cast< CItemsNameFilter* >(itemsFilter());

    CItemsNameFilter::ENameFilterMode newmode = (CItemsNameFilter::ENameFilterMode) cbMode->itemData(index).toInt();
    if (filter->nameFilterMode() != newmode)
    {
        filter->setNameFilterMode(newmode);
        emit filterChanged();
    }
}

void CNameFilterWidget::slotMatchCaseToggled()
{
    QCheckBox* ckbMatchCase = findChild< QCheckBox* >("ckbMatchCase");
    Q_ASSERT(ckbMatchCase);

    Q_ASSERT(itemsFilter()->filterType() == CItemsFilter::IFT_NAME);
    CItemsNameFilter* filter = static_cast< CItemsNameFilter* >(itemsFilter());

    if (filter->nameFilterIsCaseSensitive() != ckbMatchCase->isChecked())
    {
        filter->setNameFilterIsCaseSensitive(ckbMatchCase->isChecked());
        emit filterChanged();
    }
}

void CNameFilterWidget::slotNameFilterEditingFinished()
{
    QLineEdit* leNameFilter = findChild< QLineEdit* >("leNameFilter");
    Q_ASSERT(leNameFilter);

    Q_ASSERT(itemsFilter()->filterType() == CItemsFilter::IFT_NAME);
    CItemsNameFilter* filter = static_cast< CItemsNameFilter* >(itemsFilter());

    if (filter->nameFilterText() != leNameFilter->text())
    {
        filter->setNameFilterText(leNameFilter->text());
        emit filterChanged();
    }
}

void CNameFilterWidget::showEvent(QShowEvent* event)
{
    QWidget::showEvent(event);

    QLineEdit* leNameFilter = findChild< QLineEdit* >("leNameFilter");
    Q_ASSERT(leNameFilter);
    leNameFilter->setFocus();
}
