#ifndef CITEMSSIZEFILTER_H
#define CITEMSSIZEFILTER_H

#include "citemsfilter.h"


namespace Lihat
{
    namespace Thumbnails
    {

        class IItemsProvider;

        class CItemsSizeFilter:
            public CItemsFilter
        {
        public:
            enum ESizeFilterMode
            {
                SFM_EQUALS           = 0,
                SFM_NOTEQUALS        = 1,
                SFM_GREATER          = 2,
                SFM_GREATEROREQUALS  = 3,
                SFM_LESSER           = 4,
                SFM_LESSEROREQUALS   = 5
            };


            enum ESizeFilterScale
            {
                SFS_BYTES      = 0,
                SFS_KILOBYTES  = 1,
                SFS_MEGABYTES  = 2,
                SFS_GIGABYTES  = 3
            };


            explicit CItemsSizeFilter();
            virtual ~CItemsSizeFilter();

            // CThumbnailItemsFilter
            virtual EItemsFilterType filterType() const;
            virtual bool             itemSatisfyFilter(int itemsproviderindex, IItemsProvider* itemsprovider);

            unsigned short   sizeFilterAmount() const;
            ESizeFilterMode  sizeFilterMode() const;
            ESizeFilterScale sizeFilterScale() const;
            void             setSizeFilterAmount(unsigned short amount);
            void             setSizeFilterMode(ESizeFilterMode mode);
            void             setSizeFilterScale(ESizeFilterScale scale);

        private:
            unsigned short   m_sizefilteramount;
            ESizeFilterMode  m_sizefiltermode;
            ESizeFilterScale m_sizefilterscale;
        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CITEMSSIZEFILTER_H
