#ifndef CITEMSFILTER_H
#define CITEMSFILTER_H

namespace Lihat
{
    namespace Thumbnails
    {

        class IItemsProvider;

        class CItemsFilter
        {
        public:
            enum EItemsFilterType
            {
                IFT_INVALID  = 0,
                IFT_NAME     = 1,
                IFT_DATE     = 2,
                IFT_SIZE     = 3
            };

            explicit CItemsFilter()
            {

            }

            virtual ~CItemsFilter()
            {

            }

            /**
            * @brief Item filtering function.
            *
            * @param itemsproviderindex Index to current item to be examined in the ItemsProvider.
            * @param itemsprovider Pointer to an instance that implements IItemsProvider.
            * @return bool  Return true if the item satisfy the filter.
            */
            virtual bool             itemSatisfyFilter(int itemsproviderindex, IItemsProvider* itemsprovider) = 0;
            virtual EItemsFilterType filterType() const
            {
                return IFT_INVALID;
            }
        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CITEMSFILTER_H
