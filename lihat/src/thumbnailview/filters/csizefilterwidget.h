#ifndef CSIZEFILTERWIDGET_H
#define CSIZEFILTERWIDGET_H

#include "cfilterwidget.h"
#include "citemssizefilter.h"


class QWidget;

namespace Lihat
{
    namespace Thumbnails
    {

        class CSizeFilterWidget:
            public CFilterWidget
        {
            Q_OBJECT
        public:
            explicit CSizeFilterWidget(CItemsSizeFilter* filter,  QWidget* parent = 0);
            virtual ~CSizeFilterWidget();

        protected:
            virtual QWidget*  createFilterWidget();
            virtual void      showEvent(QShowEvent* event);

        private slots:
            void slotModeCurrentIndexChanged(int index);
            void slotScaleCurrentIndexChanged(int index);
            void slotSizeAmountEditingFinished();
        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CSIZEFILTERWIDGET_H
