#include "cviewfilterswidget.h"

#include "../cthumbnailview.h"
#include "../cthumbnailviewwidget.h"
#include "../models/cproxymodel.h"
#include "cdatefilterwidget.h"
#include "cfilterwidget.h"
#include "citemsdatefilter.h"
#include "citemsfilter.h"
#include "citemsnamefilter.h"
#include "citemssizefilter.h"
#include "cnamefilterwidget.h"
#include "csizefilterwidget.h"

#include <QAction>
#include <QComboBox>
#include <QList>
#include <QPushButton>
#include <QScrollArea>
#include <QVBoxLayout>


using namespace Lihat;
using namespace Lihat::Thumbnails;

CViewFiltersWidget::CViewFiltersWidget(QWidget* parent):
    QWidget(parent),
    m_filterwidgetscontainer(0),
    m_proxymodel(0)
{
    initActions();

    QVBoxLayout* layout = new QVBoxLayout(this);
    layout->setMargin(0);
    layout->setSpacing(2);

    QHBoxLayout* newfilterlayout = new QHBoxLayout();
    newfilterlayout->setSpacing(4);

    QComboBox* cbFilterType = new QComboBox(this);
    cbFilterType->setObjectName("cbFilterType");
    cbFilterType->addItem(tr("Name"), QVariant(int(CItemsFilter::IFT_NAME)));
    cbFilterType->addItem(tr("Date"), QVariant(int(CItemsFilter::IFT_DATE)));
    cbFilterType->addItem(tr("Size"), QVariant(int(CItemsFilter::IFT_SIZE)));
    newfilterlayout->addWidget(cbFilterType);

    QPushButton* pbNewFilter = new QPushButton(tr("&New filter"), this);
    pbNewFilter->setObjectName("pbNewFilter");
    pbNewFilter->setIcon(QIcon::fromTheme("list-add", QIcon(":lihat/glyph/list-add.png")));
    connect(pbNewFilter, SIGNAL(clicked()),
            this, SLOT(slotNewFilterClicked()));

    newfilterlayout->addWidget(pbNewFilter);
    newfilterlayout->addStretch();
    layout->addLayout(newfilterlayout);

    m_filterwidgetscontainer = new QWidget(this);
    QHBoxLayout* filterslayout = new QHBoxLayout(m_filterwidgetscontainer);
    filterslayout->setMargin(2);
    filterslayout->setSpacing(4);
    filterslayout->setSizeConstraint(QLayout::SetMinimumSize);
    filterslayout->setAlignment(Qt::AlignTop | Qt::AlignLeft);

    QScrollArea* saFilters = new QScrollArea(this);
    saFilters->setObjectName("saFilters");
    saFilters->setBackgroundRole(QPalette::Base);
    saFilters->setWidget(m_filterwidgetscontainer);
    layout->addWidget(saFilters);
}

CViewFiltersWidget::~CViewFiltersWidget()
{

}

CFilterWidget* CViewFiltersWidget::createFilterWidget(CItemsFilter* filter)
{
    CFilterWidget* filterwidget = 0;

    switch (filter->filterType())
    {
        case CItemsFilter::IFT_DATE:
        {
            CItemsDateFilter* ifilter = static_cast< CItemsDateFilter* >(filter);
            if (ifilter)
                filterwidget = new CDateFilterWidget(ifilter, this);

            break;
        }

        case CItemsFilter::IFT_NAME:
        {
            CItemsNameFilter* ifilter = static_cast< CItemsNameFilter* >(filter);
            if (ifilter)
                filterwidget = new CNameFilterWidget(ifilter, this);

            break;
        }

        case CItemsFilter::IFT_SIZE:
        {
            CItemsSizeFilter* ifilter = static_cast< CItemsSizeFilter* >(filter);
            if (ifilter)
                filterwidget = new CSizeFilterWidget(ifilter, this);

            break;
        }

        default:
            filterwidget = 0;
    }

    if (filterwidget)
    {
        connect(filterwidget, SIGNAL(filterChanged()),
                this, SIGNAL(filterChanged()));
        connect(filterwidget, SIGNAL(filterDiscarded()),
                this, SLOT(slotFilterDiscarded()));
    }

    return filterwidget;
}

CFilterWidget* CViewFiltersWidget::getFilterWidget(const CItemsFilter* filter) const
{
    QList< CFilterWidget* > widgets = m_filterwidgetscontainer->findChildren< CFilterWidget* >(QString());
    if (widgets.count() < 1)
        return 0;

    for (int i = 0; i < widgets.count(); ++i)
    {
        CFilterWidget* fwidget = widgets.at(i);
        if (fwidget->itemsFilter() == filter)
            return fwidget;
    }

    return 0;
}

void CViewFiltersWidget::newFilter(CItemsFilter::EItemsFilterType type)
{
    if (!m_proxymodel)
        return;

    CItemsFilter* filter = m_proxymodel->newItemsFilter(type);
    CFilterWidget* filterwidget = getFilterWidget(filter);
    if (filter && filterwidget && filterwidget->isVisible())
        filterwidget->show();
}

void CViewFiltersWidget::slotFilterDiscarded()
{
    CFilterWidget* senderwidget = qobject_cast< CFilterWidget* >(sender());
    if (!senderwidget)
        return;

    if (m_proxymodel)
        m_proxymodel->deleteItemsFilter(senderwidget->itemsFilter());
}

void CViewFiltersWidget::slotNewFilterClicked()
{
    QComboBox* cbFilterType = findChild< QComboBox* >("cbFilterType");
    Q_ASSERT(cbFilterType);

    if (cbFilterType->currentIndex() == -1)
        return;

    CItemsFilter::EItemsFilterType filtertype = CItemsFilter::EItemsFilterType(cbFilterType->itemData(cbFilterType->currentIndex()).toInt());
    newFilter(filtertype);
}

void CViewFiltersWidget::slotProxyModelItemsFilterAdded(CItemsFilter* filter)
{
    CFilterWidget* filterwidget = createFilterWidget(filter);
    if (filterwidget)
        m_filterwidgetscontainer->layout()->addWidget(filterwidget);
}

void CViewFiltersWidget::slotProxyModelItemsFilterDeleted(const CItemsFilter* filter)
{
    QList< CFilterWidget* > widgets = m_filterwidgetscontainer->findChildren< CFilterWidget* >(QString());
    if (widgets.count() < 1)
        return;

    QList< CFilterWidget* >::iterator widgetiter = widgets.begin();
    while (widgetiter != widgets.end())
    {
        CFilterWidget* widget = *widgetiter;
        if (widget->itemsFilter() == filter)
        {
            widget->deleteLater();
            emit filterChanged();

            widgetiter = widgets.erase(widgetiter);
        }
        else
            widgetiter++;
    }
}

void CViewFiltersWidget::slotNewFilterByDateTriggered()
{
    CThumbnailViewWidget* tvwidget = qobject_cast< CThumbnailViewWidget* >(parentWidget());
    if (tvwidget)
    {
        QAction* act = tvwidget->findChild< QAction* >("acThumbnailViewFilters");
        if (act)
            act->setChecked(true);
    }

    newFilter(CItemsFilter::IFT_DATE);
}

void CViewFiltersWidget::slotNewFilterByNameHovered()
{
    CThumbnailViewWidget* tvwidget = qobject_cast< CThumbnailViewWidget* >(parentWidget());
    if (!tvwidget)
        return;

    QAction* act = tvwidget->findChild< QAction* >("acThumbnailViewFilterByName");
    act->setEnabled(tvwidget->isVisible() && tvwidget->thumbnailView() && tvwidget->thumbnailView()->model());
}

void CViewFiltersWidget::slotNewFilterByNameTriggered()
{
    CThumbnailViewWidget* tvwidget = qobject_cast< CThumbnailViewWidget* >(parentWidget());
    if (tvwidget)
    {
        QAction* act = tvwidget->findChild< QAction* >("acThumbnailViewFilters");
        if (act)
            act->setChecked(true);
    }

    newFilter(CItemsFilter::IFT_NAME);
}

void CViewFiltersWidget::slotNewFilterBySizeTriggered()
{
    CThumbnailViewWidget* tvwidget = qobject_cast< CThumbnailViewWidget* >(parentWidget());
    if (tvwidget)
    {
        QAction* act = tvwidget->findChild< QAction* >("acThumbnailViewFilters");
        if (act)
            act->setChecked(true);
    }

    newFilter(CItemsFilter::IFT_SIZE);
}

void CViewFiltersWidget::setProxyModel(CProxyModel* proxymodel)
{
    if (m_proxymodel)
        m_proxymodel->disconnect();

    m_proxymodel = proxymodel;
    connect(m_proxymodel, SIGNAL(itemsFilterAdded(CItemsFilter*)),
            this, SLOT(slotProxyModelItemsFilterAdded(CItemsFilter*)));
    connect(m_proxymodel, SIGNAL(itemsFilterDeleted(const CItemsFilter*)),
            this, SLOT(slotProxyModelItemsFilterDeleted(const CItemsFilter*)));
}

void CViewFiltersWidget::initActions()
{
    QAction* act = new QAction(QObject::tr("Add filter by &name"), this);
    act->setObjectName("acThumbnailViewFilterByName");
    QObject::connect(act, SIGNAL(triggered()),
                     this, SLOT(slotNewFilterByNameTriggered()));
    QObject::connect(act, SIGNAL(hovered()),
                     this, SLOT(slotNewFilterByNameHovered()));

    act = new QAction(QObject::tr("Add filter by &date"), this);
    act->setObjectName("acThumbnailViewFilterByDate");
    QObject::connect(act, SIGNAL(triggered()),
                     this, SLOT(slotNewFilterByDateTriggered()));

    act = new QAction(QObject::tr("Add filter by &size"), this);
    act->setObjectName("acThumbnailViewFilterBySize");
    QObject::connect(act, SIGNAL(triggered()),
                     this, SLOT(slotNewFilterBySizeTriggered()));
}
