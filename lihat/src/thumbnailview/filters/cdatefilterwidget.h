#ifndef CDATEFILTERWIDGET_H
#define CDATEFILTERWIDGET_H

#include "cfilterwidget.h"
#include "citemsdatefilter.h"


class QWidget;

namespace Lihat
{
    namespace Thumbnails
    {

        class CDateFilterWidget:
            public CFilterWidget
        {
            Q_OBJECT
        public:
            explicit CDateFilterWidget(CItemsDateFilter* filter, QWidget* parent = 0);
            virtual ~CDateFilterWidget();

        protected:
            virtual QWidget* createFilterWidget();
            virtual void     showEvent(QShowEvent* event);

        private slots:
            void slotDateEditingFinished();
            void slotModeCurrentIndexChanged(int index);
            void slotWhichDateCurrentIndexChanged(int index);
        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CDATEFILTERWIDGET_H
