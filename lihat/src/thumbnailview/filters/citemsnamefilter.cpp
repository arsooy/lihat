#include "citemsnamefilter.h"

#include "../../pathutils.h"
#include "../iitemsprovider.h"
#include "../sthumbnailitem.h"

#include <QFileInfo>


using namespace Lihat;
using namespace Lihat::Thumbnails;

CItemsNameFilter::CItemsNameFilter():
    CItemsFilter(),
    m_namefiltercasesensitive(false),
    m_namefiltermode(NFM_CONTAINS),
    m_namefiltertext()
{

}

CItemsNameFilter::~CItemsNameFilter()
{

}

CItemsFilter::EItemsFilterType CItemsNameFilter::filterType() const
{
    return CItemsFilter::IFT_NAME;
}

bool CItemsNameFilter::itemSatisfyFilter(int itemsproviderindex, IItemsProvider* itemsprovider)
{
    if (m_namefiltertext.isNull() || m_namefiltertext.isEmpty())
        return true; // no filter specified, let this item through

    QUrl itemurl;
    SThumbnailItem* titem = itemsprovider->thumbnailItem(itemsproviderindex);
    if (titem)
    {
        itemurl = titem->loadURL();
        if (!itemurl.isValid())
            return false;
    }
    Q_ASSERT(titem);

    switch (itemsprovider->thumbnailItemsProviderType())
    {
        case IItemsProvider::PT_FILESYSTEM:
        {
            QFileInfo finfo(itemurl.toLocalFile());
            QString name = finfo.fileName();
            QString filter = m_namefiltertext;

            switch (m_namefiltermode)
            {
                case NFM_CONTAINS:
                case NFM_NOTCONTAINS:
                {
                    Qt::CaseSensitivity sensitivity = m_namefiltercasesensitive ? Qt::CaseSensitive : Qt::CaseInsensitive;
                    bool iscontains = name.contains(filter, sensitivity);
                    return (m_namefiltermode == NFM_CONTAINS) ? iscontains : !iscontains;

                    break;
                }

                default:
                    return true;
            }

            break;
        }

        default:
            return false;
    }

    return false;
}

bool CItemsNameFilter::nameFilterIsCaseSensitive() const
{
    return m_namefiltercasesensitive;
}

CItemsNameFilter::ENameFilterMode CItemsNameFilter::nameFilterMode() const
{
    return m_namefiltermode;
}

QString CItemsNameFilter::nameFilterText() const
{
    return m_namefiltertext;
}

void CItemsNameFilter::setNameFilterIsCaseSensitive(const bool casesensitive)
{
    m_namefiltercasesensitive = casesensitive;
}

void CItemsNameFilter::setNameFilterMode(const CItemsNameFilter::ENameFilterMode ftype)
{
    m_namefiltermode = ftype;
}

void CItemsNameFilter::setNameFilterText(const QString& text)
{
    m_namefiltertext = text;
}
