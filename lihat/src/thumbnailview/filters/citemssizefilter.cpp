#include "citemssizefilter.h"

#include "../iitemsprovider.h"
#include "../models/cdirectorymodel.h"
#include "../sthumbnailitem.h"

#include <QString>
#include <QFileInfo>


using namespace Lihat;
using namespace Lihat::Thumbnails;

CItemsSizeFilter::CItemsSizeFilter():
    CItemsFilter(),
    m_sizefilteramount(0),
    m_sizefiltermode(SFM_EQUALS),
    m_sizefilterscale(SFS_KILOBYTES)
{

}

CItemsSizeFilter::~CItemsSizeFilter()
{

}

CItemsFilter::EItemsFilterType CItemsSizeFilter::filterType() const
{
    return CItemsFilter::IFT_SIZE;
}

bool CItemsSizeFilter::itemSatisfyFilter(int itemsproviderindex, IItemsProvider* itemsprovider)
{
    QUrl itemurl;
    SThumbnailItem* titem = itemsprovider->thumbnailItem(itemsproviderindex);
    if (titem)
    {
        itemurl = titem->loadURL();
        if (!itemurl.isValid())
            return false;

        if (!itemurl.isLocalFile())
            return false;
    }
    Q_ASSERT(titem);

    qint64 filtersize = 0;
    qint64 itemsize = 0;

    switch (m_sizefilterscale)
    {
        case SFS_KILOBYTES:
        {
            filtersize = m_sizefilteramount * 1024;
            break;
        }

        case SFS_MEGABYTES:
        {
            filtersize = m_sizefilteramount * 1024 * 1024;
            break;
        }

        case SFS_GIGABYTES:
        {
            filtersize = m_sizefilteramount * 1024 * 1024 * 1024;
            break;
        }

        default:
            filtersize = m_sizefilteramount;
    }

    switch (itemsprovider->thumbnailItemsProviderType())
    {
        case IItemsProvider::PT_FILESYSTEM:
        {
            CDirectoryModel::ItemData* itemdata = (CDirectoryModel::ItemData*) titem->ItemsProviderData;
            // split to two lines for readibility sake
            CDirectoryModel::loadModelItemData(titem->loadURL(), &itemdata);
            titem->ItemsProviderData = itemdata;

            itemsize = itemdata->Size;

            break;
        }

        default:
            return false;
    }

    switch (m_sizefiltermode)
    {
        case SFM_EQUALS:
            return itemsize == filtersize;
        case SFM_GREATER:
            return itemsize > filtersize;
        case SFM_GREATEROREQUALS:
            return itemsize >= filtersize;
        case SFM_LESSER:
            return itemsize < filtersize;
        case SFM_LESSEROREQUALS:
            return itemsize <= filtersize;
        case SFM_NOTEQUALS:
            return itemsize != filtersize;
        default:
            return false;
    }

    return false;
}

unsigned short CItemsSizeFilter::sizeFilterAmount() const
{
    return m_sizefilteramount;
}

CItemsSizeFilter::ESizeFilterMode CItemsSizeFilter::sizeFilterMode() const
{
    return m_sizefiltermode;
}

CItemsSizeFilter::ESizeFilterScale CItemsSizeFilter::sizeFilterScale() const
{
    return m_sizefilterscale;
}

void CItemsSizeFilter::setSizeFilterAmount(unsigned short amount)
{
    m_sizefilteramount = amount;
}

void CItemsSizeFilter::setSizeFilterMode(CItemsSizeFilter::ESizeFilterMode mode)
{
    m_sizefiltermode = mode;
}

void CItemsSizeFilter::setSizeFilterScale(CItemsSizeFilter::ESizeFilterScale scale)
{
    m_sizefilterscale = scale;
}
