#ifndef CITEMSDATEFILTER_H
#define CITEMSDATEFILTER_H

#include "citemsfilter.h"

#include <QDate>


namespace Lihat
{
    namespace Thumbnails
    {

        class CItemsDateFilter:
            public CItemsFilter
        {
        public:
            enum EDateFilterMode
            {
                DFM_ON          = 0,
                DFM_NOTON       = 1,
                DFM_AFTER       = 2,
                DFM_AFTERORON   = 3,
                DFM_BEFORE      = 4,
                DFM_BEFOREORON  = 5
            };


            enum EDateFilterWhich
            {
                DFW_CREATION      = 0,
                DFW_MODIFICATION  = 1,
                DFW_ACCESS        = 2
            };


            explicit CItemsDateFilter();
            virtual ~CItemsDateFilter();

            // CItemsFilter
            virtual EItemsFilterType filterType() const;
            virtual bool             itemSatisfyFilter(int itemsproviderindex, IItemsProvider* itemsprovider);

            QDate            dateFilterDate() const;
            EDateFilterMode  dateFilterMode() const;
            EDateFilterWhich dateFilterWhich() const;
            void             setDateFilterDate(const QDate& date);
            void             setDateFilterMode(const EDateFilterMode mode);
            void             setDateFilterWhich(const EDateFilterWhich which);

        private:
            QDate            m_datefilterdate;
            EDateFilterMode  m_datefiltermode;
            EDateFilterWhich m_datefilterwhich;

        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CITEMSDATEFILTER_H
