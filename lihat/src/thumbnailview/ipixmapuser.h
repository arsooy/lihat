#ifndef IPIXMAPUSER_H
#define IPIXMAPUSER_H

namespace Lihat
{
    namespace Thumbnails
    {

        class IPixmapProvider;

        class IPixmapUser
        {
        public:
            virtual ~IPixmapUser()
            {

            }

            virtual bool setThumbnailViewPixmapProvider(IPixmapProvider* provider) = 0;
        };

    } // namespace Thumbnails
} // namespace Lihat


#endif // IPIXMAPUSER_H
