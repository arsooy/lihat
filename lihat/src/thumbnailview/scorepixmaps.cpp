#include "scorepixmaps.h"

#include <QPixmap>


using namespace Lihat;
using namespace Lihat::Thumbnails;

SCorePixmaps::SCorePixmaps():
    File(0),
    Folder(0),
    NoThumbnail(0)
{

}

SCorePixmaps::~SCorePixmaps()
{
    clear();
}

void SCorePixmaps::clear()
{
    if (File)
        delete File;
    File = 0;

    if (Folder)
        delete Folder;
    Folder = 0;

    if (NoThumbnail)
        delete NoThumbnail;
    NoThumbnail = 0;
}
