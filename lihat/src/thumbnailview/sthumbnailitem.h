#ifndef STHUMBNAILITEM_H
#define STHUMBNAILITEM_H

#include <QString>
#include <QUrl>
#include <QSize>


class QPixmap;

namespace Lihat
{
    namespace Thumbnails
    {

        class IItemsProvider;

        struct SThumbnailItem
        {

            enum EItemType
            {
                TI_INVALID  = 0,
                TI_FOLDER   = 1,
                TI_FILE     = 2
            };


            enum EItemLoadStatus
            {
                ILS_INVALID       =  0,
                ILS_NOTAVAILABLE  =  1,
                ILS_PENDING       =  2,
                ILS_LOADED        = 99
            };


            int       Identifier;
            EItemType Type;
            QString   Caption;
            void*     ItemsProviderData;
            QSize     FullSize;
            IItemsProvider* const Provider;

            explicit SThumbnailItem(IItemsProvider* provider);
            virtual ~SThumbnailItem();

            bool isValid() const;
            QUrl loadURL() const;
        };


        struct SThumbnailLocalData
        {
            unsigned char LoadStatus;
            QPixmap*      Pixmap;

            explicit SThumbnailLocalData();
            virtual ~SThumbnailLocalData();

            void  deletePixmap();
        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // STHUMBNAILITEM_H
