#include "cstatusbarbuttons.h"

#include "cthumbnailviewwidget.h"

#include <QAction>
#include <QHBoxLayout>
#include <QMenu>
#include <QToolButton>


using namespace Lihat;
using namespace Lihat::Thumbnails;

CStatusBarButtons::CStatusBarButtons(CThumbnailViewWidget* thumbnailviewwidget, QWidget* parent):
    QWidget(parent)
{
    Q_ASSERT(thumbnailviewwidget);
    setContentsMargins(0, 0, 0, 0);

    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setSpacing(0);
    layout->setMargin(0);

    QAction* acThumbnailViewFolders = getActionOf(thumbnailviewwidget, "acThumbnailViewFolders");
    {
        QToolButton* tbtnFolders = new QToolButton(this);
        tbtnFolders->setObjectName("tbtnFolders");
        tbtnFolders->setDefaultAction(acThumbnailViewFolders);
        tbtnFolders->setAutoRaise(true);
        tbtnFolders->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        tbtnFolders->setCheckable(acThumbnailViewFolders->isCheckable());

        layout->addWidget(tbtnFolders);
    }

    QAction* acThumbnailViewFilters = getActionOf(thumbnailviewwidget, "acThumbnailViewFilters");
    {
        QToolButton* tbtnFilters = new QToolButton(this);
        tbtnFilters->setObjectName("tbtnFilters");
        tbtnFilters->setAutoRaise(true);
        tbtnFilters->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        tbtnFilters->setIcon(acThumbnailViewFilters->icon());
        tbtnFilters->setText(acThumbnailViewFilters->text());
        tbtnFilters->setCheckable(acThumbnailViewFilters->isCheckable());
        connect(tbtnFilters, SIGNAL(clicked(bool)),
                acThumbnailViewFilters, SLOT(setChecked(bool)));
        connect(acThumbnailViewFilters, SIGNAL(toggled(bool)),
                tbtnFilters, SLOT(setChecked(bool)));

        QMenu* menu = new QMenu(tbtnFilters);
        menu->addAction(getActionOf(thumbnailviewwidget, "acThumbnailViewFilterByName"));
        menu->addAction(getActionOf(thumbnailviewwidget, "acThumbnailViewFilterByDate"));
        menu->addAction(getActionOf(thumbnailviewwidget, "acThumbnailViewFilterBySize"));
        tbtnFilters->setMenu(menu);

        layout->addWidget(tbtnFilters);
    }
}

CStatusBarButtons::~CStatusBarButtons()
{

}

QAction* CStatusBarButtons::getActionOf(QWidget* what, const QString& name, bool mustexists)
{
    Q_ASSERT(what);

    QAction* result = what->findChild< QAction* >(name);
    if (mustexists)
        Q_ASSERT(result);

    return result;
}
