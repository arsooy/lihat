#include "cthumbnailviewwidget.h"

#include "../cmainwindow.h"
#include "../imageload/cbaseloadjob.h"
#include "../imageload/cimageloadresult.h"
#include "../imageload/imageload.h"
#include "../fileutils.h"
#include "../imageutils.h"
#include "../pathutils.h"
#include "../persistenceutils.h"
#include "cfolderstreeview.h"
#include "citemdelegate.h"
#include "cstatusbarbuttons.h"
#include "cthumbloadjob.h"
#include "cthumbnailview.h"
#include "filters/cviewfilterswidget.h"
#include "models/cbasethumbnailmodel.h"
#include "models/cdirectorymodel.h"
#include "models/cfilesystemmodel.h"
#include "models/cproxymodel.h"
#include "sthumbnailitem.h"

#include <QAction>
#include <QComboBox>
#include <QFileInfo>
#include <QLabel>
#include <QLineEdit>
#include <QSplitter>
#include <QStatusBar>
#include <QStringList>
#include <QToolBar>
#include <QToolButton>
#include <QVBoxLayout>
#include <QtConcurrentMap>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::Thumbnails;


CThumbnailViewWidget::CThumbnailViewWidget(QWidget* parent) :
    QWidget(parent),
    m_canmodifylocationshistory(true),
    m_filterswidget(new CViewFiltersWidget(this)),
    m_folderstreeView(new CFoldersTreeView(this)),
    m_ignorelocationcurrentindexchanged(false),
    m_locationpos(0),
    m_locationshistorymax(50),
    m_model(new CDirectoryModel(this)),
    m_processloadjobresult(false),
    m_thumbnailview(new CThumbnailView(this)),
    m_waitingthumbnailloadthreadtostart(false)
{
    readPersistence();
    initActions();

    // create right container widgets
    QVBoxLayout* layout = new QVBoxLayout(this);
    layout->setMargin(0);
    layout->setSpacing(0);

    // thumbnail toolbar
    QToolBar* locationtb = new QToolBar(QObject::tr("Location"), this);
    locationtb->setObjectName("tbLocation");
    locationtb->setContentsMargins(0, 0, 0, 0);
    // 'Back' button
    locationtb->addAction(getAction("acThumbnailViewBack"));
    // 'Forward' button
    locationtb->addAction(getAction("acThumbnailViewForward"));
    // 'Up' button
    locationtb->addAction(getAction("acThumbnailViewGoUp"));
    // location combobox
    QComboBox* locationcb = new QComboBox(this);
    locationcb->setObjectName("cbLocation");
    locationcb->setEditable(true);
    locationcb->setDuplicatesEnabled(false);
    locationcb->setSizeAdjustPolicy(QComboBox::AdjustToMinimumContentsLength);
    locationcb->setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
    QObject::connect(locationcb, SIGNAL(currentIndexChanged(int)),
                     this, SLOT(slotLocationCurrentIndexChanged(int)));
    QObject::connect(locationcb->lineEdit(), SIGNAL(returnPressed()),
                     this, SLOT(slotLocationReturnPressed()));
    locationtb->addWidget(locationcb);
    populateLocationsHistory();
    // add 'Go' button
    locationtb->addAction(getAction("acThumbnailViewGo"));
    layout->addWidget(locationtb);

    // folders treeview
    m_folderstreeView->hide();

    // thumbnail listview
    m_thumbnailview->setObjectName("tvThumbnailView");
    m_thumbnailview->setThumbnailViewModel(qobject_cast< QAbstractItemModel* >(m_model));
    // by now the model return by ThumbnailView should be a proxy model to m_model
    CProxyModel* thumbnailviewmodel = qobject_cast< CProxyModel* >(m_thumbnailview->model());
    switch (thumbnailviewmodel->sortedBy())
    {
        case CProxyModel::ISB_DATE:
        {
            getAction("acThumbnailViewSortByDate")->setChecked(true);

            break;
        }

        case CProxyModel::ISB_SIZE:
        {
            getAction("acThumbnailViewSortBySize")->setChecked(true);

            break;
        }

        default:
            getAction("acThumbnailViewSortByName")->setChecked(true);
    }
    getAction("acThumbnailViewSortOrderAscending")->setChecked(thumbnailviewmodel->sortOrder() == Qt::AscendingOrder);
    CItemDelegate* idel = qobject_cast< CItemDelegate* >(m_thumbnailview->itemDelegate());
    if (idel)
    {
        getAction("acThumbnailViewShowDimension")->setChecked(idel->isDrawingImageDimension());
        getAction("acThumbnailViewShowSize")->setChecked(idel->isDrawingFileSize());
    }
    // add it to shared
    addThumbnailView(m_thumbnailview);

    // filters widget
    m_filterswidget->hide();
    m_filterswidget->setMinimumHeight(150);
    m_filterswidget->setProxyModel(thumbnailviewmodel);
    // use QAction to make 'Esc' to hide FiltersWidget
    QAction* act = new QAction(m_filterswidget);
    act->setObjectName("acFiltersWidgetEscape");
    act->setShortcut(Qt::Key_Escape);
    act->setShortcutContext(Qt::WidgetWithChildrenShortcut);
    m_filterswidget->addAction(act);
    QObject::connect(act, SIGNAL(triggered()),
                     this, SLOT(slotFiltersWidgetEscapePressed()));

    // splitter for some pop up widget below the thumbnails
    QSplitter* vsplitter = new QSplitter(Qt::Vertical, this);
    vsplitter->setObjectName("spBelowThumbnailsWidgetSplitter");
    vsplitter->addWidget(m_thumbnailview);
    vsplitter->addWidget(m_filterswidget);
    vsplitter->setStretchFactor(0, 1);

    // a left-right splitter
    QSplitter* leftrightsplitter = new QSplitter(Qt::Horizontal, this);
    leftrightsplitter->setObjectName("spLeftRightSplitter");

    leftrightsplitter->addWidget(m_folderstreeView);
    leftrightsplitter->addWidget(vsplitter);
    leftrightsplitter->setStretchFactor(1, 1);
    layout->addWidget(leftrightsplitter, 1);

    QObject::connect(m_thumbnailview, SIGNAL(goToCurrentParentPathRequested()),
                     this, SLOT(slotGoUpRequested()));
    QObject::connect(m_thumbnailview, SIGNAL(itemExecuted(QModelIndex)),
                     this, SLOT(slotThumbnailViewItemExecuted(QModelIndex)));
    QObject::connect(m_thumbnailview, SIGNAL(itemsSelected(QModelIndexList)),
                     this, SLOT(slotThumbnailViewItemsSelected(QModelIndexList)));
    QObject::connect(m_thumbnailview, SIGNAL(updateFinished()),
                     this, SLOT(slotThumbnailViewUpdateFinished()));
    QObject::connect(&m_delayedfolderstocurrentindextimer, SIGNAL(timeout()),
                     this, SLOT(slotDelayedFoldersTreeViewScrollToCurrentIndexTimeout()));
    QObject::connect(m_thumbnailview->selectionModel(), SIGNAL(currentChanged(QModelIndex, QModelIndex)),
                     this, SLOT(slotThumbnailViewCurrentItemChanged(QModelIndex, QModelIndex)));
    QObject::connect(m_folderstreeView, SIGNAL(currentIndexChanged(QModelIndex, QModelIndex)),
                     this, SLOT(slotFoldersTreeviewCurrentIndexChanged(QModelIndex, QModelIndex)));
    QObject::connect(m_filterswidget, SIGNAL(filterChanged()),
                     this, SLOT(slotFiltersWidgetFilterChanged()));
    QObject::connect(&m_thumbloadfuturewatcher, SIGNAL(resultReadyAt(int)),
                     this, SLOT(slotLoadWatcherResultReadyAt(int)));
    QObject::connect(&m_thumbloadfuturewatcher, SIGNAL(started()),
                     this, SLOT(slotLoadWatcherStarted()));
}

CThumbnailViewWidget::~CThumbnailViewWidget()
{
    cleanupThumbnailViews();
    writePersistence();
}

void CThumbnailViewWidget::cleanupThumbnailViews()
{
    for (int i = 0; i < m_managedviews.count(); ++i)
    {
        CBaseThumbnailView* tv = m_managedviews.value(i, 0);
        clearImageLoadQueueJobs(tv);
    }
}

QUrl CThumbnailViewWidget::currentThumbnailURL() const
{
    CBaseThumbnailView* view = m_managedviews.first();

    QModelIndex currentindex = view->currentIndex();
    if (currentindex.isValid())
    {
        switch (m_model->thumbnailItemsProviderType())
        {
            case IItemsProvider::PT_FILESYSTEM:
            {
                SThumbnailItem* titem = view->thumbnailItemAt(currentindex);
                if (titem)
                    return titem->loadURL();
            }

            default:
                ;
        }
    }

    return QUrl();
}

QUrl CThumbnailViewWidget::currentThumbnailParentURL() const
{
    QUrl current = currentThumbnailURL();
    QString parent;
    if (current.isValid() & (Utils::Path::parentOfPath(current, parent)))
        return QUrl::fromEncoded(parent.toLocal8Bit());

    return QUrl();
}

CViewFiltersWidget* CThumbnailViewWidget::filtersWidget() const
{
    return m_filterswidget;
}

void CThumbnailViewWidget::focusInEvent(QFocusEvent* event)
{
    if (m_thumbnailview)
    {
        Q_ASSERT(m_managedviews.contains(m_thumbnailview));
        m_thumbnailview->setFocus();
    }
    else
        QWidget::focusInEvent(event);
}

void CThumbnailViewWidget::slotFoldersHovered()
{
    QAction* act = getAction("acThumbnailViewFolders");
    act->setEnabled(isVisible() && m_model &&
                    m_model->thumbnailItemsProviderType() == IItemsProvider::PT_FILESYSTEM);
}

void CThumbnailViewWidget::slotFoldersToggled(bool visible)
{
    m_folderstreeView->setVisible(visible);

    if (visible)
    {
        dataModelRequired();
        switch (m_model->thumbnailItemsProviderType())
        {
            case IItemsProvider::PT_FILESYSTEM:
            {
                CDirectoryModel* fsmodel = qobject_cast< CDirectoryModel* >(m_model);
                updateFoldersTreeviewSelection(fsmodel->path());

                break;
            }

            default:
                ;
        }

        installDelayedFoldersTreeViewScrollToCurrentIndex();
    }
}

bool CThumbnailViewWidget::goToCurrentParent()
{
    dataModelRequired();

    switch (m_model->thumbnailItemsProviderType())
    {
        case IItemsProvider::PT_FILESYSTEM:
        {
            // get current and its parent path
            QUrl lasturl = modelURL();
            QString parentpath;
            if (lasturl.isValid() && Utils::Path::parentOfPath(lasturl.toLocalFile(), parentpath))
            {
                Q_ASSERT(!parentpath.isNull());
                if (goToPath(QUrl::fromLocalFile(parentpath), false))
                {
                    // when going up, select last directory as current index
                    m_thumbnailview->setCurrentThumbnail(lasturl, true);

                    return true;
                }
            }

            break;
        }

        default:
            ;
    }

    return false;
}

bool CThumbnailViewWidget::goToPath(const QUrl& url, bool autoselectfirstitem)
{
    dataModelRequired();

    bool result = false;
    QMutexLocker locker(m_thumbnailview->mutex());

    if (m_canmodifylocationshistory)
        m_locationpos = m_locationshistory.count();

    switch (m_model->thumbnailItemsProviderType())
    {
        case IItemsProvider::PT_FILESYSTEM:
        {
            CDirectoryModel* fsmodel = qobject_cast< CDirectoryModel* >(m_model);
            if (url.isLocalFile())
            {
                QString localfileurl = url.toLocalFile();
                if (fsmodel->setPath(localfileurl))
                {
                    if (autoselectfirstitem)
                        m_thumbnailview->setCurrentThumbnail(0, true);

                    itemsProviderPathUpdated();
                    result = true;
                }
            }

            break;
        }

        default:
            ;
    }

    return result;
}

bool CThumbnailViewWidget::hasNextThumbnail()
{
    QModelIndex currentindex = m_thumbnailview->currentIndex();
    if (currentindex.isValid())
    {
        int row = m_thumbnailview->currentIndex().row();
        return row < m_thumbnailview->model()->rowCount() - 1;
    }

    return false;
}

bool CThumbnailViewWidget::hasPreviousThumbnail()
{
    QModelIndex currentindex = m_thumbnailview->currentIndex();
    if (currentindex.isValid())
    {
        int row = m_thumbnailview->currentIndex().row();
        return row > 0;
    }

    return false;
}

void CThumbnailViewWidget::hideEvent(QHideEvent* event)
{
    QWidget::hideEvent(event);

    CMainWindow* mainwindow = getMainWindow();
    if (mainwindow)
    {
        Q_ASSERT(mainwindow->isAncestorOf(this));
        QStatusBar* mainwindowstatusbar = mainwindow->statusBar();
        if (mainwindowstatusbar)
            detachFromMainWindowStatusBar(mainwindowstatusbar);
    }
}

void CThumbnailViewWidget::itemsProviderPathUpdated()
{
    dataModelRequired();

    QUrl pathurl;
    switch (m_model->thumbnailItemsProviderType())
    {
        case IItemsProvider::PT_FILESYSTEM:
        {
            CDirectoryModel* fsmodel = qobject_cast< CDirectoryModel* >(m_model);

            QComboBox* locationcb = findChild< QComboBox* >("cbLocation");
            Q_ASSERT(locationcb);

            // get path to be shown to user
            QFileInfo finfo(fsmodel->path());
            if (finfo.isDir() && finfo.isReadable())
            {
                QString nicepath = Utils::Path::toClean(finfo.absoluteFilePath());
                pathurl = QUrl::fromLocalFile(nicepath);

                m_ignorelocationcurrentindexchanged = true;
                int locationidx = addToLocationsHistory(pathurl);
                locationcb->setCurrentIndex(locationidx);
                m_ignorelocationcurrentindexchanged = false;

                updateFoldersTreeviewSelection(pathurl.toLocalFile());
                emit pathChanged(pathurl);
            }

            break;
        }

        default:
            ;
    }

    bool athead = m_locationshistory.isEmpty() || (m_locationpos >= m_locationshistory.count() - 1);
    if (pathurl.isValid() && athead && m_canmodifylocationshistory)
    {
        while (!m_locationshistory.isEmpty() && (m_locationshistory.count() >= m_locationshistorymax))
            m_locationshistory.removeFirst();
        m_locationshistory.append(pathurl);
    }

    updateBackForwardButton();
    updateStatusTexts();
}

void CThumbnailViewWidget::readPersistence()
{

}

void CThumbnailViewWidget::writePersistence()
{
    QComboBox* cbLocation = findChild< QComboBox* >("cbLocation");
    if (cbLocation)
    {
        QStringList urls;
        for (int i = 0; i < cbLocation->count(); ++i)
        {
            QUrl loc = cbLocation->itemData(i).toUrl();
            if (!loc.isEmpty())
                urls << loc.toString();
        }
        Utils::Persistence::setStorage("CThumbnailViewWidget/LocationsHistory", QVariant(urls));
    }
}

int CThumbnailViewWidget::addToLocationsHistory(const QUrl& url, bool deserialize)
{
    QComboBox* locationcb = findChild< QComboBox* >("cbLocation");
    if (! locationcb)
        return -1;

    int locationidx = locationcb->findData(url);
    if (locationidx == -1)
    {
        // location not found in history, make room for another item (if neccessary)
        while (locationcb->count() >= m_locationshistorymax)
            locationcb->removeItem(locationcb->count() - 1);
    }
    else if (m_locationpos >= m_locationshistory.count() - 1)
    {
        // remove this item from history because we are going to re-add it
        locationcb->removeItem(locationidx);
    }

    if (deserialize)
    {
        locationcb->addItem(url.toLocalFile(), url);
        return locationcb->count() - 1;
    }
    else
    {
        if (m_locationpos >= m_locationshistory.count() - 1)
        {
            // put new item at the top, and return the new item index
            locationcb->insertItem(0, url.toLocalFile(), url);
            return 0;
        }
        else
            return locationcb->findData(url);
    }

    return -1;
}

void CThumbnailViewWidget::populateLocationsHistory()
{
    m_ignorelocationcurrentindexchanged = true;
    QStringList locations = Utils::Persistence::getStorage("CThumbnailViewWidget/LocationsHistory", QVariant(QStringList())).toStringList();
    locations.removeDuplicates();
    for (int i = 0; i < locations.count(); ++i)
    {
        QUrl url = QUrl(locations.at(i));
        if (url.isValid())
        {
            // add new item to locations combo box
            addToLocationsHistory(url, true);
        }
    }
    m_ignorelocationcurrentindexchanged = false;
}

void CThumbnailViewWidget::attachToMainWindowStatusBar(QStatusBar* statusbar)
{
    CStatusBarButtons* buttons = statusbar->findChild< CStatusBarButtons* >("tvsbButtons");
    if (!buttons)
    {
        buttons = new CStatusBarButtons(this, statusbar);
        buttons->setObjectName("tvsbButtons");
        statusbar->insertPermanentWidget(0, buttons);
    }
    else
        buttons->show();

    QLabel* lbMessages = statusbar->findChild< QLabel* >("tvLabelMessages");
    if (!lbMessages)
    {
        lbMessages = new QLabel(statusbar);
        lbMessages->setObjectName("tvLabelMessages");
        statusbar->insertPermanentWidget(1, lbMessages, 1);
    }
    else
        lbMessages->show();

    QLabel* lbSelects = statusbar->findChild< QLabel* >("tvLabelSelects");
    if (!lbSelects)
    {
        lbSelects = new QLabel(statusbar);
        lbSelects->setObjectName("tvLabelSelects");
        statusbar->addPermanentWidget(lbSelects);
    }
    else
        lbSelects->show();

    updateFiltersButtonText();
    updateStatusTexts();
}

void CThumbnailViewWidget::detachFromMainWindowStatusBar(QStatusBar* statusbar)
{
    CStatusBarButtons* buttons = statusbar->findChild< CStatusBarButtons* >("tvsbButtons");
    if (buttons)
    {
        statusbar->removeWidget(buttons);
        delete buttons;
    }

    QLabel* lbMessages = statusbar->findChild< QLabel* >("tvLabelMessages");
    if (lbMessages)
    {
        statusbar->removeWidget(lbMessages);
        delete lbMessages;
    }

    QLabel* lbSelects = statusbar->findChild< QLabel* >("tvLabelSelects");
    if (lbSelects)
    {
        statusbar->removeWidget(lbSelects);
        delete lbSelects;
    }
}

bool CThumbnailViewWidget::setStatusText(const QString& stext, int index)
{
    CMainWindow* mainwindow = getMainWindow();
    if (!mainwindow)
        return false;
    if (!mainwindow->isAncestorOf(this))
        return false;

    QStatusBar* statusbar = mainwindow->statusBar();
    if (!statusbar)
        return false;

    if (index == 0)
    {
        QLabel* lbMessages = statusbar->findChild< QLabel* >("tvLabelMessages");
        if (lbMessages)
        {
            lbMessages->setText(stext);
            lbMessages->setToolTip(stext);
        }
        return true;
    }
    else if (index == 1)
    {
        QLabel* lbSelects = statusbar->findChild< QLabel* >("tvLabelSelects");
        if (lbSelects)
            lbSelects->setText(stext);
        return true;
    }

    return false;
}

bool CThumbnailViewWidget::updateStatusTexts()
{
    CProxyModel* viewmodel = qobject_cast< CProxyModel* >(m_thumbnailview->model());
    Q_ASSERT(viewmodel);

    // update current item
    QModelIndex currentindex = m_thumbnailview->currentIndex();
    if (currentindex.isValid())
    {
        const SThumbnailItem* titem = m_thumbnailview->thumbnailItemAt(currentindex);
        if (titem)
            setStatusText(QObject::tr("Current item is '%1'").arg(titem->Caption));
    }
    else
        setStatusText("");

    // update selection count
    QModelIndexList selectedindexes = m_thumbnailview->selectionModel()->selectedIndexes();
    int visiblecount = viewmodel->rowCount();
    int selcount = selectedindexes.count();

    QString selectioncounttext = "";
    if (visiblecount > 0)
    {
        QString allnum;
        QString selnum;

        if (selcount == 0)
            selectioncounttext = QObject::tr("Showing %1 items").arg(visiblecount);
        else if ((selcount == 1) && (visiblecount == 1))
            selectioncounttext = QObject::tr("Selecting %1 out of %2 item").arg(QString::number(selcount), QString::number(visiblecount));
        else if ((selcount == 1) && (visiblecount > 1))
            selectioncounttext = QObject::tr("Selecting %1 out of %2 items").arg(QString::number(selcount), QString::number(visiblecount));
        else if ((selcount > 1) && (visiblecount > 1))
            selectioncounttext = QObject::tr("Selecting %1 out of %2 items").arg(QString::number(selcount), QString::number(visiblecount));
    }
    setStatusText(selectioncounttext, 1);

    return true;
}

void CThumbnailViewWidget::initActions()
{
    QAction* act;
    act = new QAction(QObject::tr("&Go"), this);
    act->setObjectName("acThumbnailViewGo");
    act->setIcon(QIcon::fromTheme("go-jump", this->style()->standardIcon(QStyle::SP_DialogOkButton)));
    QObject::connect(act, SIGNAL(triggered()),
                     this, SLOT(slotGoTriggered()));

    act = new QAction(QObject::tr("&Back"), this);
    act->setObjectName("acThumbnailViewBack");
    act->setIcon(style()->standardIcon(QStyle::SP_ArrowBack));
    QObject::connect(act, SIGNAL(triggered()),
                     this, SLOT(slotBackTriggered()));
    QObject::connect(act, SIGNAL(hovered()),
                     this, SLOT(slotBackHovered()));

    act = new QAction(QObject::tr("&Forward"), this);
    act->setObjectName("acThumbnailViewForward");
    act->setIcon(style()->standardIcon(QStyle::SP_ArrowForward));
    QObject::connect(act, SIGNAL(triggered()),
                     this, SLOT(slotForwardTriggered()));
    QObject::connect(act, SIGNAL(hovered()),
                     this, SLOT(slotForwardHovered()));

    act = new QAction(QObject::tr("&Up"), this);
    act->setObjectName("acThumbnailViewGoUp");
    act->setIcon(style()->standardIcon(QStyle::SP_ArrowUp));
    act->setShortcut(QKeySequence(Qt::ALT + Qt::Key_Up));
    QObject::connect(act, SIGNAL(triggered()),
                     this, SLOT(slotGoUpRequested()));
    QObject::connect(act, SIGNAL(hovered()),
                     this, SLOT(slotGoUpHovered()));

    act = new QAction(QObject::tr("Fo&lders"), this);
    act->setObjectName("acThumbnailViewFolders");
    act->setIcon(QIcon::fromTheme("view-list-tree", QIcon(":lihat/glyph/view-list-tree.png")));
    act->setCheckable(true);
    act->setShortcut(QKeySequence(Qt::Key_F7));
    act->setShortcutContext(Qt::WindowShortcut);
    QObject::connect(act, SIGNAL(toggled(bool)),
                     this, SLOT(slotFoldersToggled(bool)));
    QObject::connect(act, SIGNAL(hovered()),
                     this, SLOT(slotFoldersHovered()));

    act = new QAction(QObject::tr("Fil&ters"), this);
    act->setObjectName("acThumbnailViewFilters");
    act->setIcon(QIcon::fromTheme("view-filter", QIcon(":lihat/glyph/view-filter.png")));
    act->setCheckable(true);
    QObject::connect(act, SIGNAL(toggled(bool)),
                     this, SLOT(slotFiltersToggled(bool)));
    QObject::connect(act, SIGNAL(hovered()),
                     this, SLOT(slotFiltersHovered()));

    QActionGroup* actgrp = new QActionGroup(this);
    actgrp->setExclusive(true);

    act = new QAction(QObject::tr("&Name"), this);
    act->setObjectName("acThumbnailViewSortByName");
    act->setCheckable(true);
    QObject::connect(act, SIGNAL(triggered()),
                     this, SLOT(slotSortByNameTriggered()));
    actgrp->addAction(act);

    act = new QAction(QObject::tr("&Size"), this);
    act->setObjectName("acThumbnailViewSortBySize");
    act->setCheckable(true);
    QObject::connect(act, SIGNAL(triggered()),
                     this, SLOT(slotSortBySizeTriggered()));
    actgrp->addAction(act);

    act = new QAction(QObject::tr("&Date"), this);
    act->setObjectName("acThumbnailViewSortByDate");
    act->setCheckable(true);
    QObject::connect(act, SIGNAL(triggered()),
                     this, SLOT(slotSortByDateTriggered()));
    actgrp->addAction(act);

    act = new QAction(QObject::tr("&Ascending"), this);
    act->setObjectName("acThumbnailViewSortOrderAscending");
    act->setCheckable(true);
    QObject::connect(act, SIGNAL(triggered(bool)),
                     this, SLOT(slotSortOrderAscendingTriggered(bool)));

    act = new QAction(QObject::tr("&Dimension"), this);
    act->setObjectName("acThumbnailViewShowDimension");
    act->setCheckable(true);
    QObject::connect(act, SIGNAL(triggered(bool)),
                     this, SLOT(slotShowDimensionTriggered(bool)));
    QObject::connect(act, SIGNAL(hovered()),
                     this, SLOT(slotShowDimensionHovered()));

    act = new QAction(QObject::tr("&Size"), this);
    act->setObjectName("acThumbnailViewShowSize");
    act->setCheckable(true);
    QObject::connect(act, SIGNAL(triggered(bool)),
                     this, SLOT(slotShowSizeTriggered(bool)));
    QObject::connect(act, SIGNAL(hovered()),
                     this, SLOT(slotShowSizeHovered()));
}

QAction*CThumbnailViewWidget::getAction(const QString& name, bool mustexists)
{
    QAction* result = findChild< QAction* >(name);
    if (mustexists)
        Q_ASSERT(result);

    return result;
}

void CThumbnailViewWidget::setThumbnailsSortedBy(int sortby)
{
    CProxyModel::EItemSortingBy _sortby = (CProxyModel::EItemSortingBy) sortby;

    Q_ASSERT(m_thumbnailview);
    CProxyModel* proxmodel = qobject_cast< CProxyModel* >(m_thumbnailview->model());
    if (proxmodel)
        proxmodel->setSortedBy(_sortby);
}

void CThumbnailViewWidget::setThumbnailsSortAscending(bool asc)
{
    Q_ASSERT(m_thumbnailview);
    CProxyModel* proxmodel = qobject_cast< CProxyModel* >(m_thumbnailview->model());
    if (proxmodel)
        proxmodel->setSortOrder(asc ? Qt::AscendingOrder : Qt::DescendingOrder);
}

void CThumbnailViewWidget::updateBackForwardButton()
{
    QAction* act = getAction("acThumbnailViewBack");
    act->hover();
    act = getAction("acThumbnailViewForward");
    act->hover();
}

void CThumbnailViewWidget::updateFiltersButtonText()
{
    QAction* acThumbnailViewFilters = getAction("acThumbnailViewFilters");

    QString caption = QObject::tr("Filters");
    CMainWindow* mainwindow = getMainWindow();
    if (mainwindow && mainwindow->isAncestorOf(this))
    {
        CStatusBarButtons* buttons = mainwindow->findChild< CStatusBarButtons* >("tvsbButtons");
        if (!buttons)
            return;

        if (!acThumbnailViewFilters->isChecked())
        {
            CProxyModel* viewmodel = qobject_cast< CProxyModel* >(m_thumbnailview->model());
            Q_ASSERT(viewmodel);

            int filterscount = viewmodel->itemsFilterCount();
            if (filterscount > 0)
                caption = QObject::tr("Filters %1 %2").arg(QString(QChar(0x12, 0x20)),
                                                           QString::number(filterscount));
        }

        QToolButton* tbtnFilters = buttons->findChild< QToolButton* >("tbtnFilters");
        tbtnFilters->setText(caption);
    }
}

bool CThumbnailViewWidget::updateFoldersTreeviewSelection(const QString& path)
{
    if (!m_folderstreeView->isVisibleTo(this))
        return false;

    QFileInfo finfo(path);
    if (!finfo.isDir())
        return false;

    bool result = false;

    QModelIndex pathindex = m_folderstreeView->indexOfPath(path);
    if (pathindex.isValid())
    {
        // the incoming path might cause current index to be changed, the
        // QFileSystemModel might have to load the directory entries, it might
        // have to load the easiest way to auto-expand tree to a path is to use
        // autoScroll (ie. setAutoScroll(true)), save previous value now to
        // avoid recursion breakage, and get back to it later
        bool oldautoscroll = m_folderstreeView->hasAutoScroll();

        // we need to put inside a IgnoreFoldersTreeviewCurrentIndexChanged
        // guard because without it setCurrentIndex will call
        // slotFoldersTreeviewCurrentIndexChanged which in turn will call
        // goToPath and then itemsProviderPathUpdated and then
        // updateFoldersTreeviewSelection to complete a recursion
        m_ignorefolderscurrentindexchanged = true;
        m_folderstreeView->setAutoScroll(true);
        {
            if (pathindex != m_folderstreeView->currentIndex())
            {
                m_folderstreeView->setCurrentIndex(pathindex);
                m_folderstreeView->expand(pathindex);
            }

            // need some delay for treeview to load before scrolling to current
            // index
            installDelayedFoldersTreeViewScrollToCurrentIndex();

            // TODO: probably use more accurate way to know when that index is
            // available, for example by using QFileSystemModel::directoryLoaded
        }
        m_folderstreeView->setAutoScroll(oldautoscroll);
        m_ignorefolderscurrentindexchanged = false;

        result = true;
    }

    return result;
}

void CThumbnailViewWidget::updateGridSize(short linenum)
{
    QSize thumbnailsize = m_thumbnailview->thumbnailSize();
    int width  = thumbnailsize.width();
    int height = thumbnailsize.height();

    CItemDelegate* idel = qobject_cast< CItemDelegate* >(m_thumbnailview->itemDelegate());
    if (idel)
    {
        width += (m_thumbnailview->spacing() * 2) + (idel->cellPadding() * 2);
        height += (m_thumbnailview->spacing() * 2) + (idel->cellPadding() * 2);
    }
    QFontMetrics fm = this->fontMetrics();

    // calculate number of sensible lines to accomodate for current grid
    if (linenum == -1)
    {
        int curwidth = 0;
        int maxwidth = 0;

        for (int i = 0; i < m_model->thumbnailItemsCount(); ++i)
        {
            const SThumbnailItem* ti = m_model->thumbnailItem(i);
            curwidth = fm.width(ti->Caption);
            if (curwidth > maxwidth)
                maxwidth = curwidth;
        }

        linenum = maxwidth / thumbnailsize.width();
        if (linenum < 1)
            linenum = 1;
        else if (linenum > 3)
            linenum = 3;
    }
    height += fm.height() * linenum;

    m_thumbnailview->setGridSize(QSize(width, height));
}

void CThumbnailViewWidget::dataModelRequired()
{
    Q_ASSERT(m_model);

    if (!m_model)
        qDebug() << "CThumbnailViewWidget::dataModelRequired:" <<
                    "model not assigned";
}

void CThumbnailViewWidget::installDelayedFoldersTreeViewScrollToCurrentIndex(int delaymsecs)
{
    if (m_delayedfolderstocurrentindextimer.isActive())
        m_delayedfolderstocurrentindextimer.stop();

    m_delayedfolderstocurrentindextimer.setSingleShot(true);
    m_delayedfolderstocurrentindextimer.start(delaymsecs);
}

bool CThumbnailViewWidget::nextThumbnail()
{
    return m_thumbnailview->adjustCurrentThumbnailByOffset(1);
}

bool CThumbnailViewWidget::previousThumbnail()
{
    return m_thumbnailview->adjustCurrentThumbnailByOffset(-1);
}

bool CThumbnailViewWidget::reload()
{
    dataModelRequired();
    switch (m_model->thumbnailItemsProviderType())
    {
        case IItemsProvider::PT_FILESYSTEM:
        {
            CDirectoryModel* fsmodel = qobject_cast< CDirectoryModel* >(m_model);

            QUrl curl = currentThumbnailURL();
            QUrl path = QUrl::fromLocalFile(fsmodel->path());
            if (goToPath(path, false))
            {
                // try to preserve previous current thumbnail selected after reload
                return setCurrentThumbnail(curl, false, true);
            }

            break;
        }

        default:
            ;
    }

    return false;
}

bool CThumbnailViewWidget::setCurrentThumbnail(const QUrl& url, bool allowchangepath, bool forceinstallendupdate)
{
    dataModelRequired();

    // Go to path of ident, if neccessary, and set the item into selection
    switch (m_model->thumbnailItemsProviderType())
    {
        case IItemsProvider::PT_FILESYSTEM:
        {
            CDirectoryModel* fsmodel = qobject_cast< CDirectoryModel* >(m_model);

            if (!url.isLocalFile())
                return false;

            QFileInfo finfo(url.toLocalFile());
            if (finfo.isReadable() && !finfo.fileName().isEmpty())
            {
                bool pathisok = false;
                bool pathchanged = false;
                QUrl pathurl = QUrl::fromLocalFile(finfo.absolutePath());

                if (QUrl::fromLocalFile(fsmodel->path()) != pathurl)
                {
                    // we have to synchronize url path with current path, go to the url path
                    if (allowchangepath)
                    {
                        pathisok = goToPath(pathurl, false);
                        pathchanged = pathisok;
                    }
                }
                else
                    pathisok = true;

                bool result = false;
                if (pathisok)
                {
                    if (pathchanged || forceinstallendupdate)
                    {
                        // NOTE:
                        // The forceinstallendupdate is for the likes of reload(),
                        // where although the path didn't change (ie. !pathchanged)
                        // but we still want to setCurrentThumbnail at the end of
                        // thumbnails update.
                        //
                        // This is to preserve previous thumbnail selection after a
                        // reload. So, to support that behavior by the end of the
                        // item enumeration (that we assume not yet happened due
                        // to the nature of aysnchronous update) we install a
                        // 'setCurrentThumbnail' after the thumbnails has fully
                        // enumerated.
                        result = m_thumbnailview->setCurrentThumbnail(url, true);
                    }
                    else
                        result = m_thumbnailview->setCurrentThumbnail(url);
                }

                return result;
            }

            break;
        }

        default:
            return false;
    }

    return false;
}

QList< QUrl > CThumbnailViewWidget::selectedURLs() const
{
    QList< QUrl> result;

    if (!m_thumbnailview)
        return result;

    QModelIndexList selindexes = m_thumbnailview->selectionModel()->selectedIndexes();
    result = thumbnailViewIndexesToURLs(m_thumbnailview, selindexes);

    return result;
}

QList< QUrl > CThumbnailViewWidget::thumbnailViewIndexesToURLs(CBaseThumbnailView* tv, const QModelIndexList& indexes) const
{
    QList< QUrl > result;

    if (!tv)
        return result;

    if (!m_managedviews.contains(tv))
        return result;

    for (int i = 0; i < indexes.count(); ++i)
    {
        QModelIndex cindex = indexes.at(i);
        if (!cindex.isValid())
            continue;

        SThumbnailItem* titem = tv->thumbnailItemAt(cindex);
        Q_ASSERT(titem);

        switch (m_model->thumbnailItemsProviderType())
        {
            case IItemsProvider::PT_FILESYSTEM:
            {
                result << titem->loadURL();
                break;
            }

            default:
                ;
        }
    }

    return result;
}

void CThumbnailViewWidget::setImageViewWidget(Image::CImageViewWidget* imageviewwidget)
{
    m_linkedimageviewwidget = imageviewwidget;
}

void CThumbnailViewWidget::showEvent(QShowEvent* event)
{
    QWidget::showEvent(event);

    CMainWindow* mainwindow = getMainWindow();
    if (mainwindow)
    {
        Q_ASSERT(mainwindow->isAncestorOf(this));
        QStatusBar* statusbar = mainwindow->statusBar();
        if (statusbar)
            attachToMainWindowStatusBar(statusbar);
    }

    slotFiltersHovered();
    slotFoldersHovered();
    slotGoUpHovered();
}

CThumbnailView* CThumbnailViewWidget::thumbnailView() const
{
    return m_thumbnailview;
}

void CThumbnailViewWidget::slotDelayedFoldersTreeViewScrollToCurrentIndexTimeout()
{
    if (m_folderstreeView->isVisibleTo(this))
    {
        QModelIndex cindex = m_folderstreeView->currentIndex();
        if (!cindex.isValid())
            return;

        m_folderstreeView->scrollToWhenNecessary(cindex);
    }
}

void CThumbnailViewWidget::slotFoldersTreeviewCurrentIndexChanged(const QModelIndex& current, const QModelIndex& previous)
{
    Q_UNUSED(previous);

    if (m_ignorefolderscurrentindexchanged)
        return;

    if (!current.isValid())
        return;

    dataModelRequired();
    switch (m_model->thumbnailItemsProviderType())
    {
        case IItemsProvider::PT_FILESYSTEM:
        {
            const CFilesystemModel* fsmodel = qobject_cast< const CFilesystemModel* >(current.model());
            Q_ASSERT(fsmodel && (fsmodel == m_folderstreeView->model()));

            QFileInfo finfo = fsmodel->fileInfo(current);
            if (finfo.isDir())
                goToPath(QUrl::fromLocalFile(finfo.absoluteFilePath()));

            break;
        }

        default:
            ;
    }
}

void CThumbnailViewWidget::slotFiltersHovered()
{
    QAction* act = getAction("acThumbnailViewFilters");
    act->setEnabled(isVisible() && m_model && m_thumbnailview &&
                    m_managedviews.contains(m_thumbnailview));
}

void CThumbnailViewWidget::slotFiltersToggled(bool checked)
{
    m_filterswidget->setVisible(checked);
    updateFiltersButtonText();
}

void CThumbnailViewWidget::slotFiltersWidgetEscapePressed()
{
    // when escape pressed in filters widget, assume user want to hide it
    QAction* acThumbnailViewFilters = getAction("acThumbnailViewFilters");
    acThumbnailViewFilters->setChecked(false);
    if (m_thumbnailview->isVisible())
        m_thumbnailview->setFocus();
}

void CThumbnailViewWidget::slotFiltersWidgetFilterChanged()
{
    CProxyModel* viewmodel = qobject_cast< CProxyModel* >(m_thumbnailview->model());
    if (viewmodel)
    {
        // since this is a filter change, we do not want to flush already loaded
        // thumbnails
        bool oldautoclear = m_thumbnailview->isAutoClearLocalData();
        m_thumbnailview->setAutoClearLocalData(false);
        m_thumbnailview->beginUpdate();
        viewmodel->filterChanged();  // filter gets invalidated
        m_thumbnailview->endUpdate();
        m_thumbnailview->setAutoClearLocalData(oldautoclear);
    }
}

void CThumbnailViewWidget::slotBackHovered()
{
    QComboBox* locationcb = findChild< QComboBox* >("cbLocation");
    if (!locationcb)
        return;

    QAction* act = qobject_cast< QAction* >(sender());
    if (!act)
        return;

    act->setEnabled(isVisible() &&
                    !m_locationshistory.isEmpty() && (m_locationpos > 0));
}

void CThumbnailViewWidget::slotBackTriggered()
{
    QComboBox* locationcb = findChild< QComboBox* >("cbLocation");
    if (!locationcb)
        return;

    QUrl cururl = modelURL();
    if (!cururl.isValid())
    {
        qDebug() << "CThumbnailViewWidget::slotBackTriggered:" <<
                    "URL" << cururl << "not found in history.";
        return;
    }

    int previdx = qBound< int >(0, m_locationpos - 1, m_locationshistory.count() - 1);
    m_locationpos = previdx;
    QUrl prevurl = m_locationshistory.value(previdx, QUrl());
    if (prevurl.isValid())
    {
        bool cmlh = m_canmodifylocationshistory;
        m_canmodifylocationshistory = false;
        goToPath(prevurl);
        m_canmodifylocationshistory = cmlh;
    }
}

void CThumbnailViewWidget::slotForwardHovered()
{
    QComboBox* locationcb = findChild< QComboBox* >("cbLocation");
    if (!locationcb)
        return;

    QAction* act = qobject_cast< QAction* >(sender());
    if (!act)
        return;

    act->setEnabled(isVisible() &&
                    (!m_locationshistory.isEmpty()) && (m_locationpos < m_locationshistory.count() -1));
}


void CThumbnailViewWidget::slotForwardTriggered()
{
    QComboBox* locationcb = findChild< QComboBox* >("cbLocation");
    if (!locationcb)
        return;

    QUrl cururl = modelURL();
    if (!cururl.isValid())
    {
        qDebug() << "CThumbnailViewWidget::slotForwardTriggered:" <<
                    "URL" << cururl << "not found in history.";
        return;
    }

    int nextidx = qBound< int >(0, m_locationpos + 1, m_locationshistory.count() - 1);
    m_locationpos = nextidx;
    QUrl nexturl = m_locationshistory.value(nextidx, QUrl());
    if (nexturl.isValid())
    {
        bool cmlh = m_canmodifylocationshistory;
        m_canmodifylocationshistory = false;
        goToPath(nexturl);
        m_canmodifylocationshistory = cmlh;
    }
}

void CThumbnailViewWidget::slotGoTriggered()
{
    QComboBox* locationcb = findChild< QComboBox* >("cbLocation");
    QUrl gopath(locationcb->currentText());

    if (gopath.isValid())
        goToPath(gopath);
}

void CThumbnailViewWidget::slotLocationCurrentIndexChanged(int index)
{
    if (m_ignorelocationcurrentindexchanged)
        return;

    QComboBox* locationcb = findChild< QComboBox* >("cbLocation");
    Q_ASSERT(locationcb);

    QUrl url = locationcb->itemData(index).toUrl();
    if (url.isValid())
        goToPath(url);
}

void CThumbnailViewWidget::slotLocationReturnPressed()
{
    QComboBox* locationcb = findChild< QComboBox* >("cbLocation");
    Q_ASSERT(locationcb);

    QUrl url = QUrl::fromUserInput(locationcb->currentText());
    if (url.isValid())
        goToPath(url);
}

void CThumbnailViewWidget::slotSortByDateTriggered()
{
    setThumbnailsSortedBy((int) CProxyModel::ISB_DATE);
}

void CThumbnailViewWidget::slotSortByNameTriggered()
{
    setThumbnailsSortedBy((int) CProxyModel::ISB_NAME);
}

void CThumbnailViewWidget::slotSortBySizeTriggered()
{
    setThumbnailsSortedBy((int) CProxyModel::ISB_SIZE);
}

void CThumbnailViewWidget::slotSortOrderAscendingTriggered(bool checked)
{
    setThumbnailsSortAscending(checked);
}

void CThumbnailViewWidget::slotShowDimensionTriggered(bool checked)
{
    CItemDelegate* idel = qobject_cast< CItemDelegate* >(m_thumbnailview->itemDelegate());
    if (idel && (idel->isDrawingImageDimension() != checked))
    {
        bool acld = m_thumbnailview->isAutoClearLocalData();
        m_thumbnailview->setAutoClearLocalData(false);
        m_thumbnailview->beginUpdate();
        idel->setDrawImageDimension(checked);
        m_thumbnailview->endUpdate();
        m_thumbnailview->setAutoClearLocalData(acld);
    }
}

void CThumbnailViewWidget::slotShowDimensionHovered()
{
    QAction* act = qobject_cast< QAction* >(sender());
    if (!act)
        return;

    CItemDelegate* idel = qobject_cast< CItemDelegate* >(m_thumbnailview->itemDelegate());
    act->setEnabled(isVisible() && idel);
}

void CThumbnailViewWidget::slotShowSizeHovered()
{
    QAction* act = qobject_cast< QAction* >(sender());
    if (!act)
        return;

    CItemDelegate* idel = qobject_cast< CItemDelegate* >(m_thumbnailview->itemDelegate());
    act->setEnabled(isVisible() && idel);
}

void CThumbnailViewWidget::slotShowSizeTriggered(bool checked)
{
    CItemDelegate* idel = qobject_cast< CItemDelegate* >(m_thumbnailview->itemDelegate());
    if (idel && (idel->isDrawingFileSize() != checked))
    {
        bool acld = m_thumbnailview->isAutoClearLocalData();
        m_thumbnailview->setAutoClearLocalData(false);
        m_thumbnailview->beginUpdate();
        idel->setDrawFileSize(checked);
        m_thumbnailview->endUpdate();
        m_thumbnailview->setAutoClearLocalData(acld);
    }
}

void CThumbnailViewWidget::slotThumbnailViewCurrentItemChanged(const QModelIndex& current, const QModelIndex& )
{
    if (current.isValid())
    {
        dataModelRequired();

        const SThumbnailItem* titem = m_thumbnailview->thumbnailItemAt(current);
        if (titem)
            emit currentChanged(titem->loadURL());
    }

    updateStatusTexts();
}

void CThumbnailViewWidget::slotGoUpHovered()
{
    QAction* act = getAction("acThumbnailViewGoUp");
    act->setEnabled(isVisible());
}

void CThumbnailViewWidget::slotGoUpRequested()
{
    goToCurrentParent();
}

void CThumbnailViewWidget::slotThumbnailViewItemExecuted(const QModelIndex& index)
{
    dataModelRequired();
    CBaseThumbnailView* thumbnailview = qobject_cast< CBaseThumbnailView* >(sender());
    Q_ASSERT(thumbnailview);

    SThumbnailItem* titem = thumbnailview->thumbnailItemAt(index);
    if (titem->Type == SThumbnailItem::TI_FOLDER)
    {
        QUrl url = titem->loadURL();
        if (url.isLocalFile())
        {
            QFileInfo finfo = QFileInfo(url.toLocalFile());
            if (finfo.exists() && finfo.isDir() && finfo.isReadable())
                goToPath(url);
        }
    }
    else
        emit viewRequested(titem->loadURL());
}

void CThumbnailViewWidget::slotThumbnailViewItemsSelected(const QModelIndexList& indexes)
{
    Q_UNUSED(indexes);

    updateStatusTexts();
}

void CThumbnailViewWidget::slotThumbnailViewUpdateFinished()
{
    updateStatusTexts();
}

bool CThumbnailViewWidget::addImageLoadQueueJob(CBaseThumbnailView* view, ImageLoad::CBaseLoadJob* job)
{
    if (!m_managedviews.contains(view))
    {
        qDebug() << "CThumbnailViewWidget::addImageLoadQueueJob:" <<
                    "Refusing to add to load queue, thumbnail view is not known.";
        return false;
    }

    m_loadjobs.append(job);
    return m_loadjobs.indexOf(job) != -1;
}

void CThumbnailViewWidget::clearImageLoadQueueJobs(CBaseThumbnailView* view)
{
    if (!m_managedviews.contains(view))
        return;

    // drop any work we have now
    m_thumbloadfuturewatcher.cancel();
    m_thumbloadfuturewatcher.waitForFinished();

    quint32 deleted = 0;
    auto iter = m_loadjobs.begin();
    while (iter != m_loadjobs.end())
    {
        CThumbLoadJob* tvjob = dynamic_cast< CThumbLoadJob* >(*iter);
        if (tvjob && (tvjob->Requester == view))
        {
            iter = m_loadjobs.erase(iter);
            delete tvjob;

            ++deleted;

            continue;
        }

        iter++;
    }

    qDebug() << "CThumbnailViewWidget::clearImageLoadQueueJobs" <<
                deleted << "item(s) deleted";
}

bool CThumbnailViewWidget::createImageLoadQueueJob(CBaseThumbnailView* view, const QModelIndex& index)
{
    if (!m_managedviews.contains(view))
    {
        qDebug() << "CThumbnailViewWidget::createImageLoadQueueJob:" <<
                    "Refusing to add to load queue, thumbnail view is not known.";
        return false;
    }

    SThumbnailItem* thumbnailitem = view->thumbnailItemAt(index);
    Q_ASSERT(thumbnailitem);

    CThumbLoadJob* job = 0;
    for (int i = 0; i < m_loadjobs.count(); ++i)
    {
        job = dynamic_cast< CThumbLoadJob* >(m_loadjobs.at(i));
        if (job && (job->ThumbnailItem == thumbnailitem))
            return true;
    }

    job = new CThumbLoadJob(view, thumbnailitem, view->thumbnailSize());
    if (!addImageLoadQueueJob(view, job))
    {
        delete job;
        return false;
    }

    return true;
}

void CThumbnailViewWidget::handleImageLoadQueueLoadResult(ImageLoad::CBaseLoadJob* job)
{
    if (!m_processloadjobresult)
        return;

    if (!job)
        return;

    CThumbLoadJob* tvjob = dynamic_cast< CThumbLoadJob* >(job);
    if (!tvjob)
        return;

    ImageLoad::CImageLoadResult* jobresult = dynamic_cast< ImageLoad::CImageLoadResult* >(tvjob->result());
    if (!jobresult)
        return;

    CBaseThumbnailView* thumbnailview = tvjob->Requester;
    if (!thumbnailview)
        return;

    QMutexLocker locker(thumbnailview->mutex());

    SThumbnailItem* titem = 0;
    if (m_managedviews.contains(thumbnailview) && tvjob->ThumbnailItem->isValid())
        titem = tvjob->ThumbnailItem;

    if (!titem)
        return;

    SThumbnailLocalData*  viewlocaldata = thumbnailview->thumbnailLocalData(titem);
    QPixmap* tpixmap = viewlocaldata->Pixmap;
    if (!(tpixmap && tpixmap->paintingActive()))
    {
        if ((jobresult->ResultCode == ImageLoad::CImageLoadResult::ILRC_OK) && jobresult->Image)
        {
            // take new pixmap from QImage
            viewlocaldata->deletePixmap();
            tpixmap = new QPixmap();
            tpixmap->convertFromImage(*(jobresult->Image));
            viewlocaldata->Pixmap = tpixmap;
            viewlocaldata->LoadStatus = CBaseThumbnailView::TLS_LOADED;

            titem->FullSize = jobresult->FullSize;

            //qDebug() << "CThumbnailViewWidget::handleImageLoadQueueLoadResult:" <<
            //            "Thumbnail for" << loadurl.toString() << "is available.";
        }
        else
        {
            QString fname = tvjob->URL.toString(QUrl::RemoveScheme | QUrl::RemoveUserInfo |
                                                QUrl::RemoveQuery | QUrl::StripTrailingSlash);
            QPixmap* mimeiconpixmap = Utils::Image::createMIMEIconPixmapFromName(fname, tvjob->requestedSize().toSize());
            if (mimeiconpixmap)
            {
                viewlocaldata->deletePixmap();
                viewlocaldata->Pixmap = mimeiconpixmap;
                viewlocaldata->LoadStatus = CBaseThumbnailView::TLS_MIMETYPEPIXMAP;
            }
            else
                viewlocaldata->LoadStatus = CBaseThumbnailView::TLS_NOTAVAILABLE;
        }
    }

    // update this in its view
    QModelIndex trindex = thumbnailview->indexOfThumbnailItem(titem);
    if (trindex.isValid())
        thumbnailview->update(trindex);
}

void CThumbnailViewWidget::startImageLoadQueue(CBaseThumbnailView* requester)
{
    if (m_managedviews.contains(requester))
        startThumbnailLoaders();
}

void CThumbnailViewWidget::addThumbnailView(CBaseThumbnailView* view)
{
    if (view->objectName().isEmpty())
    {
        qWarning() << "CThumbnailViewWidget::addThumbnailView:" <<
                      "object name is empty.";
        return;
    }

    if (m_managedviews.contains(view))
        return;

    view->setThumbnailLoader(this);

    QObject::connect(view, SIGNAL(itemsGetPasted(QModelIndexList)),
                     this, SLOT(slotThumbnailViewItemsGetPasted(QModelIndexList)),
                     Qt::UniqueConnection);
    QObject::connect(view, SIGNAL(itemsWantCopy(QModelIndexList)),
                     this, SLOT(slotThumbnailViewItemsWantCopy(QModelIndexList)),
                     Qt::UniqueConnection);
    QObject::connect(view, SIGNAL(itemsWantCut(QModelIndexList)),
                     this, SLOT(slotThumbnailViewItemsWantCut(QModelIndexList)),
                     Qt::UniqueConnection);
    QObject::connect(view, SIGNAL(itemsWantDelete(QModelIndexList)),
                     this, SLOT(slotThumbnailViewItemsWantDelete(QModelIndexList)),
                     Qt::UniqueConnection);
    QObject::connect(view, SIGNAL(thumbnailClearLoadJobs()),
                     this, SLOT(slotThumbnailViewClearLoadJobs()),
                     Qt::UniqueConnection);

    CThumbnailView* tv = qobject_cast< CThumbnailView* >(view);
    if (tv)
    {
        connect(tv, SIGNAL(urlsDropped(const SDroppedURLsContext*)),
                this, SLOT(slotThumbnailViewURLsDropped(const SDroppedURLsContext*)));
    }

    m_managedviews << view;
}

void CThumbnailViewWidget::removeThumbnailView(CBaseThumbnailView* view)
{
    if (view)
    {
        disconnect(view, 0, this, 0);
        m_managedviews.removeAll(view);
    }
}

QUrl CThumbnailViewWidget::modelURL() const
{
    QUrl result;
    switch (m_model->thumbnailItemsProviderType())
    {
        case IItemsProvider::PT_FILESYSTEM:
        {
            CDirectoryModel* fsmodel = qobject_cast< CDirectoryModel* >(m_model);
            Q_ASSERT(fsmodel);

            result = QUrl::fromLocalFile(fsmodel->path());
        }

        default:
            ;
    }

    return result;
}

void CThumbnailViewWidget::startThumbnailLoaders()
{
    // if we alread loading, or waiting for previous request bail now
    if (m_waitingthumbnailloadthreadtostart || m_thumbloadfuturewatcher.isRunning())
        return;

    if (m_loadjobs.count() < 1)
        return;

    m_processloadjobresult = true;
    m_waitingthumbnailloadthreadtostart = true;
    qDebug() << "CThumbnailViewWidget::startThumbnailLoaders():" <<
                "getting thumbnails for" << m_loadjobs.count() << "item(s)";

    // WARNING HACK WARNING HACK WARNING HACK WARNING HACK WARNING HACK WARNING
    QList< ImageLoad::CBaseLoadJob* > currentthreadjobs;
    {
        // it seems Qt unable to get frames without invoking QPixmap creation which
        // in turn doesnt play too well with multithreading;
        //
        // as a work-around to this we isolate non-separate-thread friendly jobs and
        // process them in main thread instead. Of course, as long those jobs
        // doesnt take too long then it shouldn't make any difference. It also helps
        // if the QMovie is set to cache its frames.
        //
        // Isolate jobs here and run them after the threaded-friendly jobs sent to
        // work.

        //QMutexLocker locker(&d->Mutex);
        QList< ImageLoad::CBaseLoadJob* >::iterator iter = m_loadjobs.begin();
        while (iter != m_loadjobs.end())
        {
            ImageLoad::CBaseLoadJob* job = *iter;
            if (job->imageLoadType() == ImageLoad::CBaseLoadJob::ILT_THUMBNAILANIMFRAME)
            {
                currentthreadjobs << job;
                iter = m_loadjobs.erase(iter);

                continue;
            }

            ++iter;
        }
    }
    // WARNING HACK WARNING HACK WARNING HACK WARNING HACK WARNING HACK WARNING

    // run jobs in different thread(s)
    QFuture< ImageLoad::CBaseLoadJob* > fut = QtConcurrent::mapped(m_loadjobs, ImageLoad::dispatchLoadJob);
    m_thumbloadfuturewatcher.setFuture(fut);

    // WARNING HACK WARNING HACK WARNING HACK WARNING HACK WARNING HACK WARNING
    for (int i = 0; i < currentthreadjobs.count(); ++i)
    {
        ImageLoad::CBaseLoadJob* job = currentthreadjobs.at(i);
        dispatchLoadJob(job);
        handleImageLoadQueueLoadResult(job);

        // once it got here we assume thumbnail view already copy what it want
        // from the job so we can delete it
        delete job;
    }
    // WARNING HACK WARNING HACK WARNING HACK WARNING HACK WARNING HACK WARNING
}

void CThumbnailViewWidget::slotLoadWatcherResultReadyAt(int index)
{
    ImageLoad::CBaseLoadJob* job = m_thumbloadfuturewatcher.future().resultAt(index);
    if (job)
    {
        handleImageLoadQueueLoadResult(job);
        m_loadjobs.removeAll(job);

        //qDebug() << "CThumbnailViewWidget::slotLoadWatcherResultReadyAt:" <<
        //            "job" << job << "result" << job->result() << "consumed";

        // once it got here we assume thumbnail view already copy what it want
        // from the job so we can delete it
        delete job;
    }
}

void CThumbnailViewWidget::slotLoadWatcherStarted()
{
    // a thread started to load thumbnails, set the flag that we are not waiting
    // for a thread
    m_waitingthumbnailloadthreadtostart = false;
}

void CThumbnailViewWidget::slotThumbnailViewClearLoadJobs()
{
    CThumbnailView* tv = qobject_cast< CThumbnailView* >(sender());
    if (tv)
        clearImageLoadQueueJobs(tv);
}

void CThumbnailViewWidget::slotThumbnailViewItemsGetPasted(const QModelIndexList& indexes)
{
    CBaseThumbnailView* tv = qobject_cast< CBaseThumbnailView* >(sender());
    Q_ASSERT(tv);

    switch (m_model->thumbnailItemsProviderType())
    {
        case IItemsProvider::PT_FILESYSTEM:
        {
            CDirectoryModel* fsmodel = qobject_cast< CDirectoryModel* >(m_model);
            Q_ASSERT(fsmodel);

            QList< QUrl > urls = thumbnailViewIndexesToURLs(tv, indexes);
            QStringList fnames = Utils::Path::urlsToStringList(urls);
            QString pastepath;
            if (fnames.isEmpty() && !fsmodel->path().isEmpty())
                pastepath = fsmodel->path();
            else
                pastepath = fnames.first();

            if (!pastepath.isEmpty())
            {
                qDebug() << "CThumbnailViewWidget::slotThumbnailViewItemsGetPasted:" <<
                            "Pasting clipboard content to:" << pastepath;
                Utils::File::pasteFilesFromClipboardTo(pastepath);
            }

            break;
        }

        default:
            ;
    }
}

void CThumbnailViewWidget::slotThumbnailViewItemsWantCopy(const QModelIndexList& indexes)
{
    CBaseThumbnailView* thumbnailview = qobject_cast< CBaseThumbnailView* >(sender());
    Q_ASSERT(thumbnailview);

    QList< QUrl > urls = thumbnailViewIndexesToURLs(thumbnailview, indexes);
    if (urls.isEmpty())
        return;

    switch (m_model->thumbnailItemsProviderType())
    {
        case IItemsProvider::PT_FILESYSTEM:
        {
            QStringList fnames = Utils::Path::urlsToStringList(urls);
            Utils::File::copyFilesToClipboard(fnames);

            break;
        }

        default:
            ;
    }
}

void CThumbnailViewWidget::slotThumbnailViewItemsWantCut(const QModelIndexList& indexes)
{
    CBaseThumbnailView* thumbnailview = qobject_cast< CBaseThumbnailView* >(sender());
    Q_ASSERT(thumbnailview);

    QList< QUrl > urls = thumbnailViewIndexesToURLs(thumbnailview, indexes);
    if (urls.isEmpty())
        return;

    switch (m_model->thumbnailItemsProviderType())
    {
        case IItemsProvider::PT_FILESYSTEM:
        {
            QStringList fnames = Utils::Path::urlsToStringList(urls);
            Utils::File::cutFilesToClipboard(fnames);

            break;
        }

        default:
            ;
    }
}

void CThumbnailViewWidget::slotThumbnailViewItemsWantDelete(const QModelIndexList& indexes)
{
    CBaseThumbnailView* thumbnailview = qobject_cast< CBaseThumbnailView* >(sender());
    Q_ASSERT(thumbnailview);

    QList< QUrl > urls = thumbnailViewIndexesToURLs(thumbnailview, indexes);
    if (urls.isEmpty())
        return;

    switch (m_model->thumbnailItemsProviderType())
    {
        case IItemsProvider::PT_FILESYSTEM:
        {
            QStringList fnames = Utils::Path::urlsToStringList(urls);
            Utils::File::deleteFiles(fnames);

            break;
        }

        default:
            ;
    }

}

void CThumbnailViewWidget::slotThumbnailViewItemThumbnailWanted(const QModelIndex& index)
{
    CBaseThumbnailView* view = qobject_cast< CBaseThumbnailView* >(sender());
    if (m_managedviews.contains(view))
        createImageLoadQueueJob(view, index);
}

void CThumbnailViewWidget::slotThumbnailViewURLsDropped(const SDroppedURLsContext* context)
{
    Q_ASSERT(context);
    if (!m_model)
        return;

    switch (m_model->thumbnailItemsProviderType())
    {
        case IItemsProvider::PT_FILESYSTEM:
        {
            CDirectoryModel* fsmodel = qobject_cast< CDirectoryModel* >(m_model);
            Q_ASSERT(fsmodel);

            QStringList sources = Utils::Path::urlsToStringList(context->URLs);
            QString destination = Utils::Path::toClean(fsmodel->path());
            if (context->Action == Qt::CopyAction)
                Utils::File::copyFilesTo(sources, destination);
            else if (context->Action == Qt::MoveAction)
                Utils::File::moveFilesTo(sources, destination);

            break;
        }

        default:
            ;
    }

}
