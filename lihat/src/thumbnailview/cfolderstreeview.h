#ifndef CFOLDERSTREEVIEW_H
#define CFOLDERSTREEVIEW_H

#include <QTreeView>


namespace Lihat
{
    namespace Thumbnails
    {

        class CFilesystemModel;

        class CFoldersTreeView:
            public QTreeView
        {
            Q_OBJECT
        public:
            explicit CFoldersTreeView(QWidget* parent = 0);
            virtual ~CFoldersTreeView();

            bool        indexAtCenterArea(const QModelIndex& index);
            QModelIndex indexOfPath(const QString& path);
            void        scrollToWhenNecessary(const QModelIndex& index, bool center = true);

        signals:
            void currentIndexChanged(const QModelIndex& current, const QModelIndex& previous);
            void directoryAvailable(const QString& path);

        protected:
            // QTreeView
            virtual void currentChanged(const QModelIndex& current, const QModelIndex& previous);
            virtual void showEvent(QShowEvent* event);

            CFilesystemModel* m_model;
            bool              m_modelinitialized;

        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CFOLDERSTREEVIEW_H
