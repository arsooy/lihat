#ifndef CSTATUSBARBUTTONS_H
#define CSTATUSBARBUTTONS_H

#include <QWidget>


class QAction;

namespace Lihat
{
    namespace Thumbnails
    {

        class CThumbnailViewWidget;

        class CStatusBarButtons:
            public QWidget
        {
            Q_OBJECT
        public:
            explicit CStatusBarButtons(CThumbnailViewWidget* thumbnailviewwidget, QWidget* parent = 0);
            virtual ~CStatusBarButtons();

        private:
            QAction* getActionOf(QWidget* what, const QString& name, bool mustexists = true);
        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CSTATUSBARBUTTONS_H
