#ifndef CITEMDELEGATE_H
#define CITEMDELEGATE_H

#include "ipixmapuser.h"

#include <QModelIndex>
#include <QStyleOptionViewItem>
#include <QStyledItemDelegate>


class QPixmap;

namespace Lihat
{
    namespace Thumbnails
    {


        class CThumbnailView;

        class CItemDelegate:
            public QStyledItemDelegate,
            public IPixmapUser
        {
            Q_OBJECT
        public:
            explicit CItemDelegate(QObject* parent = 0);
            virtual ~CItemDelegate();

            unsigned short cellPadding() const;

            unsigned char maxLinesForName() const;

            bool isDrawingFileSize() const;
            void setDrawFileSize(bool draw);

            bool isDrawingImageDimension() const;
            void setDrawImageDimension(bool draw);

            // IPixmapUser
            virtual bool setThumbnailViewPixmapProvider(IPixmapProvider* provider);

        protected:
            // QStyledItemDelegate
            virtual void  paint(QPainter* painter, const QStyleOptionViewItem& soption, const QModelIndex& index) const;
            virtual QSize sizeHint(const QStyleOptionViewItem& soption, const QModelIndex&) const;

            unsigned short   m_cellpadding;
            bool             m_drawdimension;
            bool             m_drawfilesize;
            qreal            m_infotextpaintopacity;
            qreal            m_infotextpointpercent;
            unsigned char    m_namelines;
            IPixmapProvider* m_pixmapprovider;

        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CITEMDELEGATE_H
