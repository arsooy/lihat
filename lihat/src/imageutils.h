#ifndef IMAGEUTILS_H
#define IMAGEUTILS_H

#include <QIcon>
#include <QSize>
#include <QString>
#include <QStringList>


class QPixmap;

namespace Lihat
{
    namespace Utils
    {
        namespace Image
        {
            QPixmap*     createMIMEIconPixmapFromName(const QString& name, const QSize& requestsize);
            QStringList  getExtensions(const QString& filters);
            QIcon        getMIMEIcon(const QString& url);
            QString      getOpenFileFilters();
        } // namespace Image
    } // namespace Utils
} // namespace Lihat

#endif // IMAGEUTILS_H
