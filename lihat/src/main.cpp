#include "config_lihat.h"

#include "cmainwindow.h"

#ifdef HAS_WINDOWS
#include "windows/windowsutils.h"
#endif

#include <QApplication>
#include <QStringList>
#include <QtDebug>


using namespace Lihat;

int main(int argc, char* argv[])
{
    QApplication  app(argc, argv);
    app.setApplicationName("Lihat");
    app.setOrganizationName("ariel");
    app.setApplicationVersion("0.0.1_" + QCoreApplication::organizationName());

    Q_INIT_RESOURCE(lihat);

#ifdef HAS_WINDOWS
    Lihat::Utils::Windows::initialize();
#endif

    QStringList  cliargs = app.arguments();
    qDebug() << QApplication::applicationName() << "command line arguments:";
    for (int i = 0; i < cliargs.count(); ++i)
        qDebug() << cliargs.at(i);

    CMainWindow*  mainwindow = getMainWindow();
    mainwindow->applyCommandArguments(cliargs);
    mainwindow->show();

    int retcode = app.exec();

#ifdef HAS_WINDOWS
    Lihat::Utils::Windows::deinitialize();
#endif

    delete mainwindow;
    return retcode;
}
