#ifndef TYPES_LIHAT_H
#define TYPES_LIHAT_H

namespace Lihat
{

    enum TriState
    {
        TS_INVALID  = -1,
        TS_FALSE    =  0,
        TS_TRUE     =  1
    };

    enum ListPosition
    {
        LP_INVALID = 0,
        LP_FIRST   = 1,
        LP_MIDDLE  = 2,
        LP_LAST    = 3
    };

} // namespace Lihat

#endif // TYPES_LIHAT_H
