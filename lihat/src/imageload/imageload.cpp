#include "imageload.h"

#include "cbaseloadjob.h"

#include <QtDebug>


namespace Lihat
{
    namespace ImageLoad
    {

        CBaseLoadJob* dispatchLoadJob(CBaseLoadJob* job)
        {
            Q_ASSERT(job);

            switch (job->imageLoadType())
            {
                case CBaseLoadJob::ILT_IMAGEVIEW:
                case CBaseLoadJob::ILT_THUMBNAILSUBIMAGE:
                case CBaseLoadJob::ILT_THUMBNAILANIMFRAME:
                case CBaseLoadJob::ILT_THUMBNAILVIEW:
                case CBaseLoadJob::ILT_IMAGE:
                {
                    job->load();

                    break;
                }

                default:
                {
                    qDebug() << "ImageLoad::dispatchjob:" <<
                                "unhandled image load type:" << job->imageLoadType();
                }
            }

            return job;
        }

    } // namespace ImageLoad
} // namespace Lihat
