#ifndef IRESIZEJOB_H
#define IRESIZEJOB_H

#include <QSizeF>


namespace Lihat
{
    namespace ImageLoad
    {

        class IResizeJob
        {
        public:
            static const char InBit = 1;

            virtual ~IResizeJob() { }

            virtual void   setRequestedSize(const QSizeF& rsize) = 0;
            virtual QSizeF requestedSize() const = 0;

            virtual void                   setResizeTransformation(Qt::TransformationMode tm) = 0;
            virtual Qt::TransformationMode resizeTransformation() const = 0;
        };

    } // namespace ImageLoad
} // namespace Lihat

#endif // IRESIZEJOB_H
