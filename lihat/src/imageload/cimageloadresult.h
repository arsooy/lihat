#ifndef CIMAGELOADRESULT_H
#define CIMAGELOADRESULT_H

#include "cbaseloadresult.h"

#include <QSize>


class QImage;

namespace Lihat
{
    namespace ImageLoad
    {

        class CImageLoadResult:
            public CBaseLoadResult
        {
        public:
            QImage* Image;
            QSize FullSize;

            explicit CImageLoadResult(EImageLoadResultCode resultcode, QImage* image = 0);
            virtual ~CImageLoadResult();

            CImageLoadResult& operator=(const CImageLoadResult& other);
        };

    } // namespace ImageLoad
} // namespace Lihat

#endif // CIMAGELOADRESULT_H
