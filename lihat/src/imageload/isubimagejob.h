#ifndef ISUBIMAGEJOB_H
#define ISUBIMAGEJOB_H

namespace Lihat
{
    namespace ImageLoad
    {

        class ISubImageJob
        {
        public:
            static const char InBit = 2;

            virtual ~ISubImageJob() { }

            virtual void setRequestedSubImage(const int idx) = 0;
            virtual int requestedSubImage() const = 0;

        };

    } // namespace ImageLoad
} // namespace Lihat

#endif // ISUBIMAGEJOB_H
