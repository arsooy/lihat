#ifndef CBASEIMAGELOADRESULT_H
#define CBASEIMAGELOADRESULT_H


namespace Lihat
{
    namespace ImageLoad
    {

        class CBaseLoadResult
        {
        public:
            enum EImageLoadResultCode
            {
                ILRC_NOTPROCESSED  = 0,
                ILRC_FAIL          = 1,
                ILRC_OK            = 2
            };


            EImageLoadResultCode  ResultCode;

            explicit CBaseLoadResult(EImageLoadResultCode resultcode = EImageLoadResultCode::ILRC_NOTPROCESSED):
                ResultCode(resultcode)
            {

            }

            virtual ~CBaseLoadResult()
            {

            }

            CBaseLoadResult& operator=(const CBaseLoadResult& other)
            {
                if (this == &other)
                    return *this;

                ResultCode = other.ResultCode;

                return *this;
            }
        };

    } // namespace ImageLoad
} // namespace Lihat

#endif // CBASEIMAGELOADRESULT_H
