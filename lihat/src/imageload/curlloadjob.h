#ifndef CURLLOADJOB_H
#define CURLLOADJOB_H

#include "cbaseloadjob.h"

#include <QImageReader>
#include <QUrl>


namespace Lihat
{
    namespace ImageLoad
    {

        class CURLLoadJob:
            public CBaseLoadJob
        {
        public:
            QUrl  URL;

            explicit CURLLoadJob(const CURLLoadJob& other);
            explicit CURLLoadJob(const QUrl& url);
            virtual ~CURLLoadJob();

            CURLLoadJob& operator=(const CURLLoadJob& other);
            bool           operator==(const CURLLoadJob& other);

            virtual EImageLoadType imageLoadType() const;
            virtual bool           isValid() const;
            virtual bool           load();

        protected:
            virtual bool loadFile(const QString& fname);
            virtual bool loadImageReader(QImageReader* ir);

        };

    } // namespace ImageLoad
} // namespace Lihat

#endif // CURLLOADJOB_H
