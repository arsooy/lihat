#ifndef CBASEJOB_H
#define CBASEJOB_H

#include "cbaseloadresult.h"


namespace Lihat
{
    namespace ImageLoad
    {

        class CBaseLoadJob
        {
        public:
            enum EImageLoadType
            {
                ILT_INVALID             = 0,
                ILT_IMAGE               = 1,
                ILT_THUMBNAILSUBIMAGE   = 2,
                ILT_THUMBNAILANIMFRAME  = 3,
                ILT_IMAGEVIEW           = 4,
                ILT_THUMBNAILVIEW       = 5
            };

            explicit CBaseLoadJob():
                m_result(0),
                m_parambits(0)
            {

            }

            virtual ~CBaseLoadJob()
            {
                dropResult();
            }

            virtual EImageLoadType imageLoadType() const = 0;
            virtual bool           isValid() const = 0;
            virtual bool           load() = 0;

            unsigned char paramBits() const
            {
                return m_parambits;
            }

            CBaseLoadResult* result() const
            {
                return m_result;
            }

            virtual void dropResult()
            {
                if (m_result)
                    delete m_result;
                m_result = 0;
            }

            void setResult(CBaseLoadResult* result)
            {
                m_result = result;
            }

        protected:
            CBaseLoadResult* m_result;
            unsigned char    m_parambits;

        };

    } // namespace ImageLoad
} // namespace Lihat

#endif // CBASEJOB_H
