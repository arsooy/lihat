#include "cimageloadresult.h"

#include <QImage>


using namespace Lihat;
using namespace Lihat::ImageLoad;

CImageLoadResult::CImageLoadResult(EImageLoadResultCode resultcode, QImage* image):
    CBaseLoadResult(resultcode),
    Image(image)
{

}

CImageLoadResult::~CImageLoadResult()
{
    if (Image)
        delete Image;
    Image = 0;
}

CImageLoadResult& CImageLoadResult::operator=(const CImageLoadResult& other)
{
    if (this == &other)
        return *this;

    CBaseLoadResult::operator=(other);
    Image = other.Image;

    return *this;
}
