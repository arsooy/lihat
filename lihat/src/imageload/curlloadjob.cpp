#include "curlloadjob.h"
#include "cimageloadresult.h"

#include "../icoutils.h"

using namespace Lihat;
using namespace Lihat::ImageLoad;

CURLLoadJob::CURLLoadJob(const CURLLoadJob& other):
    URL(other.URL)
{

}

CURLLoadJob::CURLLoadJob(const QUrl& url):
    URL(url)
{

}

CURLLoadJob::~CURLLoadJob()
{

}

CURLLoadJob& CURLLoadJob::operator=(const CURLLoadJob& other)
{
    if (this == &other)
        return *this;

    URL = other.URL;

    return *this;
}

bool CURLLoadJob::operator==(const CURLLoadJob& other)
{
    return (URL == other.URL);
}

CBaseLoadJob::EImageLoadType CURLLoadJob::imageLoadType() const
{
    return ILT_IMAGE;
}

bool CURLLoadJob::isValid() const
{
    return URL.isValid();
}

bool CURLLoadJob::load()
{
    dropResult();

    if (!URL.isLocalFile())
    {
        // for now we only load local files
        setResult(new CImageLoadResult(CImageLoadResult::ILRC_FAIL));

        return false;
    }

    return loadFile(URL.toLocalFile());
}

bool CURLLoadJob::loadFile(const QString& fname)
{
    // open and load the file
    QImageReader reader(fname);
    if (!reader.canRead())
    {
        reader.setDecideFormatFromContent(true);
        reader.setFileName(fname);
    }

    return loadImageReader(&reader);
}

bool CURLLoadJob::loadImageReader(QImageReader* ir)
{
    if (QString(ir->format()) == Utils::ICO::FORMAT_STR)
        Utils::ICO::locateNicerIcon(ir, Utils::ICO::MAX_DIMENSION);

    QImage* img = new QImage();
    if (ir->read(img))
    {
        CImageLoadResult* result = new CImageLoadResult(CImageLoadResult::ILRC_OK);
        result->Image = img;
        result->ResultCode = CImageLoadResult::ILRC_OK;

        setResult(result);
        return true;
    }
    else
    {
        delete img;

        setResult(new CImageLoadResult(CImageLoadResult::ILRC_FAIL));
    }

    return false;
}
