#ifndef IMAGELOAD_H
#define IMAGELOAD_H

namespace Lihat
{
    namespace ImageLoad
    {

        class CBaseLoadJob;

        CBaseLoadJob* dispatchLoadJob(CBaseLoadJob* job);

    } // namespace ImageLoad
} // namespace Lihat

#endif // IMAGELOAD_H
