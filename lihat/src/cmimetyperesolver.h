#ifndef CMIMETYPERESOLVER_H
#define CMIMETYPERESOLVER_H

#include <QString>
#include <QStringList>


namespace Lihat
{

    class CMIMETypeResolverPrivate;

    struct SMIMEType
    {
        QString      Type;
        QString      Comment;
        QStringList  Aliases;
        QString      GenericIconName;
        QStringList  GlobPatterns;
        QString      SubClassOf;
        char         MagicPriority;

        SMIMEType();

        bool     filenameGlobMatch(const QString& fname, bool casesensitive = false, QString* matchingpattern = 0) const;
        QString  iconName(bool generic = false) const;
    };

    /**
     * @brief A MIME type resolver using FreeDesktop.org database.
     *
     * This class is rather expensive, especially on first lookup, so better use
     * one instance and do queries on it.
     *
     */
    class CMIMETypeResolver
    {
    public:
        explicit CMIMETypeResolver();
        virtual ~CMIMETypeResolver();

        SMIMEType*  fromFilename(const QString& fname);
        SMIMEType*  fromTypeName(const QString& tname);
        QString     iconNameFromTypeName(const QString& tname, bool generic);
        QString     typeNameFromFilename(const QString& fname, bool baseclass = false);

    private:
        CMIMETypeResolverPrivate* const d;
    };

    CMIMETypeResolver* getMIMETypeResolver();

} // namespace Lihat

#endif // CMIMETYPERESOLVER_H
