#ifndef CMIMETYPERESOLVER_P_H
#define CMIMETYPERESOLVER_P_H

#include <QCache>
#include <QList>
#include <QString>
#include <QStringList>


class QIODevice;
class QDomElement;

namespace Lihat
{

    class CMIMETypeResolver;

    class CMIMETypeResolverPrivate
    {
    public:
        CMIMETypeResolver* const  q;

        bool                      CacheBuilt;
        QList< SMIMEType* >       MIMETypes;
        QCache< QString, int >    PatternLookup;
        QCache< QString, int >    GlobPrefixPatternLookup;
        QCache< QString, int >    GlobSuffixPatternLookup;
        QCache< QString, int >    TypeNameLookup;

        explicit CMIMETypeResolverPrivate(CMIMETypeResolver* mtresolver);
        virtual ~CMIMETypeResolverPrivate();

        void         buildCache(bool rebuild = false);
        void         clearCache();

        QStringList  getMIMEInfoFiles() const;

        int          parseMIMEInfoXML(QIODevice* xml);
        SMIMEType*   parseMIMETypeElement(QDomElement* parent) const;

        SMIMEType*   searchByMatchingGlobPatterns(const QString& filename, char* kind = 0);
        SMIMEType*   searchCacheByFilename(const QString& fname);
        SMIMEType*   searchCacheByTypename(const QString& tname);
        QString      searchMagicDatabaseByFilename(const QString& fname);
    };

} // namespace Lihat

#endif // CMIMETYPERESOLVER_P_H
