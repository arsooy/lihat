#ifndef ICOUTILS_H
#define ICOUTILS_H

#include <QImageReader>
#include <QSize>
#include <QString>


namespace Lihat
{
    namespace Utils
    {
        namespace ICO
        {
            const QString FORMAT_STR("ico");
            const QSize   MAX_DIMENSION(256, 256);  // maximum icon size

            enum ENicerICOLocatorResult
            {
                NILR_INVALID          =   0,
                NILR_SUCCESS          =   1,
                NILR_DEVICEINCAPABLE  = -10,
                NILR_NOTICOFILE       = -11
            };


            ENicerICOLocatorResult locateNicerIcon(QImageReader* reader, const QSize& sizehint, int* pindex = 0);

        } // namespace ICO
    } // namespace Utils
} // namespace Lihat

#endif // ICOUTILS_H
