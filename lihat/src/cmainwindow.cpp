#include "cmainwindow.h"

#include "cmainwindow_p.h"
#include "config_lihat.h"
#include "cviewwidget.h"
#include "imageutils.h"
#include "pathutils.h"
#include "persistenceutils.h"
#include "types_lihat.h"

#ifdef FILEOPS_API_NONE
#include "fileoperations/cfileoperationhub.h"
#endif

#include <QAction>
#include <QActionGroup>
#include <QApplication>
#include <QDir>
#include <QFileDialog>
#include <QFileInfo>
#include <QIcon>
#include <QKeyEvent>
#include <QKeySequence>
#include <QMenuBar>
#include <QSizeGrip>
#include <QStyle>
#include <QToolBar>
#include <QVBoxLayout>
#include <QtDebug>


using namespace Lihat;

CMainWindow::CMainWindow(QWidget* parent):
    QMainWindow(parent),
    d_ptr(new CMainWindowPrivate(this))
{
    Q_D(CMainWindow);

    d->initActions();
    d->initUserInterface();
    d->applyActionKeySequences();

    slotViewWidgetActiveWidgetChanged();
}

CMainWindow::~CMainWindow()
{
    Q_D(CMainWindow);

    d->serialize();
    delete d;
}

void CMainWindow::slotFileDeleteHovered()
{
    Q_D(CMainWindow);

    bool enabled = false;
    QAction* acFileOpDelete = d->getAction("acFileOpDelete");
    if (d->isInThumbnailViewMode())
    {
        QAction* act = d->ViewWidget->ThumbnailView.findChild< QAction* >("acDelete");
        if (act)
        {
            act->hover();  // let that action do its checking
            enabled = act->isEnabled();
        }
    }

    acFileOpDelete->setEnabled(enabled);
}

void CMainWindow::slotFileDeleteTriggered()
{
    Q_D(CMainWindow);

    if (d->isInThumbnailViewMode())
    {
        QAction* act = d->ViewWidget->ThumbnailView.findChild< QAction* >("acDelete");
        if (act)
            act->trigger();
    }
}

#ifdef FILEOPS_API_NONE
void CMainWindow::slotCopyToHovered()
{
    Q_D(CMainWindow);

    QAction* act = d->getAction("acCopyTo");
    if (act)
    {
        QUrl modelurl = d->ViewWidget->ThumbnailView.modelURL();
        if (modelurl.isValid())
        {
            QList< QUrl > selurls = d->ViewWidget->ThumbnailView.selectedURLs();
            act->setEnabled(!selurls.isEmpty());
        }
        else
            act->setEnabled(false);
    }
}

void CMainWindow::slotCopyToTriggered()
{
    Q_D(CMainWindow);

    QString relative;
    QUrl modelurl = d->ViewWidget->ThumbnailView.modelURL();
    if (modelurl.isLocalFile())
        relative = modelurl.toLocalFile();
    QList< QUrl > selurls = d->ViewWidget->ThumbnailView.selectedURLs();
    if (!selurls.isEmpty())
        FileOps::getGlobalFileOperationHub()->queryCopyMoveItemsTo(this, selurls, relative, false);
}

void CMainWindow::slotMoveToHovered()
{
    Q_D(CMainWindow);

    QAction* act = d->getAction("acMoveTo");
    if (act)
    {
        QUrl modelurl = d->ViewWidget->ThumbnailView.modelURL();
        if (modelurl.isValid())
        {
            QList< QUrl > selurls = d->ViewWidget->ThumbnailView.selectedURLs();
            act->setEnabled(!selurls.isEmpty());
        }
        else
            act->setEnabled(false);
    }
}

void CMainWindow::slotMoveToTriggered()
{
    Q_D(CMainWindow);

    QString relative;
    QUrl modelurl = d->ViewWidget->ThumbnailView.modelURL();
    if (modelurl.isLocalFile())
        relative = modelurl.toLocalFile();
    QList< QUrl > selurls = d->ViewWidget->ThumbnailView.selectedURLs();
    if (!selurls.isEmpty())
        FileOps::getGlobalFileOperationHub()->queryCopyMoveItemsTo(this, selurls, relative, true);
}

void CMainWindow::slotFileOperationsTriggered()
{
    FileOps::getGlobalFileOperationHub()->showFileOperationsDialog(this);
}
#endif

void CMainWindow::slotNextHovered()
{
    Q_D(CMainWindow);

    QAction* act = d->getAction("acNext");
    act->setEnabled(d->ViewWidget->hasNext());
}

void CMainWindow::slotOpenTriggered()
{
    Q_D(CMainWindow);

    QString opendir;
    if (d->isInImageViewMode())
        opendir = d->ViewWidget->ImageView.currentLoadedURL().toLocalFile();
    else if (d->isInThumbnailViewMode())
        opendir = d->ViewWidget->ThumbnailView.currentThumbnailParentURL().toLocalFile();

    QString openfilter = Utils::Image::getOpenFileFilters();
    QString openfn = QFileDialog::getOpenFileName(this, tr("Open Image"), opendir, openfilter);
    if (!openfn.isNull())
        d->ViewWidget->imageOpen(openfn);
}

void CMainWindow::slotPreviousHovered()
{
    Q_D(CMainWindow);

    QAction* acPrevious = d->getAction("acPrevious");
    acPrevious->setEnabled(d->ViewWidget->hasPrevious());
}

bool CMainWindow::applyCommandArguments(const QStringList& args)
{
    Q_D(CMainWindow);

    SMainWindowStartContext startctx;

    if (args.count() >= 2)
    {
        QFileInfo finfo(args.at(1));
        if (finfo.exists())
        {
            startctx.Path = args.at(1);
            startctx.Mode = finfo.isDir() ? MWM_THUMBNAIL : MWM_IMAGE;
            return d->setStartContext(&startctx);
        }
    }

    QDir homedir = QDir::home();
    if (homedir.isReadable())
    {
        startctx.Path = homedir.absolutePath();
        startctx.Mode = MWM_THUMBNAIL;
        return d->setStartContext(&startctx);
    }

    return false;
}

void CMainWindow::slotEditMenuAboutToShow()
{
    QMenu* editMenu = findChild< QMenu* >("mnEdit");
    if (editMenu)
    {
        QList< QAction* > actions = editMenu->actions();
        for (int i = 0; i < actions.count(); ++i)
            actions.at(i)->hover();
    }
}

void CMainWindow::slotFileMenuAboutToShow()
{
    QMenu* fileMenu = findChild< QMenu* >("mnFile");
    if (fileMenu)
    {
        QList< QAction* > actions = fileMenu->actions();
        for (int i = 0; i < actions.count(); ++i)
            actions.at(i)->hover();
    }
}

void CMainWindow::slotGoMenuAboutToShow()
{
    QMenu* goMenu = findChild< QMenu* >("mnGo");
    if (goMenu)
    {
        QList< QAction* > actions = goMenu->actions();
        for (int i = 0; i < actions.count(); ++i)
            actions.at(i)->hover();
    }
}

void CMainWindow::slotToolsMenuAboutToShow()
{
    QMenu* goMenu = findChild< QMenu* >("mnTools");
    if (goMenu)
    {
        QList< QAction* > actions = goMenu->actions();
        for (int i = 0; i < actions.count(); ++i)
            actions.at(i)->hover();
    }
}

void CMainWindow::goNext()
{
    Q_D(CMainWindow);

    d->ViewWidget->next();
}

void CMainWindow::goPrevious()
{
    Q_D(CMainWindow);

    d->ViewWidget->previous();
}

void CMainWindow::goUp()
{
    Q_D(CMainWindow);

    if (d->isInThumbnailViewMode())
        d->ViewWidget->ThumbnailView.goToCurrentParent();
}

void CMainWindow::slotViewMenuAboutToShow()
{
    QMenu* viewMenu = findChild< QMenu* >("mnView");
    if (viewMenu)
    {
        QList< QAction* > actions = viewMenu->actions();
        for (int i = 0; i < actions.count(); ++i)
            actions.at(i)->hover();
    }
}

void CMainWindow::slotViewWidgetActiveWidgetChanged()
{
    Q_D(CMainWindow);

    if (d->isInImageViewMode())
        d->urlToWindowTitle(d->ViewWidget->ImageView.currentLoadedURL());
    else if (d->isInThumbnailViewMode())
        d->urlToWindowTitle(d->ViewWidget->ThumbnailView.modelURL());

    slotViewWidgetCurrentChanged();
}

void CMainWindow::slotGenericMenuAboutToShow()
{
    QMenu* menu = qobject_cast< QMenu* >(sender());
    if (menu)
    {
        QList< QAction* > actions = menu->actions();
        for (int i = 0; i < actions.count(); ++i)
            actions.at(i)->hover();
    }
}

void CMainWindow::slotViewWidgetCurrentChanged()
{
    Q_D(CMainWindow);

    d->currentPositionChanged();
    d->updateActionsState();
}

void CMainWindow::slotViewWidgetImageViewed(const QUrl& imgurl)
{
    Q_UNUSED(imgurl);

    slotViewWidgetActiveWidgetChanged();
}

void CMainWindow::slotViewWidgetPathChanged(const QUrl& pathurl)
{
    Q_UNUSED(pathurl);

    slotViewWidgetActiveWidgetChanged();
}


CMainWindowPrivate::CMainWindowPrivate(CMainWindow* mainwindow):
    q_ptr(mainwindow),
    AdjustWindowToImageSizeRatio(false),
    ViewWidget(new CViewWidget(q_ptr))
{
    deserialize();
}

CMainWindowPrivate::~CMainWindowPrivate()
{

}

void CMainWindowPrivate::currentPositionChanged()
{
    Q_Q(CMainWindow);

    q->slotNextHovered();
    q->slotPreviousHovered();
}


void CMainWindowPrivate::deserialize()
{
    Q_Q(CMainWindow);

    QSize winsize = Utils::Persistence::getStorage("CMainWindow/WindowSize", QVariant(QSize(620, 400))).toSize();
    q->resize(winsize);

    QStringList defkeys;
    QStringList readkeys;

    // go next
    defkeys.clear();
    defkeys << QKeySequence(Qt::Key_Space).toString();
    readkeys = Utils::Persistence::getConfig("ImageView/NextImageKeys", QVariant(defkeys)).toStringList();
    for (int i = 0; i < readkeys.count(); ++i)
    {
        QKeySequence kseq(readkeys.at(i));
        if (!kseq.isEmpty())
            KeySequences.insert("acNext", kseq);
    }

    // go previous
    defkeys.clear();
    defkeys << QKeySequence(Qt::Key_Backspace).toString();
    defkeys << QKeySequence(Qt::SHIFT + Qt::Key_Space).toString();
    readkeys = Utils::Persistence::getConfig("ImageView/PreviousImageKeys", QVariant(defkeys)).toStringList();
    for (int i = 0; i < readkeys.count(); ++i)
    {
        QKeySequence kseq(readkeys.at(i));
        if (!kseq.isEmpty())
            KeySequences.insert("acPrevious", kseq);
    }
}

void CMainWindowPrivate::applyActionKeySequences()
{
    QStringList actionnames;
    actionnames << "acNext" << "acPrevious";

    for (int i = 0; i < actionnames.count(); ++i)
    {
        QAction* act = getAction(actionnames.at(i));
        if (act)
            act->setShortcuts(KeySequences.values(act->objectName()));
    }
}

QAction* CMainWindowPrivate::getAction(const QString& name, bool mustexists)
{
    Q_Q(CMainWindow);

    QAction* result = q->findChild< QAction* >(name);
    if (mustexists)
        Q_ASSERT(result);

    return result;
}

void CMainWindowPrivate::initActions()
{
    Q_Q(CMainWindow);

    QAction* act;
    QActionGroup* actgrp;
    QList< QKeySequence > shortcuts;

    act = new QAction(q->tr("&Open ..."), q);
    act->setObjectName("acOpen");
    act->setIcon(QIcon::fromTheme("document-open", QIcon(":/lihat/glyph/document-open.png")));
    act->setShortcut(QKeySequence::Open);
    QObject::connect(act, SIGNAL(triggered(bool)),
                     q, SLOT(slotOpenTriggered()));

    act = new QAction(q->tr("&Reload"), q);
    act->setObjectName("acReload");
    act->setIcon(q->style()->standardIcon(QStyle::SP_BrowserReload));
    act->setShortcut(QKeySequence::Refresh);
    QObject::connect(act, SIGNAL(triggered(bool)),
                     ViewWidget, SLOT(reload()));

    act = new QAction(q->tr("&Delete"), q);
    act->setObjectName("acFileOpDelete");
    act->setIcon(QIcon::fromTheme("edit-delete", QIcon(":/lihat/glyph/edit-delete.png")));
    act->setShortcut(QKeySequence::Delete);
    QObject::connect(act, SIGNAL(hovered()),
                     q, SLOT(slotFileDeleteHovered()));
    QObject::connect(act, SIGNAL(triggered(bool)),
                     q, SLOT(slotFileDeleteTriggered()));

#ifdef FILEOPS_API_NONE
    act = new QAction(q->tr("Copy to ..."), q);
    act->setObjectName("acCopyTo");
    QObject::connect(act, SIGNAL(triggered()), q, SLOT(slotCopyToTriggered()));
    QObject::connect(act, SIGNAL(hovered()), q, SLOT(slotCopyToHovered()));

    act = new QAction(q->tr("Move to ..."), q);
    act->setObjectName("acMoveTo");
    QObject::connect(act, SIGNAL(triggered()), q, SLOT(slotMoveToTriggered()));
    QObject::connect(act, SIGNAL(hovered()), q, SLOT(slotMoveToHovered()));

    act = new QAction(q->tr("File Operations"), q);
    act->setObjectName("acFileOperations");
    act->setIcon(QIcon::fromTheme("system-run", QIcon(":lihat/glyph/system-run.png")));
    QObject::connect(act, SIGNAL(triggered(bool)), q, SLOT(slotFileOperationsTriggered()));
#endif

    act = new QAction(q->tr("E&xit"), q);
    act->setObjectName("acExit");
    act->setIcon(QIcon::fromTheme("application-exit", QIcon(":lihat/glyph/application-exit.png")));
    act->setShortcut(QKeySequence::Close);
    QObject::connect(act, SIGNAL(triggered(bool)), q, SLOT(close()));

    actgrp = new QActionGroup(q);

    act = new QAction(q->tr("&Thumbnails"), q);
    act->setObjectName("acSwitchToThumbnailView");
    act->setIcon(QIcon::fromTheme("view-list-icons", QIcon(":lihat/glyph/view-list-icons.png")));
    act->setCheckable(true);
    QObject::connect(act, SIGNAL(triggered(bool)),
                     ViewWidget, SLOT(showThumbnailView()));
    actgrp->addAction(act);

    act = new QAction(q->tr("&Image"), q);
    act->setObjectName("acSwitchToImageView");
    act->setIcon(QIcon::fromTheme("view-preview", QIcon(":lihat/glyph/view-preview.png")));
    act->setCheckable(true);
    QObject::connect(act, SIGNAL(triggered(bool)),
                     ViewWidget, SLOT(showImageView()));
    actgrp->addAction(act);

    act = new QAction(q->tr("&Previous"), q);
    act->setObjectName("acPrevious");
    act->setIcon(q->style()->standardIcon(QStyle::SP_ArrowBack));
    act->setShortcuts(shortcuts);
    QObject::connect(act, SIGNAL(hovered()), q, SLOT(slotPreviousHovered()));
    QObject::connect(act, SIGNAL(triggered(bool)),
                     ViewWidget, SLOT(previous()));

    act = new QAction(q->tr("&Next"), q);
    act->setObjectName("acNext");
    act->setIcon(q->style()->standardIcon(QStyle::SP_ArrowForward));
    QObject::connect(act, SIGNAL(hovered()), q, SLOT(slotNextHovered()));
    QObject::connect(act, SIGNAL(triggered(bool)),
                     ViewWidget, SLOT(next()));
}

void CMainWindowPrivate::initUserInterface()
{
    Q_Q(CMainWindow);

    q->setWindowIcon(QIcon(":/lihat/lihat_256x256.png"));
    q->setWindowTitle(QApplication::applicationName());
    q->setMinimumSize(320, 240);

    // main widgets
    ViewWidget->setObjectName("vwViewWidget");
    q->setCentralWidget(ViewWidget);
    QObject::connect(ViewWidget, SIGNAL(activeWidgetChanged()),
                     q, SLOT(slotViewWidgetActiveWidgetChanged()));
    QObject::connect(ViewWidget, SIGNAL(currentChanged()),
                     q, SLOT(slotViewWidgetCurrentChanged()));
    QObject::connect(ViewWidget, SIGNAL(imageViewed(QUrl)),
                     q, SLOT(slotViewWidgetImageViewed(QUrl)));
    QObject::connect(ViewWidget, SIGNAL(pathChanged(QUrl)),
                     q, SLOT(slotViewWidgetPathChanged(QUrl)));

    QToolBar* maintoolbar = new QToolBar(q);
    maintoolbar->setObjectName("tbMainWindow");
    maintoolbar->setFloatable(false);
    q->addToolBar(Qt::LeftToolBarArea, maintoolbar);

    maintoolbar->addAction(getAction("acSwitchToThumbnailView"));
    maintoolbar->addAction(getAction("acSwitchToImageView"));
    maintoolbar->addSeparator();
    maintoolbar->addAction(getAction("acPrevious"));
    maintoolbar->addAction(getAction("acNext"));
    maintoolbar->addSeparator();
    maintoolbar->addAction(getAction("acImageViewScaleToFit"));
    maintoolbar->addAction(getAction("acImageViewZoomReset"));
    maintoolbar->addAction(getAction("acImageViewZoomIn"));
    maintoolbar->addAction(getAction("acImageViewZoomOut"));

    // menus
    QMenuBar* menub = new QMenuBar(q);
    menub->setObjectName("menuBar");
    q->setMenuBar(menub);

    // 'File' menu
    QMenu* fileMenu = menub->addMenu(q->tr("&File"));
    fileMenu->setObjectName("mnFile");
    QObject::connect(fileMenu, SIGNAL(aboutToShow()), q, SLOT(slotFileMenuAboutToShow()));
    fileMenu->addAction(getAction("acOpen"));
    fileMenu->addAction(getAction("acReload"));
    fileMenu->addSeparator();
    fileMenu->addAction(getAction("acFileOpDelete"));

#ifdef FILEOPS_API_NONE
    fileMenu->addAction(getAction("acCopyTo"));
    fileMenu->addAction(getAction("acMoveTo"));
#endif

    fileMenu->addSeparator();
    fileMenu->addAction(getAction("acExit"));

    // 'Edit' menu
    QMenu* editMenu = menub->addMenu(q->tr("&Edit"));
    editMenu->setObjectName("mnEdit");
    QObject::connect(editMenu, SIGNAL(aboutToShow()), q, SLOT(slotEditMenuAboutToShow()));
    editMenu->addAction(getAction("acCopy"));
    editMenu->addAction(getAction("acCut"));
    editMenu->addAction(getAction("acPaste"));

    // 'View' menu
    QMenu* viewMenu = menub->addMenu(q->tr("&View"));
    viewMenu->setObjectName("mnView");
    QObject::connect(viewMenu, SIGNAL(aboutToShow()), q, SLOT(slotViewMenuAboutToShow()));
    viewMenu->addAction(getAction("acSwitchToThumbnailView"));
    viewMenu->addAction(getAction("acSwitchToImageView"));
    viewMenu->addSeparator();
    viewMenu->addAction(getAction("acImageViewScaleToFit"));
    viewMenu->addAction(getAction("acImageViewZoomReset"));
    viewMenu->addAction(getAction("acImageViewZoomIn"));
    viewMenu->addAction(getAction("acImageViewZoomOut"));
    viewMenu->addSeparator();
    QMenu* sortingMenu = viewMenu->addMenu(q->tr("&Sort By"));
    QObject::connect(sortingMenu, SIGNAL(aboutToShow()), q, SLOT(slotGenericMenuAboutToShow()));
    sortingMenu->addAction(getAction("acThumbnailViewSortByName"));
    sortingMenu->addAction(getAction("acThumbnailViewSortByDate"));
    sortingMenu->addAction(getAction("acThumbnailViewSortBySize"));
    sortingMenu->addSeparator();
    sortingMenu->addAction(getAction("acThumbnailViewSortOrderAscending"));
    QMenu* addInfoMenu = viewMenu->addMenu(q->tr("&Additional Information"));
    QObject::connect(addInfoMenu, SIGNAL(aboutToShow()), q, SLOT(slotGenericMenuAboutToShow()));
    addInfoMenu->addAction(getAction("acThumbnailViewShowDimension"));
    addInfoMenu->addAction(getAction("acThumbnailViewShowSize"));
    viewMenu->addAction(getAction("acThumbnailViewFilters"));
    viewMenu->addAction(getAction("acThumbnailViewFolders"));

    // 'Go' menu
    QMenu* goMenu = menub->addMenu(q->tr("&Go"));
    goMenu->setObjectName("mnGo");
    QObject::connect(goMenu, SIGNAL(aboutToShow()), q, SLOT(slotGoMenuAboutToShow()));
    goMenu->addAction(getAction("acPrevious"));
    goMenu->addAction(getAction("acNext"));
    goMenu->addSeparator();
    goMenu->addAction(getAction("acThumbnailViewBack"));
    goMenu->addAction(getAction("acThumbnailViewForward"));
    goMenu->addAction(getAction("acThumbnailViewGoUp"));

    // 'Tools' menu
    QMenu* toolsMenu = menub->addMenu(q->tr("&Tools"));
    toolsMenu->setObjectName("mnTools");
    QObject::connect(goMenu, SIGNAL(aboutToShow()), q, SLOT(slotToolsMenuAboutToShow()));
#ifdef FILEOPS_API_NONE
    toolsMenu->addAction(getAction("acFileOperations"));
#endif
}

bool CMainWindowPrivate::isInImageViewMode() const
{
    return ViewWidget && ViewWidget->ImageView.isVisibleTo(ViewWidget);
}

bool CMainWindowPrivate::isInThumbnailViewMode() const
{
    return ViewWidget && ViewWidget->ThumbnailView.isVisibleTo(ViewWidget);
}

void CMainWindowPrivate::serialize()
{
    Q_Q(CMainWindow);

    Utils::Persistence::setStorage("CMainWindow/WindowSize", QVariant(q->size()));

    // write key sequences
    QStringList writekeys;
    QList< QPair< QString, QString > > keypairs;  // (KeySequence Key, Config Key)
    keypairs << QPair< QString, QString >("acNext",     "NextImage")
             << QPair< QString, QString >("acPrevious", "PreviousImage")
             << QPair< QString, QString >("acImageViewScaleToFit", "ScaleToFit")
             << QPair< QString, QString >("acImageViewZoomIn",     "ZoomIn")
             << QPair< QString, QString >("acImageViewZoomOut",    "ZoomOut")
             << QPair< QString, QString >("acImageViewZoomReset",  "ZoomReset");
    for (int i = 0; i < keypairs.count(); ++i)
    {
        QPair< QString, QString > pair = keypairs.at(i);
        writekeys.clear();

        QList< QKeySequence > keyseqs = KeySequences.values(pair.first);
        for (int j = 0; j < keyseqs.count(); ++j)
        {
            QKeySequence kseq = keyseqs.at(j);
            if (!kseq.isEmpty())
                writekeys << kseq.toString();
        }

        if (!writekeys.isEmpty())
        {
            QString confkey("ImageView/" + pair.second + "Keys");
            Utils::Persistence::setConfig(confkey, QVariant(writekeys));
        }
    }
}

void CMainWindowPrivate::updateActionsState()
{
    Q_Q(CMainWindow);

    q->slotEditMenuAboutToShow();
    q->slotFileMenuAboutToShow();
    q->slotGoMenuAboutToShow();
    q->slotViewMenuAboutToShow();

    QAction* acSwitchToImageView = getAction("acSwitchToImageView");
    acSwitchToImageView->setChecked(isInImageViewMode());

    QAction* acSwitchToThumbnailView = getAction("acSwitchToThumbnailView");
    acSwitchToThumbnailView->setChecked(isInThumbnailViewMode());
}

void CMainWindowPrivate::urlToWindowTitle(const QUrl& url)
{
    Q_Q(CMainWindow);

    if (url.isValid())
    {
        QString title;
        if (url.isLocalFile())
        {
            QFileInfo finfo(url.toLocalFile());
            if (finfo.exists() && !finfo.fileName().isEmpty())
            {
                title = QString("%1 %2 %3");
                title = title.arg(finfo.fileName()).arg(QChar(0x12, 0x20)).arg(QApplication::applicationName());
            }
            else
            {
                title = QString("%1 %2 %3");
                title = title.arg(url.toLocalFile()).arg(QChar(0x12, 0x20)).arg(QApplication::applicationName());
            }
        }
        else
        {
            title = QString("%1 %2 %3");
            title = title.arg(url.toString()).arg(QChar(0x12, 0x20)).arg(QApplication::applicationName());
        }

        q->setWindowTitle(title);
    }
    else
        q->setWindowTitle(QApplication::applicationName());
}

bool CMainWindowPrivate::setStartContext(SMainWindowStartContext* startctx)
{
    switch (startctx->Mode)
    {
        case MWM_IMAGE:
        {
            QFileInfo finfo(startctx->Path);
            if (finfo.isFile())
            {
                AdjustWindowToImageSizeRatio = true;
                if (ViewWidget->imageOpen(QUrl::fromLocalFile(finfo.absoluteFilePath())))
                {
                    // Enable 'next' & 'previous' button because we start
                    // program directly to image viewing so the program
                    // didn't load/enumerate file(s) in that image's
                    // directory, yet. Those buttons can disable itself
                    // when user start to use them, ie. by clicking on them.
                    //
                    // This is to avoid expensive directory files enumeration
                    // (slow on directory full of files) for cases when user
                    // just want to view a file.

                    QAction* act = getAction("acNext");
                    act->setEnabled(true);

                    act = getAction("acPrevious");
                    act->setEnabled(true);

                    // set position as middle to enable the 'next' and
                    // 'previous' action, this is the only occasion probably
                    // where this function should be used.
                    ViewWidget->setCurrentPos(LP_MIDDLE);

                    return true;
                }
                else
                    return false;
            }

            break;
        }

        case MWM_THUMBNAIL:
        {
            QFileInfo finfo(startctx->Path);
            if (finfo.isDir())
                return ViewWidget->thumbnailOpen(QUrl::fromLocalFile(finfo.absoluteFilePath()));

            break;
        }

        default:
            ;
    }

    return false;
}


CMainWindow* Lihat::getMainWindow()
{
    static CMainWindow* const mainwindow = new CMainWindow();
    return mainwindow;
}
