#ifndef WINDOWSUTILS_H
#define WINDOWSUTILS_H

#include <QString>
#include <QStringList>


namespace Lihat
{
    namespace Utils
    {
        namespace File
        {
            namespace Windows
            {
                /**
                 * @brief Copy files to clipboard using Windows API.
                 */
                bool  copyFilesToClipboard(const QStringList& files);

                /**
                 * @brief Cut files to clipboard using Windows API.
                 */
                bool  cutFilesToClipboard(const QStringList& files);

                /**
                 * @brief Paste files from clipboard using Windows API.
                 */
                bool  pasteFilesFromClipboardTo(const QString& destination);

            } // namespace Windows
        } // namespace Files

        namespace Windows
        {
            /**
             * @brief Additional initialization when run in Windows.
             */
            void  initialize();

            /**
             * @brief Additional de-initialization when run in Windows.
             */
            void  deinitialize();
        } // namespace Windows
    } // namespace Utils
} // namespace Lihat

#endif // WINDOWSUTILS_H
