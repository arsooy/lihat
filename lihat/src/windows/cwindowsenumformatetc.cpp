#include "cwindowsenumformatetc.h"

#include "cwindowsclipboarddataobject.h"

#include <QtDebug>


using namespace Lihat;

CWindowsEnumFormatEtc::CWindowsEnumFormatEtc(const QList< FORMATETC* >& formats):
    m_index(0),
    m_refcount(1)
{
    copyFormatsFromDataObject(formats);
}

CWindowsEnumFormatEtc::~CWindowsEnumFormatEtc()
{
    clearFormats();
}

ULONG   __stdcall CWindowsEnumFormatEtc::AddRef()
{
    return InterlockedIncrement(&m_refcount);
}

HRESULT __stdcall CWindowsEnumFormatEtc::Clone(IEnumFORMATETC** /* ppEnum */)
{
    return E_NOTIMPL;
}

HRESULT __stdcall CWindowsEnumFormatEtc::Next(ULONG celt, FORMATETC* rgelt, ULONG* pceltFetched)
{
    ULONG copied = 0;
    while ((m_index < m_formats.count()) && (copied < celt))
    {
        FORMATETC* fe = m_formats.at(copied);
        if (deepCopyFormatEtc(fe, &rgelt[m_index]))
        {
            qDebug() << "CWindowsEnumFormatEtc::Next:"
                        "FORMATETC" << fe << "(cfFormat:" << fe->cfFormat << ") deep-copied.";
        }

        ++copied;
        ++m_index;
    }

    if (pceltFetched != 0)
        *pceltFetched = copied;
    return (copied == celt) ? S_OK : S_FALSE;
}

HRESULT __stdcall CWindowsEnumFormatEtc::QueryInterface(REFIID riid, void** ppvObject)
{
    *ppvObject = 0;

    if (riid == IID_IEnumFORMATETC || (riid == IID_IUnknown))
        *ppvObject = this;

    if (*ppvObject)
    {
        AddRef();
        return S_OK;
    }

    return E_NOINTERFACE;
}

ULONG   __stdcall CWindowsEnumFormatEtc::Release()
{
    ULONG result = InterlockedDecrement(&m_refcount);
    if (result == 0)
    {
        delete this;
        return 0;
    }

    return result;
}

HRESULT __stdcall CWindowsEnumFormatEtc::Reset()
{
    m_index = 0;
    return S_OK;
}

HRESULT __stdcall CWindowsEnumFormatEtc::Skip(ULONG celt)
{
    m_index += celt;
    return (m_index <= m_formats.count()) ? S_OK : S_FALSE;
}

void CWindowsEnumFormatEtc::clearFormats()
{
    while (m_formats.count() > 0)
    {
        FORMATETC* fe = m_formats.takeFirst();
        if (fe->ptd)
            CoTaskMemFree(fe->ptd);
        delete fe;
    }
}

void CWindowsEnumFormatEtc::copyFormatsFromDataObject(const QList< FORMATETC* >& formats)
{
    for (int i = 0; i < formats.count(); ++i)
    {
        FORMATETC* source = formats.at(i);
        FORMATETC* dest = new FORMATETC;
        if (!deepCopyFormatEtc(source, dest))
        {
            delete dest;
            continue;
        }

        m_formats << dest;
    }
}

bool CWindowsEnumFormatEtc::deepCopyFormatEtc(FORMATETC* source, FORMATETC* dest)
{
    *dest = *source;

    if (source->ptd)
    {
        dest->ptd = (DVTARGETDEVICE*) CoTaskMemAlloc(sizeof(DVTARGETDEVICE));
        if (!dest->ptd)
            return false;

        *(dest->ptd) = *(source->ptd);
    }

    return true;
}
