#include "cwindowsclipboarddataobject.h"

#include "cwindowsenumformatetc.h"

#include <QtDebug>

#include <Shldisp.h>


using namespace Lihat;


CWindowsClipboardDataObject::CWindowsClipboardDataObject():
    m_operationasync(false),
    m_operationended(false),
    m_refcount(1)
{

}

CWindowsClipboardDataObject::~CWindowsClipboardDataObject()
{
    clearFormatStoragePairs();
}

ULONG __stdcall CWindowsClipboardDataObject::AddRef()
{
    return InterlockedIncrement(&m_refcount);
}

HRESULT __stdcall CWindowsClipboardDataObject::DAdvise(FORMATETC* /* pformatetc */, DWORD /* advf */, IAdviseSink* /* pAdvSink */, DWORD* /* pdwConnection */)
{
    return E_NOTIMPL;
}

HRESULT __stdcall CWindowsClipboardDataObject::DUnadvise(DWORD /* dwConnection */)
{
    return OLE_E_ADVISENOTSUPPORTED;
}

HRESULT __stdcall CWindowsClipboardDataObject::EndOperation(HRESULT /* hResult */, IBindCtx* /* pbcReserved */, DWORD /* dwEffects */)
{
    m_operationended = true;
    return S_OK;
}

HRESULT __stdcall CWindowsClipboardDataObject::EnumDAdvise(IEnumSTATDATA** /* ppenumAdvise */)
{
    return OLE_E_ADVISENOTSUPPORTED;
}

HRESULT __stdcall CWindowsClipboardDataObject::EnumFormatEtc(DWORD dwDirection, IEnumFORMATETC** ppenumFormatEtc)
{
    if (dwDirection == DATADIR_GET)
    {
        QList< FORMATETC* > formats;
        for (int i = 0; i < m_datalist.count(); ++i)
            formats << &(m_datalist.at(i)->Format);

        CWindowsEnumFormatEtc* formatetcenum = new CWindowsEnumFormatEtc(formats);
        *ppenumFormatEtc = reinterpret_cast< IEnumFORMATETC* >(formatetcenum);

        return S_OK;
    }

    return E_NOTIMPL;
}

HRESULT __stdcall CWindowsClipboardDataObject::GetAsyncMode(BOOL* pfIsOpAsync)
{
    *pfIsOpAsync = m_operationasync;

    return S_OK;
}

HRESULT __stdcall CWindowsClipboardDataObject::GetCanonicalFormatEtc(FORMATETC* /* pformatetcIn */, FORMATETC* pformatetcOut)
{
    if (!pformatetcOut)
        return E_INVALIDARG;

    pformatetcOut->ptd = NULL;
    return E_NOTIMPL;
}

HRESULT __stdcall CWindowsClipboardDataObject::GetData(FORMATETC* pformatetcIn, STGMEDIUM* pmedium)
{
    int index = indexOfFORMATETC(pformatetcIn);
    if (index == -1)
        return DV_E_FORMATETC;

    SFormatStoragePair* pair = m_datalist.value(index, 0);
    if (!pair)
        return DV_E_FORMATETC;

    if (deepCopySTGMEDIUM(&pair->Storage, &pair->Format, pmedium))
        return S_OK;

    return DV_E_FORMATETC;
}

HRESULT __stdcall CWindowsClipboardDataObject::GetDataHere(FORMATETC* /* pformatetc */, STGMEDIUM* /* pmedium */)
{
    return DV_E_TYMED;
}

HRESULT __stdcall CWindowsClipboardDataObject::InOperation(BOOL* pfInAsyncOp)
{
    *pfInAsyncOp = !m_operationended;
    return S_OK;
}

HRESULT __stdcall CWindowsClipboardDataObject::QueryGetData(FORMATETC* pformatetc)
{
    return (indexOfFORMATETC(pformatetc) == -1) ? DV_E_FORMATETC : S_OK;
}

HRESULT __stdcall CWindowsClipboardDataObject::QueryInterface(REFIID riid, void** ppvObject)
{
    if (!ppvObject)
        return E_POINTER;

    *ppvObject = 0;
    if ((riid == IID_IDataObject) || (riid == IID_IUnknown))
        *ppvObject = this;
    else if (riid == IID_IAsyncOperation)
        *ppvObject = static_cast< IAsyncOperation* >(this);

    if (*ppvObject)
    {
        AddRef();
        return S_OK;
    }

    return E_NOINTERFACE;
}

ULONG __stdcall CWindowsClipboardDataObject::Release()
{
    ULONG result = InterlockedDecrement(&m_refcount);
    if (result == 0)
    {
        delete this;
        return 0;
    }

    return result;
}

HRESULT __stdcall CWindowsClipboardDataObject::SetAsyncMode(BOOL fDoOpAsync)
{
    m_operationasync = fDoOpAsync;
    return S_OK;
}

HRESULT __stdcall CWindowsClipboardDataObject::SetData(FORMATETC* pformatetc, STGMEDIUM* pmedium, BOOL fRelease)
{
    // shallow copy of pformatetc and pmedium
    SFormatStoragePair pair(pformatetc, pmedium);
    if (!fRelease)
    {
        // deep copy for ownership of data
        deepCopySTGMEDIUM(pmedium, pformatetc, &pair.Storage);
    }

    addFormatStoragePair(&pair.Format, &pair.Storage);
    return S_OK;
}

HRESULT __stdcall CWindowsClipboardDataObject::StartOperation(IBindCtx* /* pbcReserved */)
{
    m_operationended = false;
    return S_OK;
}

bool CWindowsClipboardDataObject::addFormatStoragePair(FORMATETC* formatetc, STGMEDIUM* stgmedium)
{
    SFormatStoragePair* pair = new SFormatStoragePair(formatetc, stgmedium);
    m_datalist << pair;

    qDebug() << "CWindowsClipboardDataObject::addFormatStoragePair:" <<
                "Added new SFormatStoragePair" << pair;

    return m_datalist.indexOf(pair) != -1;
}

void CWindowsClipboardDataObject::clearFormatStoragePairs()
{
    while (m_datalist.count() > 0)
    {
        SFormatStoragePair* pair = m_datalist.takeFirst();
        qDebug() << "CWindowsClipboardDataObject::clearFormatStoragePairs:" <<
                    "Deleting SFormatStoragePair" << pair;

        ReleaseStgMedium(&pair->Storage);
        delete pair;
    }
}

bool CWindowsClipboardDataObject::deepCopySTGMEDIUM(STGMEDIUM* source, FORMATETC* sourceformat, STGMEDIUM* dest)
{
    *dest = *source;

    bool result = false;
    if (source->pUnkForRelease)
    {
        dest->pUnkForRelease = source->pUnkForRelease;
        source->pUnkForRelease->AddRef();
    }

    // duplicate data in storage, this is the meat of the entire function
    switch (sourceformat->tymed)
    {
        case TYMED_HGLOBAL:
        {
            dest->hGlobal = reinterpret_cast< HGLOBAL >(OleDuplicateData(source->hGlobal, sourceformat->cfFormat, 0));
            result = true;

            break;
        }

        case TYMED_FILE:
        {
            dest->lpszFileName = reinterpret_cast< LPOLESTR >(OleDuplicateData(source->hGlobal, sourceformat->cfFormat, 0));
            result = true;

            break;
        }

        case TYMED_ISTREAM:
        {
            dest->pstm = source->pstm;
            source->pstm->AddRef();
            result = true;

            break;
        }

        case TYMED_ISTORAGE:
        {
            dest->pstg = source->pstg;
            source->pstg->AddRef();
            result = true;

            break;
        }
        case TYMED_GDI:
        {
            dest->hBitmap = reinterpret_cast< HBITMAP >(OleDuplicateData(source->hGlobal, sourceformat->cfFormat, 0));
            result = true;

            break;
        }
        case TYMED_MFPICT:
        {
            dest->hMetaFilePict = reinterpret_cast< HMETAFILE >(OleDuplicateData(source->hGlobal, sourceformat->cfFormat, 0));
            result = true;

            break;
        }
        case TYMED_ENHMF:
        {
            dest->hEnhMetaFile = reinterpret_cast< HENHMETAFILE >(OleDuplicateData(source->hGlobal, sourceformat->cfFormat, 0));
            result = true;

            break;
        }

        case TYMED_NULL:
            result = true;
    }

    qDebug() << "CWindowsClipboardDataObject::deepCopySTGMEDIUM:" <<
                "STGMEDIUM" << source << "(hGlobal:" << source->hGlobal << " tymed:" << source->tymed << ") copied to" << dest;

    return result;
}

int CWindowsClipboardDataObject::indexOfFORMATETC(FORMATETC* fe) const
{
    for (int i = 0; i < m_datalist.count(); ++i)
    {
        SFormatStoragePair* pair = m_datalist.at(i);
        if (!pair)
            continue;

        FORMATETC* dfe = &pair->Format;
        if ((dfe->tymed &  fe->tymed) &&
            (dfe->cfFormat == fe->cfFormat) &&
            (dfe->dwAspect == fe->dwAspect))
        {
            return i;
        }
    }

    return -1;
}


SFormatStoragePair::SFormatStoragePair(FORMATETC* format, STGMEDIUM* storage)
{
    ZeroMemory(&Format, sizeof(FORMATETC));
    ZeroMemory(&Storage, sizeof(STGMEDIUM));

    if (format)
        Format = *format;
    if (storage)
        Storage = *storage;
}

SFormatStoragePair::~SFormatStoragePair()
{

}
