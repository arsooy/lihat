#ifndef CWINDOWSENUMFORMATETC_H
#define CWINDOWSENUMFORMATETC_H

#include <QList>

#include <ObjIdl.h>


namespace Lihat
{

    class CWindowsClipboardDataObject;

    class CWindowsEnumFormatEtc:
        public IEnumFORMATETC
    {
    public:
        explicit CWindowsEnumFormatEtc(const QList<FORMATETC*>& formats);
        virtual ~CWindowsEnumFormatEtc();

        ULONG   __stdcall  AddRef();
        HRESULT __stdcall  QueryInterface(REFIID riid, void** ppvObject);
        ULONG   __stdcall  Release();

    private:
        QList< FORMATETC* >  m_formats;
        int                  m_index;
        long                 m_refcount;

        // IEnumFORMATETC
        HRESULT __stdcall  Clone(IEnumFORMATETC** ppEnum);
        HRESULT __stdcall  Next(ULONG celt, FORMATETC* rgelt, ULONG* pceltFetched);
        HRESULT __stdcall  Reset();
        HRESULT __stdcall  Skip(ULONG celt);

        void         clearFormats();
        void         copyFormatsFromDataObject(const QList<FORMATETC*>& formats);
        static bool  deepCopyFormatEtc(FORMATETC* source, FORMATETC* dest);
    };

} // namespace Lihat

#endif // CWINDOWSENUMFORMATETC_H
