#ifndef CWINDOWSCLIPBOARDDATAOBJECT_H
#define CWINDOWSCLIPBOARDDATAOBJECT_H

#include <ObjIdl.h>
#include <Shldisp.h>
#include <QList>
#include <QString>


namespace Lihat
{

    struct SFormatStoragePair
    {
        FORMATETC  Format;
        STGMEDIUM  Storage;

        SFormatStoragePair(FORMATETC* format = 0, STGMEDIUM* storage = 0);
        ~SFormatStoragePair();
    };


    /**
     * @brief  Custom clipboard data implementation to transfer data from Lihat.
     */
    class CWindowsClipboardDataObject:
        public IDataObject,
        public IAsyncOperation
    {
    public:
        explicit CWindowsClipboardDataObject();
        virtual ~CWindowsClipboardDataObject();

        bool  addFormatStoragePair(FORMATETC* formatetc, STGMEDIUM* stgmedium);

        // IUnknown
        ULONG    __stdcall  AddRef();
        HRESULT  __stdcall  QueryInterface(REFIID riid, void** ppvObject);
        ULONG    __stdcall  Release();

        // IDataObject
        HRESULT  __stdcall  DAdvise(FORMATETC* pformatetc, DWORD advf, IAdviseSink* pAdvSink, DWORD* pdwConnection);
        HRESULT  __stdcall  DUnadvise(DWORD dwConnection);
        HRESULT  __stdcall  EnumDAdvise(IEnumSTATDATA** ppenumAdvise);
        HRESULT  __stdcall  EnumFormatEtc(DWORD dwDirection, IEnumFORMATETC** ppenumFormatEtc);
        HRESULT  __stdcall  GetCanonicalFormatEtc(FORMATETC* pformatetcIn, FORMATETC* pformatetcOut);
        HRESULT  __stdcall  GetData(FORMATETC* pformatetcIn, STGMEDIUM* pmedium);
        HRESULT  __stdcall  GetDataHere(FORMATETC* pformatetc, STGMEDIUM* pmedium);
        HRESULT  __stdcall  QueryGetData(FORMATETC* pformatetc);
        HRESULT  __stdcall  SetData(FORMATETC* pformatetc, STGMEDIUM* pmedium, BOOL fRelease);

        // IAsyncOperation
        HRESULT  __stdcall  EndOperation(HRESULT hResult, IBindCtx* pbcReserved, DWORD dwEffects);
        HRESULT  __stdcall  GetAsyncMode(BOOL* pfIsOpAsync);
        HRESULT  __stdcall  InOperation(BOOL* pfInAsyncOp);
        HRESULT  __stdcall  SetAsyncMode(BOOL fDoOpAsync);
        HRESULT  __stdcall  StartOperation(IBindCtx* pbcReserved);

    private:
        QList< SFormatStoragePair* >  m_datalist;
        bool                          m_operationasync;
        bool                          m_operationended;
        long                          m_refcount;

        void  clearFormatStoragePairs();
        bool  deepCopySTGMEDIUM(STGMEDIUM* source, FORMATETC* sourceformat, STGMEDIUM* dest);
        int   indexOfFORMATETC(FORMATETC* fe) const;
    };

} // namespace Lihat

#endif // CWINDOWSCLIPBOARDDATAOBJECT_H
