#include "windowsutils.h"

#include "../fileutils.h"
#include "cwindowsclipboarddataobject.h"

#include <QString>
#include <QStringList>
#include <QtDebug>

#include <windows.h>
#include <shlobj.h>


using namespace Lihat;
using namespace Lihat::Utils;

bool createCFHDROP(const QStringList& files, FORMATETC* fe, STGMEDIUM* sm)
{
    // create null-terminated files list
    QString fnames = files.join(QChar('\0')) + QString(2, QChar('\0'));
    std::wstring wfnames = fnames.toStdWString();
    ULONG fnameslen = wfnames.size() * sizeof(WCHAR);

    // allocate heap memory for DROPFILES & its filenames
    unsigned int gsize = sizeof(DROPFILES) + fnameslen;
    HGLOBAL hgbl = GlobalAlloc(GMEM_MOVEABLE, gsize);
    if (!hgbl)
    {
        qDebug() << "createCFHDROP:" <<
                    "GlobalAlloc does not return valid handle.";
        return false;
    }

    LPVOID lockp = GlobalLock(hgbl);
    ZeroMemory(reinterpret_cast< void* >(lockp), gsize);

    // setup DROPFILES data
    DROPFILES* dropfiles = (DROPFILES*) lockp;
    dropfiles->pFiles = sizeof(DROPFILES);
    dropfiles->fNC = false;
    dropfiles->fWide = true;

    // and write the filenames
    void* pfnames = reinterpret_cast< void* >((long) lockp + sizeof(DROPFILES));
    memcpy(pfnames, wfnames.c_str(), fnameslen);

    GlobalUnlock(hgbl);
    if (GetLastError() == NO_ERROR)
    {
        ZeroMemory(fe, sizeof(FORMATETC));
        fe->cfFormat = CF_HDROP;
        fe->dwAspect = DVASPECT_CONTENT;
        fe->lindex = -1;
        fe->tymed = TYMED_HGLOBAL;

        ZeroMemory(sm, sizeof(STGMEDIUM));
        sm->hGlobal = hgbl;
        sm->tymed = TYMED_HGLOBAL;

        return true;
    }

    return false;
}

bool  createPREFFEREDDROPEFFECT(bool move, FORMATETC* fe, STGMEDIUM* sm)
{
    HGLOBAL hgbl = GlobalAlloc(GMEM_MOVEABLE, sizeof(DWORD));
    if (!hgbl)
    {
        qDebug() << "createPREFFEREDDROPEFFECT:" <<
                    "GlobalAlloc does not return valid handle.";
        return false;
    }

    LPVOID lockp = GlobalLock(hgbl);
    DWORD* dropeffect = reinterpret_cast< DWORD* >(lockp);
    if (move)
        *dropeffect = DROPEFFECT_MOVE;
    else
        *dropeffect = DROPEFFECT_COPY;
    GlobalUnlock(hgbl);

    if (GetLastError() == NO_ERROR)
    {
        ZeroMemory(fe, sizeof(FORMATETC));
        fe->cfFormat = (CLIPFORMAT)RegisterClipboardFormat(CFSTR_PREFERREDDROPEFFECT);
        fe->dwAspect = DVASPECT_CONTENT;
        fe->lindex = -1;
        fe->tymed = TYMED_HGLOBAL;

        ZeroMemory(sm, sizeof(STGMEDIUM));
        sm->hGlobal = hgbl;
        sm->tymed = TYMED_HGLOBAL;

        return true;
    }

    return false;
}

bool  sendFilesToWindowsClipboard(const QStringList& files, bool cut = false)
{
    /* Notes:
     * In Windows, (in my observation, it seems) to differentiate between
     * files sent to clipboard for copy, cut, or link is stored in the
     * data known as "Preferred DropEffect" of a "data object".
     *
     * Basically a data object is the data provided by an IDataObject
     * implementor.
     *
     * A data object can contain more than one "format" along with
     * "storage medium". The "format" identifies about the data, while
     * "storage medium" store that data value. For someone who familiar with
     * Python, think "data object" as a dictionary: a format in a data object
     * is the key, while a storage medium is its value.
     *
     * To copy/cut file(s) one need to create two formats. One is a) the files
     * that we want to send, and b) is the mode itself (ie. copy, move, link).
     *
     * The filename(s) can be shared by creating/reading a format known as
     * CF_HDROP, with a DROPFILES structure as its storage medium. While the
     * mode can be shared by using/evaluating a DWORD flag of DROPEFFECT_COPY,
     * DROPEFFECT_MOVE and so on.
     *
     * Both storage medium created by allocating memory through GlobalAlloc
     * which will return a HGLOBAL handle. This handle will be stored in
     * storage medium structure (ie. STGMEDIUM) member called hGlobal.
     *
     * Related links:
     *   On IDataObject.
     *      http://msdn.microsoft.com/en-us/library/windows/desktop/ms688421(v=vs.85).aspx
     *   On FORMATETC:
     *      http://msdn.microsoft.com/en-us/library/windows/desktop/ms682177(v=vs.85).aspx
     *   On STGMEDIUM:
     *      http://msdn.microsoft.com/en-us/library/windows/desktop/ms683812(v=vs.85).aspx
     *   On Windows' clipboard format:
     *      http://msdn.microsoft.com/en-us/library/windows/desktop/bb776902(v=vs.85).aspx
     *   On DROPFILES:
     *      http://msdn.microsoft.com/en-us/library/windows/desktop/bb773269(v=vs.85).aspx
     *   On CFSTR_PREFERREDDROPEFFECT:
     *      http://msdn.microsoft.com/en-us/library/windows/desktop/bb776902(v=vs.85).aspx#CFSTR_PREFERREDDROPEFFECT
     *   My first clue on Windows' OLE Data Transfer:
     *      http://www.catch22.net/tuts/ole-drag-and-drop
     *   A small tool that I use to inspect Windows' clipboard:
     *      http://www.nakka.com/soft/clcl/index_eng.html
     */

    FORMATETC cfhdropfe;
    STGMEDIUM cfhdropsm;
    if (!createCFHDROP(files, &cfhdropfe, &cfhdropsm))
        return false;

    FORMATETC preferreddropeffectfe;
    STGMEDIUM preferreddropeffectsm;
    if (!createPREFFEREDDROPEFFECT(cut, &preferreddropeffectfe, &preferreddropeffectsm))
        return false;

    // create IDataObject implementor to provide data transfer for others
    CWindowsClipboardDataObject* dataobject = new CWindowsClipboardDataObject();
    // add files and copy method data pairs
    dataobject->addFormatStoragePair(&cfhdropfe, &cfhdropsm);
    dataobject->addFormatStoragePair(&preferreddropeffectfe, &preferreddropeffectsm);
    Q_ASSERT(dataobject->SetAsyncMode(true) == S_OK);

    if (OleSetClipboard((LPDATAOBJECT) dataobject))
        return true;

    return false;
}

bool  File::Windows::copyFilesToClipboard(const QStringList& files)
{
    return sendFilesToWindowsClipboard(files, false);
}

bool  File::Windows::cutFilesToClipboard(const QStringList& files)
{
    return sendFilesToWindowsClipboard(files, true);
}

bool  File::Windows::pasteFilesFromClipboardTo(const QString& destination)
{
    QStringList files;
    IDataObject* ido = 0;
    if (OleGetClipboard(&ido) != S_OK)
        return false;

    // look for CF_HDROP data
    FORMATETC cfhdropfe;
    STGMEDIUM cfhdropsm;
    ZeroMemory(&cfhdropfe, sizeof(FORMATETC));
    ZeroMemory(&cfhdropsm, sizeof(STGMEDIUM));

    cfhdropfe.cfFormat = CF_HDROP;
    cfhdropfe.dwAspect = DVASPECT_CONTENT;
    cfhdropfe.lindex = -1;
    cfhdropfe.tymed = TYMED_HGLOBAL;

    if (ido->GetData(&cfhdropfe, &cfhdropsm) == S_OK)
    {
        LPVOID lockp = GlobalLock(cfhdropsm.hGlobal);
        if (!lockp)
            return false;

        DROPFILES* dropfiles = reinterpret_cast< DROPFILES* >(lockp);
        unsigned int infiles = DragQueryFileW(reinterpret_cast< HDROP >(dropfiles), 0xFFFFFFFF, 0, 0);
        if (infiles > 0)
        {
            char charsize = sizeof(TCHAR);
            if (dropfiles->fWide)
                charsize = sizeof(WCHAR);

            void* fnamebuffer = 0;
            int   fnamebufferlen = 0;

            for (int i = 0; i < (int) infiles; ++i)
            {
                QString fname;
                unsigned int charscount = 0;
                unsigned int nameget = 0;

                if (dropfiles->fWide)
                    charscount = DragQueryFileW(reinterpret_cast< HDROP >(dropfiles), i, 0, 0);
                else
                    charscount = DragQueryFileA(reinterpret_cast< HDROP >(dropfiles), i, 0, 0);
                Q_ASSERT(charscount != 0);

                fnamebufferlen = (charscount + 1) * charsize;
                fnamebuffer = calloc(charscount + 1, charsize);
                if (dropfiles->fWide)
                {
                    nameget = DragQueryFileW(reinterpret_cast< HDROP >(dropfiles), i,
                                             reinterpret_cast< LPWSTR >(fnamebuffer), fnamebufferlen);
                    if (nameget != 0)
                        fname = QString(reinterpret_cast< QChar* >(fnamebuffer), nameget);
                }
                else
                {
                    nameget = DragQueryFileA(reinterpret_cast< HDROP >(dropfiles), i,
                                             reinterpret_cast< LPSTR >(fnamebuffer), fnamebufferlen);
                    if (nameget != 0)
                        fname = QString::fromLatin1(reinterpret_cast< char* >(fnamebuffer), nameget);
                }
                free(fnamebuffer);

                if (!fname.isEmpty())
                    files << fname;
            }
        }
        GlobalUnlock(cfhdropsm.hGlobal);
    }

    if (files.isEmpty())
        return false;

    FORMATETC preferreddropeffectfe;
    STGMEDIUM preferreddropeffectsm;
    ZeroMemory(&preferreddropeffectfe, sizeof(FORMATETC));
    ZeroMemory(&preferreddropeffectsm, sizeof(STGMEDIUM));

    preferreddropeffectfe.cfFormat = (CLIPFORMAT) RegisterClipboardFormat(CFSTR_PREFERREDDROPEFFECT);
    preferreddropeffectfe.dwAspect = DVASPECT_CONTENT;
    preferreddropeffectfe.lindex = -1;
    preferreddropeffectfe.tymed = TYMED_HGLOBAL;

    bool dropmove = false;
    if (ido->GetData(&preferreddropeffectfe, &preferreddropeffectsm) == S_OK)
    {
        LPVOID lockp = GlobalLock(preferreddropeffectsm.hGlobal);
        if (!lockp)
            return false;

        DWORD* dropeffect = reinterpret_cast< DWORD* >(lockp);
        dropmove = (*dropeffect == DROPEFFECT_MOVE);
        GlobalUnlock(preferreddropeffectsm.hGlobal);
    }
    else
    {
        // assume method is plain copy
        qWarning() << "File::Windows::pasteFilesFromClipboardTo" <<
                      "Unable to get CFSTR_PREFERREDDROPEFFECT, going to use DROPEFFECT_COPY.";
        dropmove = false;
    }

    if (dropmove)
        return File::moveFilesTo(files, destination);
    else
        return File::copyFilesTo(files, destination);

    return false;
}

void Windows::initialize()
{
    OleInitialize(0);
}

void Windows::deinitialize()
{
    OleUninitialize();
}
