#include "icoutils.h"

#include <stdint.h>
#include <limits>

#include <QBuffer>
#include <QByteArray>
#include <QDataStream>
#include <QIODevice>
#include <QIcon>
#include <QMap>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::Utils;

typedef unsigned char  BYTE;
typedef unsigned short WORD;
typedef unsigned int   DWORD;


typedef struct
{
    BYTE    bWidth;          // Width, in pixels, of the image
    BYTE    bHeight;         // Height, in pixels, of the image
    BYTE    bColorCount;     // Number of colors in image (0 if >=8bpp)
    BYTE    bReserved;       // Reserved ( must be 0)
    WORD    wPlanes;         // Color Planes
    WORD    wBitCount;       // Bits per pixel
    DWORD   dwBytesInRes;    // How many bytes in this resource?
    DWORD   dwImageOffset;   // Where in the file is this image?
} ICONDIRENTRY, *LPICONDIRENTRY;


typedef struct
{
    WORD    idReserved;   // Reserved (must be 0)
    WORD    idType;       // Resource Type (1 for icons)
    WORD    idCount;      // How many images?
} ICONDIR, *LPICONDIR;


static const char DIB_PREFIX_BYTES[] = {'\x28', '\x00', '\x00', '\x00'};


/**
 * @brief To locate better-looking icon inside an ICO file.
 *
 * @param reader reader to look for the icon.
 * @param sizehint the size that the caller would like to get as close as
 * possible, so it MIGHT locate a larger image.
 * This will enumerate icon entr(ies) and try to get the most decently-sized
 * icon there is along with the best depth (eg. 32 instead of 8).
 *
 * This will require QIODevice to be able to do random-access, so I'm pretty
 * sure a stream-only this wont work on streaming device.
 */
ICO::ENicerICOLocatorResult ICO::locateNicerIcon(QImageReader* reader, const QSize& sizehint, int* pindex)
{
    QIODevice* device = reader->device();
    Q_ASSERT(device);

    if (!device->seek(0))
        return  NILR_DEVICEINCAPABLE;

    ICONDIR icoheader;
    qint64 readlen = device->read((char*) &icoheader, sizeof(ICONDIR));
    if (readlen == sizeof(ICONDIR))
    {
        if (!((icoheader.idReserved == 0) && (icoheader.idType == 1)))
            return NILR_NOTICOFILE;
    }
    else
        return NILR_NOTICOFILE;

    QList< ICONDIRENTRY* > iconentries;

    // read all icon entries
    for (int i = 0; i < icoheader.idCount; ++i)
    {
        ICONDIRENTRY icoentry;
        readlen = device->read((char*) &icoentry, sizeof(ICONDIRENTRY));
        if (readlen == sizeof(ICONDIRENTRY))
        {
            if ((icoentry.bWidth == 0) && (icoentry.bHeight == 0))
            {
                if ((icoentry.dwImageOffset != 0) && (icoentry.dwBytesInRes != 0))
                {
                    qint64 lastpos = device->pos();

                    device->seek(icoentry.dwImageOffset);
                    QByteArray resbytes = device->read(icoentry.dwBytesInRes);

                    static const QByteArray DIB_PREFIX(DIB_PREFIX_BYTES, 4);
                    if (resbytes.startsWith(DIB_PREFIX))
                    {
                        // read DIB format width and height
                        // reference: https://en.wikipedia.org/wiki/BMP_file_format#DIB_header_.28bitmap_information_header.29
                        qint32 pixels = 0;

                        QDataStream instream(resbytes);
                        instream.setByteOrder(QDataStream::LittleEndian);
                        instream.skipRawData(4);

                        // the size is 4 bytes, but since we piggyback on
                        // ICONDIRENTRY everything is maxed to 255; also Vista
                        // new PNG icon file according to Microsoft (see http://msdn.microsoft.com/en-us/library/windows/desktop/aa511280.aspx#concepts)
                        // maximum icon size is 256x256 pixels
                        instream >> pixels;
                        icoentry.bWidth = qMax< uchar >(abs(pixels), UCHAR_MAX);
                        instream >> pixels;
                        icoentry.bHeight = qMax< uchar >(abs(pixels), UCHAR_MAX);
                    }
                    else
                    {
                        // probably PNG file
                        QBuffer resbuffer(&resbytes);
                        QImageReader resreader(&resbuffer);

                        if (resreader.canRead())
                        {
                            resreader.read();
                            icoentry.bWidth = resreader.size().width();
                            icoentry.bHeight = resreader.size().height();
                        }
                    }

                    device->seek(lastpos);
                }
            }

            ICONDIRENTRY* newentry = new ICONDIRENTRY;
            *newentry = icoentry;
            iconentries << newentry;
        }
    }

    ENicerICOLocatorResult result = NILR_INVALID;

    if (iconentries.isEmpty())
        result = NILR_NOTICOFILE;

    if (result == NILR_INVALID)
    {
        if (iconentries.count() == 1)
        {
            // defer to the default load-routine
            device->seek(0);
            result = NILR_SUCCESS;
        }
        else
        {
            // jump around until we found the best icon
            // TODO: use sorted QList and QMap to evaluate size and depth in
            //       one loop, maybe ..

            // find the closest size to the target
            ICONDIRENTRY* closestsizeentry = 0;
            QList< QSize > sizesdone;
            qint64  closest = LONG_MAX;
            for (int i = 0; i < iconentries.count(); ++i)
            {
                ICONDIRENTRY* entry = iconentries.at(i);
                QSize currentsize(entry->bWidth, entry->bHeight);

                if ((currentsize.width() != currentsize.height()) ||
                    ((entry->dwImageOffset == 0) || (entry->dwBytesInRes == 0)))
                    continue;

                if (sizesdone.contains(currentsize))
                    continue;

                unsigned int distance = abs(currentsize.width() - sizehint.width()) + abs(currentsize.height() - sizehint.height());
                if (distance < closest)
                {
                    closest = distance;
                    closestsizeentry = entry;
                }

                sizesdone << currentsize;
            }

            Q_ASSERT(closestsizeentry);
            QSize bestsize(closestsizeentry->bWidth, closestsizeentry->bHeight);

            // we have the size, now try to get best display depth
            unsigned short lastdepth = 0;
            ICONDIRENTRY* bestdepthentry = 0;
            for (int i = 0; i < iconentries.count(); ++i)
            {
                ICONDIRENTRY* entry = iconentries.at(i);
                if ((entry->bWidth != bestsize.width()) && (entry->bHeight != bestsize.height()))
                    continue;

                if (lastdepth < entry->wBitCount)
                {
                    lastdepth = entry->wBitCount;
                    bestdepthentry = entry;
                }
            }

            if (bestdepthentry)
            {
                device->seek(0);
                int bestimageidx = iconentries.indexOf(bestdepthentry);
                if (reader->jumpToImage(bestimageidx))
                {
                    if (pindex)
                        *pindex = bestimageidx;
                    result = NILR_SUCCESS;
                }
            }
        }
    }

    while (iconentries.count() > 0)
        delete iconentries.takeFirst();

    if ((result != NILR_SUCCESS) && (result != NILR_DEVICEINCAPABLE))
        device->seek(0);

    return result;
}
