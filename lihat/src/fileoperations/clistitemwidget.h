#ifndef CLISTITEMWIDGET_H
#define CLISTITEMWIDGET_H

#include <QString>
#include <QFrame>


class QToolButton;

namespace Lihat
{
    namespace FileOps
    {

        class CFileOperationItem;

        class CListItemWidget:
            public QFrame
        {
            Q_OBJECT
        public:
            explicit CListItemWidget(CFileOperationItem* foperation, QWidget* parent = 0);
            virtual ~CListItemWidget();

            virtual void        detachFromFileOperation();
            CFileOperationItem* fileOperation() const;
            bool                isDetached() const;

        protected:
            CFileOperationItem* m_fileoperation;
            QToolButton*        m_stopbutton;

            virtual QWidget* createFileOperationWidget() = 0;
            void             initUserInterface();
            virtual void     stopOperation();

        private slots:
            void slotStopClicked();
        };

    } // namespace FileOps
} // namespace Lihat

#endif // CLISTITEMWIDGET_H
