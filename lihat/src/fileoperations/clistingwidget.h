#ifndef CLISTINGWIDGET_H
#define CLISTINGWIDGET_H

#include <QScrollArea>
#include <QString>


class QWidget;

namespace Lihat
{
    namespace FileOps
    {

        class CFileOperationItem;
        class CListItemWidget;

        class CListingWidget:
            public QScrollArea
        {
            Q_OBJECT
        public:
            explicit CListingWidget(QWidget* parent = 0);
            virtual ~CListingWidget();

            bool             addWidget(QWidget* widget);
            CListItemWidget* createFileOperationWidget(CFileOperationItem* foperation);

        private:
            QWidget* m_container;

            void initUserInterface();

        };

    } // namespace FileOps
} // namespace Lihat

#endif // CLISTINGWIDGET_H
