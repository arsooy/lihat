#include "cdestinationexistsdialog.h"

#include "../fileutils.h"
#include "../imageutils.h"
#include "../pathutils.h"
#include "copy/cfilecopier.h"
#include "ui_copymovedestinationexistsmultidialog.h"

#include <QDateTime>
#include <QFileInfo>
#include <QPushButton>
#include <QWidget>


using namespace Lihat;
using namespace Lihat::FileOps;

CDestinationExistsDialog::CDestinationExistsDialog(SDestinationExistsResponse* response, QWidget* parent):
    QDialog(parent),
    m_response(response),
    m_ui(new(Ui::CopyMoveDestinationExistsMultiDialog))
{
    initUserInterface();
}

CDestinationExistsDialog::~CDestinationExistsDialog()
{
    delete m_ui;
}

void CDestinationExistsDialog::initUserInterface()
{
    m_ui->setupUi(this);

    QPushButton* pbRename = m_ui->buttonBox->button(QDialogButtonBox::Ok);
    Q_ASSERT(pbRename);
    pbRename->setText(tr("R&ename"));
    pbRename->setIcon(QIcon::fromTheme("edit-rename", QIcon(":lihat/glyph/edit-rename.png")));
    connect(pbRename, SIGNAL(clicked(bool)),
            this, SLOT(slotRenameClicked()));

    QPushButton* pbOverwrite = m_ui->buttonBox->button(QDialogButtonBox::Retry);
    Q_ASSERT(pbOverwrite);
    pbOverwrite->setText(tr("&Overwrite"));
    connect(pbOverwrite, SIGNAL(clicked(bool)),
            this, SLOT(slotOverwriteClicked()));

    QPushButton* pbSkip = m_ui->buttonBox->button(QDialogButtonBox::Ignore);
    Q_ASSERT(pbSkip);
    pbSkip->setText(tr("&Skip"));
    connect(pbSkip, SIGNAL(clicked(bool)),
            this, SLOT(slotSkipClicked()));

    connect(m_ui->lineEditNewName, SIGNAL(textChanged(QString)),
            this, SLOT(updateRenameButton()));
    connect(m_ui->pushButtonSuggestNewName, SIGNAL(clicked(bool)),
            this, SLOT(suggestNewName()));
}

unsigned short CDestinationExistsDialog::queryResponse(QWidget* parent, const QString& source, const QString& dest, SDestinationExistsResponse* response, bool multiple)
{
    Q_ASSERT(response);

    CDestinationExistsDialog* dlg = new CDestinationExistsDialog(response, parent);
    dlg->setSourceFile(source);
    dlg->setDestinationFile(dest);
    dlg->setForMultipleFiles(multiple);

    response->Type = SDestinationExistsResponse::DERRT_ABORT;

    QDialog::DialogCode dcode = QDialog::DialogCode(dlg->exec());
    if (dcode == QDialog::Rejected)
        response->Type = SDestinationExistsResponse::DERRT_ABORT;

    return (unsigned short)(response->Type);
}

bool CDestinationExistsDialog::setDestinationFile(const QString& desturl)
{
    QFileInfo finfo(desturl);
    if (finfo.exists())
    {
        m_ui->labelDestFilename->setText(tr("%1<br />at %2").arg(finfo.fileName()).arg(finfo.path()));
        m_ui->labelDestDateModified->setText(finfo.lastModified().toString(Qt::SystemLocaleLongDate));
        m_ui->labelDestSize->setText(Utils::File::fileSizeToString(finfo.size()));

        QIcon mimeicon = Utils::Image::getMIMEIcon(desturl);
        if (!mimeicon.isNull())
        {
            QPixmap pix = mimeicon.pixmap(m_ui->labelDestIcon->size());
            m_ui->labelDestIcon->setPixmap(pix);
        }

        if (finfo.isDir())
            setWindowTitle(tr("Directory Already Exists"));
        else
            setWindowTitle(tr("File Already Exists"));
        m_originaldestinationurl = desturl;
        suggestNewName();

        return true;
    }

    return false;
}

void CDestinationExistsDialog::setForMultipleFiles(bool multi)
{
    m_ui->checkBoxRemember->setVisible(multi);
}

bool CDestinationExistsDialog::setSourceFile(const QString& sourceurl)
{
    QFileInfo finfo(sourceurl);
    if (finfo.exists())
    {
        m_ui->labelSourceFilename->setText(tr("%1<br />at %2").arg(finfo.fileName()).arg(finfo.path()));
        m_ui->labelSourceDateModified->setText(finfo.lastModified().toString(Qt::SystemLocaleLongDate));
        m_ui->labelSourceSize->setText(Utils::File::fileSizeToString(finfo.size()));

        QIcon mimeicon = Utils::Image::getMIMEIcon(sourceurl);
        if (!mimeicon.isNull())
        {
            QPixmap pix = mimeicon.pixmap(m_ui->labelSourceIcon->size());
            m_ui->labelSourceIcon->setPixmap(pix);
        }

        return true;
    }

    return false;
}

void CDestinationExistsDialog::slotOverwriteClicked()
{
    m_response->Type = SDestinationExistsResponse::DERRT_OVERWRITE;
    m_response->RememberResponse = m_ui->checkBoxRemember->isChecked();
}

void CDestinationExistsDialog::slotRenameClicked()
{
    m_response->Type = SDestinationExistsResponse::DERRT_NEWNAME;
    m_response->NewName = m_ui->lineEditNewName->text();
    m_response->RememberResponse = false;  // we cannot 'remember' for new name entry
}

void CDestinationExistsDialog::slotSkipClicked()
{
    m_response->Type = SDestinationExistsResponse::DERRT_SKIP;
    m_response->RememberResponse = m_ui->checkBoxRemember->isChecked();
}

void CDestinationExistsDialog::suggestNewName()
{
    Q_ASSERT(!m_originaldestinationurl.isEmpty());

    QFileInfo finfo(m_originaldestinationurl);
    QString basename = finfo.baseName();
    QString ext = finfo.completeSuffix();
    QString newdesturl;

    unsigned int num = 2;
    while (finfo.exists())
    {
        QString newname = QString("%1 (%2)").arg(basename, QString::number(num));
        if (!ext.isEmpty())
            newname = QString("%1.%2").arg(newname, ext);

        newdesturl = Utils::Path::join(finfo.absolutePath(), newname);
        finfo.setFile(newdesturl);
        ++num;
    }

    m_ui->lineEditNewName->setText(finfo.fileName());
    m_ui->lineEditNewName->selectAll();
    m_ui->lineEditNewName->setFocus();
}

void CDestinationExistsDialog::updateRenameButton()
{
    QPushButton* pbRename = m_ui->buttonBox->button(QDialogButtonBox::Ok);

    if (pbRename)
    {
        QString entry = m_ui->lineEditNewName->text();
        if (entry.isEmpty())
            pbRename->setEnabled(false);
        else
        {
            Q_ASSERT(!m_originaldestinationurl.isNull());

            QFileInfo finfo(m_originaldestinationurl);
            finfo.setFile(Utils::Path::join(finfo.absolutePath(), entry));
            pbRename->setEnabled(!finfo.exists());
        }
    }
}
