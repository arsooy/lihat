#include "clistingwidget.h"

#include "cfileoperationitem.h"
#include "clistitemwidget.h"
#include "copy/ccopywidget.h"
#include "delete/cdeletewidget.h"
#include "move/cmovewidget.h"

#include <QWidget>
#include <QVBoxLayout>


using namespace Lihat;
using namespace Lihat::FileOps;

CListingWidget::CListingWidget(QWidget* parent):
    QScrollArea(parent),
    m_container(0)
{
    initUserInterface();
}

CListingWidget::~CListingWidget()
{

}

bool CListingWidget::addWidget(QWidget* widget)
{
    if (m_container->isAncestorOf(widget))
        return false;
    if (!qobject_cast< CListItemWidget* >(widget))
        return false;

    m_container->layout()->addWidget(widget);
    return true;
}

CListItemWidget* CListingWidget::createFileOperationWidget(CFileOperationItem* foperation)
{
    CListItemWidget* result = 0;
    switch (foperation->type())
    {
        case CFileOperationItem::OT_COPY:
        {
            result = new CCopyWidget(foperation, this);
            break;
        }

        case CFileOperationItem::OT_MOVE:
        {
            result = new CMoveWidget(foperation, this);
            break;
        }

        case CFileOperationItem::OT_DELETE:
        {
            result = new CDeleteWidget(foperation, this);
            break;
        }

        default:
            ;
    }

    return result;
}

void CListingWidget::initUserInterface()
{
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAsNeeded);
    setBackgroundRole(QPalette::Base);
    setWidgetResizable(true);

    m_container = new QWidget(this);
    m_container->setObjectName("wdgtListingContainerWidget");
    setWidget(m_container);

    new QVBoxLayout(m_container);
}
