#include "ccopytodialog.h"

#include "../persistenceutils.h"
#include "ui_copymovetodialog.h"

#include <QDialogButtonBox>
#include <QFileDialog>
#include <QFileInfo>
#include <QLineEdit>
#include <QStringList>
#include <QVariant>

using namespace Lihat;
using namespace Lihat::FileOps;

CCopyToDialog::CCopyToDialog(QWidget* parent):
    QDialog(parent),
    m_ui(new Ui::CopyMoveToDialog)
{
    m_ui->setupUi(this);

    QObject::connect(m_ui->pushButtonBrowse, SIGNAL(clicked()),
                     this, SLOT(slotBrowseClicked()));
    QObject::connect(m_ui->comboBoxDestination->lineEdit(), SIGNAL(editingFinished()),
                     this, SLOT(slotDestinationEditingFinished()));

    setDialogMode(EDM_COPYTO);
    loadPersistence();
}

CCopyToDialog::~CCopyToDialog()
{
    delete m_ui;
}

void CCopyToDialog::setDialogMode(CCopyToDialog::DialogMode mode)
{
    m_mode = mode;
    switch (m_mode)
    {
        case EDM_COPYTO:
        {
            setWindowTitle(CCopyToDialog::tr("Copy to"));
            m_ui->labelItemsToOp->setText(CCopyToDialog::tr("Items to copy"));

            break;
        }

        case EDM_MOVETO:
        {
            setWindowTitle(CCopyToDialog::tr("Move to"));
            m_ui->labelItemsToOp->setText(CCopyToDialog::tr("Items to move"));

            break;
        }

        default:
            ;
    }
}

CCopyToDialog::DialogMode CCopyToDialog::dialogMode() const
{
    return m_mode;
}

QUrl CCopyToDialog::destination() const
{
    return QUrl::fromLocalFile(m_ui->comboBoxDestination->currentText());
}

void CCopyToDialog::setDestination(const QUrl& desturl)
{
    m_ui->comboBoxDestination->setEditText(desturl.toLocalFile());
}

void CCopyToDialog::setItems(const QStringList& items)
{
    m_ui->listWidgetItems->clear();
    m_ui->listWidgetItems->addItems(items);
}

void CCopyToDialog::writePersistence()
{
    QStringList destinations;
    for (int i = 0; i < m_ui->comboBoxDestination->count(); ++i)
        destinations << m_ui->comboBoxDestination->itemText(i);
    destinations.removeDuplicates();
    Utils::Persistence::setStorage("CCopyToDialog/Destinations", QVariant(destinations));
}

void CCopyToDialog::loadPersistence()
{
    QStringList destinations;
    destinations = Utils::Persistence::getStorage("CCopyToDialog/Destinations", QVariant(destinations)).toStringList();
    destinations.removeDuplicates();

    m_ui->comboBoxDestination->clear();
    m_ui->comboBoxDestination->addItems(destinations);
}

void CCopyToDialog::slotBrowseClicked()
{
    QString dirname = m_ui->comboBoxDestination->currentText();
    dirname = QFileDialog::getExistingDirectory(this, QString(), dirname);

    if (!dirname.isEmpty())
    {
        if (m_ui->comboBoxDestination->findText(dirname) == -1)
            m_ui->comboBoxDestination->addItem(dirname);
        m_ui->comboBoxDestination->setEditText(dirname);
        slotDestinationEditingFinished();
    }
}

void CCopyToDialog::slotDestinationEditingFinished()
{
    QPushButton* btn = m_ui->buttonBox->button(QDialogButtonBox::Ok);
    if (!btn)
        return;

    QFileInfo finfo(m_ui->comboBoxDestination->currentText());
    btn->setEnabled(finfo.isDir());
}
