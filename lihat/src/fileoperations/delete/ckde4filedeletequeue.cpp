#include "ckde4filedeletequeue.h"

#include "../../cmainwindow.h"

#include <konq_operations.h>


using namespace Lihat;
using namespace Lihat::FileOps;
using namespace Lihat::FileOps::KDE4;

CKDE4FileDeleteQueue::CKDE4FileDeleteQueue(const QStringList& files, QObject* parent):
    CFileOperationItem(parent)
{
    for (int i = 0; i < files.count(); ++i)
        addFile(files.at(i));
}

CKDE4FileDeleteQueue::~CKDE4FileDeleteQueue()
{

}

void CKDE4FileDeleteQueue::addFile(const QString& file)
{
    m_files << file;
}

void CKDE4FileDeleteQueue::start()
{
    KUrl::List kurls;
    if (!m_files.isEmpty())
    {
        for (int i = 0; i < m_files.count(); ++i)
            kurls << KUrl(m_files.at(i));
    }

    if (!kurls.isEmpty())
    {
        CMainWindow* mainwindow = getMainWindow();
        KonqOperations::del(mainwindow, KonqOperations::DEL, kurls);
    }

    emit finished();
}

CFileOperationItem::EOperationType CKDE4FileDeleteQueue::type() const
{
    return CFileOperationItem::OT_DELETE;
}
