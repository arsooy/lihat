#ifndef CDELETEWIDGET_H
#define CDELETEWIDGET_H

#include "../clistitemwidget.h"

#include <QString>


class QLabel;
class QLineEdit;
class QProgressBar;
class QWidget;

namespace Lihat
{
    namespace FileOps
    {

        class CDeleteWidget:
            public CListItemWidget
        {
            Q_OBJECT
        public:
            explicit CDeleteWidget(CFileOperationItem* foperation, QWidget* parent = 0);
            virtual ~CDeleteWidget();

            virtual void detachFromFileOperation();
            void         setDeleteProgress(int pos);
            void         setDestination(const QString& destination);
            void         setFile(const QString& source);

        protected:
            QLabel*       m_lbicon;
            QLabel*       m_lbstatus;
            QLineEdit*    m_lefile;
            QProgressBar* m_pboverallprogress;

            virtual QWidget* createFileOperationWidget();
            virtual void     stopOperation();

        private slots:
            void slotFileOperationFinished();
            void slotFileOperationQueueItemDeleted(const QString& file);
            void slotFileOperationQueueProgressed(int current, int total);

        };

    } // namespace FileOps
} // namespace Lihat

#endif // CDELETEWIDGET_H
