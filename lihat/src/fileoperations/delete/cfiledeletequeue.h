#ifndef CFILEDELETEQUEUE_H
#define CFILEDELETEQUEUE_H

#include "../cfileoperationitem.h"
#include "cfiledeleter.h"

#include <QString>
#include <QStringList>
#include <QThread>


namespace Lihat
{
    namespace FileOps
    {

        class CFileDeleteQueue:
            public CFileOperationItem
        {
            Q_OBJECT
        public:
            enum EQueueStatus
            {
                QS_IDLE     = 0,
                QS_WORKING  = 1,
                QS_ABORTED  = 2,
                QS_FINISHED = 3
            };

            explicit CFileDeleteQueue(const QStringList& files, QObject* parent = 0);
            virtual ~CFileDeleteQueue();

            void                   addFile(const QString& file);
            virtual int            elapsedSinceStart();
            bool                   isLast() const;
            bool                   isMultiple() const;
            virtual void           start();
            EQueueStatus           status() const;
            virtual void           stop();
            unsigned long          totalDeleted() const;
            virtual EOperationType type() const;

        signals:
            void queueItemDeleted(const QString& file);
            void queueItemDeletionFailed(const QString& file, SDeletionFailedResponse* response);
            void queueProgressed(const int current, const int total);

        private:
            int                      m_currentprogress;
            SDeletionFailedResponse* m_deletionfailedresponse;
            QThread                  m_deleterthread;
            QList< CFileDeleter* >   m_deleters;
            bool                     m_longprocessdetectedemitted;
            QStringList              m_pendingitems;
            QStringList              m_processeditems;
            int                      m_progressmax;
            EQueueStatus             m_status;
            bool                     m_stopped;
            unsigned long            m_totaldeleted;

            void addProcessedItem(const QString& file);
            void clearDeleters();
            void clearProcessedItems();
            void dropDeleterThread();

        private slots:
            void slotDeleterCheckpoint();
            void slotDeleterDeleted(const QString& file);
            void slotDeleterDeletionFailed(const QString& file, SDeletionFailedResponse* response);
            void slotDeleterFinished();
            bool slotProcessNextPending();

        };

    } // namespace FileOps
}  // namespace Lihat

#endif // CFILEDELETEQUEUE_H
