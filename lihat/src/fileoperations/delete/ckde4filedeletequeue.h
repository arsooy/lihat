#ifndef CKDE4FILEDELETEQUEUE_H
#define CKDE4FILEDELETEQUEUE_H

#include "../cfileoperationitem.h"

#include <QString>
#include <QStringList>


namespace Lihat
{
    namespace FileOps
    {
        namespace KDE4
        {

            /**
            * @brief File deleter that uses KDE 4 API.
            */
            class CKDE4FileDeleteQueue:
                public CFileOperationItem
            {
                Q_OBJECT
            public:
                explicit CKDE4FileDeleteQueue(const QStringList& files, QObject* parent = 0);
                virtual ~CKDE4FileDeleteQueue();

                void                   addFile(const QString& file);
                virtual void           start();
                virtual EOperationType type() const;

            private:
                QStringList m_files;
            };

        } // namespace KDE4
    } // namespace FileOps
} // namespace Lihat

#endif // CKDE4FILEDELETEQUEUE_H
