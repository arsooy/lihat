#include "cfiledeletequeue.h"

#include "cdirectorydeleter.h"

#include <QFileInfo>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::FileOps;

CFileDeleteQueue::CFileDeleteQueue(const QStringList& files, QObject* parent):
    CFileOperationItem(parent),
    m_currentprogress(0),
    m_deletionfailedresponse(0),
    m_longprocessdetectedemitted(false),
    m_progressmax(0),
    m_status(QS_IDLE),
    m_stopped(false),
    m_totaldeleted(0)
{
    for (int i = 0; i < files.count(); ++i)
        addFile(files.at(i));
}

CFileDeleteQueue::~CFileDeleteQueue()
{
    stop();
    clearProcessedItems();
    dropDeleterThread();

    if (m_deletionfailedresponse)
        delete m_deletionfailedresponse;
    m_deletionfailedresponse = 0;
}

void CFileDeleteQueue::addFile(const QString& file)
{
    Q_ASSERT(m_status != QS_WORKING);
    m_pendingitems << file;
}

void CFileDeleteQueue::addProcessedItem(const QString& file)
{
    m_processeditems << file;
}

void CFileDeleteQueue::clearDeleters()
{
    while (m_deleters.count() > 0)
    {
        CFileDeleter* deleter = m_deleters.takeFirst();
        deleter->stop();
        deleter->deleteLater();
    }
}

void CFileDeleteQueue::clearProcessedItems()
{
    m_processeditems.clear();
}

void CFileDeleteQueue::slotDeleterCheckpoint()
{
    if (m_longprocessdetectedemitted)
        return;

    CFileDeleter* filedeleter = qobject_cast< CFileDeleter* >(sender());
    if (filedeleter && !associatedWidget() && (elapsedSinceStart() >= LONG_PROCESS_TRESHOLD_MSECS))
    {
        m_longprocessdetectedemitted = true;
        emit longProcessDetected();
    }
}

void CFileDeleteQueue::slotDeleterDeleted(const QString& file)
{
    addProcessedItem(file);

    emit queueItemDeleted(file);
}

void CFileDeleteQueue::slotDeleterDeletionFailed(const QString& file, SDeletionFailedResponse* response)
{
    emit queueItemDeletionFailed(file, response);
    if (response && response->RememberResponse)
    {
        if (!m_deletionfailedresponse)
            m_deletionfailedresponse = new SDeletionFailedResponse();
        *m_deletionfailedresponse = *response;
    }
}

void CFileDeleteQueue::slotDeleterFinished()
{
    CFileDeleter* filedeleter = qobject_cast< CFileDeleter* >(sender());
    if (filedeleter)
    {
        if (!filedeleter->isAborted())
        {
            ++m_currentprogress;
            emit queueProgressed(m_currentprogress, m_progressmax);
        }
        else
            m_status = QS_ABORTED;
        m_deleters.removeAll(filedeleter);

        m_totaldeleted += filedeleter->deletedCount();
    }

    // go for next pending item (if any)
    slotProcessNextPending();
}

void CFileDeleteQueue::dropDeleterThread()
{
    clearDeleters();
    m_deleterthread.quit();
    m_deleterthread.wait();
}

int CFileDeleteQueue::elapsedSinceStart()
{
    if (m_status == QS_WORKING)
        return CFileOperationItem::elapsedSinceStart();
    else
        return m_lastelapsed;
}

bool CFileDeleteQueue::isLast() const
{
    return m_currentprogress + 1 >= m_progressmax;
}

bool CFileDeleteQueue::isMultiple() const
{
    bool result = m_progressmax > 1;
    if (!result && (m_pendingitems.count() == 1))
    {
        // if the only item to be copied is a directory blindly assume that it
        // is a multiple file job
        QFileInfo finfo(m_pendingitems.first());
        result = finfo.isDir();
    }

    return result;
}

bool CFileDeleteQueue::slotProcessNextPending()
{
    // see if current status is idle or working and we have pending item(s)
    bool hasnext = ((m_status == QS_IDLE) || (m_status == QS_WORKING)) && !m_pendingitems.isEmpty() && !m_stopped;
    if (!hasnext)
    {
        if (m_status != QS_ABORTED)
            m_status = QS_FINISHED;

        // no more processing, drop thread and emit finished
        dropDeleterThread();
        emit finished();
        return false;
    }

    while (!m_pendingitems.isEmpty() && !m_stopped)
    {
        QString fname = m_pendingitems.takeFirst();
        QFileInfo finfo = QFileInfo(fname);

        if (!(finfo.exists() && finfo.isReadable()))
        {
            qDebug() << "CFileDeleteQueue::slotProcessNextPending" <<
                        finfo.fileName() << "does not exists or readable.";
            continue;
        }

        CFileDeleter* deleter = 0;
        if (finfo.isFile())
            deleter = new CFileDeleter(finfo.absoluteFilePath());
        else if (finfo.isDir())
            deleter = new CDirectoryDeleter(finfo.absoluteFilePath());

        if (deleter)
        {
            dropDeleterThread();

            qDebug() << "CFileDeleteQueue::slotProcessNextPending:" <<
                        "Worker" << deleter << "created to delete" << finfo.absoluteFilePath();
            deleter->moveToThread(&m_deleterthread);

            // apply saved SDeletionFailedResponse
            if (m_deletionfailedresponse)
                deleter->setDeletionFailedResponse(*m_deletionfailedresponse);

            // connect to signals of the worker
            connect(deleter, SIGNAL(checkpoint()),
                    this, SLOT(slotDeleterCheckpoint()));
            connect(deleter, SIGNAL(deleted(QString)),
                    this, SLOT(slotDeleterDeleted(QString)));
            connect(deleter, SIGNAL(deletionFailed(QString, SDeletionFailedResponse*)),
                    this, SLOT(slotDeleterDeletionFailed(QString, SDeletionFailedResponse*)),
                    Qt::BlockingQueuedConnection);
            connect(deleter, SIGNAL(finished()),
                    this, SLOT(slotDeleterFinished()));

            // start worker when its thread started, and for it to be deleted
            // when its thread finished
            connect(&m_deleterthread, SIGNAL(started()),
                    deleter, SLOT(start()));
            connect(&m_deleterthread, SIGNAL(finished()),
                    deleter, SLOT(deleteLater()));

            if (!m_stopped)
            {
                m_deleters << deleter;
                m_deleterthread.start();
                return true; // we're done
            }
            else
                delete deleter;
        }
    }

    return false;
}

void CFileDeleteQueue::start()
{
    CFileOperationItem::start();

    // set some variables
    m_stopped = false;
    m_totaldeleted = 0;
    m_currentprogress = 0;
    m_progressmax = m_pendingitems.count();
    m_status = QS_WORKING;

    if (m_progressmax != 0)
    {
        // we have things to do, inform interested parties that we are at
        // the start of the job
        emit queueProgressed(m_currentprogress, m_progressmax);
    }

    // drop previous results, and ..
    clearProcessedItems();
    // start with first item
    slotProcessNextPending();
}

CFileDeleteQueue::EQueueStatus CFileDeleteQueue::status() const
{
    return m_status;
}

void CFileDeleteQueue::stop()
{
    m_stopped = true;

    // stop deleter(s)
    for (int i = 0; i < m_deleters.count(); ++i)
    {
        CFileDeleter* deleter = m_deleters.at(i);
        deleter->stop();
    }

    dropDeleterThread();
}

long unsigned int CFileDeleteQueue::totalDeleted() const
{
    return m_totaldeleted;
}

CFileOperationItem::EOperationType CFileDeleteQueue::type() const
{
    return CFileOperationItem::OT_DELETE;
}
