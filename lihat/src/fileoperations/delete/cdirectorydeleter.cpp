#include "cdirectorydeleter.h"

#include <QDir>
#include <QDirIterator>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::FileOps;

CDirectoryDeleter::CDirectoryDeleter(const QString& directory, QObject* parent):
    CFileDeleter(directory, parent),
    m_descendlevel(-1)
{

}

bool CDirectoryDeleter::directoryDelete(const QString& dir)
{
    if (isStopped())
        return false;

    ++m_descendlevel;
    emitCheckpoint();

    QDir ddir(dir);
    if (!ddir.exists())
        return false;

    // iterate directory items
    QDirIterator directoryiterator(ddir.absolutePath(),
                                   QDir::NoDotAndDotDot | QDir::Hidden | QDir::System | QDir::AllEntries | QDir::Readable | QDir::System);
    while (!isStopped() && directoryiterator.hasNext())
    {
        directoryiterator.next();

        QFileInfo currentfileinfo = directoryiterator.fileInfo();
        QString currentpath = currentfileinfo.absoluteFilePath();
        if (currentfileinfo.isFile())
        {
            if (!fileDelete(currentpath))
            {
                qWarning() << "CDirectoryDeleter::directoryDelete:" <<
                              "Unable to delete file" << currentpath;
                return false;
            }
        }
        else if (currentfileinfo.isDir())
        {
            if (!directoryDelete(currentpath))
            {
                qWarning() << "CDirectoryDeleter::directoryDelete:" <<
                              "Unable to delete directory" << currentpath;
                return false;
            }
            --m_descendlevel;
        }
    }

    // remove current directory
    if (ddir.rmdir(ddir.absolutePath()))
    {
        qDebug() << "CDirectoryDeleter::directoryDelete:" <<
                    dir << "removed.";
        increaseDeleteCount();
        emitDeleted(ddir.absolutePath());
        return true;
    }
    else
    {
        qDebug() << "CDirectoryDeleter::directoryDelete:" <<
                    "Unable to remove" << dir;

        SDeletionFailedResponse response;

        // see if we need to ask for response from user
        bool queryresponse = true;
        if (m_cacheddeletionfailedresponse.RememberResponse &&
            (m_cacheddeletionfailedresponse.Type == SDeletionFailedResponse::DFRRT_SKIP))
        {
            queryresponse = false;
            response = m_cacheddeletionfailedresponse;
        }

        if (queryresponse)
        {
            response.Type = SDeletionFailedResponse::DFRRT_ABORT;  // default to abort the process
            emitDeletionFailed(dir, &response);

            if (response.RememberResponse)
                m_cacheddeletionfailedresponse = response;
        }

        if (response.Type == SDeletionFailedResponse::DFRRT_ABORT)
        {
            qDebug() << "CDirectoryDeleter::directoryDelete:" <<
                        "Aborting delete process!";
            doAbort();
            return false;
        }
        else if (response.Type == SDeletionFailedResponse::DFRRT_SKIP)
        {
            qDebug() << "CDirectoryDeleter::directoryDelete:" <<
                        "Skipping removal of" << dir;
            doSkip();
            return true;
        }
    }

    return !isStopped();
}

void CDirectoryDeleter::start()
{
    directoryDelete(m_file);
    emit finished();
}
