#include "cdeletewidget.h"

#include "cfiledeletequeue.h"

#include <QFormLayout>
#include <QHBoxLayout>
#include <QIcon>
#include <QLabel>
#include <QLineEdit>
#include <QProgressBar>
#include <QTime>
#include <QVBoxLayout>
#include <QWidget>


using namespace Lihat;
using namespace Lihat::FileOps;

CDeleteWidget::CDeleteWidget(CFileOperationItem* foperation, QWidget* parent):
    CListItemWidget(foperation, parent),
    m_lbicon(0),
    m_lbstatus(0),
    m_lefile(0),
    m_pboverallprogress(0)
{
    initUserInterface();
}

CDeleteWidget::~CDeleteWidget()
{

}

QWidget* CDeleteWidget::createFileOperationWidget()
{
    QWidget* result = new QWidget(this);
    QHBoxLayout* lyt = new QHBoxLayout(result);

    m_lbicon = new QLabel(this);
    m_lbicon->setObjectName("lbIcon");
    m_lbicon->setPixmap(QIcon::fromTheme("edit-delete", QIcon(":lihat/glyph/edit-delete.png")).pixmap(QSize(48, 48)));
    lyt->addWidget(m_lbicon, 0, Qt::AlignHCenter | Qt::AlignTop);

    QFormLayout* lytwidgets = new QFormLayout();
    lytwidgets->setObjectName("lytFileOperationDeleteWidget");

    m_lbstatus = new QLabel(tr("Deleteing"), result);
    m_lbstatus->setObjectName("lbStatus");
    lytwidgets->addRow(tr("Status:"), m_lbstatus);

    m_lefile = new QLineEdit(result);
    m_lefile->setObjectName("leSource");
    m_lefile->setBackgroundRole(QPalette::Window);
    m_lefile->setReadOnly(true);
    m_lefile->setFrame(false);
    lytwidgets->addRow(tr("File/folder:"), m_lefile);

    m_pboverallprogress = new QProgressBar(result);
    m_pboverallprogress->setObjectName("pbOverallProgress");
    m_pboverallprogress->setRange(0, 0);
    lytwidgets->addRow(tr("Progress:"), m_pboverallprogress);

    lyt->addLayout(lytwidgets, 1);

    // connect signals from file operation
    CFileDeleteQueue* deletequeue = qobject_cast< CFileDeleteQueue* >(m_fileoperation);
    if (deletequeue)
    {
        connect(deletequeue, SIGNAL(finished()),
                this, SLOT(slotFileOperationFinished()));
        connect(deletequeue, SIGNAL(queueItemDeleted(QString)),
                this, SLOT(slotFileOperationQueueItemDeleted(QString)));
        connect(deletequeue, SIGNAL(queueProgressed(int, int)),
                this, SLOT(slotFileOperationQueueProgressed(int, int)));
    }

    return result;
}

void CDeleteWidget::detachFromFileOperation()
{
    slotFileOperationFinished();
    CListItemWidget::detachFromFileOperation();
}

void CDeleteWidget::slotFileOperationFinished()
{
    CFileDeleteQueue* deletequeue = qobject_cast< CFileDeleteQueue* >(m_fileoperation);
    if (!deletequeue)
        return;

    QString operationstatus;
    switch (deletequeue->status())
    {
        case CFileDeleteQueue::QS_ABORTED:
            operationstatus = tr("Aborted");
            break;
        case CFileDeleteQueue::QS_FINISHED:
            operationstatus = tr("Done");
            break;
        case CFileDeleteQueue::QS_IDLE:
            operationstatus = tr("Idle");
            break;
        default:
            ;
    }

    if (m_lbstatus && !operationstatus.isEmpty())
        m_lbstatus->setText(operationstatus);

    QFormLayout* lytwidgets = findChild< QFormLayout* >("lytFileOperationDeleteWidget");
    if (!lytwidgets)
        return;

    QList< QObject* > deletelist;
    deletelist << lytwidgets->labelForField(m_lefile);
    deletelist << m_lefile;
    deletelist << lytwidgets->labelForField(m_pboverallprogress);
    deletelist << m_pboverallprogress;
    while (deletelist.count() > 0)
        deletelist.takeFirst()->deleteLater();

    QTime current = QTime::currentTime();
    int deletednum = deletequeue->totalDeleted();
    QLabel* lbAftermath = new QLabel(tr("<p>Finished at %1  -  <strong>%2</strong> deleted.<p>").arg(current.toString("hh:mm AP"),
                                                                                                     QString::number(deletednum)));
    lbAftermath->setTextFormat(Qt::RichText);
    lytwidgets->addRow(lbAftermath);
}

void CDeleteWidget::slotFileOperationQueueItemDeleted(const QString& file)
{
    if (m_lefile)
        m_lefile->setText(file);
}

void CDeleteWidget::slotFileOperationQueueProgressed(int current, int total)
{
    if (m_pboverallprogress)
    {
        if ((current == 0) && (total == 0))
        {
            if ((m_pboverallprogress->minimum() != 0) && (m_pboverallprogress->maximum() != 0))
                m_pboverallprogress->setRange(0, 0);
        }
        else
        {
            if ((m_pboverallprogress->minimum() == 0) && (m_pboverallprogress->maximum() == 0))
                m_pboverallprogress->setRange(0, 100);

            int percent = int(100 * qBound< float >(0., 1. * current / total, 1.));
            m_pboverallprogress->setValue(percent);
        }
    }
}

void CDeleteWidget::setFile(const QString& file)
{
    if (!m_lefile)
        return;

    m_lefile->setText(file);
}

void CDeleteWidget::stopOperation()
{
    CFileDeleteQueue* deletequeue = qobject_cast< CFileDeleteQueue* >(m_fileoperation);
    if (!deletequeue)
        return;

    deletequeue->stop();
}
