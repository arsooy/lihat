#ifndef CFILEDELETER_H
#define CFILEDELETER_H

#include "cbasefiledeleter.h"

#include <QString>


namespace Lihat
{
    namespace FileOps
    {

        class CFileDeleter:
            public QObject,
            public CBaseFileDeleter
        {
            Q_OBJECT
        public:
            explicit CFileDeleter(const QString& file, QObject* parent = 0);
            virtual ~CFileDeleter();

            void setDeletionFailedResponse(const SDeletionFailedResponse& response);

        signals:
            void checkpoint();
            void deleted(const QString& file);
            void deletionFailed(const QString& file, SDeletionFailedResponse* response);
            void finished();

        public slots:
            void start();
            void stop();

        protected:
            virtual void emitCheckpoint();
            virtual void emitDeleted(const QString& file);
            virtual void emitDeletionFailed(const QString& file, SDeletionFailedResponse* response);

        };

    } // namespace FileOps
} // namespace Lihat

#endif // CFILEDELETER_H
