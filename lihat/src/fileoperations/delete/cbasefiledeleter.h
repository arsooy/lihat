#ifndef CBASEFILEDELETER_H
#define CBASEFILEDELETER_H

#include <QFile>
#include <QString>
#include <QIODevice>


namespace Lihat
{
    namespace FileOps
    {

        struct SDeletionFailedResponse
        {
            enum EResponseType
            {
                DFRRT_INVALID = 0,
                DFRRT_SKIP    = 1,
                DFRRT_ABORT   = 2
            };

            EResponseType Type;
            bool          RememberResponse;

            SDeletionFailedResponse():
                Type(DFRRT_INVALID),
                RememberResponse(false)
            {

            }
        };


        // basic file deleter without signals & slots
        class CBaseFileDeleter
        {
        public:
            explicit CBaseFileDeleter(const QString& file);
            virtual ~CBaseFileDeleter();

            QString       file() const;
            bool          isAborted() const;
            bool          isSkipped() const;
            bool          isStopped() const;
            void          setDeletionFailedResponse(const SDeletionFailedResponse& response);
            void          start();
            unsigned long deletedCount() const;

        protected:
            SDeletionFailedResponse m_cacheddeletionfailedresponse;
            unsigned long           m_count;
            QString                 m_file;
            bool                    m_isaborted;
            bool                    m_isskipped;
            bool                    m_isstopped;

            virtual bool fileDelete(const QString& file);
            virtual void doAbort();
            virtual void doSkip();
            virtual void doStop();
            virtual void increaseDeleteCount();
            virtual void resetSkip();

            virtual void emitCheckpoint() = 0;
            virtual void emitDeleted(const QString& file) = 0;
            virtual void emitDeletionFailed(const QString& file, SDeletionFailedResponse* response) = 0;

        };

    } // namespace FileOps
} // namespace Lihat

#endif // CBASEFILEDELETER_H
