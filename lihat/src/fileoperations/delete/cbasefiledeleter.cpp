#include "cbasefiledeleter.h"

#include <QFile>
#include <QThread>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::FileOps;

CBaseFileDeleter::CBaseFileDeleter(const QString& file):
    m_count(0),
    m_file(file),
    m_isaborted(false),
    m_isskipped(false),
    m_isstopped(false)
{

}

CBaseFileDeleter::~CBaseFileDeleter()
{

}

unsigned long CBaseFileDeleter::deletedCount() const
{
    return m_count;
}

void CBaseFileDeleter::doAbort()
{
    m_isaborted = true;
}

void CBaseFileDeleter::doSkip()
{
    m_isskipped = true;
}

void CBaseFileDeleter::doStop()
{
    m_isstopped = true;
}

QString CBaseFileDeleter::file() const
{
    return m_file;
}

bool CBaseFileDeleter::fileDelete(const QString& file)
{
    if (isStopped())
        return false;

    QFile ffile(file);

    if (!ffile.exists())
        return false;

    qDebug() << "CBaseFileDeleter::fileDelete: (" << QThread::currentThreadId() << ")" <<
                "Processing request to delete" << file;

    emitCheckpoint();

    if (ffile.remove())
    {
        increaseDeleteCount();
        emitDeleted(file);
        return true;
    }
    else
    {
        qDebug() << "CBaseFileDeleter::fileDelete:" <<
                    "File deletion for" << ffile.fileName() << "failed!";

        SDeletionFailedResponse response;

        // see if we need to ask for response from user
        bool queryreponse = true;
        if (m_cacheddeletionfailedresponse.RememberResponse &&
            (m_cacheddeletionfailedresponse.Type == SDeletionFailedResponse::DFRRT_SKIP))
        {
            // the situation allows to use cached response, skip emitting copyDestinationExists signal
            queryreponse = false;
            response = m_cacheddeletionfailedresponse;
        }

        if (queryreponse)
        {
            response.Type = SDeletionFailedResponse::DFRRT_ABORT; // default to abort the process
            emitDeletionFailed(ffile.fileName(), &response);

            if (response.RememberResponse)
                m_cacheddeletionfailedresponse = response;
        }

        switch (response.Type)
        {
            case SDeletionFailedResponse::DFRRT_ABORT:
            {
                qDebug() << "CBaseFileDeleter::fileDelete:" <<
                            "Aborting delete process!";
                doAbort();
                return false;

                break;
            }

            case SDeletionFailedResponse::DFRRT_SKIP:
            {
                qDebug() << "CBaseFileDeleter::fileDelete:" <<
                            "Skipping" << file;
                doSkip();
                return true;

                break;
            }

            default:
                return false;
        }
    }
}

void CBaseFileDeleter::increaseDeleteCount()
{
    ++m_count;
}

bool CBaseFileDeleter::isAborted() const
{
    return m_isaborted;
}

bool CBaseFileDeleter::isSkipped() const
{
    return m_isskipped;
}

bool CBaseFileDeleter::isStopped() const
{
    return m_isstopped;
}

void CBaseFileDeleter::resetSkip()
{
    m_isskipped = false;
}

void CBaseFileDeleter::setDeletionFailedResponse(const SDeletionFailedResponse& response)
{
    // this will be useful for CDirectoryDeleter
    m_cacheddeletionfailedresponse = response;
}

void CBaseFileDeleter::start()
{
    fileDelete(m_file);
}
