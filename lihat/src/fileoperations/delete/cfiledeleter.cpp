#include "cfiledeleter.h"


using namespace Lihat;
using namespace Lihat::FileOps;

CFileDeleter::CFileDeleter(const QString& file, QObject* parent):
    QObject(parent),
    CBaseFileDeleter(file)
{

}


CFileDeleter::~CFileDeleter()
{

}

void CFileDeleter::emitCheckpoint()
{
    emit checkpoint();
}

void CFileDeleter::emitDeleted(const QString& file)
{
    emit deleted(file);
}

void CFileDeleter::emitDeletionFailed(const QString& file, SDeletionFailedResponse* response)
{
    emit deletionFailed(file, response);
}

void CFileDeleter::setDeletionFailedResponse(const SDeletionFailedResponse& response)
{
    m_cacheddeletionfailedresponse = response;
}

void CFileDeleter::start()
{
    CBaseFileDeleter::start();
    emit finished();
}

void CFileDeleter::stop()
{
    doStop();
}
