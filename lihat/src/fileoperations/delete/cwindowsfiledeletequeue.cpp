#include "cwindowsfiledeletequeue.h"

#include <windows.h>
#include <Shellapi.h>


using namespace Lihat;

CWindowsFileDeleteQueue::CWindowsFileDeleteQueue(const QStringList& files, QObject* parent):
    CFileOperationItem(parent)
{
    for (int i = 0; i < files.count(); ++i)
        addFile(files.at(i));
}

CWindowsFileDeleteQueue::~CWindowsFileDeleteQueue()
{

}

void CWindowsFileDeleteQueue::addFile(const QString& file)
{
    m_files << file;
}

void CWindowsFileDeleteQueue::start()
{
    if (!m_files.isEmpty())
    {
        SHFILEOPSTRUCTW* shfops = new SHFILEOPSTRUCTW;
        ZeroMemory(shfops, sizeof(SHFILEOPSTRUCTW));

        shfops->fFlags = FOF_ALLOWUNDO;
        shfops->wFunc = FO_DELETE;

        QString files = m_files.join(QChar(QChar::Null));

        shfops->pFrom = (LPCWSTR) calloc(files.length() + 2, sizeof(wchar_t));
        files.toWCharArray((wchar_t*) shfops->pFrom);

        // do the file operation
        SHFileOperationW(shfops);

        free((void*) shfops->pFrom);
        free((void*) shfops->pTo);
        delete shfops;
    }

    emit finished();
}

CFileOperationItem::EOperationType CWindowsFileDeleteQueue::type() const
{
    return OT_DELETE;
}
