#ifndef CWINDOWSFILEDELETEQUEUE_H
#define CWINDOWSFILEDELETEQUEUE_H

#include "../cfileoperationitem.h"

#include <QString>
#include <QStringList>


namespace Lihat
{

    class CWindowsFileDeleteQueue:
        public CFileOperationItem
    {
        Q_OBJECT
    public:
        explicit CWindowsFileDeleteQueue(const QStringList& files, QObject* parent = 0);
        virtual ~CWindowsFileDeleteQueue();

        void                    addFile(const QString& file);
        virtual void            start();
        virtual EOperationType  type() const;

    private:
        QStringList  m_files;
    };

} // namespace Lihat

#endif  // CWINDOWSFILEDELETEQUEUE_H
