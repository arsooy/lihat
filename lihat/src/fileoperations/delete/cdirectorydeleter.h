#ifndef CDIRECTORYDELETER_H
#define CDIRECTORYDELETER_H

#include "cfiledeleter.h"


namespace Lihat
{
    namespace FileOps
    {

        class CDirectoryDeleter:
            public CFileDeleter
        {
            Q_OBJECT
        public:
            explicit CDirectoryDeleter(const QString& file, QObject* parent = 0);

        public slots:
            void  start();

        protected:
            int  m_descendlevel;

            bool  directoryDelete(const QString& dir);
        };

    } // namespace FileOps
} // namespace Lihat

#endif // CDIRECTORYDELETER_H
