#ifndef CBASEFILEMOVER_H
#define CBASEFILEMOVER_H

#include "../copy/cbasefilecopier.h"
#include "cbasemover.h"


namespace Lihat
{
    namespace FileOps
    {

        struct SSourceRemovalFailedResponse
        {
            enum EResponseType
            {
                SRFRT_INVALID   = 0,
                SRFRT_ABORT     = 1,
                SRFRT_SKIP      = 2
            };

            EResponseType Type;
            bool          RememberResponse;

            SSourceRemovalFailedResponse():
                Type(SRFRT_INVALID),
                RememberResponse(false)
            {

            }
        };


        // file copier without slots & signals
        class CBaseFileMover:
            public CBaseFileCopier
        {
        public:
            explicit CBaseFileMover(const QString& source, const QString& copyto);

            void setMoveSourceRemovalFailedResponse(const SSourceRemovalFailedResponse& response);
            void start();

        protected:
            SSourceRemovalFailedResponse  m_cachedsourceremovalfailedresponse;

            virtual void emitSourceRemovalFailed(const QString& source, SSourceRemovalFailedResponse* response) = 0;
            bool         fileMove(const QString& source, const QString& destination);

        };

    } // namespace FileOps
} // namespace Lihat

#endif // CBASEFILEMOVER_H
