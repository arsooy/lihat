#include "cdirectorymover.h"

#include "../../pathutils.h"
#include "cfilemover.h"

#include <QDir>
#include <QDirIterator>
#include <QIODevice>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::FileOps;

CDirectoryMover::CDirectoryMover(const QString& source, const QString& moveto, QObject* parent):
    CFileMover(source, moveto, parent),
    m_descendlevel(-1)
{

}

bool CDirectoryMover::directoryMove(const QString& source, const QString& destination)
{
    if (isStopped())
        return false;

    ++m_descendlevel;
    emitCheckpoint();

    QFileInfo sourcefileinfo(source);
    if (!sourcefileinfo.exists())
        return false;

    QDir destinationdir(Utils::Path::join(destination, sourcefileinfo.fileName()));
    bool destinationexists = destinationdir.exists();
    if (!destinationexists)
    {
        destinationexists = destinationdir.mkpath(destinationdir.absolutePath());
        if (!destinationexists)
        {
            qWarning() << "CDirectoryMover::directoryMove:" <<
                          "Unable to create" << destinationdir.absolutePath();
            return false;
        }
        else
            emitDirectoryCreated(destinationdir.absolutePath());
    }

    // iterate to copy source directory items
    QDirIterator sourcedirectoryiterator(sourcefileinfo.absoluteFilePath(),
                                         QDir::NoDotAndDotDot | QDir::Hidden | QDir::System | QDir::AllEntries | QDir::Readable | QDir::System);
    while (!isStopped() && sourcedirectoryiterator.hasNext())
    {
        sourcedirectoryiterator.next();

        QFileInfo currentsourcefileinfo = sourcedirectoryiterator.fileInfo();

        QString currentsource = currentsourcefileinfo.absoluteFilePath();
        QString currentdestination = destinationdir.absolutePath();
        if (currentsourcefileinfo.isFile())
        {
            int readerrcode = 0;
            int writeerrcode = 0;
            if (!fileMove(currentsource, currentdestination))
            {
                qWarning() << "CDirectoryMover::directoryMove:" <<
                              "Unable to move file" << currentsource << "to" <<  currentdestination <<
                              "(read error code:" << readerrcode << ", write error code:" << writeerrcode << ")";
                return false;
            }
        }
        else if (currentsourcefileinfo.isDir())
        {
            if (!directoryMove(currentsource, currentdestination))
            {
                qWarning() << "CDirectoryMover::directoryMove:" <<
                              "Unable to fully copy" << currentsource << "to" << currentdestination;
                return false;
            }
            --m_descendlevel;
        }
    }

    // remove current directory
    QDir dir;
    if (dir.rmdir(source))
    {
        qDebug() << "CDirectoryMover::directoryMove:" <<
                    source << "removed.";
        increaseTransferCount();
        return true;
    }
    else
    {
        qDebug() << "CDirectoryMover::directoryMove:" <<
                    "Unable to remove" << source;

        SSourceRemovalFailedResponse response;

        // see if we need to ask for response from user
        bool queryresponse = true;
        if (m_cachedsourceremovalfailedresponse.RememberResponse &&
            (m_cachedsourceremovalfailedresponse.Type == SSourceRemovalFailedResponse::SRFRT_SKIP))
        {
            queryresponse = false;
            response = m_cachedsourceremovalfailedresponse;
        }

        if (queryresponse)
        {
            response.Type = SSourceRemovalFailedResponse::SRFRT_ABORT;  // default to abort the process
            emitSourceRemovalFailed(source, &response);

            if (response.RememberResponse)
                m_cachedsourceremovalfailedresponse = response;
        }

        if (response.Type == SSourceRemovalFailedResponse::SRFRT_ABORT)
        {
            qDebug() << "CDirectoryMover::directoryMove:" <<
                        "Aborting move process!";
            doAbort();
            return false;
        }
        else if (response.Type == SSourceRemovalFailedResponse::SRFRT_SKIP)
        {
            qDebug() << "CDirectoryMover::directoryMove:" <<
                        "Skipping removal of" << source;
            doSkip();
            return true;
        }
    }

    return !isStopped();
}

void CDirectoryMover::emitDirectoryCreated(const QString& directory)
{
    emit directoryCreated(directory);
}

void CDirectoryMover::start()
{
    directoryMove(source(), destination());
    emit finished();
}
