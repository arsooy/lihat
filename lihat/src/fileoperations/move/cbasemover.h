#ifndef CBASEMOVER_H
#define CBASEMOVER_H

#include "../copy/cbasecopier.h"


namespace Lihat
{
    namespace FileOps
    {

        class CBaseMover:
            public CBaseCopier
        {
            Q_OBJECT
        public:
            explicit CBaseMover(QObject* parent = 0):
                CBaseCopier(parent)
            {

            }
        };

    } // namespace FileOps
} // namespace Lihat

#endif // CBASEMOVER_H
