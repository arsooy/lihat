#include "cfilemovequeue.h"

#include "cdirectorymover.h"
#include "cfilemover.h"

#include <QFileInfo>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::FileOps;

CFileMoveQueue::CFileMoveQueue(const QStringList& files, const QString& moveto, QObject* parent):
    CFileOperationItem(parent),
    m_currentprogress(0),
    m_longprocessdetectedemitted(false),
    m_destinationexistsresponse(0),
    m_progressmax(0),
    m_sourceremovalfailedresponse(0),
    m_status(QS_IDLE),
    m_stopped(false),
    m_totalmoved(0)
{
    for (int i = 0; i < files.count(); ++i)
        addFile(files.at(i));
    setDestination(moveto);
}

CFileMoveQueue::~CFileMoveQueue()
{
    stop();
    clearProcessedItems();
    dropMoverThread();

    if (m_destinationexistsresponse)
        delete m_destinationexistsresponse;
    m_destinationexistsresponse = 0;
}

void CFileMoveQueue::addFile(const QString& file)
{
    Q_ASSERT(m_status != QS_WORKING);
    m_pendingitems << file;
}

void CFileMoveQueue::addProcessedItem(const QString& source, const QString& destination)
{
    SProcessedItem* pitem = new SProcessedItem(source, destination);
    m_processeditems << pitem;
}

void CFileMoveQueue::clearMovers()
{
    while (m_movers.count() > 0)
    {
        CFileMover* mover = m_movers.takeFirst();
        mover->stop();
        mover->deleteLater();
    }
}

void CFileMoveQueue::clearProcessedItems()
{
    while (m_processeditems.count() > 0)
        delete m_processeditems.takeFirst();
}

QString CFileMoveQueue::destination() const
{
    return m_destination;
}

int CFileMoveQueue::elapsedSinceStart()
{
    if (m_status == QS_WORKING)
        return CFileOperationItem::elapsedSinceStart();
    else
        return m_lastelapsed;
}

void CFileMoveQueue::dropMoverThread()
{
    clearMovers();
    m_moverthread.quit();
    m_moverthread.wait();
}

bool CFileMoveQueue::isMultiple() const
{
    bool result = m_progressmax > 1;
    if (!result && (m_pendingitems.count() == 1))
    {
        // if the only item to be copied is a directory blindly assume that it
        // is a multiple file job
        QFileInfo finfo(m_pendingitems.first());
        result = finfo.isDir();
    }

    return result;
}

void CFileMoveQueue::slotMoverCheckpoint()
{
    if (m_longprocessdetectedemitted)
        return;

    CFileMover* filemover = qobject_cast< CFileMover* >(sender());
    if (filemover && !associatedWidget() && (elapsedSinceStart() >= LONG_PROCESS_TRESHOLD_MSECS))
    {
        m_longprocessdetectedemitted = true;
        emit longProcessDetected();
    }
}

void CFileMoveQueue::slotMoverFinished()
{
    CFileMover* filemover = qobject_cast< CFileMover* >(sender());
    if (filemover)
    {
        if (!filemover->isAborted())
        {
            ++m_currentprogress;
            emit queueProgressed(m_currentprogress, m_progressmax);
        }
        else
            m_status = QS_ABORTED;
        m_movers.removeAll(filemover);

        m_totalmoved += filemover->transferredCount();
    }

    // go for next pending item (if any)
    slotProcessNextPending();
}

void CFileMoveQueue::slotMoverDestinationExists(const QString& source, const QString& destination, SDestinationExistsResponse* response)
{
    emit queueItemDestinationExists(source, destination, response);
    if (response && response->RememberResponse)
    {
        if (!m_destinationexistsresponse)
            m_destinationexistsresponse = new SDestinationExistsResponse();
        *m_destinationexistsresponse = *response;
    }
}

void CFileMoveQueue::slotMoverSourceRemovalFailed(const QString& source, SSourceRemovalFailedResponse* response)
{
    emit queueItemSourceRemovalFailed(source, response);
    if (response && response->RememberResponse)
    {
        if (!m_sourceremovalfailedresponse)
            m_sourceremovalfailedresponse = new SSourceRemovalFailedResponse();
        *m_sourceremovalfailedresponse = *response;
    }
}

void CFileMoveQueue::slotMoverTransferred(const QString& source, const QString& destination)
{
    addProcessedItem(source, destination);

    emit queueItemMoved(source, destination);
}

bool CFileMoveQueue::slotProcessNextPending()
{
    // see if current status is idle or working and we have pending item(s)
    bool hasnext = ((m_status == QS_IDLE) || (m_status == QS_WORKING)) && !m_pendingitems.isEmpty() && !m_stopped;
    if (!hasnext)
    {
        if (m_status != QS_ABORTED)
            m_status = QS_FINISHED;

        // no more processing, drop thread and emit finished
        dropMoverThread();
        emit finished();
        return false;
    }

    while (!m_pendingitems.isEmpty() && !m_stopped)
    {
        QString fname = m_pendingitems.takeFirst();
        QFileInfo finfo = QFileInfo(fname);

        if (!(finfo.exists() && finfo.isReadable()))
        {
            qDebug() << "CFileMoveQueue::slotProcessNextPending" <<
                        finfo.fileName() << "does not exists or readable.";
            continue;
        }

        CFileMover* mover = 0;
        if (finfo.isFile())
            mover = new CFileMover(finfo.absoluteFilePath(), m_destination);
        else if (finfo.isDir())
            mover = new CDirectoryMover(finfo.absoluteFilePath(), m_destination);

        if (mover)
        {
            dropMoverThread();

            qDebug() << "CFileMoveQueue::slotProcessNextPending:" <<
                        "Worker" << mover << "created to move" << finfo.absoluteFilePath() << "to" << m_destination;
            mover->moveToThread(&m_moverthread);

            // apply saved SFileMoverDestinationExistsResponse
            if (m_destinationexistsresponse)
                mover->setDestinationExistsResponse(*m_destinationexistsresponse);
            // apply saved SSourceRemovalFailedResponse
            if (m_sourceremovalfailedresponse)
                mover->setSourceRemovalFailedResponse(*m_sourceremovalfailedresponse);

            // connect to signals of the worker
            connect(mover, SIGNAL(checkpoint()),
                    this, SLOT(slotMoverCheckpoint()));
            connect(mover, SIGNAL(copying(QString, QString, quint64, quint64)),
                    this, SIGNAL(queueItemMoving(QString, QString, quint64, quint64)));
            connect(mover, SIGNAL(destinationExists(QString, QString, SDestinationExistsResponse*)),
                    this, SLOT(slotMoverDestinationExists(QString, QString, SDestinationExistsResponse*)),
                    Qt::BlockingQueuedConnection);
            connect(mover, SIGNAL(finished()),
                    this, SLOT(slotMoverFinished()));
            connect(mover, SIGNAL(readErrOccurred(QString, int, EIOErrResponse*)),
                    this, SIGNAL(queueItemReadError(QString, int, EIOErrResponse*)),
                    Qt::BlockingQueuedConnection);
            connect(mover, SIGNAL(sourceRemovalFailed(QString, SSourceRemovalFailedResponse*)),
                    this, SLOT(slotMoverSourceRemovalFailed(QString, SSourceRemovalFailedResponse*)),
                    Qt::BlockingQueuedConnection);
            connect(mover, SIGNAL(transferred(QString, QString)),
                    this, SLOT(slotMoverTransferred(QString, QString)));
            connect(mover, SIGNAL(writeErrOccurred(QString, int, EIOErrResponse*)),
                    this, SIGNAL(queueItemWriteError(QString, int, EIOErrResponse*)),
                    Qt::BlockingQueuedConnection);

            // start worker when its thread started, and for it to be deleted
            // when its thread finished
            connect(&m_moverthread, SIGNAL(started()),
                    mover, SLOT(start()));
            connect(&m_moverthread, SIGNAL(finished()),
                    mover, SLOT(deleteLater()));

            if (!m_stopped)
            {
                m_movers << mover;
                m_moverthread.start();
                return true; // we're done
            }
            else
                delete mover;
        }
    }

    return false;
}

void CFileMoveQueue::setDestination(const QString& destination)
{
    m_destination = destination;
}

void CFileMoveQueue::start()
{
    CFileOperationItem::start();

    // set some variables
    m_stopped = false;
    m_totalmoved = 0;
    m_currentprogress = 0;
    m_progressmax = m_pendingitems.count();
    m_status = QS_WORKING;

    if (m_progressmax != 0)
    {
        // we have things to do, inform interested parties that we are at
        // the start of the job
        emit queueProgressed(m_currentprogress, m_progressmax);
    }

    // drop previous results, and ..
    clearProcessedItems();
    // start with first item
    slotProcessNextPending();
}

CFileMoveQueue::EQueueStatus CFileMoveQueue::status() const
{
    return m_status;
}

void CFileMoveQueue::stop()
{
    m_stopped = true;

    // stop mover(s)
    for (int i = 0; i < m_movers.count(); ++i)
    {
        CBaseMover* mover = m_movers.at(i);
        mover->stop();
    }

    dropMoverThread();
}

long unsigned int CFileMoveQueue::totalMoved() const
{
    return m_totalmoved;
}

CFileOperationItem::EOperationType CFileMoveQueue::type() const
{
    return CFileOperationItem::OT_MOVE;
}
