#include "ckde4filemovequeue.h"

#include "../../cmainwindow.h"

#include <konq_operations.h>


using namespace Lihat;
using namespace Lihat::FileOps;
using namespace Lihat::FileOps::KDE4;

CKDE4FileMoveQueue::CKDE4FileMoveQueue(const QStringList& files, const QString& moveto, QObject* parent):
    CFileOperationItem(parent)
{
    for (int i = 0; i < files.count(); ++i)
        addFile(files.at(i));
    setDestination(moveto);
}

CKDE4FileMoveQueue::~CKDE4FileMoveQueue()
{

}

void CKDE4FileMoveQueue::addFile(const QString& file)
{
    m_files << file;
}

QString CKDE4FileMoveQueue::destination() const
{
    return m_destination;
}

void CKDE4FileMoveQueue::setDestination(const QString& destination)
{
    m_destination = destination;
}

void CKDE4FileMoveQueue::start()
{
    KUrl::List kurls;

    if (!m_files.isEmpty())
    {
        for (int i = 0; i < m_files.count(); ++i)
            kurls << KUrl(m_files.at(i));
    }

    if (!kurls.isEmpty() && !m_destination.isEmpty())
    {
        KUrl dest(m_destination);

        QWidget* mainwindow = getMainWindow();
        KonqOperations::copy(mainwindow, KonqOperations::MOVE, kurls, dest);
    }

    emit finished();
}

CFileOperationItem::EOperationType CKDE4FileMoveQueue::type() const
{
    return OT_MOVE;
}
