#ifndef CMOVEWIDGET_H
#define CMOVEWIDGET_H

#include "../clistitemwidget.h"

#include <QString>


class QLabel;
class QLineEdit;
class QProgressBar;
class QWidget;

namespace Lihat
{
    namespace FileOps
    {

        class CMoveWidget:
            public CListItemWidget
        {
            Q_OBJECT
        public:
            explicit CMoveWidget(CFileOperationItem* foperation, QWidget* parent = 0);
            virtual ~CMoveWidget();

            virtual void detachFromFileOperation();
            void         setMoveProgress(int pos);
            void         setDestination(const QString& destination);
            void         setSource(const QString& source);

        protected:
            QLabel*       m_lbicon;
            QLabel*       m_lbstatus;
            QLineEdit*    m_ledestination;
            QLineEdit*    m_lesource;
            QProgressBar* m_pbmoveprogress;
            QProgressBar* m_pboverallprogress;

            virtual QWidget* createFileOperationWidget();
            virtual void     stopOperation();

        private slots:
            void slotFileOperationFinished();
            void slotFileOperationQueueItemMoved(const QString& source, const QString& destination);
            void slotFileOperationQueueItemMoving(const QString& source, const QString& destination, quint64 written, quint64 total);
            void slotFileOperationQueueProgressed(int current, int total);

        };

    } // namespace FileOps
} // namespace Lihat

#endif // CMOVEWIDGET_H
