#include "cfilemover.h"

#include <QFile>


using namespace Lihat;
using namespace Lihat::FileOps;

CFileMover::CFileMover(const QString& source, const QString& moveto, QObject* parent):
    CBaseMover(parent),
    CBaseFileMover(source, moveto)
{

}

CFileMover::~CFileMover()
{

}

void CFileMover::emitCheckpoint()
{
    emit checkpoint();
}

void CFileMover::emitFinished()
{
    emit finished();
}

void CFileMover::emitCopying(const QString& source, const QString& destination, quint64 written, quint64 total)
{
    emit copying(source, destination, written, total);
}

void CFileMover::emitDestinationExists(const QString& source, const QString& destination, SDestinationExistsResponse* response)
{
    emit destinationExists(source, destination, response);
}

void CFileMover::emitTransferred(const QString& source, const QString& destination)
{
    emit transferred(source, destination);
}

void CFileMover::emitReadErrOccurred(const QString& source, int error, EIOErrResponse* response)
{
    emit readErrOccurred(source, error, response);
}

void CFileMover::emitSourceRemovalFailed(const QString& source, SSourceRemovalFailedResponse* response)
{
    emit sourceRemovalFailed(source, response);
}

void CFileMover::emitWriteErrOccurred(const QString& destination, int error, EIOErrResponse* response)
{
    emit writeErrOccurred(destination, error, response);
}

void CFileMover::setSourceRemovalFailedResponse(const SSourceRemovalFailedResponse& response)
{
    CBaseFileMover::setMoveSourceRemovalFailedResponse(response);
}

void CFileMover::start()
{
    CBaseFileMover::start();
    emitFinished();
}

void CFileMover::stop()
{
    doStop();
}
