#include "cwindowsfilemovequeue.h"

#include <windows.h>
#include <Shellapi.h>


using namespace Lihat;

CWindowsFileMoveQueue::CWindowsFileMoveQueue(const QStringList& files, const QString& moveto, QObject* parent):
    CFileOperationItem(parent)
{
    for (int i = 0; i < files.count(); ++i)
        addFile(files.at(i));
    setDestination(moveto);
}

CWindowsFileMoveQueue::~CWindowsFileMoveQueue()
{

}

void CWindowsFileMoveQueue::addFile(const QString& file)
{
    m_files << file;
}

QString CWindowsFileMoveQueue::destination() const
{
    return m_destination;
}

void CWindowsFileMoveQueue::setDestination(const QString& destination)
{
    m_destination = destination;
}

void CWindowsFileMoveQueue::start()
{
    if (!m_files.isEmpty() && !m_destination.isEmpty())
    {
        SHFILEOPSTRUCTW* shfops = new SHFILEOPSTRUCTW;
        ZeroMemory(shfops, sizeof(SHFILEOPSTRUCTW));

        shfops->fFlags = FOF_ALLOWUNDO;
        shfops->wFunc = FO_MOVE;

        QString files = m_files.join(QChar(QChar::Null));
        QString destination = m_destination;

        shfops->pFrom = (LPCWSTR) calloc(files.length() + 2, sizeof(wchar_t));
        files.toWCharArray((wchar_t*) shfops->pFrom);

        shfops->pTo = (LPCWSTR) calloc(destination.length() + 2, sizeof(wchar_t));
        destination.toWCharArray((wchar_t*) shfops->pTo);

        // do the file operation
        SHFileOperationW(shfops);

        free((void*) shfops->pFrom);
        free((void*) shfops->pTo);
        delete shfops;
    }

    emit finished();
}

CFileOperationItem::EOperationType CWindowsFileMoveQueue::type() const
{
    return OT_COPY;
}
