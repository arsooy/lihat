#ifndef CDIRECTORYMOVER_H
#define CDIRECTORYMOVER_H

#include "cfilemover.h"


namespace Lihat
{
    namespace FileOps
    {

        class CDirectoryMover:
            public CFileMover
        {
            Q_OBJECT
        public:
            explicit CDirectoryMover(const QString& source, const QString& moveto, QObject* parent = 0);

        signals:
            void  directoryCreated(const QString& directory);

        public slots:
            void  start();

        protected:
            int   m_descendlevel;

            bool          directoryMove(const QString& source, const QString& destination);
            virtual void  emitDirectoryCreated(const QString& directory);
        };

    } // namespace FileOps
} // namespace Lihat

#endif // CDIRECTORYMOVER_H
