#include "cbasefilemover.h"

#include <QtDebug>


using namespace Lihat;
using namespace Lihat::FileOps;

CBaseFileMover::CBaseFileMover(const QString& source, const QString& moveto):
    CBaseFileCopier(source, moveto)
{

}

bool CBaseFileMover::fileMove(const QString& source, const QString& destination)
{
    // a move, of course, is basically a copy followed by remove of the source
    // file

    if (fileCopy(source, destination))
    {
        // if last file is skipped, do NOT delete the source file, as we take
        // "skip" semantic as "skip processing this file", therefore we should
        // not touch it
        if (m_isskipped)
        {
            qDebug() << "CBaseFileMover::fileMove:" <<
                        "Not removing" << source << "because it was skipped from copy.";
            return true;
        }

        if (QFile::remove(source))
        {
            qDebug() << "CBaseFileMover::fileMove:" <<
                        source << "removed.";
            return true;
        }
        else
        {
            qDebug() << "CBaseFileMover::fileMove:" <<
                        "Unable to remove" << source;

            SSourceRemovalFailedResponse response;

            // see if we need to ask for response from user
            bool queryresponse = true;
            if (m_cachedsourceremovalfailedresponse.RememberResponse &&
                (m_cachedsourceremovalfailedresponse.Type == SSourceRemovalFailedResponse::SRFRT_SKIP))
            {
                queryresponse = false;
                response = m_cachedsourceremovalfailedresponse;
            }

            if (queryresponse)
            {
                response.Type = SSourceRemovalFailedResponse::SRFRT_ABORT;  // default to abort the process
                emitSourceRemovalFailed(source, &response);

                if (response.RememberResponse)
                    m_cachedsourceremovalfailedresponse = response;
            }

            if (response.Type == SSourceRemovalFailedResponse::SRFRT_ABORT)
            {
                qDebug() << "CBaseFileMover::fileMove:" <<
                            "Aborting move process!";
                doAbort();
                return false;
            }
            else if (response.Type == SSourceRemovalFailedResponse::SRFRT_SKIP)
            {
                qDebug() << "CBaseFileMover::fileMove:" <<
                            "Skipping removal of" << source;
                doSkip();
                return true;
            }
        }
    }

    return false;
}

void CBaseFileMover::setMoveSourceRemovalFailedResponse(const SSourceRemovalFailedResponse& response)
{
    m_cachedsourceremovalfailedresponse = response;
}

void CBaseFileMover::start()
{
    fileMove(source(), destination());
}
