#ifndef CFILEMOVEQUEUE_H
#define CFILEMOVEQUEUE_H

#include "../cfileoperationitem.h"
#include "../copy/cfilecopyqueue.h"
#include "cfilemover.h"

#include <QString>
#include <QStringList>
#include <QThread>


namespace Lihat
{
    namespace FileOps
    {

        class CFileMoveQueue:
            public CFileOperationItem
        {
            Q_OBJECT
        public:
            enum EQueueStatus
            {
                QS_IDLE     = 0,
                QS_WORKING  = 1,
                QS_ABORTED  = 2,
                QS_FINISHED = 3
            };
            typedef CFileCopyQueue::SProcessedItem  SProcessedItem;

            explicit CFileMoveQueue(const QStringList& files, const QString& moveto, QObject* parent = 0);
            virtual ~CFileMoveQueue();

            void                   addFile(const QString& file);
            QString                destination() const;
            virtual int            elapsedSinceStart();
            bool                   isMultiple() const;
            void                   setDestination(const QString& destination);
            virtual void           start();
            EQueueStatus           status() const;
            virtual void           stop();
            unsigned long          totalMoved() const;
            virtual EOperationType type() const;

        signals:
            void queueItemDestinationExists(const QString& source, const QString& destination, SDestinationExistsResponse* response);
            void queueItemMoved(const QString& source, const QString& destination);
            void queueItemMoving(const QString& source, const QString& destination, quint64 written, quint64 total);
            void queueItemReadError(const QString& source, int error, EIOErrResponse* response);
            void queueItemSourceRemovalFailed(const QString& source, SSourceRemovalFailedResponse* response);
            void queueItemWriteError(const QString& destination, int error, EIOErrResponse* response);
            void queueProgressed(const int current, const int total);

        private:
            int                           m_currentprogress;
            QString                       m_destination;
            bool                          m_longprocessdetectedemitted;
            SDestinationExistsResponse*   m_destinationexistsresponse;
            QThread                       m_moverthread;
            QList< CFileMover* >          m_movers;
            QStringList                   m_pendingitems;
            QList< SProcessedItem* >      m_processeditems;
            int                           m_progressmax;
            SSourceRemovalFailedResponse* m_sourceremovalfailedresponse;
            EQueueStatus                  m_status;
            bool                          m_stopped;
            unsigned long                 m_totalmoved;

            void addProcessedItem(const QString& source, const QString& destination);
            void clearMovers();
            void clearProcessedItems();
            void dropMoverThread();

        private slots:
            void slotMoverCheckpoint();
            void slotMoverDestinationExists(const QString& source, const QString& destination, SDestinationExistsResponse* response);
            void slotMoverFinished();
            void slotMoverSourceRemovalFailed(const QString& source, SSourceRemovalFailedResponse* response);
            void slotMoverTransferred(const QString& source, const QString& destination);
            bool slotProcessNextPending();

        };

    } // namespace FileOps
}  // namespace Lihat

#endif // CFILEMOVEQUEUE_H
