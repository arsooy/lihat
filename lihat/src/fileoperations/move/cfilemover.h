#ifndef CFILEMOVER_H
#define CFILEMOVER_H

#include "../copy/cfilecopier.h"
#include "cbasefilemover.h"
#include "cbasemover.h"

#include <QObject>
#include <QString>


namespace Lihat
{
    namespace FileOps
    {

        class CFileMover:
            public CBaseMover,
            public CBaseFileMover
        {
            Q_OBJECT
        public:
            explicit CFileMover(const QString& source, const QString& moveto, QObject* parent = 0);
            virtual ~CFileMover();

            void setSourceRemovalFailedResponse(const SSourceRemovalFailedResponse& response);

        signals:
            /**
            * @brief Signal emitted when destination item already exists.
            *
            * @param source Source url to copy to destination.
            * @param destination Destination url to move to.
            * @param response Pointer to SFileMoverDestinationExistsReponse structure
            * to take response from user.
            * @return void
            */
            void destinationExists(const QString& source, const QString& destination,
                                   SDestinationExistsResponse* response);
            /**
            * @brief Signal emitted when program unable to remove destination item.
            *
            * @param source Source url to move to destination.
            * @param response Pointer to SFileMoverSourceRemovalFailedResponse
            * structure to take response from user.
            * @return void
            */
            void sourceRemovalFailed(const QString& source, SSourceRemovalFailedResponse* response);

        public slots:
            void start();
            void stop();

        protected:
            // implementing CBaseFileMover (CBaseFileCopier) signals emmitter
            virtual void emitCopying(const QString& source, const QString& destination, quint64 written, quint64 total);
            virtual void emitDestinationExists(const QString& source, const QString& destination, SDestinationExistsResponse* response);
            virtual void emitTransferred(const QString& source, const QString& destination);

            // implementing IBaseCopierEmitter signals emitter
            virtual void emitCheckpoint();
            virtual void emitFinished();
            virtual void emitReadErrOccurred(const QString& source, int error, EIOErrResponse* response);
            virtual void emitSourceRemovalFailed(const QString& source, SSourceRemovalFailedResponse* response);
            virtual void emitWriteErrOccurred(const QString& destination, int error, EIOErrResponse* response);
        };

    } // namespace FileOps
} // namespace Lihat

#endif // CFILEMOVER_H
