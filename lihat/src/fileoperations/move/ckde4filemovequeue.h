#ifndef CKDEFILEMOVEQUEUE_H
#define CKDEFILEMOVEQUEUE_H

#include "../cfileoperationitem.h"

#include <QString>
#include <QStringList>


namespace Lihat
{
    namespace FileOps
    {
        namespace KDE4
        {

            /**
            * @brief File mover that uses KDE 4 API.
            *
            * NOTE: see comment on CKDE4FileCopyQueue.
            */
            class CKDE4FileMoveQueue:
                public CFileOperationItem
            {
                Q_OBJECT
            public:
                explicit CKDE4FileMoveQueue(const QStringList& files, const QString& moveto, QObject* parent = 0);
                virtual ~CKDE4FileMoveQueue();

                void                   addFile(const QString& file);
                QString                destination() const;
                void                   setDestination(const QString& destination);
                virtual void           start();
                virtual EOperationType type() const;

            private:
                QStringList m_files;
                QString     m_destination;

            };

        } // namespace KDE4
    } // namespace FileOps
} // namespace Lihat

#endif // CKDEFILEMOVEQUEUE_H
