#ifndef CWINDOWSFILEMOVEQUEUE_H
#define CWINDOWSFILEMOVEQUEUE_H

#include "../cfileoperationitem.h"

#include <QString>
#include <QStringList>


namespace Lihat
{

    class CWindowsFileMoveQueue:
        public CFileOperationItem
    {
        Q_OBJECT
    public:
        explicit CWindowsFileMoveQueue(const QStringList& files, const QString& moveto, QObject* parent = 0);
        virtual ~CWindowsFileMoveQueue();

        void                    addFile(const QString& file);
        QString                 destination() const;
        void                    setDestination(const QString& destination);
        virtual void            start();
        virtual EOperationType  type() const;

    private:
        QStringList  m_files;
        QString      m_destination;
    };

} // namespace Lihat

#endif  // CWINDOWSFILEMOVEQUEUE_H
