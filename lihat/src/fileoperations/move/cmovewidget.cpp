#include "cmovewidget.h"

#include "cfilemovequeue.h"

#include <QFormLayout>
#include <QIcon>
#include <QLabel>
#include <QLineEdit>
#include <QList>
#include <QObject>
#include <QProgressBar>
#include <QString>
#include <QTime>
#include <QHBoxLayout>


using namespace Lihat;
using namespace Lihat::FileOps;

CMoveWidget::CMoveWidget(CFileOperationItem* foperation, QWidget* parent):
    CListItemWidget(foperation, parent),
    m_lbicon(0),
    m_lbstatus(0),
    m_ledestination(0),
    m_lesource(0),
    m_pbmoveprogress(0),
    m_pboverallprogress(0)
{
    initUserInterface();
}

CMoveWidget::~CMoveWidget()
{

}

QWidget* CMoveWidget::createFileOperationWidget()
{
    QWidget* result = new QWidget(this);
    QHBoxLayout* lyt = new QHBoxLayout(result);

    m_lbicon = new QLabel(this);
    m_lbicon->setObjectName("lbIcon");
    m_lbicon->setPixmap(QIcon::fromTheme("edit-copy", QIcon(":lihat/glyph/edit-copy.png")).pixmap(QSize(48, 48)));
    lyt->addWidget(m_lbicon, 0, Qt::AlignHCenter | Qt::AlignTop);

    QFormLayout* lytwidgets = new QFormLayout();
    lytwidgets->setObjectName("lytFileOperationMoveWidget");

    m_lbstatus = new QLabel(tr("Moving"), result);
    m_lbstatus->setObjectName("lbStatus");
    lytwidgets->addRow(tr("Status:"), m_lbstatus);

    m_lesource = new QLineEdit(result);
    m_lesource->setObjectName("leSource");
    m_lesource->setBackgroundRole(QPalette::Window);
    m_lesource->setReadOnly(true);
    m_lesource->setFrame(false);
    lytwidgets->addRow(tr("Source:"), m_lesource);

    m_ledestination = new QLineEdit(result);
    m_ledestination->setObjectName("leDestination");
    m_ledestination->setBackgroundRole(QPalette::Window);
    m_ledestination->setReadOnly(true);
    m_ledestination->setFrame(false);
    lytwidgets->addRow(tr("Destination:"), m_ledestination);

    m_pbmoveprogress = new QProgressBar(result);
    m_pbmoveprogress->setObjectName("pbMoveProgress");
    m_pbmoveprogress->setRange(0, 0);
    m_pboverallprogress = new QProgressBar(result);
    m_pboverallprogress->setObjectName("pbOverallProgress");
    m_pboverallprogress->setRange(0, 0);

    QVBoxLayout* lytprogress = new QVBoxLayout();
    lytprogress->setObjectName("lytProgress");
    lytprogress->addWidget(m_pbmoveprogress);
    lytprogress->addWidget(m_pboverallprogress);
    lytwidgets->addRow(tr("Progress:"), lytprogress);

    lyt->addLayout(lytwidgets, 1);

    // connect signals from file operation
    CFileMoveQueue* movequeue = qobject_cast< CFileMoveQueue* >(m_fileoperation);
    if (movequeue)
    {
        connect(movequeue, SIGNAL(finished()),
                this, SLOT(slotFileOperationFinished()));
        connect(movequeue, SIGNAL(queueItemMoved(QString, QString)),
                this, SLOT(slotFileOperationQueueItemMoved(QString, QString)));
        connect(movequeue, SIGNAL(queueItemMoving(QString, QString, quint64, quint64)),
                this, SLOT(slotFileOperationQueueItemMoving(QString, QString, quint64, quint64)));
        connect(movequeue, SIGNAL(queueProgressed(int, int)),
                this, SLOT(slotFileOperationQueueProgressed(int, int)));
    }

    return result;
}

void CMoveWidget::detachFromFileOperation()
{
    slotFileOperationFinished();
    CListItemWidget::detachFromFileOperation();
}

void CMoveWidget::slotFileOperationFinished()
{
    CFileMoveQueue* movequeue = qobject_cast< CFileMoveQueue* >(m_fileoperation);
    if (!movequeue)
        return;

    QString operationstatus;
    switch (movequeue->status())
    {
        case CFileMoveQueue::QS_ABORTED:
            operationstatus = tr("Aborted");
            break;
        case CFileMoveQueue::QS_FINISHED:
            operationstatus = tr("Done");
            break;
        case CFileMoveQueue::QS_IDLE:
            operationstatus = tr("Idle");
            break;
        default:
            ;
    }

    if (m_lbstatus && !operationstatus.isEmpty())
        m_lbstatus->setText(operationstatus);
    if (m_ledestination && !movequeue->destination().isEmpty())
        m_ledestination->setText(movequeue->destination());

    QFormLayout* lytwidgets = findChild< QFormLayout* >("lytFileOperationMoveWidget");
    if (!lytwidgets)
        return;

    QVBoxLayout* lytprogress =  lytwidgets->findChild< QVBoxLayout* >("lytProgress");
    if (!lytprogress)
        return;

    QList< QObject* > deletelist;
    deletelist << lytwidgets->labelForField(m_lesource);
    deletelist << m_lesource;
    deletelist << lytwidgets->labelForField(lytprogress);
    deletelist << m_pbmoveprogress;
    deletelist << m_pboverallprogress;
    deletelist << lytprogress;
    while (deletelist.count() > 0)
        deletelist.takeFirst()->deleteLater();

    QTime current = QTime::currentTime();
    int movednum = movequeue->totalMoved();
    QLabel* lbAftermath = new QLabel(tr("<p>Finished at %1  -  <strong>%2</strong> moved.<p>").arg(current.toString("hh:mm AP"),
                                                                                                   QString::number(movednum)));
    lbAftermath->setTextFormat(Qt::RichText);
    lytwidgets->addRow(lbAftermath);
}

void CMoveWidget::slotFileOperationQueueItemMoved(const QString& /* source */, const QString& /* destination */)
{
    if (m_pbmoveprogress)
    {
        m_pbmoveprogress->setRange(0, 100);
        m_pbmoveprogress->setValue(100);
    }
}

void CMoveWidget::slotFileOperationQueueItemMoving(const QString& source, const QString& destination, quint64 written, quint64 total)
{
    if (m_lesource)
        m_lesource->setText(source);
    if (m_ledestination)
        m_ledestination->setText(destination);

    if (m_pbmoveprogress)
    {
        if ((written == 0) && (total == 0))
        {
            if ((m_pbmoveprogress->minimum() != 0) && (m_pbmoveprogress->maximum() != 0))
                m_pbmoveprogress->setRange(0, 0);
        }
        else
        {
            if ((m_pbmoveprogress->minimum() == 0) && (m_pbmoveprogress->maximum() == 0))
                m_pbmoveprogress->setRange(0, 100);

            int percent = int(100 * qBound< float >(0., 1. * written / total, 1.));
            m_pbmoveprogress->setValue(percent);
        }
    }
}

void CMoveWidget::slotFileOperationQueueProgressed(int current, int total)
{
    if (m_pboverallprogress)
    {
        if ((current == 0) && (total == 0))
        {
            if ((m_pboverallprogress->minimum() != 0) && (m_pboverallprogress->maximum() != 0))
                m_pboverallprogress->setRange(0, 0);
        }
        else
        {
            if ((m_pboverallprogress->minimum() == 0) && (m_pboverallprogress->maximum() == 0))
                m_pboverallprogress->setRange(0, 100);

            int percent = int(100 * qBound< float >(0., 1. * current / total, 1.));
            m_pboverallprogress->setValue(percent);
        }
    }
}

void CMoveWidget::setDestination(const QString& destination)
{
    if (!m_ledestination)
        return;

    m_ledestination->setText(destination);
}

void CMoveWidget::setMoveProgress(int pos)
{
    if (!m_pbmoveprogress)
        return;

    m_pbmoveprogress->setValue(pos);
}

void CMoveWidget::setSource(const QString& source)
{
    if (!m_lesource)
        return;

    m_lesource->setText(source);
}

void CMoveWidget::stopOperation()
{
    CFileMoveQueue* movequeue = qobject_cast< CFileMoveQueue* >(m_fileoperation);
    if (!movequeue)
        return;

    movequeue->stop();
}
