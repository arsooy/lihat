#ifndef CFILEOPERATIONSVIEWDIALOG_H
#define CFILEOPERATIONSVIEWDIALOG_H

#include <QDialog>


class QWidget;

namespace Ui
{
    class FileOperationsViewDialog;
}

namespace Lihat
{
    namespace FileOps
    {

        class CListingWidget;
        class CFileOperationHub;

        class CFileOperationsViewDialog:
            public QDialog
        {
            Q_OBJECT
        public:
            explicit CFileOperationsViewDialog(QWidget* parent = 0);
            virtual ~CFileOperationsViewDialog();

            CListingWidget* listingWidget();

        private:
            Ui::FileOperationsViewDialog* const m_ui;
            static CFileOperationsViewDialog*   m_appdialog;

            void initUserInterface();

        private slots:
            void slotSelectedCancelTriggered();

        };

    } // namespace FileOps
} // namespace Lihat

#endif // CFILEOPERATIONSVIEWDIALOG_H
