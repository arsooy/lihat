#include "cfileoperationitem.h"

#include <QWidget>


using namespace Lihat;
using namespace Lihat::FileOps;

CFileOperationItem::CFileOperationItem(QObject* parent):
    QObject(parent),
    m_lastelapsed(0),
    m_associatedwidget(0)
{

}

CFileOperationItem::~CFileOperationItem()
{

}

QWidget* CFileOperationItem::associatedWidget() const
{
    return m_associatedwidget;
}

int CFileOperationItem::elapsedSinceStart()
{
    m_lastelapsed = m_operationstart.elapsed();
    return m_lastelapsed;
}

void CFileOperationItem::setAssociatedWidget(QWidget* widget)
{
    m_associatedwidget = widget;
}

void CFileOperationItem::start()
{
    m_operationstart.start();
}

void CFileOperationItem::stop()
{

}

CFileOperationItem::EOperationType CFileOperationItem::type() const
{
    return OT_INVALID;
}
