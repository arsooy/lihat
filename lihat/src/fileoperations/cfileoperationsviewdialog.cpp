#include "cfileoperationsviewdialog.h"

#include "cfileoperationhub.h"
#include "ui_fileoperationsviewdialog.h"

#include <QAction>
#include <QWidget>


using namespace Lihat;
using namespace Lihat::FileOps;

CFileOperationsViewDialog::CFileOperationsViewDialog(QWidget* parent):
    QDialog(parent),
    m_ui(new(Ui::FileOperationsViewDialog))
{
    initUserInterface();
}

CFileOperationsViewDialog::~CFileOperationsViewDialog()
{
    delete m_ui;
}

void CFileOperationsViewDialog::slotSelectedCancelTriggered()
{

}

void CFileOperationsViewDialog::initUserInterface()
{
    m_ui->setupUi(this);

    QAction* acSelectedCancel = new QAction(this);
    acSelectedCancel->setObjectName("acSelectedCancel");
    acSelectedCancel->setShortcut(QKeySequence(Qt::Key_Alt | Qt::Key_C));
    connect(acSelectedCancel, SIGNAL(triggered(bool)),
            this, SLOT(slotSelectedCancelTriggered()));
    this->addAction(acSelectedCancel);
}

CListingWidget* CFileOperationsViewDialog::listingWidget()
{
    return m_ui->widgetFileOperations;
}
