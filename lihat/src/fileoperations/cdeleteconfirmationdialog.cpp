#include "cdeleteconfirmationdialog.h"
#include "fileoperations/delete/cbasefiledeleter.h"

#include "ui_deleteconfirmationdialog.h"

#include <QPushButton>
#include <QWidget>


using namespace Lihat;
using namespace Lihat::FileOps;

CDeleteConfirmationDialog::CDeleteConfirmationDialog(QWidget* parent):
    QDialog(parent),
    m_ui(new(Ui::DeleteConfirmationDialog))
{
    initUserInterface();
}

CDeleteConfirmationDialog::~CDeleteConfirmationDialog()
{
    delete m_ui;
}

void CDeleteConfirmationDialog::initUserInterface()
{
    m_ui->setupUi(this);

    QSize iconsize = m_ui->labelDialogIcon->size();
    m_ui->labelDialogIcon->setPixmap(QIcon::fromTheme("dialog-warning", QIcon(":lihat/glyph/dialog-warning.png")).pixmap(iconsize));

    QPushButton* pbDelete = m_ui->buttonBox->button(QDialogButtonBox::Yes);
    Q_ASSERT(pbDelete);
    pbDelete->setText(tr("&Delete"));
    pbDelete->setIcon(QIcon::fromTheme("edit-delete", QIcon(":lihat/glyph/edit-delete.png")));
}

unsigned short CDeleteConfirmationDialog::queryResponse(QWidget* parent, const QStringList& items)
{
    CDeleteConfirmationDialog* dlg = new CDeleteConfirmationDialog(parent);
    dlg->setItems(items);

    return (unsigned short)(dlg->exec());
}

bool CDeleteConfirmationDialog::setItems(const QStringList& items)
{
    m_ui->plainTextEditItems->clear();
    for (int i = 0; i < items.count(); ++i)
        m_ui->plainTextEditItems->appendPlainText(items.at(i));

    // update label text based on items number
    if (items.count() == 1)
        m_ui->labelText->setText(tr("Do you really want to delete this item?"));
    else
        m_ui->labelText->setText(tr("Do you really want to delete these %1 items?").arg(QString::number(items.count())));

    return true;
}
