#ifndef CDELETECONFIRMATIONDIALOG_H
#define CDELETECONFIRMATIONDIALOG_H

#include <QDialog>
#include <QStringList>



class QWidget;

namespace Ui
{
    class DeleteConfirmationDialog;
}

namespace Lihat
{
    namespace FileOps
    {

        struct SDeletionFailedResponse;

        class CDeleteConfirmationDialog:
            public QDialog
        {
            Q_OBJECT
        public:
            explicit CDeleteConfirmationDialog(QWidget* parent = 0);
            virtual ~CDeleteConfirmationDialog();

            bool  setItems(const QStringList& items);

            static unsigned short queryResponse(QWidget* parent, const QStringList& items);

        private:
            Ui::DeleteConfirmationDialog* const  m_ui;

            void initUserInterface();
        };

    } // namespace FileOps
} // namespace Lihat

#endif // CDELETECONFIRMATIONDIALOG_H
