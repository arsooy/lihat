#ifndef CFILEOPERATIONITEM_H
#define CFILEOPERATIONITEM_H

#include <QObject>
#include <QTime>


class QWidget;

namespace Lihat
{
    namespace FileOps
    {

        class CFileOperationItem:
            public QObject
        {
            Q_OBJECT

        public:
            enum EOperationType
            {
                OT_INVALID = 0,
                OT_COPY    = 1,
                OT_MOVE    = 2,
                OT_DELETE  = 3
            };

            explicit CFileOperationItem(QObject* parent = 0);
            virtual ~CFileOperationItem();

            QWidget*               associatedWidget() const;
            virtual int            elapsedSinceStart();
            virtual void           setAssociatedWidget(QWidget* widget);
            virtual void           start();
            virtual void           stop();
            virtual EOperationType type() const;

        signals:
            void finished();
            void longProcessDetected();

        protected:
            static const unsigned short LONG_PROCESS_TRESHOLD_MSECS = 2500;
            int m_lastelapsed;

        private:
            QWidget* m_associatedwidget;
            QTime    m_operationstart;
           
        };

    } // namespace FileOps
} // namespace Lihat

#endif // CFILEOPERATIONITEM_H
