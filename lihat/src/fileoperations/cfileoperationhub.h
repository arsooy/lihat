#ifndef CFILEOPERATIONSHUB_H
#define CFILEOPERATIONSHUB_H

#include <QList>
#include <QObject>
#include <QString>
#include <QStringList>
#include <QUrl>


namespace Lihat
{
    namespace FileOps
    {

        class  CFileCopyQueue;
        class  CFileMoveQueue;
        class  CListingWidget;
        class  CFileOperationItem;
        class  CFileOperationsViewDialog;
        struct SDeletionFailedResponse;
        struct SDestinationExistsResponse;
        struct SSourceRemovalFailedResponse;

        class CFileOperationHub:
            public QObject
        {
            Q_OBJECT
        public:
            explicit CFileOperationHub(QObject* parent = 0);
            virtual ~CFileOperationHub();

            CFileOperationItem* createFileCopyOperation(const QStringList& files, const QString& copyto, bool autostart = true);
            CFileOperationItem* createFileDeleteOperation(const QStringList& files, bool autostart = true);
            CFileOperationItem* createFileMoveOperation(const QStringList& files, const QString& moveto, bool autostart = true);
            void                showFileOperationsDialog(QWidget* parentw);
            void                queryCopyMoveItemsTo(QWidget* parent, QList< QUrl > urls, QString itemsrelative = QString(), bool move = false);


        private:
            QList< CFileOperationItem* > m_operations;
            CFileOperationsViewDialog*   m_viewdialog;

            void clearFileOperations();
            int  createAssociatedWidgetsForOperations(CListingWidget* listingwidget);
            bool deleteFileOperation(CFileOperationItem* foperation);

        private slots:
            void slotCopyQueueDestinationExists(const QString& source, const QString& destination, SDestinationExistsResponse* response);
            void slotDeleteQueueDeletionFailed(const QString& file, SDeletionFailedResponse* response);
            void slotFileOperationFinished();
            void slotFileOperationLongProcessDetected();
            void slotMoveQueueDestinationExists(const QString& source, const QString& destination, SDestinationExistsResponse* response);
            void slotMoveQueueSourceRemovalFailed(const QString& source, SSourceRemovalFailedResponse* response);
            void slotViewDialogClosed(int result);

        };

        CFileOperationHub* getGlobalFileOperationHub();

    } // namespace FileOps
} // namespace Lihat

#endif // CFILEOPERATIONSHUB_H
