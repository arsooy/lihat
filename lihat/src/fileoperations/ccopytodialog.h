#ifndef CCOPYTODIALOG_H
#define CCOPYTODIALOG_H

#include <QDialog>
#include <QList>
#include <QUrl>

class QWidget;

namespace Ui
{
    class CopyMoveToDialog;
}

namespace Lihat
{
    namespace FileOps
    {

        class CCopyToDialog:
            public QDialog
        {
            Q_OBJECT
        public:
            enum DialogMode
            {
                EDM_COPYTO = 1,
                EDM_MOVETO = 2
            };


            explicit CCopyToDialog(QWidget* parent = 0);
            virtual ~CCopyToDialog();

            void setDialogMode(DialogMode mode);
            DialogMode dialogMode() const;

            QUrl destination() const;
            void setDestination(const QUrl& desturl);

            void setItems(const QStringList& items);

            void writePersistence();

        private:
            DialogMode 			        m_mode;
            Ui::CopyMoveToDialog* const m_ui;

            void loadPersistence();

        private slots:
            void slotBrowseClicked();
            void slotDestinationEditingFinished();

        };

    }
}

#endif // CCOPYTODIALOG_H
