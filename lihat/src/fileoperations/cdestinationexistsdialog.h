#ifndef CDESTINATIONEXISTSDIALOG_H
#define CDESTINATIONEXISTSDIALOG_H

#include <QDialog>
#include <QString>


class QWidget;

namespace Ui
{
    class CopyMoveDestinationExistsMultiDialog;
}

namespace Lihat
{
    namespace FileOps
    {

        struct SDestinationExistsResponse;

        class CDestinationExistsDialog:
            public QDialog
        {
            Q_OBJECT
        public:
            explicit CDestinationExistsDialog(SDestinationExistsResponse* response, QWidget* parent = 0);
            virtual ~CDestinationExistsDialog();

            bool setDestinationFile(const QString& desturl);
            void setForMultipleFiles(bool multi);
            bool setSourceFile(const QString& sourceurl);

            static unsigned short queryResponse(QWidget* parent, const QString& source, const QString& dest, SDestinationExistsResponse* response, bool multiple = false);

        private:
            QString                                         m_originaldestinationurl;
            SDestinationExistsResponse* const               m_response;
            Ui::CopyMoveDestinationExistsMultiDialog* const m_ui;

            void initUserInterface();

        private slots:
            void slotOverwriteClicked();
            void slotRenameClicked();
            void slotSkipClicked();
            void suggestNewName();
            void updateRenameButton();

        };

        typedef CDestinationExistsDialog CCopyDestinationExistsDialog;
        typedef CDestinationExistsDialog CMoveDestinationExistsDialog;

    } // namespace FileOps
} // namespace Lihat

#endif // CDESTINATIONEXISTSDIALOG_H
