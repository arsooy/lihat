#include "cfilecopyqueue.h"

#include "cdirectorycopier.h"
#include "cfilecopier.h"

#include <QFileInfo>
#include <QWidget>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::FileOps;

CFileCopyQueue::CFileCopyQueue(const QStringList& files, const QString& copyto, QObject* parent):
    CFileOperationItem(parent),
    m_destinationexistsresponse(0),
    m_currentprogress(0),
    m_longprocessdetectedemitted(false),
    m_progressmax(0),
    m_status(QS_IDLE),
    m_stopped(false),
    m_totalcopied(0)
{
    for (int i = 0; i < files.count(); ++i)
        addFile(files.at(i));
    setDestination(copyto);
}

CFileCopyQueue::~CFileCopyQueue()
{
    stop();
    clearProcessedItems();
    dropCopierThread();

    if (m_destinationexistsresponse)
        delete m_destinationexistsresponse;
    m_destinationexistsresponse = 0;
}

void CFileCopyQueue::addFile(const QString& file)
{
    Q_ASSERT(m_status != QS_WORKING);
    m_pendingitems << file;
}

void CFileCopyQueue::addProcessedItem(const QString& source, const QString& destination)
{
    SProcessedItem* pitem = new SProcessedItem(source, destination);
    m_processeditems << pitem;
}

void CFileCopyQueue::slotCopierCheckpoint()
{
    if (m_longprocessdetectedemitted)
        return;

    CFileCopier* filecopier = qobject_cast< CFileCopier* >(sender());
    if (filecopier && !associatedWidget() && (elapsedSinceStart() >= LONG_PROCESS_TRESHOLD_MSECS))
    {
        m_longprocessdetectedemitted = true;
        emit longProcessDetected();
    }
}

void CFileCopyQueue::clearCopiers()
{
    while (m_copiers.count() > 0)
    {
        CFileCopier* copier = m_copiers.takeFirst();
        copier->stop();
        copier->deleteLater();
    }
}

void CFileCopyQueue::clearProcessedItems()
{
    while (m_processeditems.count() > 0)
        delete m_processeditems.takeFirst();
}

void CFileCopyQueue::slotCopierDestinationExists(const QString& source, const QString& destination, SDestinationExistsResponse* response)
{
    emit queueItemDestinationExists(source, destination, response);
    if (response && response->RememberResponse)
    {
        if (!m_destinationexistsresponse)
            m_destinationexistsresponse = new SDestinationExistsResponse();
        *m_destinationexistsresponse = *response;
    }
}

void CFileCopyQueue::slotCopierTransferred(const QString& source, const QString& destination)
{
    addProcessedItem(source, destination);

    emit queueItemCopied(source, destination);
}

void CFileCopyQueue::slotCopierFinished()
{
    CFileCopier* filecopier = qobject_cast< CFileCopier* >(sender());
    if (filecopier)
    {
        if (!filecopier->isAborted())
        {
            ++m_currentprogress;
            emit queueProgressed(m_currentprogress, m_progressmax);
        }
        else
            m_status = QS_ABORTED;
        m_copiers.removeAll(filecopier);

        m_totalcopied += filecopier->transferredCount();
    }

    // go for next pending item (if any)
    slotProcessNextPending();
}

QString CFileCopyQueue::destination() const
{
    return m_destination;
}

void CFileCopyQueue::dropCopierThread()
{
    clearCopiers();
    m_copierthread.quit();
    m_copierthread.wait();
}

int CFileCopyQueue::elapsedSinceStart()
{
    if (m_status == QS_WORKING)
        return CFileOperationItem::elapsedSinceStart();
    else
        return m_lastelapsed;
}

bool CFileCopyQueue::isMultiple() const
{
    bool result = m_progressmax > 1;
    if (!result && (m_pendingitems.count() == 1))
    {
        // if the only item to be copied is a directory blindly assume that it
        // is a multiple file job
        QFileInfo finfo(m_pendingitems.first());
        result = finfo.isDir();
    }

    return result;
}

bool CFileCopyQueue::slotProcessNextPending()
{
    // see if current status is idle or working and we have pending item(s)
    bool hasnext = ((m_status == QS_IDLE) || (m_status == QS_WORKING)) && !m_pendingitems.isEmpty() && !m_stopped;
    if (!hasnext)
    {
        if (m_status != QS_ABORTED)
            m_status = QS_FINISHED;

        // no more processing, drop thread and emit finished
        dropCopierThread();
        emit finished();
        return false;
    }

    while (!m_pendingitems.isEmpty() && !m_stopped)
    {
        QString fname = m_pendingitems.takeFirst();
        QFileInfo finfo = QFileInfo(fname);

        if (!(finfo.exists() && finfo.isReadable()))
        {
            qDebug() << "CFileCopyQueue::slotProcessNextPending" <<
                        finfo.fileName() << "does not exists or readable.";
            continue;
        }

        CFileCopier* copier = 0;
        if (finfo.isFile())
            copier = new CFileCopier(finfo.absoluteFilePath(), m_destination);
        else if (finfo.isDir())
            copier = new CDirectoryCopier(finfo.absoluteFilePath(), m_destination);

        if (copier)
        {
            dropCopierThread();

            qDebug() << "CFileCopyQueue::slotProcessNextPending:" <<
                        "Worker" << copier << "created to copy" << finfo.absoluteFilePath() << "to" << m_destination;
            copier->moveToThread(&m_copierthread);

            // apply saved SFileCopierDestinationExistsResponse
            if (m_destinationexistsresponse)
                copier->setDestinationExistsResponse(*m_destinationexistsresponse);

            // connect to signals of the worker
            connect(copier, SIGNAL(checkpoint()), this, SLOT(slotCopierCheckpoint()));
            connect(copier, SIGNAL(copying(QString, QString, quint64, quint64)),
                    this, SIGNAL(queueItemCopying(QString, QString, quint64, quint64)));
            connect(copier, SIGNAL(destinationExists(QString, QString, SDestinationExistsResponse*)),
                    this, SLOT(slotCopierDestinationExists(QString, QString, SDestinationExistsResponse*)),
                    Qt::BlockingQueuedConnection);
            connect(copier, SIGNAL(finished()),
                    this, SLOT(slotCopierFinished()));
            connect(copier, SIGNAL(readErrOccurred(QString, int, EIOErrResponse*)),
                    this, SIGNAL(queueItemReadError(QString, int, EIOErrResponse*)),
                    Qt::BlockingQueuedConnection);
            connect(copier, SIGNAL(transferred(QString, QString)),
                    this, SLOT(slotCopierTransferred(QString, QString)));
            connect(copier, SIGNAL(writeErrOccurred(QString, int, EIOErrResponse*)),
                    this, SIGNAL(queueItemWriteError(QString, int, EIOErrResponse*)),
                    Qt::BlockingQueuedConnection);

            // start worker when its thread started, and for it to be deleted
            // when its thread finished
            connect(&m_copierthread, SIGNAL(started()),
                    copier, SLOT(start()));
            connect(&m_copierthread, SIGNAL(finished()),
                    copier, SLOT(deleteLater()));

            if (!m_stopped)
            {
                m_copiers << copier;
                m_copierthread.start();
                return true; // we're done
            }
            else
                delete copier;
        }
    }

    return false;
}

void CFileCopyQueue::setDestination(const QString& destination)
{
    m_destination = destination;
}

void CFileCopyQueue::start()
{
    CFileOperationItem::start();

    // set some variables
    m_stopped = false;
    m_totalcopied = 0;
    m_currentprogress = 0;
    m_progressmax = m_pendingitems.count();
    m_status = QS_WORKING;

    if (m_progressmax != 0)
    {
        // we have things to do, inform interested parties that we are at
        // the start of the job
        emit queueProgressed(m_currentprogress, m_progressmax);
    }

    // drop previous results, and ..
    clearProcessedItems();
    // start with first item
    slotProcessNextPending();
}

CFileCopyQueue::EQueueStatus CFileCopyQueue::status() const
{
    return m_status;
}

void CFileCopyQueue::stop()
{
    m_stopped = true;

    // stop copier(s)
    for (int i = 0; i < m_copiers.count(); ++i)
    {
        CBaseCopier* copier = m_copiers.at(i);
        copier->stop();
    }

    dropCopierThread();
}

long unsigned int CFileCopyQueue::totalCopied() const
{
    return m_totalcopied;
}

CFileOperationItem::EOperationType CFileCopyQueue::type() const
{
    return CFileOperationItem::OT_COPY;
}


CFileCopyQueue::SProcessedItem::SProcessedItem(const QString& source, const QString& destination):
    Source(source),
    Destination(destination)
{

}
