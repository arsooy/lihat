#ifndef CFILECOPYQUEUE_H
#define CFILECOPYQUEUE_H

#include "../cfileoperationitem.h"
#include "cfilecopier.h"

#include <QObject>
#include <QString>
#include <QStringList>
#include <QThread>


namespace Lihat
{
    namespace FileOps
    {

        class CFileCopyQueue:
            public CFileOperationItem
        {
            Q_OBJECT
        public:
            enum EQueueStatus
            {
                QS_IDLE     = 0,
                QS_WORKING  = 1,
                QS_ABORTED  = 2,
                QS_FINISHED = 3
            };


            struct SProcessedItem
            {
                QString  Source;
                QString  Destination;

                explicit SProcessedItem(const QString& source, const QString& destination);
            };


            explicit CFileCopyQueue(const QStringList& files, const QString& copyto, QObject* parent = 0);
            virtual ~CFileCopyQueue();

            void                   addFile(const QString& file);
            QString                destination() const;
            virtual int            elapsedSinceStart();
            bool                   isMultiple() const;
            void                   setDestination(const QString& destination);
            virtual void           start();
            EQueueStatus           status() const;
            virtual void           stop();
            unsigned long          totalCopied() const;
            virtual EOperationType type() const;

        signals:
            void queueItemCopied(const QString& source, const QString& destination);
            void queueItemCopying(const QString& source, const QString& destination, quint64 written, quint64 total);
            void queueItemDestinationExists(const QString& source, const QString& destination, SDestinationExistsResponse* response);
            void queueItemReadError(const QString& source, int error, EIOErrResponse* response);
            void queueItemWriteError(const QString& destination, int error, EIOErrResponse* response);
            void queueProgressed(const int current, const int total);

        private:
            SDestinationExistsResponse* m_destinationexistsresponse;
            QThread                     m_copierthread;
            QList< CFileCopier* >       m_copiers;
            int                         m_currentprogress;
            QString                     m_destination;
            bool                        m_longprocessdetectedemitted;
            QStringList                 m_pendingitems;
            QList< SProcessedItem* >    m_processeditems;
            int                         m_progressmax;
            EQueueStatus                m_status;
            bool                        m_stopped;
            unsigned long               m_totalcopied;

            void addProcessedItem(const QString& source, const QString& destination);
            void clearCopiers();
            void clearProcessedItems();
            void dropCopierThread();

        private slots:
            void slotCopierCheckpoint();
            void slotCopierDestinationExists(const QString& source, const QString& destination, SDestinationExistsResponse* response);
            void slotCopierFinished();
            void slotCopierTransferred(const QString& source, const QString& destination);
            bool slotProcessNextPending();

        };

    } // namespace FileOps
} // namespace Lihat

#endif // CFILECOPYQUEUE_H
