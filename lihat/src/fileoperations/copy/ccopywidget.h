#ifndef CCOPYWIDGET_H
#define CCOPYWIDGET_H

#include "../clistitemwidget.h"

#include <QString>


class QLabel;
class QLineEdit;
class QProgressBar;
class QWidget;

namespace Lihat
{
    namespace FileOps
    {

        class CCopyWidget:
            public CListItemWidget
        {
            Q_OBJECT
        public:
            explicit CCopyWidget(CFileOperationItem* foperation, QWidget* parent = 0);
            virtual ~CCopyWidget();

            virtual void detachFromFileOperation();
            void         setCopyProgress(int pos);
            void         setDestination(const QString& destination);
            void         setSource(const QString& source);

        protected:
            QLabel*       m_lbicon;
            QLabel*       m_lbstatus;
            QLineEdit*    m_ledestination;
            QLineEdit*    m_lesource;
            QProgressBar* m_pbcopyprogress;
            QProgressBar* m_pboverallprogress;

            virtual QWidget* createFileOperationWidget();
            virtual void     stopOperation();

        private slots:
            void slotFileOperationFinished();
            void slotFileOperationQueueItemCopied(const QString& source, const QString& destination);
            void slotFileOperationQueueItemCopying(const QString& source, const QString& destination, quint64 written, quint64 total);
            void slotFileOperationQueueProgressed(int current, int total);

        };

    } // namespace FileOps
} // namespace Lihat

#endif // CCOPYWIDGET_H
