#include "cdirectorycopier.h"

#include "../../pathutils.h"

#include <QDirIterator>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::FileOps;

CDirectoryCopier::CDirectoryCopier(const QString& source, const QString& destinationdir, QObject* parent):
    CFileCopier(source, destinationdir, parent),
    m_descendlevel(-1)
{

}

bool CDirectoryCopier::directoryCopy(const QString& source, const QString& destination)
{
    if (isStopped())
        return false;

    ++m_descendlevel;
    emitCheckpoint();

    QFileInfo sourcefileinfo(source);
    if (!sourcefileinfo.exists())
        return false;

    QDir destinationdir(Utils::Path::join(destination, sourcefileinfo.fileName()));
    bool destinationexists = destinationdir.exists();
    if (!destinationexists)
    {
        destinationexists = destinationdir.mkpath(destinationdir.absolutePath());
        if (!destinationexists)
        {
            qWarning() << "CDirectoryCopier::directoryCopy:" <<
                          "Unable to create" << destinationdir.absolutePath();
            return false;
        }
        else
            emitDirectoryCreated(destinationdir.absolutePath());
    }

    // iterate to copy source directory items
    QDirIterator sourcedirectoryiterator(sourcefileinfo.absoluteFilePath(),
                                         QDir::NoDotAndDotDot | QDir::Hidden | QDir::System | QDir::AllEntries | QDir::Readable | QDir::System);
    while (!isStopped() && sourcedirectoryiterator.hasNext())
    {
        sourcedirectoryiterator.next();

        QFileInfo currentsourcefileinfo = sourcedirectoryiterator.fileInfo();

        QString currentsource = currentsourcefileinfo.absoluteFilePath();
        QString currentdestination = destinationdir.absolutePath();
        if (currentsourcefileinfo.isFile())
        {
            int readerrcode = 0;
            int writeerrcode = 0;
            if (!fileCopy(currentsource, currentdestination))
            {
                qWarning() << "CDirectoryCopier::directoryCopy:" <<
                              "Unable to copy file" << currentsource << "to" <<  currentdestination <<
                              "(read error code:" << readerrcode << ", write error code:" << writeerrcode << ")";
                return false;
            }
        }
        else if (currentsourcefileinfo.isDir())
        {
            if (!directoryCopy(currentsource, currentdestination))
            {
                qWarning() << "CDirectoryCopier::directoryCopy:" <<
                              "Unable to fully copy" << currentsource << "to" << currentdestination;
                return false;
            }
            --m_descendlevel;
        }
    }

    increaseTransferCount();
    return true;
}

void CDirectoryCopier::emitDirectoryCreated(const QString& directory)
{
    emit directoryCreated(directory);
}

void CDirectoryCopier::start()
{
    directoryCopy(source(), destination());
    emitFinished();
}
