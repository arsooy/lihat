#include "ckde4filecopyqueue.h"

#include "../../cmainwindow.h"

#include <konq_operations.h>


using namespace Lihat;
using namespace Lihat::FileOps;
using namespace Lihat::FileOps::KDE4;

CKDE4FileCopyQueue::CKDE4FileCopyQueue(const QStringList& files, const QString& copyto, QObject* parent):
    CFileOperationItem(parent)
{
    for (int i = 0; i < files.count(); ++i)
        addFile(files.at(i));
    setDestination(copyto);
}

CKDE4FileCopyQueue::~CKDE4FileCopyQueue()
{

}

void CKDE4FileCopyQueue::addFile(const QString& file)
{
    m_files << file;
}

QString CKDE4FileCopyQueue::destination() const
{
    return m_destination;
}

void CKDE4FileCopyQueue::setDestination(const QString& destination)
{
    m_destination = destination;
}

void CKDE4FileCopyQueue::start()
{
    KUrl::List kurls;

    if (!m_files.isEmpty())
    {
        for (int i = 0; i < m_files.count(); ++i)
            kurls << KUrl(m_files.at(i));
    }

    if (!kurls.isEmpty() && !m_destination.isEmpty())
    {
        KUrl dest(m_destination);

        QWidget* mainwindow = qobject_cast< QWidget* >(getMainWindow());
        KonqOperations::copy(mainwindow, KonqOperations::COPY, kurls, dest);
    }

    emit finished();
    // emit finished right away, because libkonq is a high level API which
    // (in my opinion after looking at the API) seems like a
    // "fire and forget" operation, as in no feedback when it's done, so we
    // should drop out here.
    //
    // after further observation, it looks like when lihat closed while in
    // "transfer" the job gets stalled; moving to use KIO API might allow a
    // stoppage/cancellation prompt for the user, but that seems like more
    // work (heh).
    //
    // amusingly, let's just call it as a feature then: the "closing lihat is
    // another way to abort your file(s) transfers" feature!  ~^_^ v
}

CFileOperationItem::EOperationType CKDE4FileCopyQueue::type() const
{
    return OT_COPY;
}
