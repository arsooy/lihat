#ifndef CWINDOWSFILECOPYQUEUE_H
#define CWINDOWSFILECOPYQUEUE_H

#include "../cfileoperationitem.h"

#include <QString>
#include <QStringList>


namespace Lihat
{

    class CWindowsFileCopyQueue:
        public CFileOperationItem
    {
        Q_OBJECT
    public:
        explicit CWindowsFileCopyQueue(const QStringList& files, const QString& copyto, QObject* parent = 0);
        virtual ~CWindowsFileCopyQueue();

        void                    addFile(const QString& file);
        QString                 destination() const;
        void                    setDestination(const QString& destination);
        virtual void            start();
        virtual EOperationType  type() const;

    private:
        QStringList  m_files;
        QString      m_destination;
    };

} // namespace Lihat

#endif  // CWINDOWSFILECOPYQUEUE_H
