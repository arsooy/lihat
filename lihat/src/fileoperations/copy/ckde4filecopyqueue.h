#ifndef CKDE4FILECOPYQUEUE_H
#define CKDE4FILECOPYQUEUE_H

#include "../cfileoperationitem.h"

#include <QString>
#include <QStringList>


namespace Lihat
{
    namespace FileOps
    {
        namespace KDE4
        {

            /**
            * @brief File copier that uses KDE 4 API.
            *
            * TODO: For now we use LibKonq. Would be nice to use KIO to actually know
            * when it's done and detailed progress which theoretically I can rewire it
            * to behave just like a CFileCopyQueue, but that means fiddling KIO:CopyJob
            * signals which means more work.
            *
            * KDE 4 file copy progress notification looks great though, and with libkonq
            * I already accomplish that, so maybe next time.
            */
            class CKDE4FileCopyQueue:
                public CFileOperationItem
            {
                Q_OBJECT
            public:
                explicit CKDE4FileCopyQueue(const QStringList& files, const QString& copyto, QObject* parent = 0);
                virtual ~CKDE4FileCopyQueue();

                void                   addFile(const QString& file);
                QString                destination() const;
                void                   setDestination(const QString& destination);
                virtual void           start();
                virtual EOperationType type() const;

            private:
                QStringList m_files;
                QString     m_destination;
            };

        } // namespace KDE4
    } // namespace FileOps
} // namespace Lihat

#endif // CKDE4FILECOPYQUEUE_H
