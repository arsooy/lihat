#include "cbasefilecopier.h"

#include "../../pathutils.h"

#include <QDir>
#include <QFileInfo>
#include <QIODevice>
#include <QThread>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::FileOps;

CBaseFileCopier::CBaseFileCopier(const QString& source, const QString& copyto):
    m_count(0),
    m_destinationdir(copyto),
    m_isaborted(false),
    m_isskipped(false),
    m_isstopped(false),
    m_source(source)
{

}

bool CBaseFileCopier::copy(const SCopyParams& params)
{
    if (isStopped() || isAborted())
        return false;

    resetSkip();

    EIOErrResponse ioerruserresponse = EIOErrResponse::IOER_ABORT;

    bool openfail = false;

    if (params.ReadErrorCodePtr)
        *params.ReadErrorCodePtr = 0;
    if (!params.Source->open(params.SourceOpenMode))
    {
        ioerruserresponse = EIOErrResponse::IOER_ABORT;
        if (params.ReadErrorCodePtr)
            *params.ReadErrorCodePtr = params.Source->error();
        emitReadErrOccurred(params.Source->fileName(), params.Source->error(), &ioerruserresponse);

        openfail = (ioerruserresponse == EIOErrResponse::IOER_ABORT);
    }
    if (openfail)
        return false;

    if (params.WriteErrorCodePtr)
        *params.WriteErrorCodePtr = 0;
    if (!params.Destination->open(params.DestinationOpenMode))
    {
        ioerruserresponse = EIOErrResponse::IOER_ABORT;
        if (params.WriteErrorCodePtr)
            *params.WriteErrorCodePtr = params.Destination->error();
        emitWriteErrOccurred(params.Destination->fileName(), params.Destination->error(), &ioerruserresponse);

        openfail = (ioerruserresponse == EIOErrResponse::IOER_ABORT);
    }
    if (openfail)
        return false;

    qDebug() << "CBaseFileCopier::copy:" <<
                params.Source->fileName() << "opened for reading &" << params.Destination->fileName() << "opened for writing.";

    static const unsigned short  BUFFER_SIZE         = 4 * 1024;  // 4KiB
    static const char            COPY_WRITE_STEP_MOD = 10;
    QByteArray buffer(BUFFER_SIZE, Qt::Uninitialized);

    qint64 red = 0;
    qint64 written = 0;
    quint64 total = 0;
    quint64 haswritten = 0;
    bool firstwrite = true;

    if (!params.Source->isSequential())
        total = params.Source->size();

    while ( !(isStopped() || params.Source->atEnd()) )
    {
        red = params.Source->read(buffer.data(), buffer.length());
        if (red == -1)
        {
            ioerruserresponse = EIOErrResponse::IOER_ABORT;
            if (params.ReadErrorCodePtr)
                *params.ReadErrorCodePtr = params.Source->error();
            emitReadErrOccurred(params.Source->fileName(), params.Source->error(), &ioerruserresponse);

            if (ioerruserresponse == EIOErrResponse::IOER_ABORT)
                doStop();

            return false;
        }

        written = params.Destination->write(buffer.data(), red);
        if (written == -1)
        {
            ioerruserresponse = EIOErrResponse::IOER_ABORT;
            if (params.WriteErrorCodePtr)
                *params.WriteErrorCodePtr = params.Destination->error();
            emitWriteErrOccurred(params.Destination->fileName(), params.Destination->error(), &ioerruserresponse);

            if (ioerruserresponse == EIOErrResponse::IOER_ABORT)
                doStop();

            return false;
        }
        else
            haswritten += written;

        if (firstwrite)
        {
            firstwrite = false;
            emitCopying(params.Source->fileName(), params.Destination->fileName(), haswritten, total);
        }
        else if (haswritten % COPY_WRITE_STEP_MOD == 0) // after the first write, space out the 'emit' a little bit
            emitCopying(params.Source->fileName(), params.Destination->fileName(), haswritten, total);

        buffer.clear();
        buffer.resize(BUFFER_SIZE);
        emitCheckpoint();
    }

    return !isStopped();
}

QString CBaseFileCopier::destination() const
{
    return m_destinationdir;
}

bool CBaseFileCopier::fileCopy(const QString& source, const QString& destination)
{
    if (isStopped())
        return false;

    QFileInfo sourcefileinfo(source);
    if (!sourcefileinfo.exists())
        return false;

    qDebug() << "CBaseFileCopier::fileCopy: (" << QThread::currentThreadId() << ")" <<
                "Processing request to copy" << source << "to" << destination;

    emitCheckpoint();

    QDir destinationdir(destination);
    bool destinationexists = destinationdir.exists();
    if (!destinationexists)
    {
        destinationexists = destinationdir.mkpath(destinationdir.absolutePath());
        if (!destinationexists)
        {
            qWarning() << "CBaseFileCopier::fileCopy:" <<
                          "Unable to create" << destinationdir.absolutePath();
            return false;
        }
    }

    bool cancopy = true;
    QFile sourcefile(sourcefileinfo.absoluteFilePath());
    QFile destinationfile(Utils::Path::join(destinationdir.absolutePath(), sourcefileinfo.fileName()));
    SCopyParams copyparams(&sourcefile, &destinationfile);
    destinationexists = destinationfile.exists();
    while (destinationexists && !isStopped())
    {
        qDebug() << "CBaseFileCopier::fileCopy:" <<
                    "Copied-to file" << destinationfile.fileName() << "already exists!";

        SDestinationExistsResponse response;

        // see if we need to ask for response from user
        bool queryreponse = true;
        if (m_cacheddestinationexistsresponse.RememberResponse &&
            ((m_cacheddestinationexistsresponse.Type == SDestinationExistsResponse::DERRT_OVERWRITE) ||
             (m_cacheddestinationexistsresponse.Type == SDestinationExistsResponse::DERRT_SKIP)))
        {
            // the situation allows to use cached response, skip emitting copyDestinationExists signal
            queryreponse = false;
            response = m_cacheddestinationexistsresponse;
        }

        if (queryreponse)
        {
            response.Type = SDestinationExistsResponse::DERRT_ABORT; // default to abort the process
            emitDestinationExists(sourcefileinfo.absoluteFilePath(), destinationfile.fileName(), &response);

            if (response.RememberResponse)
                m_cacheddestinationexistsresponse = response;
        }

        switch (response.Type)
        {
            case SDestinationExistsResponse::DERRT_ABORT:
            {
                qDebug() << "CBaseFileCopier::fileCopy:" <<
                            "Aborting copy process!";
                doAbort();
                return false;

                break;
            }

            case SDestinationExistsResponse::DERRT_OVERWRITE:
            {
                qDebug() << "CBaseFileCopier::fileCopy:" <<
                            "Overwriting file" << destinationfile.fileName();

                copyparams.DestinationOpenMode = copyparams.DestinationOpenMode | QIODevice::Truncate;
                // ignore existence to break out of loop
                destinationexists = false;
                cancopy = true;

                break;
            }

            case SDestinationExistsResponse::DERRT_NEWNAME:
            {
                Q_ASSERT(!response.NewName.isNull());

                qDebug() << "CBaseFileCopier::fileCopy:" <<
                            "Assigning new name" << response.NewName;

                destinationfile.setFileName(Utils::Path::join(destinationdir.absolutePath(), response.NewName));
                // recheck for existence
                destinationexists = destinationfile.exists();

                break;
            }

            case SDestinationExistsResponse::DERRT_SKIP:
            {
                qDebug() << "CBaseFileCopier::fileCopy:" <<
                            "Skipping" << source;
                doSkip();
                return true;

                break;
            }

            default:
                return false;
        }
    }

    if (cancopy && !isStopped())
    {
        if (copy(copyparams))
        {
            qDebug() << "CBaseFileCopier::fileCopy: (" << QThread::currentThreadId() << ")" <<
                        "File" << sourcefile.fileName() << "successfully copied to file" << destinationfile.fileName();
            increaseTransferCount();

            emitTransferred(sourcefile.fileName(), destinationfile.fileName());
            return true;
        }
        else
            qDebug() << "CBaseFileCopier::fileCopy:" <<
                        "Failed to copy" << sourcefile.fileName() << "to file" << destinationfile.fileName();
    }

    return false;
}

void CBaseFileCopier::doAbort()
{
    m_isaborted = true;
}

unsigned long CBaseFileCopier::transferredCount() const
{
    return m_count;
}

void CBaseFileCopier::doSkip()
{
    m_isskipped = true;
}

void CBaseFileCopier::doStop()
{
    m_isstopped = true;
}

void CBaseFileCopier::increaseTransferCount()
{
    ++m_count;
}

bool CBaseFileCopier::isAborted() const
{
    return m_isaborted;
}

bool CBaseFileCopier::isSkipped() const
{
    return m_isskipped;
}

bool CBaseFileCopier::isStopped() const
{
    return m_isstopped;
}

void CBaseFileCopier::resetSkip()
{
    m_isskipped = false;
}

void CBaseFileCopier::setDestinationExistsResponse(const SDestinationExistsResponse& response)
{
    // this will be useful for CDirectoryCopier
    m_cacheddestinationexistsresponse = response;
}

QString CBaseFileCopier::source() const
{
    return m_source;
}

void CBaseFileCopier::start()
{
    fileCopy(m_source, m_destinationdir);
}


CBaseFileCopier::SCopyParams::SCopyParams(QFile* source, QFile* destination):
    Source(source),
    Destination(destination),
    SourceOpenMode(QIODevice::ReadOnly),
    DestinationOpenMode(QIODevice::WriteOnly),
    ReadErrorCodePtr(0),
    WriteErrorCodePtr(0)
{

}
