#ifndef CBASECOPIER_H
#define CBASECOPIER_H

#include <QObject>
#include <QString>


namespace Lihat
{
    namespace FileOps
    {

        enum EIOErrResponse
        {
            IOER_ABORT    = 0,
            IOER_CONTINUE = 1
        };


        class IBaseCopierEmitter
        {
        public:
            virtual ~IBaseCopierEmitter()
            {

            }

            virtual void emitCheckpoint() = 0;
            virtual void emitFinished() = 0;
            virtual void emitReadErrOccurred(const QString& source, int error, EIOErrResponse* response) = 0;
            virtual void emitTransferred(const QString& source, const QString& destination)  = 0;
            virtual void emitWriteErrOccurred(const QString& destination, int error, EIOErrResponse* response) = 0;
        };


        class CBaseCopier:
            public QObject
        {
            Q_OBJECT
        public:
            explicit CBaseCopier(QObject* parent = 0):
                QObject(parent)
            {

            }

        public slots:
            virtual void start() = 0;
            virtual void stop() = 0;

        signals:
            void checkpoint();
            void copying(const QString& source, const QString& destination, quint64 written, quint64 total);
            void finished();
            void readErrOccurred(const QString& source, int error, EIOErrResponse* response);
            void transferred(const QString& source, const QString& destination);
            void writeErrOccurred(const QString& destination, int error, EIOErrResponse* response);

        };

    } // namespace FileOps
} // namespace Lihat

#endif // CBASECOPIER_H
