#include "cfilecopier.h"

#include "../../pathutils.h"

#include <QDir>
#include <QFileInfo>
#include <QThread>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::FileOps;

CFileCopier::CFileCopier(const QString& source, const QString& copyto, QObject* parent):
    CBaseCopier(parent),
    CBaseFileCopier(source, copyto),
    m_readerr(0),
    m_writeerr(0)
{

}

CFileCopier::~CFileCopier()
{

}

void CFileCopier::emitCheckpoint()
{
    emit checkpoint();
}

void CFileCopier::emitDestinationExists(const QString& source, const QString& destination, SDestinationExistsResponse* response)
{
    emit destinationExists(source, destination, response);
}

void CFileCopier::emitTransferred(const QString& source, const QString& destination)
{
    emit transferred(source, destination);
}

void CFileCopier::emitCopying(const QString& source, const QString& destination, quint64 written, quint64 total)
{
    emit copying(source, destination, written, total);
}

void CFileCopier::emitFinished()
{
    emit finished();
}

void CFileCopier::emitReadErrOccurred(const QString& source, int error, EIOErrResponse* response)
{
    emit readErrOccurred(source, error, response);
}

void CFileCopier::emitWriteErrOccurred(const QString& destination, int error, EIOErrResponse* response)
{
    emit writeErrOccurred(destination, error, response);
}

void CFileCopier::setDestinationExistsResponse(const SDestinationExistsResponse& response)
{
    CBaseFileCopier::setDestinationExistsResponse(response);
}

void CFileCopier::start()
{
    CBaseFileCopier::start();
    emitFinished();
}

void CFileCopier::stop()
{
    doStop();
}
