#ifndef CBASEFILECOPIER_H
#define CBASEFILECOPIER_H

#include "cbasecopier.h"

#include <QFile>
#include <QIODevice>


namespace Lihat
{
    namespace FileOps
    {

        struct SDestinationExistsResponse
        {
            enum EResponseType
            {
                DERRT_INVALID   = 0,
                DERRT_ABORT     = 1,
                DERRT_OVERWRITE = 2,
                DERRT_NEWNAME   = 3,
                DERRT_SKIP      = 4
            };

            EResponseType  Type;
            QString        NewName;
            bool           RememberResponse;

            SDestinationExistsResponse():
                Type(DERRT_INVALID),
                RememberResponse(false)
            {

            }
        };


        // basic file copier without slots & signals
        class CBaseFileCopier:
            public IBaseCopierEmitter
        {
        public:
            struct SCopyParams
            {
                QFile* const        Source;
                QFile* const        Destination;
                QIODevice::OpenMode SourceOpenMode;
                QIODevice::OpenMode DestinationOpenMode;
                int*                ReadErrorCodePtr;
                int*                WriteErrorCodePtr;

                SCopyParams(QFile* source, QFile* destination);
            };


            explicit CBaseFileCopier(const QString& source, const QString& copyto);

            QString       destination() const;
            bool          isAborted() const;
            bool          isSkipped() const;
            bool          isStopped() const;
            void          setDestinationExistsResponse(const SDestinationExistsResponse& response);
            QString       source() const;
            void          start();
            unsigned long transferredCount() const;

        protected:
            SDestinationExistsResponse m_cacheddestinationexistsresponse;
            unsigned long              m_count;
            QString                    m_destinationdir;
            bool                       m_isaborted;
            bool                       m_isskipped;
            bool                       m_isstopped;
            QString                    m_source;

            bool         copy(const SCopyParams& params);
            virtual void doAbort();
            virtual void doSkip();
            virtual void doStop();
            bool         fileCopy(const QString& source, const QString& destination);
            virtual void increaseTransferCount();
            virtual void resetSkip();

            // these virtual members should be fully implemented by subclasses having the SIGNALS
            virtual void emitCopying(const QString& source, const QString& destination, quint64 written, quint64 total) = 0;
            virtual void emitDestinationExists(const QString& source, const QString& destination, SDestinationExistsResponse* response) = 0;
            virtual void emitTransferred(const QString& source, const QString& destination) = 0;
        };

    } // namespace FileOps
} // namespace Lihat

#endif // CBASEFILECOPIER_H
