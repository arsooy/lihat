#ifndef CFILECOPIER_H
#define CFILECOPIER_H

#include "cbasecopier.h"
#include "cbasefilecopier.h"

#include <QFile>
#include <QIODevice>
#include <QObject>
#include <QString>


namespace Lihat
{
    namespace FileOps
    {

        class CFileCopier:
            public CBaseCopier,
            public CBaseFileCopier
        {
            Q_OBJECT
        public:
            explicit CFileCopier(const QString& source, const QString& copyto, QObject* parent = 0);
            virtual ~CFileCopier();

            void setDestinationExistsResponse(const SDestinationExistsResponse& response);

        public slots:
            void start();
            void stop();

        signals:
            /**
            * @brief Signal emitted when destination item already exists.
            *
            * @param source Source url to copy to destination.
            * @param destination Destination url to copy to.
            * @param response Pointer to SFileCopierDestinationExistsResponse structure
            * to take response from user.
            * @return void
            */
            void destinationExists(const QString& source, const QString& destination,
                                   SDestinationExistsResponse* response);

        protected:
            int* m_readerr;
            int* m_writeerr;

            // implementing CBaseFileCopier signals emmitter
            virtual void emitCopying(const QString& source, const QString& destination, quint64 written, quint64 total);
            virtual void emitDestinationExists(const QString& source, const QString& destination, SDestinationExistsResponse* response);
            virtual void emitTransferred(const QString& source, const QString& destination);

            // implementing IBaseCopierEmitter signals emitter
            virtual void emitCheckpoint();
            virtual void emitFinished();
            virtual void emitReadErrOccurred(const QString& source, int error, EIOErrResponse* response);
            virtual void emitWriteErrOccurred(const QString& destination, int error, EIOErrResponse* response);
        };


    } // namespace FileOps
} // namespace Lihat

#endif // CFILECOPIER_H
