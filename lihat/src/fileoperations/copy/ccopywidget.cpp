#include "ccopywidget.h"

#include "cfilecopyqueue.h"

#include <QFormLayout>
#include <QHBoxLayout>
#include <QIcon>
#include <QLabel>
#include <QLineEdit>
#include <QProgressBar>
#include <QTime>
#include <QVBoxLayout>
#include <QWidget>


using namespace Lihat;
using namespace Lihat::FileOps;

CCopyWidget::CCopyWidget(CFileOperationItem* foperation, QWidget* parent):
    CListItemWidget(foperation, parent),
    m_lbicon(0),
    m_lbstatus(0),
    m_ledestination(0),
    m_lesource(0),
    m_pbcopyprogress(0),
    m_pboverallprogress(0)
{
    initUserInterface();
}

CCopyWidget::~CCopyWidget()
{

}

QWidget* CCopyWidget::createFileOperationWidget()
{
    QWidget* result = new QWidget(this);
    QHBoxLayout* lyt = new QHBoxLayout(result);

    m_lbicon = new QLabel(this);
    m_lbicon->setObjectName("lbIcon");
    m_lbicon->setPixmap(QIcon::fromTheme("edit-copy", QIcon(":lihat/glyph/edit-copy.png")).pixmap(QSize(48, 48)));
    lyt->addWidget(m_lbicon, 0, Qt::AlignHCenter | Qt::AlignTop);

    QFormLayout* lytwidgets = new QFormLayout();
    lytwidgets->setObjectName("lytFileOperationCopyWidget");

    m_lbstatus = new QLabel(tr("Copying"), result);
    m_lbstatus->setObjectName("lbStatus");
    lytwidgets->addRow(tr("Status:"), m_lbstatus);

    m_lesource = new QLineEdit(result);
    m_lesource->setObjectName("leSource");
    m_lesource->setBackgroundRole(QPalette::Window);
    m_lesource->setReadOnly(true);
    m_lesource->setFrame(false);
    lytwidgets->addRow(tr("Source:"), m_lesource);

    m_ledestination = new QLineEdit(result);
    m_ledestination->setObjectName("leDestination");
    m_ledestination->setBackgroundRole(QPalette::Window);
    m_ledestination->setReadOnly(true);
    m_ledestination->setFrame(false);
    lytwidgets->addRow(tr("Destination:"), m_ledestination);

    m_pbcopyprogress = new QProgressBar(result);
    m_pbcopyprogress->setObjectName("pbCopyProgress");
    m_pbcopyprogress->setRange(0, 0);
    m_pboverallprogress = new QProgressBar(result);
    m_pboverallprogress->setObjectName("pbOverallProgress");
    m_pboverallprogress->setRange(0, 0);

    QVBoxLayout* lytprogress = new QVBoxLayout();
    lytprogress->setObjectName("lytProgress");
    lytprogress->addWidget(m_pbcopyprogress);
    lytprogress->addWidget(m_pboverallprogress);
    lytwidgets->addRow(tr("Progress:"), lytprogress);

    lyt->addLayout(lytwidgets, 1);

    // connect signals from file operation
    CFileCopyQueue* copyqueue = qobject_cast< CFileCopyQueue* >(m_fileoperation);
    if (copyqueue)
    {
        connect(copyqueue, SIGNAL(finished()),
                this, SLOT(slotFileOperationFinished()));
        connect(copyqueue, SIGNAL(queueItemCopied(QString, QString)),
                this, SLOT(slotFileOperationQueueItemCopied(QString, QString)));
        connect(copyqueue, SIGNAL(queueItemCopying(QString, QString, quint64, quint64)),
                this, SLOT(slotFileOperationQueueItemCopying(QString, QString, quint64, quint64)));
        connect(copyqueue, SIGNAL(queueProgressed(int, int)),
                this, SLOT(slotFileOperationQueueProgressed(int, int)));
    }

    return result;
}

void CCopyWidget::detachFromFileOperation()
{
    slotFileOperationFinished();
    CListItemWidget::detachFromFileOperation();
}

void CCopyWidget::slotFileOperationFinished()
{
    CFileCopyQueue* copyqueue = qobject_cast< CFileCopyQueue* >(m_fileoperation);
    if (!copyqueue)
        return;

    QString operationstatus;
    switch (copyqueue->status())
    {
        case CFileCopyQueue::QS_ABORTED:
            operationstatus = tr("Aborted");
            break;
        case CFileCopyQueue::QS_FINISHED:
            operationstatus = tr("Done");
            break;
        case CFileCopyQueue::QS_IDLE:
            operationstatus = tr("Idle");
            break;
        default:
            ;
    }

    if (m_lbstatus && !operationstatus.isEmpty())
        m_lbstatus->setText(operationstatus);
    if (m_ledestination && !copyqueue->destination().isEmpty())
        m_ledestination->setText(copyqueue->destination());

    QFormLayout* lytwidgets = findChild< QFormLayout* >("lytFileOperationCopyWidget");
    if (!lytwidgets)
        return;

    QVBoxLayout* lytprogress =  lytwidgets->findChild< QVBoxLayout* >("lytProgress");
    if (!lytprogress)
        return;

    QList< QObject* > deletelist;
    deletelist << lytwidgets->labelForField(m_lesource);
    deletelist << m_lesource;
    deletelist << lytwidgets->labelForField(lytprogress);
    deletelist << m_pbcopyprogress;
    deletelist << m_pboverallprogress;
    deletelist << lytprogress;
    while (deletelist.count() > 0)
        deletelist.takeFirst()->deleteLater();

    QTime current = QTime::currentTime();
    int copiednum = copyqueue->totalCopied();
    QLabel* lbAftermath = new QLabel(tr("<p>Finished at %1  -  <strong>%2</strong> copied.<p>").arg(current.toString("hh:mm AP"),
                                                                                                    QString::number(copiednum)));
    lbAftermath->setTextFormat(Qt::RichText);
    lytwidgets->addRow(lbAftermath);
}

void CCopyWidget::slotFileOperationQueueItemCopied(const QString& /* source */, const QString& /* destination */)
{
    if (m_pbcopyprogress)
    {
        m_pbcopyprogress->setRange(0, 100);
        m_pbcopyprogress->setValue(100);
    }
}

void CCopyWidget::slotFileOperationQueueItemCopying(const QString& source, const QString& destination, quint64 written, quint64 total)
{
    if (m_lesource)
        m_lesource->setText(source);
    if (m_ledestination)
        m_ledestination->setText(destination);

    if (m_pbcopyprogress)
    {
        if ((written == 0) && (total == 0))
        {
            if ((m_pbcopyprogress->minimum() != 0) && (m_pbcopyprogress->maximum() != 0))
                m_pbcopyprogress->setRange(0, 0);
        }
        else
        {
            if ((m_pbcopyprogress->minimum() == 0) && (m_pbcopyprogress->maximum() == 0))
                m_pbcopyprogress->setRange(0, 100);

            int percent = int(100 * qBound< float >(0., 1. * written / total, 1.));
            m_pbcopyprogress->setValue(percent);
        }
    }
}

void CCopyWidget::slotFileOperationQueueProgressed(int current, int total)
{
    if (m_pboverallprogress)
    {
        if ((current == 0) && (total == 0))
        {
            if ((m_pboverallprogress->minimum() != 0) && (m_pboverallprogress->maximum() != 0))
                m_pboverallprogress->setRange(0, 0);
        }
        else
        {
            if ((m_pboverallprogress->minimum() == 0) && (m_pboverallprogress->maximum() == 0))
                m_pboverallprogress->setRange(0, 100);

            int percent = int(100 * qBound< float >(0., 1. * current / total, 1.));
            m_pboverallprogress->setValue(percent);
        }
    }
}

void CCopyWidget::setCopyProgress(int pos)
{
    if (!m_pbcopyprogress)
        return;

    m_pbcopyprogress->setValue(pos);
}

void CCopyWidget::setDestination(const QString& destination)
{
    if (!m_ledestination)
        return;

    m_ledestination->setText(destination);
}

void CCopyWidget::setSource(const QString& source)
{
    if (!m_lesource)
        return;

    m_lesource->setText(source);
}

void CCopyWidget::stopOperation()
{
    CFileCopyQueue* copyqueue = qobject_cast< CFileCopyQueue* >(m_fileoperation);
    if (!copyqueue)
        return;

    copyqueue->stop();
}
