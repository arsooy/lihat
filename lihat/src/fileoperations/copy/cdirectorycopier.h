#ifndef CDIRECTORYCOPIER_H
#define CDIRECTORYCOPIER_H

#include "cfilecopier.h"

#include <QObject>


namespace Lihat
{
    namespace FileOps
    {

        class CDirectoryCopier:
            public CFileCopier
        {
            Q_OBJECT
        public:
            explicit CDirectoryCopier(const QString& source, const QString& destinationdir, QObject* parent = 0);

        signals:
            void directoryCreated(const QString& directory);

        public slots:
            void start();

        protected:
            int  m_descendlevel;

            bool         directoryCopy(const QString& source, const QString& destination);
            virtual void emitDirectoryCreated(const QString& directory);
        };

    } // namespace FileOps
} // namespace Lihat

#endif // CDIRECTORYCOPIER_H
