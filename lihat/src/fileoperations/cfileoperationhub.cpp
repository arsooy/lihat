#include "cfileoperationhub.h"

#include "../cmainwindow.h"
#include "config_lihat.h"

#include "../pathutils.h"
#include "ccopytodialog.h"
#include "cdeleteconfirmationdialog.h"
#include "cdestinationexistsdialog.h"
#include "cfileoperationsviewdialog.h"
#include "clistingwidget.h"
#include "clistitemwidget.h"
#include "copy/cfilecopyqueue.h"
#include "delete/cfiledeletequeue.h"
#include "move/cfilemovequeue.h"

#include <QAbstractButton>
#include <QDialogButtonBox>
#include <QDir>
#include <QFileInfo>
#include <QMessageBox>
#include <QThread>
#include <QVBoxLayout>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::FileOps;

CFileOperationHub::CFileOperationHub(QObject* parent):
    QObject(parent),
    m_viewdialog(0)
{

}

CFileOperationHub::~CFileOperationHub()
{
    clearFileOperations();
}

void CFileOperationHub::slotCopyQueueDestinationExists(const QString& source, const QString& destination, SDestinationExistsResponse* response)
{
    qDebug() << "CFileOperationHub::slotCopyQueueDestinationExists: (" << QThread::currentThreadId() << ")" <<
                "File operation" << sender() << "wants user response for resolving existing copy destination:" << destination;

    CFileCopyQueue* copyqueue = qobject_cast< CFileCopyQueue* >(sender());
    bool ismultiple = copyqueue->isMultiple();

    CMainWindow* mainwindow = getMainWindow();
    if (!mainwindow)
    {
        qWarning() << "CFileOperationHub::slotCopyQueueDestinationExists:" <<
                      "No main window detected, unable to get user's response.";
        return;
    }

    response->Type = SDestinationExistsResponse::DERRT_ABORT;
    CCopyDestinationExistsDialog::queryResponse(mainwindow, source, destination, response, ismultiple);
}

CFileOperationItem* CFileOperationHub::createFileCopyOperation(const QStringList& files, const QString& copyto, bool autostart)
{
    qDebug() << "CFileOperationHub::createFileCopyOperation:" <<
                "Copying" << files << " to " << copyto;

    CFileCopyQueue* result = new CFileCopyQueue(files, copyto, this);
    // connect to signals
    connect(result, SIGNAL(finished()),
            this, SLOT(slotFileOperationFinished()));
    connect(result, SIGNAL(longProcessDetected()),
            this, SLOT(slotFileOperationLongProcessDetected()));
    connect(result, SIGNAL(queueItemDestinationExists(QString, QString, SDestinationExistsResponse*)),
            this, SLOT(slotCopyQueueDestinationExists(QString, QString, SDestinationExistsResponse*)));

    m_operations << result;
    if (autostart)
        result->start();

    return result;
}

CFileOperationItem* CFileOperationHub::createFileDeleteOperation(const QStringList& files, bool autostart)
{
    qDebug() << "CFileOperationHub::createFileDeleteOperation:" <<
                "Deleting" << files;

    // show dialog to user for confirmation
    CMainWindow* mainwindow = getMainWindow();
    Q_ASSERT(mainwindow);
    if (CDeleteConfirmationDialog::queryResponse(mainwindow, files) != (unsigned short)(QDialog::Accepted))
        return 0;

    CFileDeleteQueue* result = new CFileDeleteQueue(files, this);
    // connect to signals
    connect(result, SIGNAL(finished()),
            this, SLOT(slotFileOperationFinished()));
    connect(result, SIGNAL(longProcessDetected()),
            this, SLOT(slotFileOperationLongProcessDetected()));
    connect(result, SIGNAL(queueItemDeletionFailed(QString, SDeletionFailedResponse*)),
            this, SLOT(slotDeleteQueueDeletionFailed(QString, SDeletionFailedResponse*)));

    m_operations << result;
    if (autostart)
        result->start();

    return result;
}

CFileOperationItem* CFileOperationHub::createFileMoveOperation(const QStringList& files, const QString& moveto, bool autostart)
{
    qDebug() << "CFileOperationHub::createFileMoveOperation:" <<
                "Moving" << files << " to " << moveto;

    CFileMoveQueue* result = new CFileMoveQueue(files, moveto, this);
    // connect to signals
    connect(result, SIGNAL(finished()),
            this, SLOT(slotFileOperationFinished()));
    connect(result, SIGNAL(longProcessDetected()),
            this, SLOT(slotFileOperationLongProcessDetected()));
    connect(result, SIGNAL(queueItemDestinationExists(QString, QString, SDestinationExistsResponse*)),
            this, SLOT(slotMoveQueueDestinationExists(QString, QString, SDestinationExistsResponse*)));
    connect(result, SIGNAL(queueItemSourceRemovalFailed(QString, SSourceRemovalFailedResponse*)),
            this, SLOT(slotMoveQueueSourceRemovalFailed(QString, SSourceRemovalFailedResponse*)));

    m_operations << result;
    if (autostart)
        result->start();

    return result;
}

void CFileOperationHub::slotDeleteQueueDeletionFailed(const QString& file, SDeletionFailedResponse* response)
{
    qDebug() << "CFileOperationHub::slotDeleteQueueDeletionFailed: (" << QThread::currentThreadId() << ")" <<
                "File operation" << sender() << "wants user response for resolving a failed file removal:" << file;

    CFileDeleteQueue* deletequeue = qobject_cast< CFileDeleteQueue* >(sender());

    CMainWindow* mainwindow = getMainWindow();
    if (!mainwindow)
    {
        qWarning() << "CFileOperationHub::slotDeleteQueueDeletionFailed:" <<
                      "No main window detected, unable to get user's response.";
        return;
    }

    response->Type = SDeletionFailedResponse::DFRRT_ABORT;
    QMessageBox::StandardButtons buttons;
    if (deletequeue->isLast())
        buttons = QMessageBox::Ok;
    else
    {
        buttons = QMessageBox::Yes | QMessageBox::Abort;
        if (deletequeue->isMultiple())
            buttons |= QMessageBox::YesToAll;
    }

    QMessageBox msgbox(QMessageBox::NoIcon,
                       tr("Delete Failed"),
                       tr("Unable to remove file:%1%2").arg(QString(QChar::LineSeparator), file),
                       buttons, mainwindow);

    QAbstractButton* button = msgbox.button(QMessageBox::YesToAll);
    if (button)
        button->setText(tr("Auto-skip"));

    button = msgbox.button(QMessageBox::Yes);
    if (button)
        button->setText(tr("Skip"));

    msgbox.setDefaultButton(QMessageBox::Yes);
    int userinput = msgbox.exec();
    if ((userinput == QMessageBox::YesToAll) || (userinput == QMessageBox::Yes))
    {
        response->Type = SDeletionFailedResponse::DFRRT_SKIP;
        response->RememberResponse = userinput == QMessageBox::YesToAll;
    }
}

void CFileOperationHub::slotFileOperationFinished()
{
    qDebug() << "CFileOperationHub::slotFileOperationFinished:" <<
                sender() << "is finished.";

    CFileOperationItem* foperation = qobject_cast< CFileOperationItem* >(sender());
    if (foperation && m_viewdialog)
    {
        CListItemWidget* associatedwidget = qobject_cast< CListItemWidget* >(foperation->associatedWidget());
        if (associatedwidget)
            associatedwidget->detachFromFileOperation();
    }

    deleteFileOperation(foperation);
}

void CFileOperationHub::slotFileOperationLongProcessDetected()
{
    if (!m_viewdialog)
        return;

    CFileOperationItem* foperation = qobject_cast< CFileOperationItem* >(sender());
    if (!foperation)
        return;

    qDebug() << "CFileOperationHub::slotFileOperationLongProcessDetected:" <<
                foperation << "is taking too long to complete, creating its widget for user.";

    // create a widget to inform user on the operation
    CListingWidget* listingwidget = m_viewdialog->listingWidget();
    if (listingwidget)
    {
        foperation->setAssociatedWidget(listingwidget->createFileOperationWidget(foperation));
        if (foperation->associatedWidget())
            listingwidget->addWidget(foperation->associatedWidget());
    }
}

void CFileOperationHub::slotMoveQueueDestinationExists(const QString& source, const QString& destination, SDestinationExistsResponse* response)
{
    qDebug() << "CFileOperationHub::slotMoveQueueDestinationExists: (" << QThread::currentThreadId() << ")" <<
                "File operation" << sender() << "wants user response for resolving existing move destination:" << destination;

    CFileMoveQueue* movequeue = qobject_cast< CFileMoveQueue* >(sender());
    bool ismultiple = movequeue->isMultiple();

    CMainWindow* mainwindow = getMainWindow();
    if (!mainwindow)
    {
        qWarning() << "CFileOperationHub::slotMoveQueueDestinationExists:" <<
                      "No main window detected, unable to get user's response.";
        return;
    }

    response->Type = SDestinationExistsResponse::DERRT_ABORT;
    CMoveDestinationExistsDialog::queryResponse(mainwindow, source, destination, response, ismultiple);
}

void CFileOperationHub::slotMoveQueueSourceRemovalFailed(const QString& source, SSourceRemovalFailedResponse* response)
{
    qDebug() << "CFileOperationHub::sourceRemovalFailed: (" << QThread::currentThreadId() << ")" <<
                "File operation" << sender() << "wants user response for resolving a failed file removal:" << source;

    CMainWindow* mainwindow = getMainWindow();
    if (!mainwindow)
    {
        qWarning() << "CFileOperationHub::sourceRemovalFailed:" <<
                      "No main window detected, unable to get user's response.";
        return;
    }

    response->Type = SSourceRemovalFailedResponse::SRFRT_ABORT;
    QMessageBox msgbox(QMessageBox::NoIcon,
                       tr("Move Failed"),
                       tr("Unable to remove source file:%1%2").arg(QString(QChar::LineSeparator), source),
                       QMessageBox::YesToAll | QMessageBox::Yes | QMessageBox::Abort,
                       mainwindow);

    QAbstractButton* button = msgbox.button(QMessageBox::YesToAll);
    if (button)
        button->setText(tr("Auto-skip"));

    button = msgbox.button(QMessageBox::Yes);
    if (button)
        button->setText(tr("Skip"));

    msgbox.setDefaultButton(QMessageBox::Yes);
    int userinput = msgbox.exec();
    if ((userinput == QMessageBox::YesToAll) || (userinput == QMessageBox::Yes))
    {
        response->Type = SSourceRemovalFailedResponse::SRFRT_SKIP;
        response->RememberResponse = userinput == QMessageBox::YesToAll;
    }
}

void CFileOperationHub::showFileOperationsDialog(QWidget* parentw)
{
    if (!m_viewdialog)
    {
        m_viewdialog = new CFileOperationsViewDialog(parentw);
        createAssociatedWidgetsForOperations(m_viewdialog->listingWidget());

        connect(m_viewdialog, SIGNAL(finished(int)),
                this, SLOT(slotViewDialogClosed(int)));
    }

    m_viewdialog->show();
    m_viewdialog->raise();
    m_viewdialog->activateWindow();
}

void CFileOperationHub::queryCopyMoveItemsTo(QWidget* parent, QList< QUrl > urls, QString itemsrelative, bool move)
{
    CCopyToDialog dlg(parent);

    QStringList inames;
    if (!itemsrelative.isEmpty())
    {
        QDir reldir(itemsrelative);
        if (reldir.exists())
        {
            for (int i = 0; i < urls.count(); ++i)
            {
                QUrl url = urls.at(i);
                if (url.isLocalFile())
                    inames << reldir.relativeFilePath(url.toLocalFile());
            }
        }
    }
    else
        inames = Utils::Path::urlsToStringList(urls);
    inames.removeDuplicates();
    inames.removeAll("");
    if (inames.isEmpty())
        return;

    dlg.setItems(inames);
    if (move)
        dlg.setDialogMode(CCopyToDialog::EDM_MOVETO);
    else
        dlg.setDialogMode(CCopyToDialog::EDM_COPYTO);

    if (dlg.exec() != QDialog::Accepted)
        return;

    QUrl desturl = dlg.destination();
    if (desturl.isLocalFile() && QFileInfo(desturl.toLocalFile()).isDir())
    {
        QStringList fnames;
        for (int i = 0; i < urls.count(); ++i)
        {
            QUrl url = urls.at(i);
            if (url.isLocalFile())
                fnames << urls.at(i).toLocalFile();
        }

        if (!fnames.isEmpty())
        {
            if (move)
                createFileMoveOperation(fnames, desturl.toLocalFile());
            else
                createFileCopyOperation(fnames, desturl.toLocalFile());

            // write destination history for next time
            dlg.writePersistence();
        }
    }
}

void CFileOperationHub::clearFileOperations()
{
    while (m_operations.count() > 0)
        deleteFileOperation(m_operations.takeFirst());
}

int CFileOperationHub::createAssociatedWidgetsForOperations(CListingWidget* listingwidget)
{
    if (!listingwidget)
        return 0;

    int result = 0;
    for (int i = 0; i < m_operations.count(); ++i)
    {
        CFileOperationItem* foperation = m_operations.at(i);
        if (!foperation)
            continue;

        if (!foperation->associatedWidget())
            foperation->setAssociatedWidget(listingwidget->createFileOperationWidget(foperation));

        if (foperation->associatedWidget())
        {
            listingwidget->addWidget(foperation->associatedWidget());
            ++result;
        }
    }

    return result;
}

bool CFileOperationHub::deleteFileOperation(CFileOperationItem* foperation)
{
    bool removedfromlist = m_operations.removeAll(foperation) > 0;
    if (removedfromlist)
    {
        if (foperation)
        {
            foperation->stop();
            delete foperation;
        }
        return true;
    }

    return false;
}

void CFileOperationHub::slotViewDialogClosed(int result)
{
    Q_UNUSED(result);

    if (m_viewdialog)
    {
        // drop associated widget for operations; the widgets are childrens of
        // the dialog, when it (the dialog) gets deleted the pointer will be
        // invalid
        for (int i = 0; i < m_operations.count(); ++i)
        {
            CFileOperationItem* foperation = m_operations.at(i);
            if (!foperation)
                continue;

            if (foperation && foperation->associatedWidget() &&
                m_viewdialog->isAncestorOf(foperation->associatedWidget()))
            {
                foperation->associatedWidget()->deleteLater();
                foperation->setAssociatedWidget(0);
            }
        }

        disconnect(m_viewdialog, 0, this, 0);
        m_viewdialog->deleteLater();
    }

    m_viewdialog = 0;
}


Q_GLOBAL_STATIC(CFileOperationHub, glFileOperationHub)

CFileOperationHub* Lihat::FileOps::getGlobalFileOperationHub()
{
    return glFileOperationHub();
}
