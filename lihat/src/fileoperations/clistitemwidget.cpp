#include "clistitemwidget.h"

#include "cfileoperationitem.h"

#include <QHBoxLayout>
#include <QStyle>
#include <QToolButton>
#include <QVBoxLayout>


using namespace Lihat;
using namespace Lihat::FileOps;

CListItemWidget::CListItemWidget(CFileOperationItem* foperation, QWidget* parent):
    QFrame(parent),
    m_fileoperation(foperation)
{
    setFrameShape(QFrame::StyledPanel);
}

CListItemWidget::~CListItemWidget()
{
    detachFromFileOperation();
}

void CListItemWidget::detachFromFileOperation()
{
    QToolButton* stopButton = findChild< QToolButton* >("tbtnStop");
    QToolButton* removeButton = findChild< QToolButton* >("tbtnRemove");

    if (stopButton)
        stopButton->hide();
    if (removeButton)
        removeButton->show();

    if (m_fileoperation)
        disconnect(m_fileoperation, 0, this, 0);
    m_fileoperation = 0;
}

CFileOperationItem* CListItemWidget::fileOperation() const
{
    return m_fileoperation;
}

void CListItemWidget::initUserInterface()
{
    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setMargin(0);
    layout->setSpacing(4);

    layout->addWidget(createFileOperationWidget(), 1);

    QVBoxLayout* lytbuttons = new QVBoxLayout();
    m_stopbutton = new QToolButton(this);
    m_stopbutton->setObjectName("tbtnStop");
    m_stopbutton->setAutoRaise(true);
    m_stopbutton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    m_stopbutton->setIcon(QIcon::fromTheme("process-stop", QIcon(":lihat/glyph/process-stop.png")));
    m_stopbutton->setIconSize(QSize(16, 16));
    connect(m_stopbutton, SIGNAL(clicked(bool)),
            this, SLOT(slotStopClicked()));

    QToolButton* removeButton = new QToolButton(this);
    removeButton->setObjectName("tbtnRemove");
    removeButton->setAutoRaise(true);
    removeButton->setSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
    removeButton->setIcon(QIcon::fromTheme("edit-delete", QIcon(":lihat/glyph/edit-delete.png")));
    removeButton->setIconSize(QSize(16, 16));
    removeButton->hide();
    connect(removeButton, SIGNAL(clicked(bool)),
            this, SLOT(deleteLater()));

    lytbuttons->addWidget(m_stopbutton, 0, Qt::AlignTop);
    lytbuttons->addWidget(removeButton, 0, Qt::AlignTop);
    layout->addLayout(lytbuttons);
}

bool CListItemWidget::isDetached() const
{
    return m_fileoperation == 0;
}

void CListItemWidget::slotStopClicked()
{
    stopOperation();
}

void CListItemWidget::stopOperation()
{
    // do nothing
}
