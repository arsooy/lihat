#ifndef PERSISTENCEUTILS_H
#define PERSISTENCEUTILS_H

#include <QString>
#include <QVariant>

namespace Lihat
{
    namespace Utils
    {
        namespace Persistence
        {
            enum EPersistenceType
            {
                PT_CONFIG  = 1,
                PT_STORAGE = 2
            };


            QVariant get(const EPersistenceType type, const QString& key, const QVariant& value = QVariant());
            QVariant getConfig(const QString& key, const QVariant& value = QVariant());
            QVariant getStorage(const QString& key, const QVariant& value = QVariant());
            bool     set(const EPersistenceType type, const QString& key, const QVariant& value);
            QVariant setConfig(const QString& key, const QVariant& value);
            QVariant setStorage(const QString& key, const QVariant& value);

        } // namespace Persistence
    } // namespace Utils
} // namespace Lihat

#endif // PERSISTENCEUTILS_H
