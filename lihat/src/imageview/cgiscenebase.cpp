#include "cgiscenebase.h"

#include "../persistenceutils.h"

#include <QBrush>
#include <QColor>
#include <QImage>
#include <QPainter>
#include <QResource>
#include <QStyleOptionGraphicsItem>


using namespace Lihat;
using namespace Lihat::Image;


CGISceneBase::CGISceneBase(QGraphicsItem* parent, Qt::WindowFlags wFlags):
    QGraphicsWidget(parent, wFlags)
{
    BackgroundColor = QColor(Utils::Persistence::getStorage("ImageView/BackgroundColor", QVariant(QColor("#333333").name())).toString());
}


CGISceneBase::~CGISceneBase()
{

}

void CGISceneBase::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* /* widget */)
{
    painter->save();
    painter->fillRect(option->exposedRect, BackgroundColor.darker(120));
    painter->fillRect(option->exposedRect, QBrush(BackgroundColor, Qt::Dense2Pattern));
    painter->restore();
}
