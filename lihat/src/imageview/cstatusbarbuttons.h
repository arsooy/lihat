#ifndef CSTATUSBARBUTTONS_H
#define CSTATUSBARBUTTONS_H

#include <QString>
#include <QWidget>


class QAction;

namespace Lihat
{
    namespace Image
    {

        class CImageViewWidget;

        class CStatusBarButtons:
            public QWidget
        {
            Q_OBJECT
        public:
            explicit CStatusBarButtons(CImageViewWidget* imageviewwidget, QWidget* parent = 0);
            virtual ~CStatusBarButtons();

        private:
            QAction* getActionOf(QWidget* what, const QString& name, bool mustexists = true);
        };

    } // namespace Image
} // namespace Lihat

#endif // CSTATUSBARBUTTONS_H
