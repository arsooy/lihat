#ifndef CGIOVERVIEW_H
#define CGIOVERVIEW_H

#include <QBrush>
#include <QGraphicsWidget>
#include <QPainterPath>
#include <QPen>
#include <QPointF>
#include <QRectF>
#include <QTimeLine>
#include <QTimer>

namespace Lihat
{
    namespace Image
    {

        class CGIImage;
        class CGIImageView;

        class CGIOverview:
            public QGraphicsWidget
        {
            Q_OBJECT
        public:
            explicit CGIOverview(CGIImageView* container, CGIImage* imageview);
            virtual ~CGIOverview();

            // QGraphicsWidget
            virtual QRectF boundingRect() const;
            virtual QPainterPath shape() const;

        signals:
            void hintAutoHide();

        public slots:
            void fadeIn();
            void fadeOut();

        protected:
            unsigned short m_autohideinterval;
            QTimer         m_autohidetimer;
            QBrush         m_basebrush;
            QPen           m_basepen;
            unsigned char  m_dragviewstate;
            bool           m_isfadingin;
            QTimeLine      m_fadetimeline;
            //bool           HasMouseOver;
            CGIImage*      m_image;
            QPointF        m_lastdragposition;
            qreal          m_maximumopacity;
            QBrush         m_viewbrush;
            QPen           m_viewpen;

            // QGraphicsWidget
            virtual void hoverEnterEvent(QGraphicsSceneHoverEvent* event);
            virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent* event);
            virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* event);
            virtual void mousePressEvent(QGraphicsSceneMouseEvent* event);
            virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
            virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);

            void updateHideCountdown();
            qreal scaleToImageView() const;
            QRectF viewRect();

        private slots:
            void slotFadeTimeLineFinished();
            void slotFadeTimeLineFrameChanged(int frame);

        };

    } // namespace Image
} // namespace Lihat

#endif // CGIOVERVIEW_H
