#include "cgiimageview.h"

#include "cgiimage.h"
#include "cgioverview.h"

#include <QAction>
#include <QGraphicsSceneResizeEvent>
#include <QGraphicsWidget>
#include <QCursor>
#include <QPainter>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::Image;

static const short int MOUSE_WHEEL_STEP_AC = 120;

CGIImageView::CGIImageView(QGraphicsItem* parent, Qt::WindowFlags wFlags):
    QGraphicsWidget(parent, wFlags),
    m_adjustimagepositiononnextresize(false),
    m_image(new CGIImage(this)),
    m_mousewheelscrollsteppercent(0.25),
    m_overview(new CGIOverview(this, m_image)),
    m_overviewmargin(20),
    m_scaledtofit(false)
{
    setAcceptHoverEvents(true);
    setCursor(Qt::OpenHandCursor);

    QObject::connect(m_image, SIGNAL(imageLoadFinished(bool, QUrl)),
                     this, SIGNAL(loadFinished(bool, QUrl)));
    QObject::connect(m_image, SIGNAL(hintNextImage()),
                     this, SIGNAL(toNextImageRequested()));
    QObject::connect(m_image, SIGNAL(hintPreviousImage()),
                     this, SIGNAL(toPreviousImageRequested()));
    QObject::connect(m_image, SIGNAL(containerRepaintNeeded()),
                     this, SLOT(slotImageViewContainerRepaintNeeded()));
    QObject::connect(m_image, SIGNAL(viewableAreaUpdated()),
                     this, SLOT(slotImageViewViewableAreaUpdated()));
    QObject::connect(m_image, SIGNAL(scaleChanged()),
                     this, SIGNAL(imageScaleChanged()));
    QObject::connect(m_overview, SIGNAL(hintAutoHide()),
                     m_overview, SLOT(fadeOut()));
}

CGIImageView::~CGIImageView()
{
    if (m_image)
        delete m_image;
    m_image = 0;

    if (m_overview)
        delete m_overview;
    m_overview = 0;
}

QRectF CGIImageView::boundingRect() const
{
    return geometry();
}

void CGIImageView::centerView()
{
    adjustImagePosition();
    adjustImageViewPosition();
}

void CGIImageView::hoverMoveEvent(QGraphicsSceneHoverEvent* gshevent)
{
    QGraphicsWidget::hoverMoveEvent(gshevent);

    showOverview();
}

CGIImage* CGIImageView::image()
{
    return m_image;
}

bool CGIImageView::isScaledToFit() const
{
    return m_scaledtofit;
}

bool CGIImageView::load(const QUrl& url)
{
    bool loadok = image()->load(url);
    if (loadok)
    {
        //qDebug() << "CGIImageView::load:" <<
        //            "Size:" << size();

        scaledToFitUpdate();
        centerView();
        adjustOverviewPosition();

        // we cannot rely on current container size at this moment, as it the
        // caller might not yet visible (ie. by calling Lihat using image file
        // as argument) postpone the actual readjustment until the next time
        // this get resized (by Qt, prior showing the image view widget)
        m_adjustimagepositiononnextresize = isVisible();
    }

    return loadok;
}

CGIOverview* CGIImageView::overview()
{
    return m_overview;
}

bool CGIImageView::reload()
{
    return load(url());
}

QPainterPath CGIImageView::shape() const
{
    QPainterPath result;
    result.addRect(boundingRect());
    return result;
}

void CGIImageView::slotImageViewContainerRepaintNeeded()
{
    update();
}

void CGIImageView::slotImageViewViewableAreaUpdated()
{
    if (!isVisible())
        return;

    prepareGeometryChange();
    adjustImagePosition();
    adjustImageViewPosition(false);
    showOverview();
    update();
}

void CGIImageView::focusInEvent(QFocusEvent* event)
{
    QGraphicsWidget::focusInEvent(event);

    emit focusRecieved();
}

void CGIImageView::focusOutEvent(QFocusEvent* event)
{
    QGraphicsWidget::focusOutEvent(event);

    emit focusLost();
}

void CGIImageView::mouseMoveEvent(QGraphicsSceneMouseEvent* gsmevent)
{
    if (m_image)
        m_image->imageMouseDragMove(gsmevent, this);
}

void CGIImageView::mousePressEvent(QGraphicsSceneMouseEvent* gsmevent)
{
    if (m_image)
        m_image->imageMouseDragStart(gsmevent, this);
}

void CGIImageView::mouseReleaseEvent(QGraphicsSceneMouseEvent* gsmevent)
{
    if (m_image)
        m_image->imageMouseDragEnd(gsmevent, this);
}

void CGIImageView::resizeEvent(QGraphicsSceneResizeEvent* gsrevent)
{
    QGraphicsWidget::resizeEvent(gsrevent);
    //qDebug() << "CGIImageView::resizeEvent:" <<
    //            "Size:" << size();

    prepareGeometryChange();
    m_size = gsrevent->newSize();

    if (m_scaledtofit)
        scaledToFitUpdate();
    if (m_adjustimagepositiononnextresize)
        adjustImagePosition();
    adjustImageViewPosition(false);
    adjustOverviewPosition();

    emit sizeChanged();
}

void CGIImageView::wheelEvent(QGraphicsSceneWheelEvent* gswevent)
{
    QGraphicsItem::wheelEvent(gswevent);

    qreal rdelta = -gswevent->delta();
    qreal amount = rdelta / (MOUSE_WHEEL_STEP_AC / m_mousewheelscrollsteppercent);

    QPointF scamnt;
    Qt::KeyboardModifiers kemod = gswevent->modifiers();
    if (kemod & Qt::AltModifier)
        scamnt.setX(amount);
    else
        scamnt.setY(amount);

    m_image->scrollAdjust(scamnt);
}

QAction* CGIImageView::getAction(const QString& name, bool mustexists)
{
    QAction* result = findChild< QAction* >(name);
    if (mustexists)
        Q_ASSERT(result);

    return result;
}

void CGIImageView::adjustOverviewPosition()
{
    Q_ASSERT(m_overview);

    QSizeF contsize = boundingRect().size();
    QRectF overviewrect = m_overview->boundingRect();
    overviewrect.moveBottomRight(QPointF(contsize.width()  - m_overviewmargin,
                                         contsize.height() - m_overviewmargin));
    m_overview->setPos(overviewrect.topLeft());
}

bool CGIImageView::isOverviewNeccessary() const
{
    Q_ASSERT(m_image);

    QSizeF imgsize = m_image->boundingRect().size();
    QSizeF dispsize = boundingRect().size() / m_image->scale();

    // see if overview is needed
    return (!m_scaledtofit && ((imgsize.width() > dispsize.width()) || (imgsize.height() > dispsize.height())));
}

void CGIImageView::showOverview()
{
    Q_ASSERT(m_overview);

    if (isOverviewNeccessary())
        m_overview->fadeIn();
    else
        m_overview->hide();
}

void CGIImageView::adjustImagePosition()
{
    Q_ASSERT(m_image);

    qreal imgscale = m_image->scale();
    QSizeF imgsize = m_image->boundingRect().size();
    QSizeF contsize = geometry().size() / imgscale;

    QPointF newpos(imgscale * (contsize.width()  - imgsize.width())  / 2.0,
                   imgscale * (contsize.height() - imgsize.height()) / 2.0);

    m_image->setPos(QPointF(qMax< qreal >(newpos.x(), 0),
                            qMax< qreal >(newpos.y(), 0)));
}

void CGIImageView::adjustImageViewPosition(bool center)
{
    Q_ASSERT(m_image);

    if (m_scaledtofit)
        m_image->setViewTopLeft(QPointF(0, 0));
    else
    {
        QSizeF imgsize = m_image->boundingRect().size();
        QSizeF contsize = geometry().size() / m_image->scale();

        if (center)
        {
            // go to center of image
            QPointF newpos((contsize.width()  - imgsize.width())  / 2.0,
                           (contsize.height() - imgsize.height()) / 2.0);
            m_image->setViewTopLeft(QPointF(qMax< qreal >(-newpos.x(), 0),
                                            qMax< qreal >(-newpos.y(), 0)));
        }
        else
        {
            QPointF vpoint = m_image->viewTopLeft();
            // minimum: 0, maximum: the image size minus current view size
            QPointF rpoint(qBound< qreal >(0, vpoint.x(), qMax< qreal >(0, imgsize.width()  - contsize.width())),
                           qBound< qreal >(0, vpoint.y(), qMax< qreal >(0, imgsize.height() - contsize.height())));
            m_image->setViewTopLeft(rpoint);
        }
    }
}

void CGIImageView::scaledToFitUpdate()
{
    Q_ASSERT(m_image);

    QRectF imagerect = m_image->boundingRect();
    QSizeF containersize = size();

    if (m_scaledtofit)
    {
        qreal downscale = qBound< qreal >(0, qMin< qreal >(qreal(containersize.width())  / imagerect.width(),
                                                           qreal(containersize.height()) / imagerect.height()), 1.);
        m_image->setScale(downscale);
        adjustImagePosition();
        adjustImageViewPosition();
        m_overview->hide();
    }
    else
    {
        m_image->setScale(1.0);
        showOverview();
    }

    update();
}

void CGIImageView::setScaledToFit(bool scaledtofit)
{
    m_scaledtofit = scaledtofit;
    scaledToFitUpdate();
    adjustImagePosition();
}

QUrl CGIImageView::url() const
{
    return m_image->url();
}
