#include "cimageviewwidget.h"

#include "../cmainwindow.h"
#include "../pathutils.h"
#include "../persistenceutils.h"
#include "../thumbnailview/cthumbnailview.h"
#include "../thumbnailview/cthumbnailviewwidget.h"
#include "cgiimage.h"
#include "cgiimageview.h"
#include "cgiscenebase.h"
#include "cgraphicsview.h"
#include "cloadresult.h"
#include "cstatusbarbuttons.h"
#include "images/cimageslistview.h"
#include "images/cimageswidget.h"

#include <QAction>
#include <QFileInfo>
#include <QGraphicsGridLayout>
#include <QLabel>
#include <QMovie>
#include <QPainter>
#include <QPointF>
#include <QResizeEvent>
#include <QSplitter>
#include <QStatusBar>
#include <QVBoxLayout>
#include <QWheelEvent>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::Image;


static const qreal SCALE_STEP_PERCENT = 0.5;
static const QString STATUSBAR_INFO_FORMAT = "%1 x %2 %3 %4%";

CImageViewWidget::CImageViewWidget(QWidget* parent):
    QWidget(parent),
    m_currentimageview(0),
    m_docenteronshow(false),
    m_graphicsview(new CGraphicsView(this)),
    m_lastloadstatus(false),
    m_scenebasewidget(0),
    m_updateviewcount(0)
{
    readPersistence();
    initActions();
    applyActionKeySequences();

    setContentsMargins(0, 0, 0, 0);

    QVBoxLayout* layout = new QVBoxLayout(this);
    layout->setMargin(0);
    layout->setSpacing(0);

    // left-right splitter
    QSplitter* hsplitter = new QSplitter(Qt::Horizontal, this);
    hsplitter->setObjectName("spLeftRightSplitter");
    hsplitter->setChildrenCollapsible(false);

    // images view
    m_imageswidget = new CImagesWidget(this);
    m_imageswidget->setObjectName("iwImages");
    QObject::connect(m_imageswidget, SIGNAL(currentModeChanged()),
                     this, SLOT(slotImagesCurrentModeChanged()));

    m_imageswidget->hide();
    hsplitter->addWidget(m_imageswidget);

    // graphics view
    m_graphicsview->setCacheMode(QGraphicsView::CacheBackground);
    m_graphicsview->setRenderHint(QPainter::SmoothPixmapTransform, true);
    m_graphicsview->setViewportUpdateMode(QGraphicsView::MinimalViewportUpdate);
    m_graphicsview->setScene(&m_graphicsscene);
    QObject::connect(m_graphicsview, SIGNAL(resized()),
                     this, SLOT(slotGraphicsViewResized()));


    QGraphicsGridLayout* lyout = new QGraphicsGridLayout();
    lyout->setContentsMargins(0, 0, 0 ,0);
    m_scenebasewidget = new CGISceneBase();
    m_scenebasewidget->setLayout(lyout);
    m_graphicsscene.addItem(m_scenebasewidget);

    hsplitter->addWidget(m_graphicsview);
    hsplitter->setStretchFactor(1, 1);

    layout->addWidget(hsplitter, 1);

    // default image view
    CGIImageView* imgv = new CGIImageView();
    addImageView(imgv, 0, 0);

    m_currentimageview = imgv;
}

CImageViewWidget::~CImageViewWidget()
{
    writePersistence();
}

void CImageViewWidget::beginViewUpdate()
{
    m_updateviewcount++;
    if (m_updateviewcount > 1)
        return;
}

void CImageViewWidget::centerView()
{
    if (hasCurrentImage())
        currentImageView()->centerView();
}

QUrl CImageViewWidget::currentLoadedURL() const
{
    if (hasCurrentImage())
        return currentImageView()->url();

    return QUrl();
}

QUrl CImageViewWidget::currentLoadedParentURL() const
{
    if (hasCurrentImage())
    {
        QString parent;
        if (Utils::Path::parentOfPath(currentImageView()->url().toString(QUrl::StripTrailingSlash), parent))
            return QUrl::fromEncoded(parent.toLocal8Bit());
    }

    return QString();
}

void CImageViewWidget::endViewUpdate()
{
    m_updateviewcount--;
    if (m_updateviewcount > 0)
        return;
}

void CImageViewWidget::hideEvent(QHideEvent* event)
{
    QWidget::hideEvent(event);

    CMainWindow* mainwindow = getMainWindow();
    if (mainwindow)
    {
        Q_ASSERT(mainwindow->isAncestorOf(this));
        QStatusBar* sb = mainwindow->statusBar();
        if (sb)
            detachFromMainWindowStatusBar(sb);
    }
}

CGIImageView* CImageViewWidget::currentImageView() const
{
    if (hasCurrentImage())
        return m_currentimageview;

    return 0;
}

bool CImageViewWidget::isScaledToFit() const
{
    if (hasCurrentImage())
        return currentImageView()->isScaledToFit();

    return false;
}

void CImageViewWidget::slotGraphicsViewResized()
{
    updateImageViewGeometry();
}

void CImageViewWidget::slotImageViewFocused()
{
    CGIImageView* imgcontainer = qobject_cast< CGIImageView* >(sender());
    if (imgcontainer)
    {
        qDebug() << "CImageViewWidget::slotImageViewFocused:" <<
                    "Current image view" << imgcontainer;
        if (m_imageviews.indexOf(imgcontainer) != -1)
            m_currentimageview = imgcontainer;
    }
}

void CImageViewWidget::slotImageViewLoadFinished(bool loaded, const QUrl& url)
{
    CGIImageView* imgcontainer = qobject_cast< CGIImageView* >(sender());
    if (imgcontainer && m_imageviews.contains(imgcontainer))
    {
        m_lastloadstatus = loaded;
        if (loaded && (m_currentimageview == imgcontainer))
            updateStatusTexts();

        emit imageLoaded(loaded, url);
    }
}

void CImageViewWidget::slotImageViewScaleChanged(qreal scale)
{
    Q_UNUSED(scale);

    CGIImageView* imgcontainer = qobject_cast< CGIImageView* >(sender());
    if (imgcontainer && m_imageviews.contains(imgcontainer) && isVisible())
        updateStatusTexts();
}

void CImageViewWidget::slotImageViewSizeChanged()
{
    CGIImageView* imgcontainer = qobject_cast< CGIImageView* >(sender());
    if (imgcontainer && m_imageviews.contains(imgcontainer) && isVisible())
        updateStatusTexts();
}

void CImageViewWidget::slotImageViewURLDropped(const QUrl& url)
{
    CGIImageView* imgcontainer = qobject_cast< CGIImageView* >(sender());
    if (imgcontainer && m_imageviews.contains(imgcontainer) && isVisible())
        imgcontainer->load(url);
}

void CImageViewWidget::slotImagesCurrentModeChanged()
{
    if (!hasCurrentImage())
        return;

    switch (m_imageswidget->ImagesListView->viewMode())
    {
        case CImagesListView::IVM_SUBIMAGES:
        {
            CGIImage* img = m_currentimageview->image();
            if (img && img->loadResult())
            {
                CLoadResult* lresult = img->loadResult();
                if (lresult->LoadedKind == CLoadResult::LK_MOVIE)
                    lresult->Movie->stop();
            }

            break;
        }

        default:
            ;
    }
}

void CImageViewWidget::slotImagesToggled(bool checked)
{
    m_imageswidget->setVisible(checked);

    if (checked && m_linkedthumbnailviewwidget && hasCurrentImage())
    {
        switch (m_imageswidget->ImagesListView->viewMode())
        {
            case CImagesListView::IVM_SIBLINGS:
            case CImagesListView::IVM_SELECTIONS:
            {
                QUrl loadedurl = currentLoadedURL();
                if (loadedurl.isValid() && (m_linkedthumbnailviewwidget->currentThumbnailURL() != loadedurl))
                {
                    // synchronize thumbnails to image current URL; this most
                    // likely occured because user view an image directly from
                    // the command line, as usually it is the image view that
                    // synchronize itself with the thumbnail view
                    m_linkedthumbnailviewwidget->setCurrentThumbnail(loadedurl);
                }

                break;
            }

            default:
                ;
        }
    }

    updateImageViewGeometry();
}

void CImageViewWidget::slotScaleToFitHovered()
{
    QAction* act = getAction("acImageViewScaleToFit");
    act->setEnabled(isVisible());
}

void CImageViewWidget::slotScaleToFitTriggered(bool checked)
{
    QAction* act = getAction("acImageViewScaleToFit");

    beginViewUpdate();
    setScaledToFit(checked);
    if (!isScaledToFit())
        centerView();
    endViewUpdate();

    act->setChecked(checked);
}

void CImageViewWidget::slotScrollImageHomeEndTriggered()
{
    if (!hasCurrentImage())
        return;

    QAction* action = qobject_cast< QAction* >(sender());
    bool ishome = action == getAction("acScrollHome");
    bool isend = action == getAction("acScrollEnd");

    if (m_currentimageview && m_currentimageview->image())
    {
        CGIImage* imgview = m_currentimageview->image();
        if (ishome)
            imgview->scrollTo(QPointF(0, 0));
        else if (isend)
        {
            QSizeF imgsize = imgview->boundingRect().size() * imgview->scale();
            imgview->scrollTo(QPointF(imgsize.width(), imgsize.height()));
        }
    }
}

void CImageViewWidget::slotScrollTriggered()
{
    if (!hasCurrentImage())
        return;

    QAction* action = qobject_cast< QAction* >(sender());
    bool isright = (action == getAction("acImageScrollRight")) || (action == getAction("acImageScrollPageRight"));
    bool isdown  = (action == getAction("acImageScrollDown"))  || (action == getAction("acImageScrollPageDown"));
    bool isleft  = (action == getAction("acImageScrollLeft"))  || (action == getAction("acImageScrollPageLeft"));
    bool isup    = (action == getAction("acImageScrollUp"))    || (action == getAction("acImageScrollPageUp"));
    bool ispagescroll = (action == getAction("acImageScrollPageRight")) ||
                        (action == getAction("acImageScrollPageDown"))  ||
                        (action == getAction("acImageScrollPageLeft"))  ||
                        (action == getAction("acImageScrollPageUp"));

    if (isright || isdown)
    {
        if (isScaledToFit() && !ispagescroll)
        {
            emit nextImageRequested();
            return;
        }
    }
    else if (isleft || isup)
    {
        if (isScaledToFit() && !ispagescroll)
        {
            emit previousImageRequested();
            return;
        }
    }

    if (m_currentimageview)
    {
        qreal  scrollstep = 0.25; // progress 1/4 of entire view
        if (ispagescroll)
            scrollstep = 1.0;

        QPointF scrolldist(0, 0);
        if (isright || isleft)
            scrolldist.setX(isright ? scrollstep : -scrollstep);
        if (isdown || isup)
            scrolldist.setY(isdown ? scrollstep : -scrollstep);

        if (m_currentimageview->image())
            m_currentimageview->image()->scrollAdjust(scrolldist);
    }
}

void CImageViewWidget::slotZoomInHovered()
{
    QAction* act = getAction("acImageViewZoomIn");
    act->setEnabled(isVisible() && hasCurrentImage() &&
                    currentImageView()->image() && currentLoadedURL().isValid());
}

void CImageViewWidget::zoomIn()
{
    bool wasscaledtofit = isScaledToFit();
    QAction* acImageViewScaleToFit = getAction("acImageViewScaleToFit");

    qreal nscale = scale();
    nscale = qBound< qreal >(0.05, nscale + SCALE_STEP_PERCENT, 8.);

    beginViewUpdate();
    if (wasscaledtofit)
    {
        setScaledToFit(false);
        acImageViewScaleToFit->setChecked(false);
    }

    setScale(nscale);

    if (wasscaledtofit)
        centerView();
    endViewUpdate();
}

void CImageViewWidget::slotZoomOutHovered()
{
    QAction* act = getAction("acImageViewZoomOut");
    act->setEnabled(isVisible() && hasCurrentImage() &&
                    currentImageView()->image() && currentLoadedURL().isValid());
}

void CImageViewWidget::zoomOut()
{
    bool wasscaledtofit = isScaledToFit();
    QAction* acImageViewScaleToFit = getAction("acImageViewScaleToFit");

    qreal nscale = scale();
    nscale = qBound< qreal >(0.25, nscale - (SCALE_STEP_PERCENT / 2), 8.);

    beginViewUpdate();
    if (wasscaledtofit)
    {
        setScaledToFit(false);
        acImageViewScaleToFit->setChecked(false);
    }

    setScale(nscale);

    if (wasscaledtofit)
        centerView();
    endViewUpdate();
}

void CImageViewWidget::slotZoomResetHovered()
{
    QAction* act = getAction("acImageViewZoomReset");
    act->setEnabled(isVisible() && hasCurrentImage() &&
                    currentImageView()->image() && currentLoadedURL().isValid());
}

void CImageViewWidget::zoomReset()
{
    QAction* acImageViewScaleToFit = getAction("acImageViewScaleToFit");

    beginViewUpdate();
    setScaledToFit(false);
    acImageViewScaleToFit->setChecked(false);

    setScale(1.0);

    endViewUpdate();
}

bool CImageViewWidget::load(const QUrl& url)
{
    if (hasCurrentImage())
    {
        m_docenteronshow = !isVisible();

        return currentImageView()->load(url);
    }

    return false;
}

bool CImageViewWidget::reload()
{
    return load(currentLoadedURL());
}

void CImageViewWidget::resizeEvent(QResizeEvent* event)
{
    QWidget::resizeEvent(event);

    updateImageViewGeometry();
}

qreal CImageViewWidget::scale() const
{
    if (!hasCurrentImage())
        return 0;

    CGIImage* img = currentImageView()->image();
    if (img)
        return currentImageView()->image()->scale();

    return 0;
}

bool CImageViewWidget::setScale(qreal scale)
{
    if (!hasCurrentImage())
        return 0;

    CGIImage* img = currentImageView()->image();
    if (img)
    {
        img->setScale(scale);
        return true;
    }

    return false;
}

void CImageViewWidget::setScaledToFit(bool scaled)
{
    if (hasCurrentImage())
        currentImageView()->setScaledToFit(scaled);
}

void CImageViewWidget::setThumbnailViewWidget(Thumbnails::CThumbnailViewWidget* thumbnailviewwidget)
{
    m_linkedthumbnailviewwidget = thumbnailviewwidget;

    if (m_linkedthumbnailviewwidget)
    {
        Thumbnails::CThumbnailView* thumbnailview = thumbnailviewwidget->findChild< Thumbnails::CThumbnailView* >("tvThumbnailView");
        m_imageswidget->ImagesListView->setSourceThumbnailView(thumbnailview);
        m_imageswidget->ImagesListView->setImagesViewMode(CImagesListView::IVM_SIBLINGS);

        m_linkedthumbnailviewwidget->addThumbnailView(m_imageswidget->ImagesListView);
    }
}

void CImageViewWidget::showEvent(QShowEvent* event)
{
    QWidget::showEvent(event);

    CMainWindow* mainwindow = getMainWindow();
    if (mainwindow)
    {
        Q_ASSERT(mainwindow->isAncestorOf(this));
        QStatusBar* sb = mainwindow->statusBar();
        if (sb)
            attachToMainWindowStatusBar(sb);
    }

    if (hasCurrentImage())
    {
        updateImageViewGeometry();
        if (m_docenteronshow)
            currentImageView()->centerView();
    }
}

void CImageViewWidget::attachToMainWindowStatusBar(QStatusBar* statusbar)
{
    Q_ASSERT(statusbar);

    CStatusBarButtons* buttons = statusbar->findChild< CStatusBarButtons* >("ivsbButtons");
    if (!buttons)
    {
        buttons = new CStatusBarButtons(this, statusbar);
        buttons->setObjectName("ivsbButtons");
        statusbar->insertPermanentWidget(0, buttons);
    }
    else
        buttons->show();

    QLabel* lbMessages = statusbar->findChild< QLabel* >("ivLabelMessages");
    if (!lbMessages)
    {
        lbMessages = new QLabel(statusbar);
        lbMessages->setObjectName("ivLabelMessages");
        statusbar->insertPermanentWidget(1, lbMessages, 1);
    }
    else
        lbMessages->show();

    QLabel* lbInfo = statusbar->findChild< QLabel* >("ivLabelInformation");
    if (!lbInfo)
    {
        lbInfo = new QLabel(statusbar);
        lbInfo->setObjectName("ivLabelInformation");
        statusbar->addPermanentWidget(lbInfo);
    }
    else
        lbInfo->show();

    updateStatusTexts();
}

void CImageViewWidget::detachFromMainWindowStatusBar(QStatusBar* statusbar)
{
    CStatusBarButtons* buttons = statusbar->findChild< CStatusBarButtons* >("ivsbButtons");
    if (buttons)
    {
        statusbar->removeWidget(buttons);
        delete buttons;
    }

    QLabel* lbMessages = statusbar->findChild< QLabel* >("ivLabelMessages");
    if (lbMessages)
    {
        statusbar->removeWidget(lbMessages);
        delete lbMessages;
    }

    QLabel* lbInfo = statusbar->findChild< QLabel* >("ivLabelInformation");
    if (lbInfo)
    {
        statusbar->removeWidget(lbInfo);
        delete lbInfo;
    }
}

void CImageViewWidget::updateImageViewGeometry()
{
    QRect viewarea = m_graphicsview->viewport()->rect();
    m_graphicsview->setSceneRect(viewarea);
    m_scenebasewidget->resize(viewarea.size());
}

bool CImageViewWidget::setStatusText(const QString& stext, const QString& stooltip, int index)
{
    CMainWindow* mainwindow = getMainWindow();
    if (!mainwindow)
        return false;
    if (!mainwindow->isAncestorOf(this))
        return false;

    QStatusBar* statusbar = mainwindow->statusBar();
    if (!statusbar)
        return false;

    if (index == 0)
    {
        QLabel* lbMessages = statusbar->findChild< QLabel* >("ivLabelMessages");
        if (lbMessages)
        {
            lbMessages->setText(stext);
            lbMessages->setToolTip(stooltip);
        }
        return true;
    }
    else if (index == 1)
    {
        QLabel* lbInfo = statusbar->findChild< QLabel* >("ivLabelInformation");
        if (lbInfo)
            lbInfo->setText(stext);
        return true;
    }

    return false;
}

bool CImageViewWidget::updateStatusTexts(CGIImageView* imgcontainer)
{
    if (!imgcontainer && (m_currentimageview && (m_imageviews.indexOf(m_currentimageview) != -1)))
        imgcontainer = m_currentimageview;

    if (!imgcontainer)
        return false;

    // update current loaded url
    QUrl loadedurl = currentLoadedURL();
    if (loadedurl.isValid())
    {
        if (loadedurl.isLocalFile())
        {
            QFileInfo loadedfile(loadedurl.toLocalFile());
            setStatusText(loadedfile.fileName(), loadedfile.absoluteFilePath());
        }
        else
            setStatusText(loadedurl.toString(QUrl::RemovePath), loadedurl.toString());
    }

    CGIImage* img = imgcontainer->image();
    QSize imagesize = img->boundingRect().size().toSize();
    QString infostr = STATUSBAR_INFO_FORMAT.arg(QString::number(imagesize.width()),
                                                QString::number(imagesize.height()));
    infostr = infostr.arg(QChar(0x12, 0x20));

    unsigned int imgscale = (int)(qMax< qreal >(0., img->scale()) * 100);
    infostr = infostr.arg(imgscale);

    setStatusText(infostr, QString(), 1);
    return true;
}

bool CImageViewWidget::addImageView(CGIImageView* ivcont, int row, int col)
{
    QGraphicsGridLayout* lyout = dynamic_cast< QGraphicsGridLayout* >(m_scenebasewidget->layout());
    if (!lyout)
        return false;

    lyout->addItem(ivcont, row, col, Qt::AlignCenter);
    m_imageviews.append(ivcont);

    QObject::connect(ivcont, SIGNAL(focusRecieved()),
                     this, SLOT(slotImageViewFocused()),
                     Qt::UniqueConnection);
    QObject::connect(ivcont, SIGNAL(toNextImageRequested()),
                     this, SIGNAL(nextImageRequested()),
                     Qt::UniqueConnection);
    QObject::connect(ivcont, SIGNAL(toPreviousImageRequested()),
                     this, SIGNAL(previousImageRequested()),
                     Qt::UniqueConnection);
    QObject::connect(ivcont, SIGNAL(loadFinished(bool, QUrl)),
                     this, SLOT(slotImageViewLoadFinished(bool, QUrl)),
                     Qt::UniqueConnection);
    QObject::connect(ivcont, SIGNAL(urlDropped(QUrl)),
                     this, SLOT(slotImageViewURLDropped(QUrl)),
                     Qt::UniqueConnection);
    QObject::connect(ivcont, SIGNAL(sizeChanged()),
                     this, SLOT(slotImageViewSizeChanged()),
                     Qt::UniqueConnection);
    QObject::connect(ivcont, SIGNAL(imageScaleChanged()),
                     this, SLOT(slotImageViewSizeChanged()),
                     Qt::UniqueConnection);

    return true;
}

bool CImageViewWidget::hasCurrentImage() const
{
    if (m_currentimageview)
    {
        bool result = m_graphicsscene.items().indexOf(m_currentimageview) != -1;
        if (!result)
            qDebug() << "CImageViewWidget::hasCurrentImage:" <<
                        "unable to find current image view";

        return result;
    }

    return false;
}

QAction* CImageViewWidget::getAction(const QString& name, bool mustexists)
{
    QAction* result = findChild< QAction* >(name);
    if (mustexists)
        Q_ASSERT(result);

    return result;
}

void CImageViewWidget::readPersistence()
{
    QStringList defkeys;
    QStringList readkeys;

    // scroll up
    defkeys.clear();
    defkeys << QKeySequence(Qt::Key_Up).toString();
    defkeys << QKeySequence(Qt::Key_W).toString();
    readkeys = Utils::Persistence::getConfig("ImageView/ScrollUpKeys", QVariant(defkeys)).toStringList();
    for (int i = 0; i < readkeys.count(); ++i)
    {
        QKeySequence kseq(readkeys.at(i));
        if (!kseq.isEmpty())
            m_keysequences.insert("acImageScrollUp", kseq);
    }

    // scroll right
    defkeys.clear();
    defkeys << QKeySequence(Qt::Key_Right).toString();
    defkeys << QKeySequence(Qt::Key_D).toString();
    readkeys = Utils::Persistence::getConfig("ImageView/ScrollRightKeys", QVariant(defkeys)).toStringList();
    for (int i = 0; i < readkeys.count(); ++i)
    {
        QKeySequence kseq(readkeys.at(i));
        if (!kseq.isEmpty())
            m_keysequences.insert("acImageScrollRight", kseq);
    }

    // scroll down
    defkeys.clear();
    defkeys << QKeySequence(Qt::Key_Down).toString();
    defkeys << QKeySequence(Qt::Key_S).toString();
    readkeys = Utils::Persistence::getConfig("ImageView/ScrollDownKeys", QVariant(defkeys)).toStringList();
    for (int i = 0; i < readkeys.count(); ++i)
    {
        QKeySequence kseq(readkeys.at(i));
        if (!kseq.isEmpty())
            m_keysequences.insert("acImageScrollDown", kseq);
    }

    // scroll left
    defkeys.clear();
    defkeys << QKeySequence(Qt::Key_Left).toString();
    defkeys << QKeySequence(Qt::Key_A).toString();
    readkeys = Utils::Persistence::getConfig("ImageView/ScrollLeftKeys", QVariant(defkeys)).toStringList();
    for (int i = 0; i < readkeys.count(); ++i)
    {
        QKeySequence kseq(readkeys.at(i));
        if (!kseq.isEmpty())
            m_keysequences.insert("acImageScrollLeft", kseq);
    }

    // scale to fit
    defkeys.clear();
    defkeys << QKeySequence(Qt::Key_F).toString();
    readkeys = Utils::Persistence::getConfig("ImageView/ScaleToFitKeys", QVariant(defkeys)).toStringList();
    for (int i = 0; i < readkeys.count(); ++i)
    {
        QKeySequence kseq(readkeys.at(i));
        if (!kseq.isEmpty())
            m_keysequences.insert("acImageViewScaleToFit", kseq);
    }

    // zoom in
    defkeys.clear();
    defkeys << QKeySequence(Qt::Key_Plus).toString();
    defkeys << QKeySequence(Qt::Key_ZoomIn).toString();
    defkeys << QKeySequence(Qt::KeypadModifier + Qt::Key_Plus).toString();
    readkeys = Utils::Persistence::getConfig("ImageView/ZoomInKeys", QVariant(defkeys)).toStringList();
    for (int i = 0; i < readkeys.count(); ++i)
    {
        QKeySequence kseq(readkeys.at(i));
        if (!kseq.isEmpty())
            m_keysequences.insert("acImageViewZoomIn", kseq);
    }

    // zoom out
    defkeys.clear();
    defkeys << QKeySequence(Qt::Key_Minus).toString();
    defkeys << QKeySequence(Qt::Key_ZoomOut).toString();
    defkeys << QKeySequence(Qt::KeypadModifier + Qt::Key_Minus).toString();
    readkeys = Utils::Persistence::getConfig("ImageView/ZoomOutKeys", QVariant(defkeys)).toStringList();
    for (int i = 0; i < readkeys.count(); ++i)
    {
        QKeySequence kseq(readkeys.at(i));
        if (!kseq.isEmpty())
            m_keysequences.insert("acImageViewZoomOut", kseq);
    }

    // zoom reset
    defkeys.clear();
    defkeys << QKeySequence(Qt::Key_Equal).toString();
    defkeys << QKeySequence(Qt::CTRL + Qt::Key_0).toString();
    defkeys << QKeySequence(Qt::KeypadModifier + Qt::Key_Asterisk).toString();
    readkeys = Utils::Persistence::getConfig("ImageView/ZoomResetKeys", QVariant(defkeys)).toStringList();
    for (int i = 0; i < readkeys.count(); ++i)
    {
        QKeySequence kseq(readkeys.at(i));
        if (!kseq.isEmpty())
            m_keysequences.insert("acImageViewZoomReset", kseq);
    }
}

void CImageViewWidget::writePersistence()
{
    // write key sequences
    QStringList writekeys;
    QList< QPair< QString, QString > > keypairs;  // (KeySequence Key, Config Key)
    keypairs << QPair< QString, QString >("acImageScrollUp", "ScrollUp")
             << QPair< QString, QString >("acImageScrollRight", "ScrollRight")
             << QPair< QString, QString >("acImageScrollDown", "ScrollDown")
             << QPair< QString, QString >("acImageScrollLeft", "ScrollLeft");
    for (int i = 0; i < keypairs.count(); ++i)
    {
        QPair< QString, QString > pair = keypairs.at(i);
        writekeys.clear();

        QList< QKeySequence > keyseqs = m_keysequences.values(pair.first);
        for (int j = 0; j < keyseqs.count(); ++j)
        {
            QKeySequence kseq = keyseqs.at(j);
            if (!kseq.isEmpty())
                writekeys << kseq.toString();
        }

        if (!writekeys.isEmpty())
        {
            QString confkey("ImageView/" + pair.second + "Keys");
            Utils::Persistence::setConfig(confkey, QVariant(writekeys));
        }
    }
}

void CImageViewWidget::applyActionKeySequences()
{
    QStringList actionnames;
    actionnames << "acImageScrollUp"
                << "acImageScrollRight"
                << "acImageScrollDown"
                << "acImageScrollLeft"
                << "acImageViewScaleToFit"
                << "acImageViewZoomIn"
                << "acImageViewZoomOut"
                << "acImageViewZoomReset";

    for (int i = 0; i < actionnames.count(); ++i)
    {
        QAction* act = getAction(actionnames.at(i));
        if (act)
            act->setShortcuts(m_keysequences.values(act->objectName()));
    }
}

void CImageViewWidget::initActions()
{
    QAction* act;

    act = new QAction(QObject::tr("Switch to &Thumbnail view"), this);
    act->setObjectName("acEscapeToThumbnailView");
    act->setShortcut(QKeySequence(Qt::Key_Escape));
    QObject::connect(act, SIGNAL(triggered(bool)),
                     this, SIGNAL(toThumbnailViewRequested()));
    addAction(act);

    act = new QAction(QObject::tr("Scale to &fit"), this);
    act->setObjectName("acImageViewScaleToFit");
    act->setIcon(QIcon::fromTheme("zoom-fit-best", QIcon(":lihat/glyph/zoom-fit-best.png")));
    act->setCheckable(true);
    act->setShortcutContext(Qt::WindowShortcut);
    QObject::connect(act, SIGNAL(hovered()),
                     this, SLOT(slotScaleToFitHovered()));
    QObject::connect(act, SIGNAL(triggered(bool)),
                     this, SLOT(slotScaleToFitTriggered(bool)));
    act->setChecked(Utils::Persistence::getConfig("ImageView/ScaleToFit", QVariant(false)).toBool());

    act = new QAction(QObject::tr("&Reset zoom"), this);
    act->setObjectName("acImageViewZoomReset");
    act->setIcon(QIcon::fromTheme("zoom-original", QIcon(":lihat/glyph/zoom-original.png")));
    QObject::connect(act, SIGNAL(hovered()),
                     this, SLOT(slotZoomResetHovered()));
    QObject::connect(act, SIGNAL(triggered(bool)),
                     this, SLOT(zoomReset()));

    act = new QAction(QObject::tr("Zoom &in"), this);
    act->setObjectName("acImageViewZoomIn");
    act->setIcon(QIcon::fromTheme("zoom-in", QIcon(":lihat/glyph/zoom-in.png")));
    QObject::connect(act, SIGNAL(hovered()),
                     this, SLOT(slotZoomInHovered()));
    QObject::connect(act, SIGNAL(triggered(bool)),
                     this, SLOT(zoomIn()));

    act = new QAction(QObject::tr("Zoom &out"), this);
    act->setObjectName("acImageViewZoomOut");
    act->setIcon(QIcon::fromTheme("zoom-out", QIcon(":lihat/glyph/zoom-out.png")));
    QObject::connect(act, SIGNAL(hovered()),
                     this, SLOT(slotZoomOutHovered()));
    QObject::connect(act, SIGNAL(triggered(bool)),
                     this, SLOT(zoomOut()));


    act = new QAction(QObject::tr("Images"), this);
    act->setObjectName("acImageViewImages");
    act->setIcon(QIcon::fromTheme("image-stack", QIcon(":lihat/glyph/image-stack.png")));
    act->setCheckable(true);
    QObject::connect(act, SIGNAL(toggled(bool)),
                     this, SLOT(slotImagesToggled(bool)));

    act = new QAction(QObject::tr("Information"), this);
    act->setObjectName("acImageViewInformation");
    act->setIcon(QIcon::fromTheme("dialog-information", QIcon(":lihat/glyph/dialog-information.png")));


    act = new QAction(QObject::tr("Scroll up"), m_graphicsview);
    act->setObjectName("acImageScrollUp");
    act->setShortcutContext(Qt::WidgetShortcut);
    QObject::connect(act, SIGNAL(triggered(bool)),
                     this, SLOT(slotScrollTriggered()));
    m_graphicsview->addAction(act);

    act = new QAction(QObject::tr("Scroll right"), m_graphicsview);
    act->setObjectName("acImageScrollRight");
    act->setShortcutContext(Qt::WidgetShortcut);
    QObject::connect(act, SIGNAL(triggered(bool)),
                     this, SLOT(slotScrollTriggered()));
    m_graphicsview->addAction(act);

    act = new QAction(QObject::tr("Scroll down"), m_graphicsview);
    act->setObjectName("acImageScrollDown");
    act->setShortcutContext(Qt::WidgetShortcut);
    QObject::connect(act, SIGNAL(triggered(bool)),
                     this, SLOT(slotScrollTriggered()));
    m_graphicsview->addAction(act);

    act = new QAction(QObject::tr("Scroll left"), m_graphicsview);
    act->setObjectName("acImageScrollLeft");
    act->setShortcutContext(Qt::WidgetShortcut);
    QObject::connect(act, SIGNAL(triggered(bool)),
                     this, SLOT(slotScrollTriggered()));
    m_graphicsview->addAction(act);

    act = new QAction(QObject::tr("Scroll page up"), m_graphicsview);
    act->setObjectName("acImageScrollPageUp");
    act->setShortcut(QKeySequence(Qt::Key_PageUp));
    act->setShortcutContext(Qt::WidgetShortcut);
    QObject::connect(act, SIGNAL(triggered(bool)),
                     this, SLOT(slotScrollTriggered()));
    m_graphicsview->addAction(act);

    act = new QAction(QObject::tr("Scroll page down"), m_graphicsview);
    act->setObjectName("acImageScrollPageDown");
    act->setShortcut(QKeySequence(Qt::Key_PageDown));
    act->setShortcutContext(Qt::WidgetShortcut);
    QObject::connect(act, SIGNAL(triggered(bool)),
                     this, SLOT(slotScrollTriggered()));
    m_graphicsview->addAction(act);

    act = new QAction(QObject::tr("Scroll page right"), m_graphicsview);
    act->setObjectName("acImageScrollPageRight");
    act->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_PageDown));
    act->setShortcutContext(Qt::WidgetShortcut);
    QObject::connect(act, SIGNAL(triggered(bool)),
                     this, SLOT(slotScrollTriggered()));
    m_graphicsview->addAction(act);

    act = new QAction(QObject::tr("Scroll page left"), m_graphicsview);
    act->setObjectName("acImageScrollPageLeft");
    act->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_PageUp));
    act->setShortcutContext(Qt::WidgetShortcut);
    QObject::connect(act, SIGNAL(triggered(bool)),
                     this, SLOT(slotScrollTriggered()));
    m_graphicsview->addAction(act);

    act = new QAction(QObject::tr("Scroll to start"), m_graphicsview);
    act->setObjectName("acScrollHome");
    act->setShortcut(QKeySequence(Qt::Key_Home));
    act->setShortcutContext(Qt::WidgetShortcut);
    QObject::connect(act, SIGNAL(triggered(bool)),
                     this, SLOT(slotScrollImageHomeEndTriggered()));
    m_graphicsview->addAction(act);

    act = new QAction(QObject::tr("Scroll to end"), m_graphicsview);
    act->setObjectName("acScrollEnd");
    act->setShortcut(QKeySequence(Qt::Key_End));
    act->setShortcutContext(Qt::WidgetShortcut);
    QObject::connect(act, SIGNAL(triggered(bool)),
                     this, SLOT(slotScrollImageHomeEndTriggered()));
    m_graphicsview->addAction(act);
}

QSizeF CImageViewWidget::viewSize() const
{
    if (hasCurrentImage())
        return QSizeF(currentImageView()->rect().size());

    return QSizeF();
}
