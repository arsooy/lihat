#ifndef CIMAGEVIEWWIDGET_H
#define CIMAGEVIEWWIDGET_H

#include <QGraphicsScene>
#include <QMultiMap>
#include <QSizeF>
#include <QString>
#include <QUrl>
#include <QWidget>


class QAction;
class QResizeEvent;
class QStatusBar;
class QWheelEvent;

namespace Lihat
{
    namespace Thumbnails
    {

        class CThumbnailView;
        class CThumbnailViewWidget;

    } // namespace Thumbnails

    namespace Image
    {
        class CGIImageView;
        class CGISceneBase;
        class CGraphicsView;
        class CImagesWidget;

        class CImageViewWidget:
            public QWidget
        {
            Q_OBJECT
        public:
            explicit CImageViewWidget(QWidget* parent = 0);
            virtual ~CImageViewWidget();

            QUrl currentLoadedParentURL() const;
            QUrl currentLoadedURL() const;

            bool  isScaledToFit() const;
            void  setScaledToFit(bool scaled);

            qreal scale() const;
            bool  setScale(qreal scale);

            CGIImageView* currentImageView() const;
            void          centerView();

            bool   load(const QUrl& url);
            void   setThumbnailViewWidget(Thumbnails::CThumbnailViewWidget* thumbnailviewwidget);
            QSizeF viewSize() const;

        public slots:
            void beginViewUpdate();
            void endViewUpdate();
            bool reload();
            void zoomIn();
            void zoomOut();
            void zoomReset();

        signals:
            void imageLoaded(bool loaded, const QUrl& url);
            void toThumbnailViewRequested();
            void nextImageRequested();
            void previousImageRequested();

        protected:
            CGIImageView*                      m_currentimageview;
            bool                               m_docenteronshow;
            QSize                              m_fullsize;
            QGraphicsScene                     m_graphicsscene;
            CGraphicsView*                     m_graphicsview;
            QList< CGIImageView* >             m_imageviews;
            CImagesWidget*                     m_imageswidget;
            QMultiMap< QString, QKeySequence > m_keysequences;
            bool                               m_lastloadstatus;
            Thumbnails::CThumbnailViewWidget*  m_linkedthumbnailviewwidget;
            CGISceneBase*                      m_scenebasewidget;
            unsigned int                       m_updateviewcount;

            // QWidget
            virtual void hideEvent(QHideEvent* event);
            virtual void resizeEvent(QResizeEvent* event);
            virtual void showEvent(QShowEvent* event);

            void attachToMainWindowStatusBar(QStatusBar* statusbar);
            void detachFromMainWindowStatusBar(QStatusBar* statusbar);
            void updateImageViewGeometry();

            bool setStatusText(const QString& stext, const QString& stooltip = QString(), int index = 0);
            bool updateStatusTexts(CGIImageView* imgcontainer = 0);

            bool addImageView(CGIImageView* ivcont, int row, int col);
            bool hasCurrentImage() const;

            QAction* getAction(const QString& name, bool mustexists = true);

            void readPersistence();
            void writePersistence();

            void applyActionKeySequences();
            void initActions();

        private slots:
            void slotGraphicsViewResized();
            void slotImageViewFocused();
            void slotImageViewLoadFinished(bool loaded, const QUrl& url);
            void slotImageViewScaleChanged(qreal scale);
            void slotImageViewSizeChanged();
            void slotImageViewURLDropped(const QUrl& url);
            void slotImagesCurrentModeChanged();
            void slotImagesToggled(bool checked);
            void slotScaleToFitHovered();
            void slotScaleToFitTriggered(bool checked);
            void slotScrollImageHomeEndTriggered();
            void slotScrollTriggered();
            void slotZoomInHovered();
            void slotZoomOutHovered();
            void slotZoomResetHovered();

        };

    } // namespace Image
} // namespace Lihat

#endif // CIMAGEVIEWWIDGET_H
