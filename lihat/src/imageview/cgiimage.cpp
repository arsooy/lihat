#include "cgiimage.h"

#include "../imageload/imageload.h"
#include "cgiimageview.h"
#include "cloadresult.h"

#include <QAction>
#include <QCursor>
#include <QGraphicsObject>
#include <QGraphicsSceneMouseEvent>
#include <QList>
#include <QMovie>
#include <QPainter>
#include <QRectF>
#include <QString>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::Image;

CGIImage::CGIImage(CGIImageView* container, Qt::WindowFlags wFlags):
    QGraphicsWidget(container, wFlags),
    m_ismousedowndragging(false),
    m_loadedjob(0)
{
    QObject::connect(&m_scrollcontext.TimeLine, SIGNAL(frameChanged(int)),
                     this, SLOT(slotScrollTimeLineFrameChanged(int)));
    QObject::connect(&m_scrollcontext.TimeLine, SIGNAL(finished()),
                     this, SLOT(slotScrollTimeLineFinished()));
    QObject::connect(container, SIGNAL(geometryChanged()),
                     this, SLOT(slotContainerGeometryChanged()));

    readPersistence();
}

CGIImage::~CGIImage()
{
    writePersistence();
    if (m_loadedjob)
    {
        CLoadResult* lresult = dynamic_cast< CLoadResult* >(m_loadedjob->result());
        if (lresult)
            lresult->dropLoaded();
        delete m_loadedjob;
    }
    m_loadedjob = 0;
}

QRectF CGIImage::boundingRect() const
{
    if (m_loadedjob)
    {
        CLoadResult* lresult = dynamic_cast< CLoadResult* >(m_loadedjob->result());
        if (lresult && (lresult->ResultCode == CLoadResult::ILRC_OK))
        {
            QSizeF isize;
            switch (lresult->LoadedKind)
            {
                case CLoadResult::LK_MOVIE:
                {
                    isize = lresult->Movie->currentPixmap().size();

                    break;
                }

                case CLoadResult::LK_PIXMAP:
                {
                    isize = lresult->Pixmap->size();

                    break;
                }

                default:
                    ;
            }

            if (isize.isValid())
            {
                QRectF result(QPointF(0, 0), isize);
                return result;
            }
        }
    }

    return QRectF();
}

bool CGIImage::centerView()
{
    return false;
}

void CGIImage::imageMouseDragEnd(QGraphicsSceneMouseEvent* gsmevent, QGraphicsItem* gitem)
{
    Q_UNUSED(gsmevent);

    m_ismousedowndragging = !m_ismousedowndragging;
    if (gitem->cursor().shape() == Qt::ClosedHandCursor)
        gitem->setCursor(Qt::OpenHandCursor);
}

void CGIImage::imageMouseDragMove(QGraphicsSceneMouseEvent* gsmevent, QGraphicsItem* gitem)
{
    Q_UNUSED(gitem);

    QPointF distance(gsmevent->lastPos() - gsmevent->pos());
    QPointF topleft = viewTopLeft();
    setViewTopLeft(topleft + distance);
}

void CGIImage::imageMouseDragStart(QGraphicsSceneMouseEvent* gsmevent, QGraphicsItem* gitem)
{
    Q_UNUSED(gsmevent);

    m_ismousedowndragging = true;
    gitem->setCursor(Qt::ClosedHandCursor);
}

CGIImageView* CGIImage::container() const
{
    QGraphicsObject* parentobj = parentObject();
    if (!parentobj)
        return 0;

    CGIImageView* result = qobject_cast< CGIImageView* >(parentobj);
    return result;
}

void CGIImage::setScale(qreal factor)
{
    QGraphicsWidget::setScale(factor);
    emit viewableAreaUpdated();
    emit scaleChanged();
}

bool CGIImage::load(const CLoadJob& job)
{
    CLoadJob* loadjob = new CLoadJob(job);
    bool loaded = processLoadJob(loadjob);
    if (!loaded)
    {
        loadjob->dropResult();
        delete loadjob;

        return false;
    }

    if (loadjob->result())
        return loadjob->result()->ResultCode == ImageLoad::CBaseLoadResult::ILRC_OK;

    return false;
}

bool CGIImage::load(const QUrl& url)
{
    CLoadJob job(this, url);
    return load(job);
}

CLoadResult* CGIImage::loadResult() const
{
    return dynamic_cast< CLoadResult* >(m_loadedjob->result());
}

void CGIImage::mouseMoveEvent(QGraphicsSceneMouseEvent* gsmevent)
{
    QGraphicsItem::mouseMoveEvent(gsmevent);

    if (m_ismousedowndragging)
    {
        imageMouseDragMove(gsmevent, this);
        gsmevent->accept();
    }
}

void CGIImage::mousePressEvent(QGraphicsSceneMouseEvent* gsmevent)
{
    QGraphicsItem::mousePressEvent(gsmevent);

    if (gsmevent->button() == Qt::LeftButton)
    {
        imageMouseDragStart(gsmevent, this);
        gsmevent->accept();
    }
}

void CGIImage::mouseReleaseEvent(QGraphicsSceneMouseEvent* gsmevent)
{
    QGraphicsItem::mouseReleaseEvent(gsmevent);

    if (gsmevent->button() == Qt::LeftButton)
    {
        imageMouseDragEnd(gsmevent, this);
        gsmevent->accept();
    }
}

void CGIImage::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    QPixmap drawpixmap;
    CLoadResult* lresult = 0;
    if (m_loadedjob)
        lresult = dynamic_cast< CLoadResult* >(m_loadedjob->result());

    if (!lresult)
        return;

    switch (lresult->LoadedKind)
    {
        case CLoadResult::LK_MOVIE:
        {
            drawpixmap = QPixmap(lresult->Movie->currentPixmap());

            break;
        }

        case CLoadResult::LK_PIXMAP:
        {
            drawpixmap = *(lresult->Pixmap);

            break;
        }

        default:
            ;
    }

    painter->save();
    painterDrawPixmap(painter, &drawpixmap);
    painter->restore();
}

void CGIImage::writePersistence()
{

}

void CGIImage::readPersistence()
{
    m_scrollcontext.TimeLine.setFrameRange(1, 100);
    m_scrollcontext.TimeLine.setDuration(200);
}

void CGIImage::startScrolling()
{
    if (m_scrollcontext.Start.isNull() && m_scrollcontext.Finish.isNull())
        return;

    m_scrollcontext.LastAt.start();
    m_scrollcontext.TimeLine.start();
}

void CGIImage::stopScrolling()
{
    m_scrollcontext.TimeLine.stop();
    m_scrollcontext.Start = m_scrollcontext.Finish = QPointF(0, 0);
}

bool CGIImage::processLoadJob(CLoadJob* job)
{
    dispatchLoadJob(job);
    bool loaded = (job->result()) && (job->result()->ResultCode == CLoadResult::ILRC_OK);
    if (loaded)
    {
        CLoadResult* lastloadresult = 0;
        if (m_loadedjob)
            lastloadresult = dynamic_cast< CLoadResult* >(m_loadedjob->result());

        if (lastloadresult)
        {
            if((lastloadresult->LoadedKind == CLoadResult::LK_MOVIE) && lastloadresult->Movie)
                lastloadresult->Movie->stop();

            lastloadresult->dropLoaded();
        }

        CLoadResult* lresult = dynamic_cast< CLoadResult* >(job->result());

        if (lresult)
        {
            switch (lresult->LoadedKind)
            {
                case CLoadResult::LK_IMAGE:
                {
                    // replace QImage to QPixmap, we do this here because
                    // QPixmap need to be (re)created in main thread
                    QPixmap* pix = new QPixmap(QPixmap::fromImage(*(lresult->Image)));
                    // drop the previously loaded image ..
                    lresult->dropLoaded();
                    // to replace it with pixmap
                    lresult->setLoadedPixmap(pix);

                    break;
                }

                case CLoadResult::LK_MOVIE:
                {
                    QObject::connect(lresult->Movie, SIGNAL(frameChanged(int)),
                                     this, SLOT(slotMovieFrameChanged(int)));
                    lresult->Movie->start();

                    break;
                }

                default:
                    ;
            }
        }

        if (m_loadedjob)
            delete m_loadedjob;
        m_loadedjob = job;

        // recalculate rectangles
        m_viewrect = QRectF();
        slotContainerGeometryChanged();
        emit containerRepaintNeeded();
    }

    emit imageLoadFinished(loaded, job->URL);
    return loaded;
}

void CGIImage::painterDrawPixmap(QPainter* painter, QPixmap* pixmap, QRectF boundingrect, QPointF topleft)
{
    if (!pixmap || pixmap->isNull())
        return;

    if (boundingrect.isNull())
        boundingrect= boundingRect();

    if (topleft.isNull())
        topleft = m_viewrect.topLeft();

    QRectF drawsrcrect(topleft, boundingrect.size());
    //painter->fillRect(boundingrect, QColor(128, 0, 128));
    painter->drawPixmap(boundingrect, *pixmap, drawsrcrect);

}

void CGIImage::panViewRect(const QPointF& amount, bool usescroll)
{
    QSizeF isize = boundingRect().size();
    QSizeF drawnsize = m_viewrect.size() / scale();

    qreal viewingratio = 1.;
    qreal step = 0;

    if (amount.y() == 0)
    {
        // scrolling horizontally
        viewingratio = qMin(isize.width() / drawnsize.width(), 1.);
        step = (drawnsize.width()  * viewingratio) * amount.x();
    }
    else if (amount.x() == 0)
    {
        // scrolling vertically
        viewingratio = qMin(isize.height() / drawnsize.height(), 1.);
        step = (drawnsize.height()  * viewingratio) * amount.y();
    }
    else
    {
        // scrolling horizontally & vertically
        viewingratio = qMax< qreal >( qMin(isize.width()  / drawnsize.width(),  1.),
                                      qMin(isize.height() / drawnsize.height(), 1.) );
        step = qMax< qreal >((drawnsize.width()  * viewingratio) * amount.x(),
                             (drawnsize.height() * viewingratio) * amount.y());
    }

    QPointF delta(0, 0);
    if (amount.x() != 0)
        delta.setX(step);
    if (amount.y() != 0)
        delta.setY(step);

    if (usescroll)
    {
        // set new view position
        m_scrollcontext.Start = m_viewrect.topLeft();
        m_scrollcontext.Finish = m_scrollcontext.Start + delta;

        startScrolling();
    }
    else
        setViewTopLeft(m_viewrect.topLeft() + delta);
}

bool CGIImage::setViewTopLeft(const QPointF& topleft)
{
    CGIImageView* ctnr = container();
    Q_ASSERT(ctnr);

    QSizeF drawablesize;
    if (m_loadedjob)
        drawablesize = boundingRect().size();

    QSizeF containersize;
    containersize = ctnr->boundingRect().size() / scale();

    if (!(containersize.isValid() && drawablesize.isValid()))
        return false;

    // get paint topleft by taking consideration of viewable dimension (ie.
    // currently available viewing size) boundaries:
    //  - no less than (0, 0) which is the most top-left, and ..
    //  - no more than current dimension - its viewable dimension
    // its harder to put it into words, but (I hope) the next code should be
    // self-explanatory
    QPointF validtopleft(qBound< qreal >(0, topleft.x(), drawablesize.width() - containersize.width()),
                         qBound< qreal >(0, topleft.y(), drawablesize.height() - containersize.height()));
    if (validtopleft != m_viewrect.topLeft())
    {
        m_viewrect.moveTopLeft(validtopleft);
        emit viewableAreaUpdated();

        return true;
    }

    return false;
}

QPainterPath CGIImage::shape() const
{
    QPainterPath result;
    QRectF brect = boundingRect();

    brect.adjust(-5, -5, 5, 5);
    result.addRect(brect);

    return result;
}

void CGIImage::slotContainerGeometryChanged()
{
    Q_ASSERT(container());

    CGIImageView* imgcontainer = container();
    QSizeF nsize(imgcontainer->size());

    CLoadResult* lresult = 0;
    if (m_loadedjob)
        lresult = dynamic_cast< CLoadResult* >(m_loadedjob->result());

    if (lresult)
        nsize.boundedTo(lresult->viewSize());

    prepareGeometryChange();
    m_viewrect.setSize(nsize);
    emit viewableAreaUpdated();
}

void CGIImage::slotScrollImageHomeEndTriggered()
{

}

void CGIImage::slotScrollTimeLineFinished()
{
    stopScrolling();
}

void CGIImage::slotScrollTimeLineFrameChanged(int /* frame */)
{
    QPointF traveldistance = (m_scrollcontext.Finish - m_scrollcontext.Start) * m_scrollcontext.TimeLine.currentValue();
    setViewTopLeft(m_scrollcontext.Start + traveldistance);
}

void CGIImage::slotMovieFrameChanged(int /* frame */)
{
    update();
}

QUrl CGIImage::url() const
{
    if (m_loadedjob)
        return m_loadedjob->URL;

    return QUrl();
}

QPointF CGIImage::viewTopLeft() const
{
    return m_viewrect.topLeft();
}

bool CGIImage::scrollTo(const QPointF pos, bool scrolleffect)
{
    stopScrolling();

    if (scrolleffect)
    {
        // set new view position
        m_scrollcontext.Start = m_viewrect.topLeft();
        m_scrollcontext.Finish = pos;

        startScrolling();
        return true;
    }
    else
        return setViewTopLeft(pos);

    return false;
}

bool CGIImage::scrollAdjust(const QPointF dist)
{
    int dirf = 0;
    if (dist.x() > 0)
        dirf |= 1;
    else if (dist.x() < 0)
        dirf |= 2;
    if (dist.y() > 0)
        dirf |= 4;
    else if (dist.y() < 0)
        dirf |= 8;

    bool matcheslast = false;
    bool rapidkeystroke = (m_scrollcontext.LastAt.elapsed() <= m_scrollcontext.RapidTreshold);
    if (rapidkeystroke)
        matcheslast = dirf == m_scrollcontext.LastType;

    // do not do 'smooth' scrolling if user press the key in quick succession
    bool scrolleffect = !(rapidkeystroke && matcheslast);

    stopScrolling();

    m_scrollcontext.LastType = dirf;
    panViewRect(dist, scrolleffect);

    return true;
}

bool CGIImage::stopAtFrame(int framenum)
{
    if (m_loadedjob && m_loadedjob->result())
    {
        CLoadResult* lresult = dynamic_cast< CLoadResult* >(m_loadedjob->result());
        if ((lresult->LoadedKind == CLoadResult::LK_MOVIE) && lresult->Movie)
        {
            lresult->Movie->stop();
            return lresult->Movie->jumpToFrame(framenum);
        }
    }

    return false;
}


CGIImage::SViewScrollContext::SViewScrollContext():
    RapidTreshold(250)
{

}
