#include "cstatusbarbuttons.h"

#include "cimageviewwidget.h"

#include <QAction>
#include <QHBoxLayout>
#include <QToolButton>


using namespace Lihat;
using namespace Lihat::Image;

CStatusBarButtons::CStatusBarButtons(CImageViewWidget* imageviewwidget, QWidget* parent):
    QWidget(parent)
{
    Q_ASSERT(imageviewwidget);
    setContentsMargins(0, 0, 0, 0);

    QHBoxLayout* layout = new QHBoxLayout(this);
    layout->setSpacing(0);
    layout->setMargin(0);

    QAction* acImageViewImages = getActionOf(imageviewwidget, "acImageViewImages");
    {
        QToolButton* tbtnImages = new QToolButton(this);
        tbtnImages->setObjectName("tbtnImages");
        tbtnImages->setDefaultAction(acImageViewImages);
        tbtnImages->setAutoRaise(true);
        tbtnImages->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
        tbtnImages->setCheckable(acImageViewImages->isCheckable());

        layout->addWidget(tbtnImages);
    }

    QAction* acImageViewInformation = getActionOf(imageviewwidget, "acImageViewInformation");
    {
        QToolButton* tbtnInformation = new QToolButton(this);
        tbtnInformation->setObjectName("tbtnInformation");
        tbtnInformation->setDefaultAction(acImageViewInformation);
        tbtnInformation->setAutoRaise(true);
        tbtnInformation->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);

        layout->addWidget(tbtnInformation);
    }

}

CStatusBarButtons::~CStatusBarButtons()
{

}

QAction* CStatusBarButtons::getActionOf(QWidget* what, const QString& name, bool mustexists)
{
    Q_ASSERT(what);

    QAction* result = what->findChild< QAction* >(name);
    if (mustexists)
        Q_ASSERT(result);

    return result;
}
