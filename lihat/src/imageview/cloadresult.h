#ifndef CLOADRESULT_H
#define CLOADRESULT_H

#include "../imageload/cbaseloadresult.h"

#include <QRect>


class QImage;
class QMovie;
class QPixmap;

namespace Lihat
{
    namespace Image
    {

        class CLoadResult:
            public ImageLoad::CBaseLoadResult
        {
        public:
            enum ELoadedKind
            {
                LK_INVALID = 0,
                LK_PIXMAP  = 1,
                LK_IMAGE   = 2,
                LK_MOVIE   = 3
            };


            ELoadedKind  LoadedKind;
            int          SubImageIndex;
            union
            {
                QImage*   Image;
                QMovie*   Movie;
                QPixmap*  Pixmap;
            };

            explicit CLoadResult(EImageLoadResultCode resultcode);
            virtual ~CLoadResult();

            CLoadResult& operator=(const CLoadResult& other);
            void dropLoaded();

            QPixmap* copyAsPixmap(const QRect& srcrect = QRect());
            void     setLoadedImage(QImage* image);
            void     setLoadedMovie(QMovie* movie);
            void     setLoadedPixmap(QPixmap* pixmap);
            QSize    viewSize() const;
        };

    } // namespace Image
} // namespace Lihat

#endif // CLOADRESULT_H
