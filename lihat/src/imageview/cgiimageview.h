#ifndef CGIIMAGEVIEW_H
#define CGIIMAGEVIEW_H

#include <QGraphicsWidget>
#include <QUrl>


namespace Lihat
{
    namespace Image
    {

        class CGIImage;
        class CGIOverview;

        class CGIImageView:
            public QGraphicsWidget
        {
            Q_OBJECT
        public:
            explicit CGIImageView(QGraphicsItem* parent = 0, Qt::WindowFlags wFlags = 0);
            virtual ~CGIImageView();

            // QGraphicsWidget
            virtual QRectF       boundingRect() const;
            virtual void         hoverMoveEvent(QGraphicsSceneHoverEvent* gshevent);
            virtual QPainterPath shape() const;

            QUrl url() const;

            bool isScaledToFit() const;
            void setScaledToFit(bool scaledtofit);

            CGIImage*    image();
            CGIOverview* overview();

            void centerView();
            bool load(const QUrl& url);

        signals:
            void focusLost();
            void focusRecieved();
            void loadFinished(bool loaded, QUrl);
            void imageScaleChanged();
            void sizeChanged();
            void toNextImageRequested();
            void toPreviousImageRequested();
            void urlDropped(const QUrl& url);

        public slots:
            bool reload();

        protected:
            bool           m_adjustimagepositiononnextresize;
            CGIImage*      m_image;
            qreal          m_mousewheelscrollsteppercent;
            CGIOverview*   m_overview;
            unsigned short m_overviewmargin;
            bool           m_scaledtofit;
            QSizeF         m_size;

            // QGraphicsWidget
            virtual void focusInEvent(QFocusEvent* event);
            virtual void focusOutEvent(QFocusEvent* event);
            virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* gsmevent);
            virtual void mousePressEvent(QGraphicsSceneMouseEvent* gsmevent);
            virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* gsmevent);
            virtual void resizeEvent(QGraphicsSceneResizeEvent* gsrevent);
            virtual void wheelEvent(QGraphicsSceneWheelEvent* gswevent);

            QAction* getAction(const QString& name, bool mustexists = true);

            void adjustOverviewPosition();
            bool isOverviewNeccessary() const;
            void showOverview();

            void adjustImagePosition();
            void adjustImageViewPosition(bool center=true);
            void scaledToFitUpdate();

        private slots:
            void slotImageViewContainerRepaintNeeded();
            void slotImageViewViewableAreaUpdated();

        };

    } // namespace Image
} // namespace Lihat

#endif // CGIIMAGEVIEW_H
