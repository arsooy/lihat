#ifndef CGIIMAGE_H
#define CGIIMAGE_H

#include "cloadjob.h"

#include <QGraphicsWidget>
#include <QKeySequence>
#include <QTime>
#include <QTimeLine>
#include <QUrl>

namespace Lihat
{
    namespace Image
    {

        class CGIImageView;
        class CLoadResult;

        class CGIImage:
            public QGraphicsWidget
        {
            Q_OBJECT
        public:
            explicit CGIImage(CGIImageView* container, Qt::WindowFlags wFlags = 0);
            virtual ~CGIImage();

            // QGraphicsWidget
            virtual QRectF       boundingRect() const;
            virtual QPainterPath shape() const;

            virtual void setScale(qreal factor);

            bool         load(const CLoadJob& params);
            bool         load(const QUrl& url);
            CLoadResult* loadResult() const;

            bool    setViewTopLeft(const QPointF& topleft);
            QPointF viewTopLeft() const;

            bool scrollTo(const QPointF pos, bool smooth = true);
            bool scrollAdjust(const QPointF dist);

            CGIImageView* container() const;
            QUrl          url() const;
            bool          stopAtFrame(int framenum);

        public slots:
            bool centerView();
            void imageMouseDragEnd(QGraphicsSceneMouseEvent* gsmevent, QGraphicsItem* gitem);
            void imageMouseDragMove(QGraphicsSceneMouseEvent* gsmevent, QGraphicsItem* gitem);
            void imageMouseDragStart(QGraphicsSceneMouseEvent* gsmevent, QGraphicsItem* gitem);

        signals:
            void containerRepaintNeeded();
            void hintNextImage();
            void hintPreviousImage();
            void imageLoadFinished(bool loaded, const QUrl& url);
            void viewableAreaUpdated();

        protected:
            // QGraphicsWidget
            virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* gsmevent);
            virtual void mousePressEvent(QGraphicsSceneMouseEvent* gsmevent);
            virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* gsmevent);
            virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);

            void writePersistence();
            void readPersistence();

            void startScrolling();
            void stopScrolling();

            bool processLoadJob(CLoadJob* job);

            void painterDrawPixmap(QPainter* painter, QPixmap* pixmap, QRectF boundingrect = QRectF(), QPointF topleft = QPointF());
            void panViewRect(const QPointF& amount, bool usescroll);

        private:
            struct SViewScrollContext
            {
                QPointF Start;
                QPointF Finish;

                QTime LastAt;
                int   LastType;

                unsigned short RapidTreshold;
                QTimeLine      TimeLine;

                SViewScrollContext();
            };

            bool               m_ismousedowndragging;
            CLoadJob*          m_loadedjob;
            SViewScrollContext m_scrollcontext;
            QRectF             m_viewrect;

        private slots:
            void slotContainerGeometryChanged();
            void slotMovieFrameChanged(int frame);
            void slotScrollImageHomeEndTriggered();
            void slotScrollTimeLineFinished();
            void slotScrollTimeLineFrameChanged(int frame);

        };

    } // namespace Image
} // namespace Lihat

#endif // CGIIMAGE_H
