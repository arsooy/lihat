#include "cloadresult.h"

#include <QImage>
#include <QMovie>
#include <QPixmap>


using namespace Lihat;
using namespace Lihat::Image;

CLoadResult::CLoadResult(CBaseLoadResult::EImageLoadResultCode resultcode):
    CBaseLoadResult(resultcode),
    LoadedKind(LK_INVALID),
    SubImageIndex(-1),
    Pixmap(0)
{

}

CLoadResult::~CLoadResult()
{

}

CLoadResult& CLoadResult::operator=(const CLoadResult& other)
{
    if (this == &other)
        return *this;

    CBaseLoadResult::operator=(other);
    LoadedKind = other.LoadedKind;
    SubImageIndex = other.SubImageIndex;
    Pixmap = other.Pixmap;
    //Movie = other.Movie;
    //Image = other.Image;

    return *this;
}

void CLoadResult::dropLoaded()
{
    switch (LoadedKind)
    {
        case LK_IMAGE:
        {
            if (Image)
                delete Image;
            Image = 0;

            break;
        }

        case LK_MOVIE:
        {
            if (Movie)
                delete Movie;
            Movie = 0;

            break;
        }

        case LK_PIXMAP:
        {
            if (Pixmap)
                delete Pixmap;
            Pixmap = 0;

            break;
        }

        default:
            ;
    }
}

QPixmap* CLoadResult::copyAsPixmap(const QRect& srcrect)
{
    QPixmap* result = 0;
    switch (LoadedKind)
    {
        case LK_MOVIE:
        {
            if (Movie)
                result = new QPixmap(Movie->currentPixmap().copy(srcrect));

            break;
        }

        case LK_PIXMAP:
        {
            if (Pixmap)
                result = new QPixmap(Pixmap->copy(srcrect));

            break;
        }

        default:
            ;
    }

    return result;
}

void CLoadResult::setLoadedImage(QImage* image)
{
    LoadedKind = LK_IMAGE;
    Image = image;
}

void CLoadResult::setLoadedMovie(QMovie* movie)
{
    LoadedKind = LK_MOVIE;
    Movie = movie;
}

void CLoadResult::setLoadedPixmap(QPixmap* pixmap)
{
    LoadedKind = LK_PIXMAP;
    Pixmap = pixmap;
}

QSize CLoadResult::viewSize() const
{
    switch (LoadedKind)
    {
        case LK_IMAGE:
            return Image->size();

        case LK_MOVIE:
            return Movie->currentPixmap().size();

        case LK_PIXMAP:
            return Pixmap->size();

        default:
            return QSize();
    }
}
