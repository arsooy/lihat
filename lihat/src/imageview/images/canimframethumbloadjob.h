#ifndef CANIMFRAMETHUMBLOADJOB_H
#define CANIMFRAMETHUMBLOADJOB_H

#include "../../thumbnailview/cthumbloadjob.h"

#include <QSize>

class QMovie;

namespace Lihat
{
    namespace Thumbnails
    {

        class  CBaseThumbnailView;
        struct SThumbnailItem;

    } // namespace Thumbnails

    namespace Image
    {

        class CAnimFrameThumbLoadJob:
            public Thumbnails::CThumbLoadJob
        {
        public:
            QMovie*      Animation;
            unsigned int Frame;

            explicit CAnimFrameThumbLoadJob(Thumbnails::CBaseThumbnailView* requester, Thumbnails::SThumbnailItem* titem, QMovie* anim, const QSize& loadsize, unsigned int frame);
            virtual ~CAnimFrameThumbLoadJob();

            bool operator==(const CAnimFrameThumbLoadJob& other);

            // CThumbLoadJob
            virtual Thumbnails::CThumbLoadJob::EImageLoadType imageLoadType() const;
            virtual bool                                      isValid() const;
            virtual bool                                      load();

        };

    } // namespace Image
} // namespace Lihat

#endif // CANIMFRAMETHUMBLOADJOB_H
