#ifndef CIMAGESITEMDELEGATE_H
#define CIMAGESITEMDELEGATE_H

#include "../../thumbnailview/ipixmapuser.h"

#include <QStyledItemDelegate>


class QPixmap;

namespace Lihat
{

    class CImagesItemDelegate:
        public QStyledItemDelegate,
        public Thumbnails::IPixmapUser
    {
        Q_OBJECT
    public:
        const unsigned char  ITEM_SPACING = 2;
        const unsigned char  ITEM_PADDING = 6;


        explicit CImagesItemDelegate(QObject* parent = 0);
        virtual ~CImagesItemDelegate();

        // IPixmapUser
        virtual bool setThumbnailViewPixmapProvider(Thumbnails::IPixmapProvider* provider);

    protected:
        virtual void  paint(QPainter* painter, const QStyleOptionViewItem& soption, const QModelIndex& index) const;
        virtual QSize sizeHint(const QStyleOptionViewItem& soption, const QModelIndex& index) const;

        Thumbnails::IPixmapProvider* m_thumbnailviewpixmapprovider;

    };

} // namespace Lihat

#endif // CIMAGESITEMDELEGATE_H
