#ifndef CIMAGESLISTVIEW_H
#define CIMAGESLISTVIEW_H

#include "../../thumbnailview/cthumbnailview.h"
#include "../../thumbnailview/ithumbnailsloader.h"
#include "../../thumbnailview/sthumbnailitem.h"

#include <QUrl>


class QPixmap;

namespace Lihat
{
    namespace Thumbnails
    {

        class CThumbnailView;

    } // namespace Thumbnails

    namespace Image
    {

        class CImagesListViewPrivate;

        class CImagesListView:
            public Thumbnails::CThumbnailView
        {
            Q_OBJECT
        public:
            enum EImagesViewMode
            {
                IVM_INVALID     = 0,
                IVM_SIBLINGS    = 1,
                IVM_SELECTIONS  = 2,
                IVM_SUBIMAGES   = 3
            };


            explicit CImagesListView(QWidget* parent = 0);
            virtual ~CImagesListView();

            bool setSourceThumbnailView(Thumbnails::CThumbnailView* thumbnailview);
            Thumbnails::CThumbnailView* sourceThumbnailView() const;

            bool setImagesViewMode(EImagesViewMode mode);
            EImagesViewMode viewMode() const;

            virtual Thumbnails::SThumbnailItem* thumbnailItemAt(const QModelIndex& index);

        protected:
            // QListView
            virtual void  currentChanged(const QModelIndex& current, const QModelIndex& previous);
            virtual void  hideEvent(QHideEvent* event);
            virtual void  resizeEvent(QResizeEvent* event);
            virtual void  showEvent(QShowEvent* event);

            virtual void  initItemDelegate();
            virtual bool  siftLoadItemThumbnail(const QModelIndex& index);
            virtual void  thumbnailViewResized();

        private:
            Q_DECLARE_PRIVATE(CImagesListView)

        private slots:
            void slotSourceViewSelectionModelCurrentChanged(const QModelIndex& current, const QModelIndex& previous);
            void slotImageViewLoadFinished(bool loaded, const QUrl& url);
        };

    } // namespace Thumbnails
} // namespace Lihat

#endif // CIMAGESLISTVIEW_H
