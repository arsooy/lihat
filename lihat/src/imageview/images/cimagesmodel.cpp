#include "cimagesmodel.h"

#include "../../thumbnailview/sthumbnailitem.h"

#include <QFile>

using namespace Lihat;
using namespace Lihat::Image;

CImagesModel::CImagesModel(QObject* parent):
    CBaseThumbnailModel(parent),
    m_imagereader(0),
    m_iodevice(0)
{

}

CImagesModel::~CImagesModel()
{
    dropImageReader();
    dropIODevice();
    thumbnailItemsClear();
}

QVariant CImagesModel::data(const QModelIndex& index, int role) const
{
    Q_UNUSED(index);
    Q_UNUSED(role);

    return QVariant();
}

QModelIndex CImagesModel::index(int row, int column, const QModelIndex& parent) const
{
    if (!hasIndex(row, column, parent))
        return QModelIndex();

    return createIndex(row, column, m_items.at(row));
}

void CImagesModel::disposeThumbnailItem(Thumbnails::SThumbnailItem* titem)
{
    if (titem)
        delete titem;
}

int CImagesModel::indexOfThumbnailItem(Thumbnails::SThumbnailItem* titem)
{
    return m_items.indexOf(titem);
}

int CImagesModel::rowCount(const QModelIndex& /* parent */) const
{
    return thumbnailItemsCount();
}

Thumbnails::SThumbnailItem* CImagesModel::thumbnailItem(int providerindex)
{
    return m_items.at(providerindex);
}

int CImagesModel::thumbnailItemsCount() const
{
    return m_items.count();
}

void CImagesModel::thumbnailItemsClear()
{
    int idx;
    while (m_items.count() > 0)
    {
        idx = m_items.count() - 1;
        thumbnailItemRemove(idx);
    }
}

QUrl CImagesModel::thumbnailItemURL(Thumbnails::SThumbnailItem* titem)
{
    QUrl result;
    if (indexOfThumbnailItem(titem) == -1)
        return QUrl();

    return imageURL();
}

void CImagesModel::thumbnailItemRemove(int idx)
{
    beginRemoveRows(QModelIndex(), idx, idx);
    disposeThumbnailItem(m_items.takeAt(idx));
    endRemoveRows();
}

Thumbnails::IItemsProvider::EProviderType CImagesModel::thumbnailItemsProviderType() const
{
    return Thumbnails::IItemsProvider::PT_SUBIMAGE;
}

void CImagesModel::dropImageReader()
{
    if (m_imagereader)
        delete m_imagereader;
    m_imagereader = 0;
}

void CImagesModel::dropIODevice()
{
    if (m_iodevice)
        delete m_iodevice;
    m_iodevice = 0;
}

bool CImagesModel::inspectImageReader()
{
    if (!m_imagereader->canRead())
        return false;

    for (int i = 0; i < m_imagereader->imageCount(); ++i)
    {
        Thumbnails::SThumbnailItem* newtitem = new Thumbnails::SThumbnailItem(this);
        newtitem->Identifier = i;
        newtitem->Type = Thumbnails::SThumbnailItem::TI_FILE;

        m_items << newtitem;
    }

    return m_items.count() > 0;
}

void CImagesModel::slotProviderItemsCollectionFinished()
{
    endResetModel();

    emit itemsProviderCollectFinished();
}

void CImagesModel::slotProviderItemsCollectionStarted()
{
    emit itemsProviderCollectStarted();

    beginResetModel();
}

QImageReader* CImagesModel::imageReader() const
{
    return m_imagereader;
}

QUrl CImagesModel::imageURL() const
{
    Q_ASSERT(m_imagereader);

    return QUrl::fromLocalFile(m_imagereader->fileName());
}

CImagesModel::ESetSourceImageResult CImagesModel::setImageURL(const QUrl& imageurl)
{
    if (!imageurl.isValid())
        return SSIR_INVALIDURL;

    thumbnailItemsClear();
    dropImageReader();
    dropIODevice();

    if (imageurl.isLocalFile())
    {
        beginResetModel();

        // for now we can only load from local file
        ESetSourceImageResult result = SSIR_INVALID;

        QFile* openf = new QFile(imageurl.toLocalFile(), this);
        if (!openf->open(QIODevice::ReadOnly))
            result = SSIR_NOTLOADABLE;
        if ((result != SSIR_NOTLOADABLE) && !openf->size())
            result = SSIR_NOTLOADABLE;

        if (result != SSIR_NOTLOADABLE)
        {
            m_iodevice = openf;

            m_imagereader = new QImageReader(imageurl.toLocalFile());
            m_imagereader->setDevice(m_iodevice);
            if (m_imagereader->canRead())
            {
                if (inspectImageReader())
                    result = SSIR_SUCCESS;
            }
            else
                result = SSIR_NOTLOADABLE;
        }

        if (result != SSIR_SUCCESS)
        {
            dropImageReader();
            dropIODevice();
        }

        endResetModel();

        return result;
    }
    else
        return SSIR_INVALIDURL;
}
