#include "cimageswidget.h"

#include "../../thumbnailview/cthumbnailview.h"
#include "../../thumbnailview/cthumbnailviewwidget.h"
#include "cimageslistview.h"

#include <QAction>
#include <QHBoxLayout>
#include <QMenu>
#include <QToolButton>
#include <QVBoxLayout>


using namespace Lihat;
using namespace Lihat::Image;

CImagesWidget::CImagesWidget(QWidget* parent):
    QWidget(parent),
    ImagesListView(0)
{
    initActions();
    initUserInterface();
}

CImagesWidget::~CImagesWidget()
{

}

void CImagesWidget::slotSubImagesTriggered(bool /* checked */)
{
    ImagesListView->setImagesViewMode(CImagesListView::IVM_SUBIMAGES);
    emit currentModeChanged();
}

void CImagesWidget::slotSelectedSiblingsTriggered(bool checked)
{
    if (checked)
        ImagesListView->setImagesViewMode(CImagesListView::IVM_SELECTIONS);
    else
        ImagesListView->setImagesViewMode(CImagesListView::IVM_SIBLINGS);

    emit currentModeChanged();
}

void CImagesWidget::slotSiblingsTriggered(bool /* checked */)
{
    ImagesListView->setImagesViewMode(CImagesListView::IVM_SIBLINGS);
    emit currentModeChanged();
}

QAction* CImagesWidget::getAction(const QString& name, bool mustexists)
{
    QAction* result = findChild< QAction* >(name);
    if (mustexists)
        Q_ASSERT(result);

    return result;
}

void CImagesWidget::initActions()
{
    QAction* act;
    QList< QKeySequence > shortcuts;

    QActionGroup* actgroup = new QActionGroup(this);

    act = new QAction(tr("&Siblings"), this);
    act->setObjectName("acImagesListViewSiblings");
    actgroup->addAction(act);
    act->setCheckable(true);
    act->setChecked(true);
    connect(act, SIGNAL(triggered(bool)),
            this, SLOT(slotSiblingsTriggered(bool)));

    act = new QAction(tr("S&election"), this);
    act->setObjectName("acImagesListViewSelectedSiblings");
    actgroup->addAction(act);
    act->setCheckable(true);
    act->setChecked(false);
    connect(act, SIGNAL(triggered(bool)),
            this, SLOT(slotSelectedSiblingsTriggered(bool)));

    act = new QAction(tr("Sub-images"), this);
    act->setObjectName("acImagesListViewSubImages");
    actgroup->addAction(act);
    act->setCheckable(true);
    connect(act, SIGNAL(triggered(bool)),
            this, SLOT(slotSubImagesTriggered(bool)));
}

void CImagesWidget::initUserInterface()
{
    QVBoxLayout* layout = new QVBoxLayout(this);
    layout->setMargin(0);

    ImagesListView = new CImagesListView(this);
    ImagesListView->setObjectName("imagesListView");
    layout->addWidget(ImagesListView);

    QHBoxLayout* buttonslayout = new QHBoxLayout();
    buttonslayout->setSizeConstraint(QLayout::SetMaximumSize);

    QToolButton* tbtnSiblings = new QToolButton(this);
    tbtnSiblings->setObjectName("tbtnSiblings");
    tbtnSiblings->setAutoRaise(true);
    tbtnSiblings->setDefaultAction(getAction("acImagesListViewSiblings"));
    buttonslayout->addWidget(tbtnSiblings);

    QToolButton* tbtnSelection = new QToolButton(this);
    tbtnSelection->setObjectName("tbtnSelection");
    tbtnSelection->setAutoRaise(true);
    tbtnSelection->setDefaultAction(getAction("acImagesListViewSelectedSiblings"));
    buttonslayout->addWidget(tbtnSelection);

    QToolButton* tbtnSubImages = new QToolButton(this);
    tbtnSubImages->setObjectName("tbtnSubImages");
    tbtnSubImages->setAutoRaise(true);
    tbtnSubImages->setDefaultAction(getAction("acImagesListViewSubImages"));
    buttonslayout->addWidget(tbtnSubImages);

    buttonslayout->addStretch();

    layout->addLayout(buttonslayout);
}
