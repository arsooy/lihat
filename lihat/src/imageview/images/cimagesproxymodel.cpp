#include "cimagesproxymodel.h"

#include "../../thumbnailview/cthumbnailview.h"
#include "../../thumbnailview/sthumbnailitem.h"

#include <QtDebug>


using namespace Lihat;
using namespace Lihat::Image;


CImagesProxyModel::CImagesProxyModel(Thumbnails::CThumbnailView* source, QObject* parent):
    QSortFilterProxyModel(parent),
    m_mode(IVTVPM_INVALID),
    m_sourceview(source)
{
    if (hasSourceView())
    {
        setSourceModel(m_sourceview->model());
        connect(m_sourceview, SIGNAL(itemsSelected(QModelIndexList)),
                this, SLOT(slotSourceViewItemsSelected(QModelIndexList)));
    }
}

CImagesProxyModel::~CImagesProxyModel()
{

}

int CImagesProxyModel::columnCount(const QModelIndex& /* parent */) const
{
    return 1;
}

bool CImagesProxyModel::filterAcceptsRow(int source_row, const QModelIndex& source_parent) const
{
    if (hasSourceView())
    {
        if (m_mode == IVTVPM_SIBLINGS)
        {
            QModelIndex sindex = sourceModel()->index(source_row, 0, source_parent);
            Thumbnails::SThumbnailItem* titem = m_sourceview->thumbnailItemAt(sindex);
            Q_ASSERT(titem);
            Thumbnails::SThumbnailLocalData* localdata = m_sourceview->thumbnailLocalData(titem);

            // only show files, that has thumbnail (or waiting for it)
            return (titem->Type == Thumbnails::SThumbnailItem::TI_FILE &&
                    ((localdata->LoadStatus == Thumbnails::SThumbnailItem::ILS_PENDING) ||
                     (localdata->LoadStatus == Thumbnails::SThumbnailItem::ILS_LOADED)));
        }
        else if (m_mode == IVTVPM_SELECTIONS)
        {
            QModelIndex sindex = sourceModel()->index(source_row, 0, source_parent);
            Thumbnails::SThumbnailItem* titem = m_sourceview->thumbnailItemAt(sindex);
            Q_ASSERT(titem);
            Thumbnails::SThumbnailLocalData* localdata = m_sourceview->thumbnailLocalData(titem);

            // only show selected files, that has thumbnail (or waiting for it)
            return (m_selectedindexrows.contains(source_row) &&
                    titem->Type == Thumbnails::SThumbnailItem::TI_FILE &&
                    ((localdata->LoadStatus == Thumbnails::SThumbnailItem::ILS_PENDING) ||
                     (localdata->LoadStatus == Thumbnails::SThumbnailItem::ILS_LOADED)));
        }
    }

    return false;
}

bool CImagesProxyModel::hasSourceView() const
{
    return m_sourceview;
}

CImagesProxyModel::EImagesViewThumbnailViewProxyMode CImagesProxyModel::proxyMode() const
{
    return m_mode;
}

void CImagesProxyModel::setProxyMode(EImagesViewThumbnailViewProxyMode mode)
{
    m_mode = mode;
    updateSelectedIndexes();
    invalidateFilter();
}

void CImagesProxyModel::slotSourceViewItemsSelected(const QModelIndexList& /* indexes */)
{
    updateSelectedIndexes();
    invalidateFilter();
}

bool CImagesProxyModel::updateSelectedIndexes()
{
    m_selectedindexrows.clear();
    if (m_mode != IVTVPM_SELECTIONS)
        return true;
    if (!m_sourceview)
        return false;

    QModelIndexList indexes = m_sourceview->selectionModel()->selectedIndexes();
    for (int i = 0; i < indexes.count(); ++i)
        m_selectedindexrows << indexes.at(i).row();

    return true;
}
