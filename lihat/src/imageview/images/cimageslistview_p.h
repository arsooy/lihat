#ifndef CIMAGESLISTVIEW_P_H
#define CIMAGESLISTVIEW_P_H

#include "../../thumbnailview/cthumbnailview_p.h"

#include <QHash>
#include <QTimer>


namespace Lihat
{
    namespace Thumbnails
    {

        class  CThumbnailView;
        struct SThumbnailItem;

    } // namespace Thumbnails

    namespace Image
    {

        class  CImagesListView;
        class  CGIImageView;
        class  CLoadResult;
        class  CImageViewWidget;

        class CImagesListViewPrivate:
            public Thumbnails::CThumbnailViewPrivate
        {
        public:
            Q_DECLARE_PUBLIC(CImagesListView)

            bool                         IgnoreCurrentChanged;
            bool                         IgnoreSourceThumbnailCurrentIndexChange;
            bool                         IgnoreImageViewLoadFinished;
            unsigned char                ImagesMode;
            bool                         IsPrepared;
            bool                         LinkIsActive;
            Thumbnails::CThumbnailView*  SourceThumbnailView;

            explicit CImagesListViewPrivate(CImagesListView* imageslistview);
            virtual ~CImagesListViewPrivate();

            QModelIndex  modelIndexFromThumbnailView();
            QModelIndex  subImageModelIndexFromImageView(CGIImageView* imageview = 0);

            /**
            * @brief try to get pointer that allows direct access to Movie/Pixmap
            * of current image (shown at image view area) visible to user
            *
            * this function will compare current model image URL with current
            * image's URL, if it match it will update out pointer to that image's
            * CImageViewLoadResult.
            *
            * @param plresult pointer to placeholder of matching load result.
            * @param imageview specify which image view to compare with, if not
            * specified it will use currently active image view
            * @return bool returns true if image of that image view has the same
            * URL with sub-image URL.
            */
            bool  getCurrentSubImageLoadResult(CLoadResult** plresult, CGIImageView* imageview = 0);

            void              connectWithImageView(CGIImageView* imgcontainer);
            CGIImageView*     imageView();
            CImageViewWidget* imageViewWidget();

            void imagesModeUpdated();
            void initActions();
            void initUserInterface();
            void invalidateThumbnailsAfterResize();
            bool synchronizeSubImageSourceImageURL();
            void recreateModel();
        };

    } // namespace Image
} // namespace Lihat

#endif // CIMAGESLISTVIEW_P_H
