#ifndef CIMAGESMODEL_H
#define CIMAGESMODEL_H

#include "../../thumbnailview/models/cbasethumbnailmodel.h"
#include "../../thumbnailview/sthumbnailitem.h"

#include <QImageReader>
#include <QString>
#include <QUrl>


class QIODevice;
class QPixmap;

namespace Lihat
{
    namespace ImageLoad
    {

        class  CImageLoadQueue;

    } // namespace ImageLoad

    namespace Thumbnails
    {

        struct SThumbnailLocalData;

    } // namespace Thumbnails

    namespace Image
    {

        class CImagesModel:
            public Thumbnails::CBaseThumbnailModel
        {
            Q_OBJECT
        public:
            enum  ESetSourceImageResult
            {
                SSIR_INVALID      =   0,
                SSIR_INVALIDURL   =   1,
                SSIR_NOTLOADABLE  =   2,
                SSIR_NOSUBIMAGES  =   3,
                SSIR_SUCCESS      = 255
            };


            explicit CImagesModel(QObject* parent = 0);
            virtual ~CImagesModel();

            // QAbstractListModel
            virtual QVariant    data(const QModelIndex& index, int role = Qt::DisplayRole) const;
            virtual QModelIndex index(int row, int column = 0, const QModelIndex& parent = QModelIndex()) const;
            virtual int         rowCount(const QModelIndex& parent = QModelIndex()) const;

            // IItemsProvider
            virtual void                        disposeThumbnailItem(Thumbnails::SThumbnailItem* titem);
            virtual int                         indexOfThumbnailItem(Thumbnails::SThumbnailItem* titem);
            virtual Thumbnails::SThumbnailItem* thumbnailItem(int providerindex);
            virtual void                        thumbnailItemRemove(int idx);
            virtual void                        thumbnailItemsClear();
            virtual QUrl                        thumbnailItemURL(Thumbnails::SThumbnailItem* titem);
            virtual int                         thumbnailItemsCount() const;
            virtual EProviderType               thumbnailItemsProviderType() const;

            QUrl                  imageURL() const;
            ESetSourceImageResult setImageURL(const QUrl& imageurl);

            QImageReader* imageReader() const;

        protected:
            QList< Thumbnails::SThumbnailItem* > m_items;
            QImageReader* m_imagereader;
            QIODevice*    m_iodevice;

            void dropImageReader();
            void dropIODevice();

            bool inspectImageReader();

        protected slots:
            void slotProviderItemsCollectionFinished();
            void slotProviderItemsCollectionStarted();
        };

    } // namespace Image
} // namespace Lihat

#endif // CIMAGESMODEL_H
