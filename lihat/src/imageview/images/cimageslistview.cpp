#include "cimageslistview.h"

#include "../../thumbnailview/cbasescheduledtask.h"
#include "../../thumbnailview/citemdelegate.h"
#include "../../thumbnailview/cthumbnailview.h"
#include "../../thumbnailview/sthumbnailitem.h"
#include "../cgiimage.h"
#include "../cimageviewwidget.h"
#include "../cloadjob.h"
#include "../cloadresult.h"
#include "canimframethumbloadjob.h"
#include "cimagesitemdelegate.h"
#include "cimageslistview_p.h"
#include "cimagesmodel.h"
#include "cimagesproxymodel.h"
#include "csubimagethumbloadjob.h"
#include "imageview/cgiimageview.h"

#include <QAbstractItemModel>
#include <QScrollBar>
#include <QMovie>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::Image;

class CScheduledSetCurrentThumbnailAndScroll:
    public Thumbnails::CScheduledSetCurrentThumbnail
{
public:
    explicit CScheduledSetCurrentThumbnailAndScroll(const int index);

    virtual int runTask(Thumbnails::CBaseThumbnailView* thumbnailview);
};


CImagesListView::CImagesListView(QWidget* parent):
    Thumbnails::CThumbnailView(*new CImagesListViewPrivate(this), parent)
{
    Q_D(CImagesListView);

    d->initActions();
    d->initUserInterface();

    initItemDelegate();
}

CImagesListView::~CImagesListView()
{

}

void CImagesListView::currentChanged(const QModelIndex& current, const QModelIndex& previous)
{
    Q_D(CImagesListView);

    CThumbnailView::currentChanged(current, previous);

    if (d->IgnoreCurrentChanged)
        return;

    if (!d->LinkIsActive)
        return;

    switch ((EImagesViewMode) d->ImagesMode)
    {
        case IVM_SIBLINGS:
        case IVM_SELECTIONS:
        {
            // to synchronize current selection with source view and 'execute it' (ie. to view it)
            CImagesProxyModel* proxymodel = qobject_cast< CImagesProxyModel* >(model());
            Q_ASSERT(proxymodel);

            if (!current.isValid())
                return;

            bool islastactive = isActiveWindow();

            QModelIndex sindex = proxymodel->mapToSource(current);
            if (sindex.isValid() && (d->SourceThumbnailView->currentIndex() != sindex))
            {
                // we initiate the change, avoid circular execution
                d->IgnoreSourceThumbnailCurrentIndexChange = true;

                QItemSelectionModel::SelectionFlags selflags = QItemSelectionModel::Current;
                if (d->ImagesMode == IVM_SIBLINGS)
                    selflags |= QItemSelectionModel::Clear | QItemSelectionModel::Select;

                d->SourceThumbnailView->selectionModel()->setCurrentIndex(sindex, selflags);
                d->SourceThumbnailView->executeIndex(sindex);

                if (islastactive)
                    setFocus();

                d->IgnoreSourceThumbnailCurrentIndexChange = false;
            }

            break;
        }

        case IVM_SUBIMAGES:
        {
            // to load sub-image of current selection in source view
            CImagesModel* subimagesmodel = qobject_cast< CImagesModel* >(model());
            Q_ASSERT(subimagesmodel);

            if (!current.isValid())
                return;

            CGIImageView* imgcontainer = d->imageView();
            Thumbnails::SThumbnailItem* titem = thumbnailItemAt(current);
            if (titem && imgcontainer)
            {
                // avoid circular run
                d->IgnoreImageViewLoadFinished = true;

                CGIImage* img = imgcontainer->image();
                if (img && (subimagesmodel->imageURL() == img->url()))
                {
                    CLoadResult* imglresult = img->loadResult();
                    if (imglresult->LoadedKind == CLoadResult::LK_MOVIE)
                    {
                        // ask to stop at frame # of animation
                        img->stopAtFrame(titem->Identifier);
                    }
                    else
                    {
                        // load sub-image
                        CLoadJob ivloadjob(imgcontainer->image(), subimagesmodel->imageURL());
                        ivloadjob.setRequestedSubImage(titem->Identifier);

                        imgcontainer->image()->load(ivloadjob);
                    }
                }

                d->IgnoreImageViewLoadFinished = false;
            }

            break;
        }

        default:
            ;
    }
}

void CImagesListView::hideEvent(QHideEvent* event)
{
    Q_D(CImagesListView);

    d->LinkIsActive = false;

    QWidget::hideEvent(event);
}

void CImagesListView::initItemDelegate()
{
    CImagesItemDelegate* idel = new CImagesItemDelegate(this);
    idel->setThumbnailViewPixmapProvider(this);

    setItemDelegate(idel);
}

void CImagesListView::resizeEvent(QResizeEvent* event)
{
    Q_D(CImagesListView);

    QListView::resizeEvent(event);

    // use timer to compress multiple resize within an interval with one handler
    d->installResizeSettledTimer();
}

bool CImagesListView::setImagesViewMode(CImagesListView::EImagesViewMode mode)
{
    Q_D(CImagesListView);

    if (mode == (EImagesViewMode) d->ImagesMode)
        return true;

    d->ImagesMode = mode;
    d->imagesModeUpdated();

    return true;
}

bool CImagesListView::setSourceThumbnailView(Thumbnails::CThumbnailView* thumbnailview)
{
    Q_D(CImagesListView);

    if (thumbnailview == d->SourceThumbnailView)
        return true;

    if (d->SourceThumbnailView)
        disconnect(d->SourceThumbnailView, 0, this, 0);

    d->SourceThumbnailView = thumbnailview;
    // set to be notified selection from the source thumbnail view
    // (ie. CThumbnailView) changed;
    // we need to do this because when 'next' & 'previous' button clicked it
    // will update source thumbnail's current index, we just have to know this
    // event to keep in touch with user's position in the directory
    QObject::connect(d->SourceThumbnailView->selectionModel(), SIGNAL(currentChanged(QModelIndex, QModelIndex)),
                     this, SLOT(slotSourceViewSelectionModelCurrentChanged(QModelIndex, QModelIndex)));

    if (d->ImagesMode != IVM_INVALID)
        d->recreateModel();

    return true;
}

void CImagesListView::showEvent(QShowEvent* event)
{
    CThumbnailView::showEvent(event);

    Q_D(CImagesListView);

    d->LinkIsActive = true;

    switch (d->ImagesMode)
    {
        case IVM_SIBLINGS:
        case IVM_SELECTIONS:
        {
            QModelIndex cindex = d->modelIndexFromThumbnailView();
            if (cindex.isValid())
            {
                CScheduledSetCurrentThumbnailAndScroll* task = new CScheduledSetCurrentThumbnailAndScroll(cindex.row());
                d->addSetCurrentThumbnailScheduledTask("EndThumbnailsUpdate", task);
            }

            break;
        }

        case IVM_SUBIMAGES:
        {
            if (d->synchronizeSubImageSourceImageURL())
            {
                QModelIndex cindex = d->subImageModelIndexFromImageView();
                if (cindex.isValid())
                {
                    CScheduledSetCurrentThumbnailAndScroll* task = new CScheduledSetCurrentThumbnailAndScroll(cindex.row());
                    d->addSetCurrentThumbnailScheduledTask("EndThumbnailsUpdate", task);
                }
            }

            break;
        }

        default:
            ;
    }

}

void CImagesListView::slotSourceViewSelectionModelCurrentChanged(const QModelIndex& current, const QModelIndex& /* previous */)
{
    Q_D(CImagesListView);

    if (!d->LinkIsActive)
        return;

    if (d->IgnoreSourceThumbnailCurrentIndexChange)
        return;

    switch (d->ImagesMode)
    {
        case IVM_SIBLINGS:
        case IVM_SELECTIONS:
        {
            const CImagesProxyModel* proxymodel = qobject_cast< const CImagesProxyModel* >(model());
            Q_ASSERT(proxymodel);

            QModelIndex tcurrent = proxymodel->mapFromSource(current);
            setCurrentIndex(tcurrent);

            if (d->ImagesMode == IVM_SELECTIONS)
                viewportChanged();

            break;
        }

        case IVM_SUBIMAGES:
        {
            d->synchronizeSubImageSourceImageURL();

            break;
        }

        default:
            ;
    }
}

void CImagesListView::slotImageViewLoadFinished(bool loaded, const QUrl& /* url */)
{
    Q_D(CImagesListView);

    if (!d->LinkIsActive)
        return;

    if (!d->IgnoreImageViewLoadFinished)
        return;

    if (!loaded)
        return;

    if (d->ImagesMode != IVM_SUBIMAGES)
        return;

    CGIImageView* imgcontainer = qobject_cast< CGIImageView* >(sender());
    if (!imgcontainer)
        return;

    QModelIndex imgindex = d->subImageModelIndexFromImageView(imgcontainer);
    if (imgindex.isValid())
    {
        setCurrentIndex(imgindex);
        scrollTo(imgindex);
    }
}

Thumbnails::CThumbnailView* CImagesListView::sourceThumbnailView() const
{
    Q_D(const CImagesListView);

    return d->SourceThumbnailView;
}

Thumbnails::SThumbnailItem* CImagesListView::thumbnailItemAt(const QModelIndex& index)
{
    Q_D(const CImagesListView);

    switch ((EImagesViewMode) d->ImagesMode)
    {
        case IVM_SIBLINGS:
        case IVM_SELECTIONS:
        {
            const CImagesProxyModel* proxymodel = qobject_cast< const CImagesProxyModel* >(model());
            Q_ASSERT(proxymodel);

            QModelIndex sourceindex = proxymodel->mapToSource(index);
            return d->SourceThumbnailView->thumbnailItemAt(sourceindex);

            break;
        }

        case IVM_SUBIMAGES:
        {
            CImagesModel* subimagesmodel = qobject_cast< CImagesModel* >(model());
            Q_ASSERT(subimagesmodel);

            if (index.isValid())
                return subimagesmodel->thumbnailItem(index.row());

            break;
        }

        default:
            ;
    }

    return 0;
}

bool CImagesListView::siftLoadItemThumbnail(const QModelIndex& index)
{
    Q_D(CImagesListView);

    CLoadResult* lresult = 0;
    d->getCurrentSubImageLoadResult(&lresult);

    switch ((CImagesListView::EImagesViewMode) d->ImagesMode)
    {
        case IVM_SIBLINGS:
        case IVM_SELECTIONS:
        {
            Thumbnails::SThumbnailItem* titem = thumbnailItemAt(index);
            // skip folder items, we'll use CorePixmap.Folder for these items
            if (titem->Type == Thumbnails::SThumbnailItem::TI_FILE)
            {
                Thumbnails::SThumbnailLocalData* localdata = thumbnailLocalData(titem);
                if ((localdata->LoadStatus == TLS_PENDING) || (localdata->LoadStatus == TLS_RELOADNEEDED))
                    return d->ThumbnailLoader->createImageLoadQueueJob(this, index);
            }

            break;
        }

        case IVM_SUBIMAGES:
        {
            Thumbnails::SThumbnailItem* titem = thumbnailItemAt(index);
            Thumbnails::SThumbnailLocalData* localdata = thumbnailLocalData(titem);

            if (lresult && ((localdata->LoadStatus == TLS_PENDING) || (localdata->LoadStatus == TLS_RELOADNEEDED)))
            {
                if (lresult->LoadedKind == CLoadResult::LK_MOVIE)
                {
                    CAnimFrameThumbLoadJob* frameloadjob = new CAnimFrameThumbLoadJob(this, titem, lresult->Movie, thumbnailSize(), titem->Identifier);
                    return d->ThumbnailLoader->addImageLoadQueueJob(this, frameloadjob);
                }
                else
                {
                    CSubImageThumbLoadJob* subimageloadjob = new CSubImageThumbLoadJob(this, titem, thumbnailSize(), titem->Identifier);
                    return d->ThumbnailLoader->addImageLoadQueueJob(this, subimageloadjob);
                }
            }

            break;
        }

        default:
            ;
    }

    return Thumbnails::CThumbnailView::siftLoadItemThumbnail(index);
}

void CImagesListView::thumbnailViewResized()
{
    Q_D(CImagesListView);

    if (d->ViewportChangedOnResize && isVisible())
    {
        QSize lastsize = thumbnailSize();

        setUpdatesEnabled(false);
        beginUpdate();

        int twidth = contentsRect().width();
        CImagesItemDelegate* idel = qobject_cast< CImagesItemDelegate* >(itemDelegate());
        if (idel)
            twidth -= (spacing() * 2) + (idel->ITEM_PADDING * 2);

        QModelIndex currentindex = currentIndex();

        QSize nsize(twidth, twidth * 3 / 4);
        if (lastsize != nsize)
        {
            QModelIndexList visibleindexes = visibleIndexes();
            bool bCurrentMustShown = (currentindex.isValid() && visibleindexes.contains(currentindex));

            // as this view is basically one line for each item, we can save
            // current vertical position and later set new position relative
            // to the difference between old and the new thumbnail height
            int lastpos = verticalScrollBar()->value();
            qreal vchangescale = 1;
            if (!lastsize.isEmpty())
                vchangescale = qreal(nsize.height()) / qreal(lastsize.height());

            // now, change the thumbnail size
            setThumbnailSize(nsize);
            // re-layout the items
            doItemsLayout();

            // set new position as close as last position
            if (bCurrentMustShown)
            {
                visibleindexes = visibleIndexes();
                if (!visibleindexes.contains(currentindex))
                    scrollToIndex(currentindex);
            }
            else
                verticalScrollBar()->setValue(int(lastpos * vchangescale));

            d->rebuildCorePixmaps();

            d->invalidateThumbnailsAfterResize();
        }

        endUpdate();
        setUpdatesEnabled(true);
    }

    CThumbnailView::thumbnailViewResized();
}

CImagesListView::EImagesViewMode CImagesListView::viewMode() const
{
    Q_D(const CImagesListView);

    return (EImagesViewMode) d->ImagesMode;
}


CImagesListViewPrivate::CImagesListViewPrivate(CImagesListView* imageslistview):
    CThumbnailViewPrivate(imageslistview),
    IgnoreCurrentChanged(false),
    IgnoreSourceThumbnailCurrentIndexChange(false),
    ImagesMode((unsigned char) CImagesListView::IVM_INVALID),
    IsPrepared(false),
    LinkIsActive(false),
    SourceThumbnailView(0)
{

}

CImagesListViewPrivate::~CImagesListViewPrivate()
{

}

CGIImageView* CImagesListViewPrivate::imageView()
{
    // find a CGIImageView instance that goes along with this images listview
    CImageViewWidget* imageviewwidget = imageViewWidget();
    if (imageviewwidget)
        return imageviewwidget->currentImageView();

    return 0;
}

CImageViewWidget* CImagesListViewPrivate::imageViewWidget()
{
    Q_Q(CImagesListView);


    QObject* current = q;
    bool found = false;

    while ((current != 0) && !found)
    {
        found = bool(qobject_cast< CImageViewWidget* >(current));
        if (!found)
            current = current->parent();
    }

    if (current)
        return qobject_cast< CImageViewWidget* >(current);

    return 0;
}

void CImagesListViewPrivate::imagesModeUpdated()
{
    Q_Q(CImagesListView);

    q->beginUpdate();

    // this should create the correct model for the listview
    recreateModel();

    CImagesListView::EImagesViewMode imode = (CImagesListView::EImagesViewMode) ImagesMode;
    switch (imode)
    {
        case CImagesListView::IVM_SIBLINGS:
        case CImagesListView::IVM_SELECTIONS:
        {
            CImagesProxyModel* proxymodel = qobject_cast< CImagesProxyModel* >(q->model());
            Q_ASSERT(proxymodel);

            if (imode == CImagesListView::IVM_SIBLINGS)
                proxymodel->setProxyMode(CImagesProxyModel::IVTVPM_SIBLINGS);
            else
                proxymodel->setProxyMode(CImagesProxyModel::IVTVPM_SELECTIONS);

            QModelIndex cindex = modelIndexFromThumbnailView();
            if (cindex.isValid())
            {
                q->setCurrentThumbnail(cindex.row());
                q->scrollTo(q->currentIndex());
            }

            break;
        }

        case CImagesListView::IVM_SUBIMAGES:
        {
            QModelIndex imgindex = subImageModelIndexFromImageView();
            if (imgindex.isValid())
            {
                IgnoreCurrentChanged = true;

                q->setCurrentIndex(imgindex);
                q->scrollToIndex(imgindex);

                IgnoreCurrentChanged = false;
            }

            break;
        }

        default:
            ;
    }
    q->viewportChanged();
    q->endUpdate();
}

bool CImagesListViewPrivate::getCurrentSubImageLoadResult(CLoadResult** plresult, CGIImageView* imageview)
{
    Q_Q(CImagesListView);

    CGIImageView* imgview = imageview ? imageview : imageView();
    if (!imgview)
        return false;

    CImagesModel* simodel = qobject_cast< CImagesModel* >(q->model());
    if (!simodel)
        return false;

    CGIImage* img = imgview->image();
    CLoadResult* lresult = img ? img->loadResult() : 0;
    if (!lresult)
    {
        if (plresult)
            *plresult = 0;

        return true;
    }

    if (simodel->imageURL() == img->url())
    {
        if (plresult)
            *plresult = lresult;

        return true;
    }

    return false;
}

QModelIndex CImagesListViewPrivate::subImageModelIndexFromImageView(CGIImageView* imageview)
{
    Q_Q(CImagesListView);

    CLoadResult* lresult = 0;
    if (getCurrentSubImageLoadResult(&lresult, imageview) && lresult)
    {
        CImagesModel* simodel = qobject_cast< CImagesModel* >(q->model());

        switch (lresult->LoadedKind)
        {
            case CLoadResult::LK_IMAGE:
            case CLoadResult::LK_PIXMAP:
            {
                if (lresult->SubImageIndex != -1)
                {
                    Thumbnails::SThumbnailItem* titem = simodel->thumbnailItem(lresult->SubImageIndex);
                    int modelidx = simodel->indexOfThumbnailItem(titem);

                    return simodel->index(modelidx, 0);
                }

                break;
            }

            case CLoadResult::LK_MOVIE:
            {
                Thumbnails::SThumbnailItem* titem = simodel->thumbnailItem(lresult->Movie->currentFrameNumber());
                if (lresult->Movie)
                    return simodel->index(simodel->indexOfThumbnailItem(titem), 0);

                break;
            }

            default:
                ;
        }
    }

    return QModelIndex();
}

void CImagesListViewPrivate::connectWithImageView(CGIImageView* imgcontainer)
{
    Q_Q(CImagesListView);

    if (!imgcontainer)
        return;

    // when we're visible and the user switch to another image we need to know
    // this so that we can update/synchronize ourself with what can be seen
    QObject::connect(imgcontainer, SIGNAL(imageLoadFinished(bool, QUrl)),
                     q, SLOT(slotImageViewLoadFinished(bool, QUrl)),
                     Qt::UniqueConnection);
}

bool CImagesListViewPrivate::synchronizeSubImageSourceImageURL()
{
    Q_Q(CImagesListView);

    CImagesModel* simodel = qobject_cast< CImagesModel* >(q->model());
    Q_ASSERT(simodel);

    QModelIndex sourcecurrentindex = SourceThumbnailView->currentIndex();
    if (sourcecurrentindex.isValid())
    {
        Thumbnails::SThumbnailItem* titem = SourceThumbnailView->thumbnailItemAt(sourcecurrentindex);
        if (titem && (simodel->imageURL() != titem->loadURL()))
            return simodel->setImageURL(titem->loadURL()) == CImagesModel::SSIR_SUCCESS;
    }

    return false;
}

void CImagesListViewPrivate::initActions()
{

}

void CImagesListViewPrivate::initUserInterface()
{
    Q_Q(CImagesListView);

    q->setSpacing(4);
    q->setSelectionMode(QAbstractItemView::SingleSelection);
    q->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    rebuildCorePixmaps();
}

void CImagesListViewPrivate::invalidateThumbnailsAfterResize()
{
    Q_Q(CImagesListView);

    // went through all loaded thumbnails, and invalidate them
    QModelIndexList visibleindexes = q->visibleIndexes();
    QAbstractItemModel* model = q->model();
    for (int i = 0; i < model->rowCount(); ++i)
    {
        QModelIndex index = model->index(i, 0);

        Thumbnails::SThumbnailItem* titem = q->thumbnailItemAt(index);
        Q_ASSERT(titem);
        Thumbnails::SThumbnailLocalData* localdata = q->thumbnailLocalData(titem);

        if (localdata->LoadStatus == Thumbnails::CThumbnailView::TLS_LOADED)
            localdata->LoadStatus = Thumbnails::CThumbnailView::TLS_RELOADNEEDED;
    }
}

QModelIndex CImagesListViewPrivate::modelIndexFromThumbnailView()
{
    Q_Q(CImagesListView);

    if (!SourceThumbnailView)
        return QModelIndex();

    QModelIndex sourcecurrentindex = SourceThumbnailView->currentIndex();
    if (sourcecurrentindex.isValid())
    {
        CImagesProxyModel* proxymodel = qobject_cast< CImagesProxyModel* >(q->model());
        Q_ASSERT(proxymodel);

        return proxymodel->mapFromSource(sourcecurrentindex);
    }

    return QModelIndex();
}

void CImagesListViewPrivate::recreateModel()
{
    Q_Q(CImagesListView);

    QAbstractItemModel* itemsmodel = 0;
    QAbstractItemModel* lastmodel = q->model();
    bool modelready = false;
    bool deletelastmodel = true;

    switch ((CImagesListView::EImagesViewMode) ImagesMode)
    {
        case CImagesListView::IVM_SIBLINGS:
        case CImagesListView::IVM_SELECTIONS:
        {
            Q_ASSERT(SourceThumbnailView);

            bool newmodel = false;

            CImagesProxyModel* proxymodel = qobject_cast< CImagesProxyModel* >(lastmodel);
            deletelastmodel = !proxymodel;
            if (!proxymodel)
            {
                proxymodel = new CImagesProxyModel(SourceThumbnailView, q);
                newmodel = true;
            }

            modelready = proxymodel && proxymodel->hasSourceView();
            if (modelready)
            {
                if ((CImagesListView::EImagesViewMode) ImagesMode == CImagesListView::IVM_SIBLINGS)
                    proxymodel->setProxyMode(CImagesProxyModel::IVTVPM_SIBLINGS);
                else
                    proxymodel->setProxyMode(CImagesProxyModel::IVTVPM_SELECTIONS);
                itemsmodel = proxymodel;
            }
            else if (proxymodel && newmodel)
                delete proxymodel;

            break;
        }

        case CImagesListView::IVM_SUBIMAGES:
        {
            Q_ASSERT(SourceThumbnailView);

            QModelIndex sourcecurrentindex = SourceThumbnailView->currentIndex();
            if (sourcecurrentindex.isValid())
            {
                Thumbnails::SThumbnailItem* sourcetitem = SourceThumbnailView->thumbnailItemAt(sourcecurrentindex);
                Q_ASSERT(sourcetitem);

                // create model and make it load current item on source view
                CImagesModel* simodel = new CImagesModel(q);
                bool urlok = simodel->setImageURL(sourcetitem->loadURL()) == CImagesModel::SSIR_SUCCESS;
                if (urlok)
                {
                    modelready = (bool) simodel;
                    if (modelready)
                        itemsmodel = simodel;
                }
                else
                    delete simodel;
            }

            break;
        }

        default:
            ;
    }

    if (modelready)
    {
        q->setModel(itemsmodel);
        LastScrollBarsPosition.Vertical = 0;

        // delete previous proxy model if last model is of CImagesProxyModel
        // type then we are the one who create it, delete it as we do not track
        // it anymore
        if (deletelastmodel && lastmodel)
            delete lastmodel;
    }
}


CScheduledSetCurrentThumbnailAndScroll::CScheduledSetCurrentThumbnailAndScroll(const int index):
    CScheduledSetCurrentThumbnail(index)
{

}

int CScheduledSetCurrentThumbnailAndScroll::runTask(Thumbnails::CBaseThumbnailView* btv)
{
    CScheduledSetCurrentThumbnail::runTask(btv);

    CImagesListView* view = qobject_cast< CImagesListView* >(btv);
    if (view)
    {
        QAbstractItemModel* model = btv->model();
        if (!model)
            return -1;

        QModelIndex mindex = model->index(Index, 0);
        if (mindex.isValid())
            view->scrollToIndex(mindex, true);
    }

    return 0;
}
