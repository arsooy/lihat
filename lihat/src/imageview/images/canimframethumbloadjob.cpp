#include "canimframethumbloadjob.h"

#include "../../imageload/cimageloadresult.h"

#include <QMovie>

using namespace Lihat;
using namespace Lihat::Image;


CAnimFrameThumbLoadJob::CAnimFrameThumbLoadJob(Thumbnails::CBaseThumbnailView* requester, Thumbnails::SThumbnailItem* titem, QMovie* anim, const QSize& loadsize, unsigned int frame):
    Thumbnails::CThumbLoadJob(requester, titem, loadsize),
    Animation(anim),
    Frame(frame)
{

}

CAnimFrameThumbLoadJob::~CAnimFrameThumbLoadJob()
{

}

bool CAnimFrameThumbLoadJob::operator==(const CAnimFrameThumbLoadJob& other)
{
    return CThumbLoadJob::operator==(other) &&
           (Animation == other.Animation) &&
           (Frame == other.Frame);
}

Thumbnails::CThumbLoadJob::EImageLoadType CAnimFrameThumbLoadJob::imageLoadType() const
{
    return ILT_THUMBNAILANIMFRAME;
}

bool CAnimFrameThumbLoadJob::isValid() const
{
    return CThumbLoadJob::isValid() && (Animation != 0);
}

bool CAnimFrameThumbLoadJob::load()
{
    if (!Animation)
    {
        setResult(new ImageLoad::CImageLoadResult(ImageLoad::CImageLoadResult::ILRC_FAIL));
        return false;
    }

    int oldframe = Animation->currentFrameNumber();
    if (Animation->jumpToFrame(Frame))
    {
        QImage* frameimg = new QImage(Animation->currentImage());

        QSize thumbsize = requestedSize().toSize();
        bool doscaling = thumbsize.isValid() && !thumbsize.isEmpty();

        QSize loadedsize = frameimg->size();
        if (doscaling && ((loadedsize.width()  > thumbsize.width()) ||
                          (loadedsize.height() > thumbsize.height())))
        {
            // not resized by read(), use scaled() into size to force it
            QImage scaled = frameimg->scaled(thumbsize, Qt::KeepAspectRatio, resizeTransformation());
            scaled.swap(*frameimg);
        }

        ImageLoad::CImageLoadResult* result = new ImageLoad::CImageLoadResult(ImageLoad::CImageLoadResult::ILRC_OK);
        result->Image = frameimg;

        setResult(result);

        // try to get back to previous frame
        Animation->jumpToFrame(oldframe);

        return true;
    }

    return Lihat::Thumbnails::CThumbLoadJob::load();
}
