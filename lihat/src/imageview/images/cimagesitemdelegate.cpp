#include "cimagesitemdelegate.h"

#include "../../thumbnailview/cbasethumbnailview.h"
#include "../../thumbnailview/sthumbnailitem.h"
#include "cimageslistview.h"

#include <QPainter>
#include <QScrollBar>


using namespace Lihat;
using namespace Lihat::Image;

CImagesItemDelegate::CImagesItemDelegate(QObject* parent):
    QStyledItemDelegate(parent)
{

}

CImagesItemDelegate::~CImagesItemDelegate()
{

}

void CImagesItemDelegate::paint(QPainter* painter, const QStyleOptionViewItem& soption, const QModelIndex& index) const
{
    Q_ASSERT(index.isValid());
    Q_ASSERT(m_thumbnailviewpixmapprovider);

    QStyleOptionViewItemV4 option(soption);
    CImagesListView* view = qobject_cast< CImagesListView* >(const_cast< QWidget* >(option.widget));
    if (!view)
        return;

    Thumbnails::SThumbnailItem* titem = view->thumbnailItemAt(index);
    if (!titem)
        return;

    QSize thumbsize = view->thumbnailSize();
    QRect cellrect = option.rect;
    // calculate picture and caption rect

    QRect pixrect(cellrect.topLeft() + QPoint(ITEM_PADDING, ITEM_PADDING), thumbsize);
    Qt::Alignment thumbnailalignment = Qt::AlignCenter;
    bool doresizepaint = false;

    QPixmap* tpixmap = 0;

    Thumbnails::SThumbnailLocalData* viewlocaldata = view->thumbnailLocalData(titem);
    switch (viewlocaldata->LoadStatus)
    {
        case CImagesListView::TLS_NOTAVAILABLE:
        {
            tpixmap = m_thumbnailviewpixmapprovider->fetchThumbnailViewPixmap((int) Thumbnails::SThumbnailItem::TI_INVALID);

            break;
        }

        case CImagesListView::TLS_PENDING:
        {
            tpixmap = m_thumbnailviewpixmapprovider->fetchThumbnailViewPixmap((int) Thumbnails::SThumbnailItem::TI_FILE);

            break;
        }

        case CImagesListView::TLS_RELOADNEEDED:
        {
            if (viewlocaldata->Pixmap)
            {
                tpixmap = viewlocaldata->Pixmap;
                doresizepaint = true;
            }
            else
                tpixmap = m_thumbnailviewpixmapprovider->fetchThumbnailViewPixmap((int) Thumbnails::SThumbnailItem::TI_FILE);

            break;
        }

        case CImagesListView::TLS_LOADED:
        {
            tpixmap = viewlocaldata->Pixmap;
            break;
        }

        default:
            tpixmap = m_thumbnailviewpixmapprovider->fetchThumbnailViewPixmap((int) Thumbnails::SThumbnailItem::TI_INVALID);
    }

    QStyle* style = view->style();

    // draw background
    {
        painter->save();
        bool isSelected = option.state & QStyle::State_Selected;
        bool isCurrent = view->selectionModel()->currentIndex() == index;

        painter->setOpacity(1.0);
        QColor bgcolor = view->palette().color(QPalette::Base);
        if (isSelected)
            bgcolor = view->palette().color(QPalette::Highlight);

        painter->fillRect(cellrect, bgcolor);
        if (isCurrent)
        {
            QStyleOptionFocusRect opt;
            opt.initFrom(view);

            opt.backgroundColor = bgcolor.darker();
            opt.rect = cellrect;
            opt.type = QStyleOptionFocusRect::SO_FocusRect;
            opt.state |= QStyle::State_KeyboardFocusChange;
            if (view->isActiveWindow())
                opt.state |= QStyle::State_HasFocus;

            style->drawPrimitive(QStyle::PE_FrameFocusRect, &opt, painter, view);
        }

        painter->restore();
    }

    {
        // draw thumbnail
        Q_ASSERT(tpixmap);
        if (doresizepaint)
        {
            QPixmap tpix = tpixmap->scaled(thumbsize, Qt::KeepAspectRatio, Qt::FastTransformation);
            style->drawItemPixmap(painter, pixrect, int(thumbnailalignment), tpix);
        }
        else
            style->drawItemPixmap(painter, pixrect, int(thumbnailalignment), *tpixmap);
    }
}

bool CImagesItemDelegate::setThumbnailViewPixmapProvider(Thumbnails::IPixmapProvider* provider)
{
    m_thumbnailviewpixmapprovider = provider;

    return m_thumbnailviewpixmapprovider == provider;
}

QSize CImagesItemDelegate::sizeHint(const QStyleOptionViewItem& soption, const QModelIndex& /* index */) const
{
    QStyleOptionViewItemV4 option(soption);
    const Thumbnails::CBaseThumbnailView* view = dynamic_cast< const Thumbnails::CBaseThumbnailView* >(option.widget);
    if (view)
    {
        QSize thsize = view->thumbnailSize();
        QSize isize(view->contentsRect().width() - (view->spacing() * 2),
                    thsize.height() + (ITEM_PADDING * 2));

        return isize;
    }

    return QSize();
}
