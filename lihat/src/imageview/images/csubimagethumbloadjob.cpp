#include "csubimagethumbloadjob.h"

#include "../../imageload/cimageloadresult.h"


using namespace Lihat;
using namespace Lihat::Image;

CSubImageThumbLoadJob::CSubImageThumbLoadJob(Thumbnails::CBaseThumbnailView* requester, Thumbnails::SThumbnailItem* titem, const QSize& loadsize, int index):
    Thumbnails::CThumbLoadJob(requester, titem, loadsize)
{
    setRequestedSubImage(index);
}

CSubImageThumbLoadJob::~CSubImageThumbLoadJob()
{

}

bool CSubImageThumbLoadJob::operator==(const CSubImageThumbLoadJob& other)
{
    return Thumbnails::CThumbLoadJob::operator==(other) &&
           (requestedSubImage() == other.requestedSubImage());
}

Thumbnails::CThumbLoadJob::EImageLoadType CSubImageThumbLoadJob::imageLoadType() const
{
    return ILT_THUMBNAILSUBIMAGE;
}

bool CSubImageThumbLoadJob::applySubImageToImageReader(QImageReader* ir)
{
    if (m_parambits & ImageLoad::ISubImageJob::InBit)
    {
        if (ir->jumpToImage(requestedSubImage()))
            return true;
    }

    return false;
}

int CSubImageThumbLoadJob::requestedSubImage() const
{
    return m_subimgidx;
}

void CSubImageThumbLoadJob::setRequestedSubImage(const int idx)
{
    m_subimgidx = idx;
    if (m_subimgidx >= 0)
        m_parambits |= ImageLoad::ISubImageJob::InBit;
}
