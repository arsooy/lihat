#ifndef CSUBIMAGETHUMBNLOADJOB_H
#define CSUBIMAGETHUMBNLOADJOB_H

#include "../../imageload/isubimagejob.h"
#include "../../thumbnailview/cthumbloadjob.h"

#include <QSize>

class QImageReader;

namespace Lihat
{
    namespace Thumbnails
    {

        class  CBaseThumbnailView;
        struct SThumbnailItem;

    } // namespace Thumbnails

    namespace Image
    {

        class CSubImageThumbLoadJob:
            public Thumbnails::CThumbLoadJob,
            public ImageLoad::ISubImageJob
        {
        public:
            explicit CSubImageThumbLoadJob(Thumbnails::CBaseThumbnailView* requester, Thumbnails::SThumbnailItem* titem, const QSize& loadsize, int index);
            virtual ~CSubImageThumbLoadJob();

            bool operator==(const CSubImageThumbLoadJob& other);

            // CThumbLoadJob
            virtual Thumbnails::CThumbLoadJob::EImageLoadType imageLoadType() const;

            // ISubImageJob
            virtual int requestedSubImage() const;
            virtual void setRequestedSubImage(const int idx);

        protected:
            int m_subimgidx;

            virtual bool applySubImageToImageReader(QImageReader* ir);
        };

    } // namespace Image
} // namespace Lihat

#endif // CSUBIMAGETHUMBLOADJOB_H
