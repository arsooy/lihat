#ifndef CIMAGESPROXYMODEL_H
#define CIMAGESPROXYMODEL_H

#include <QSortFilterProxyModel>
#include <QModelIndex>
#include <QModelIndexList>


namespace Lihat
{
    namespace Thumbnails
    {

        class CThumbnailView;

    } // namespace Thumbnails

    namespace Image
    {

        class CImagesProxyModel:
            public QSortFilterProxyModel
        {
            Q_OBJECT
        public:
            enum EImagesViewThumbnailViewProxyMode
            {
                IVTVPM_INVALID    = 0,
                IVTVPM_SIBLINGS   = 1,
                IVTVPM_SELECTIONS = 2
            };


            explicit CImagesProxyModel(Thumbnails::CThumbnailView* source, QObject* parent = 0);
            virtual ~CImagesProxyModel();

            bool                               hasSourceView() const;
            EImagesViewThumbnailViewProxyMode  proxyMode() const;
            void                               setProxyMode(EImagesViewThumbnailViewProxyMode mode);

        protected:
            virtual bool  filterAcceptsRow(int source_row, const QModelIndex& source_parent) const;
            virtual int   columnCount(const QModelIndex& parent = QModelIndex()) const;

        private:
            EImagesViewThumbnailViewProxyMode  m_mode;
            Thumbnails::CThumbnailView*        m_sourceview;
            QList< int >                       m_selectedindexrows;

            bool  updateSelectedIndexes();

        private slots:
            void  slotSourceViewItemsSelected(const QModelIndexList& indexes);
        };

    } // namespace Image
} // namespace Lihat

#endif // CIMAGESPROXYMODEL_H
