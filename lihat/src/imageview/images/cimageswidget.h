#ifndef CIMAGESWIDGET_H
#define CIMAGESWIDGET_H

#include <QWidget>


class QAction;

namespace Lihat
{
    namespace Image
    {

        class CImagesListView;
        class CThumbnailView;
        class CThumbnailViewWidget;

        class CImagesWidget:
            public QWidget
        {
            Q_OBJECT
        public:
            CImagesListView*  ImagesListView;

            explicit CImagesWidget(QWidget* parent = 0);
            virtual ~CImagesWidget();

        signals:
            void currentModeChanged();

        private:
            CThumbnailViewWidget*  m_thumbnailviewwidget;

            QAction*  getAction(const QString& name, bool mustexists = true);
            void      initActions();
            void      initUserInterface();

        private slots:
            void  slotSelectedSiblingsTriggered(bool checked);
            void  slotSiblingsTriggered(bool checked);
            void  slotSubImagesTriggered(bool checked);
        };

    } // namespace Image
} // namespace Lihat

#endif // CIMAGESWIDGET_H
