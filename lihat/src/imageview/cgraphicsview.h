#ifndef CGRAPHICSVIEW_H
#define CGRAPHICSVIEW_H

#include <QGraphicsView>

namespace Lihat
{
    namespace Image
    {

        class CGraphicsView:
            public QGraphicsView
        {
            Q_OBJECT

        public:
            CGraphicsView(QWidget* parent = 0);

            virtual void resizeEvent(QResizeEvent* event);

        signals:
            void resized();

        };

    } // namespace Image
} // namespace Lihat

#endif // CGRAPHICSVIEW_H
