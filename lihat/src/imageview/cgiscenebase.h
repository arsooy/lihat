#ifndef CGISCENEBASE_H
#define CGISCENEBASE_H

#include <QColor>
#include <QGraphicsWidget>


class QGraphicsItem;
class QPainter;

namespace Lihat
{
    namespace Image
    {

        class CGISceneBase:
            public QGraphicsWidget
        {
            Q_OBJECT
        public:
            QColor BackgroundColor;

            explicit CGISceneBase(QGraphicsItem* parent = 0, Qt::WindowFlags wFlags = 0);
            virtual ~CGISceneBase();

        protected:
            virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget = 0);

        };

    } // namespace Image
} // namespace Lihat

#endif // CGISCENEBASE_H
