#include "cgraphicsview.h"


using namespace Lihat::Image;


CGraphicsView::CGraphicsView(QWidget* parent):
    QGraphicsView(parent)
{

}

void CGraphicsView::resizeEvent(QResizeEvent* event)
{
    QGraphicsView::resizeEvent(event);

    emit resized();
}
