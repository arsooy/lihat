#ifndef CLOADJOB_H
#define CLOADJOB_H

#include "../imageload/curlloadjob.h"
#include "../imageload/isubimagejob.h"

#include <QString>

namespace Lihat
{
    namespace Image
    {

        class CGIImage;

        class CLoadJob:
            public ImageLoad::CURLLoadJob,
            public ImageLoad::ISubImageJob
        {
        public:
            CGIImage* const Requester;

            explicit CLoadJob(const CLoadJob& other);
            explicit CLoadJob(CGIImage* requester, const QUrl& url);
            virtual ~CLoadJob();

            // CURLLoadJob
            virtual EImageLoadType imageLoadType() const;
            virtual bool           load();

            // ISubImageJob
            virtual void setRequestedSubImage(const int idx);
            virtual int requestedSubImage() const;

        protected:
            int m_subimgidx;

            bool loadFile(const QString& fname);
            bool applySubImageToImageReader(QImageReader* ir, int* pindex);

        };

    } // namespace Image
} // namespace Lihat

#endif // CLOADJOB_H
