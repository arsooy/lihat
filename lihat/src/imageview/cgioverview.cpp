#include "cgioverview.h"

#include "cgiimage.h"
#include "cgiimageview.h"

#include <QColor>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QCursor>
#include <QtDebug>


using namespace Lihat;
using namespace Lihat::Image;

static const unsigned short MAX_WIDTH  = 128;
static const unsigned short MAX_HEIGHT = 96;
static const qreal          DRAG_DISTANCE_TRESHOLD = 1;

enum EOverviewViewDragState
{
    OVDS_NONE     = 0,
    OVDS_PREDRAG  = 1,
    OVDS_DRAGGING = 2
};

CGIOverview::CGIOverview(CGIImageView* container, CGIImage* imageview):
    QGraphicsWidget(container),
    m_autohideinterval(3500),
    m_basebrush(QColor(100, 100, 100), Qt::SolidPattern),
    m_basepen(QBrush(QColor(156, 156, 156, 128)), 1, Qt::SolidLine),
    m_dragviewstate((uchar) OVDS_NONE),
    m_isfadingin(true),
    m_image(imageview),
    m_maximumopacity(0.55),
    m_viewbrush(QColor(120, 120, 120), Qt::SolidPattern),
    m_viewpen(QBrush(QColor(170, 170, 170, 128)), 1, Qt::SolidLine)
{
    setVisible(false);
    setOpacity(0.0);
    setAcceptHoverEvents(true);
    setCursor(Qt::ArrowCursor);

    m_basepen.setCosmetic(true);
    m_viewpen.setCosmetic(true);
    m_fadetimeline.setDuration(400);
    m_fadetimeline.setFrameRange(1, 100);

    QObject::connect(&m_fadetimeline, SIGNAL(frameChanged(int)),
                     this, SLOT(slotFadeTimeLineFrameChanged(int)));
    QObject::connect(&m_fadetimeline, SIGNAL(finished()),
                     this, SLOT(slotFadeTimeLineFinished()));
    QObject::connect(&m_autohidetimer, SIGNAL(timeout()),
                     this, SIGNAL(hintAutoHide()));
}

CGIOverview::~CGIOverview()
{

}

QRectF CGIOverview::boundingRect() const
{
    if (m_image)
    {
        QSizeF imgsize = m_image->boundingRect().size();
        qreal downscale = scaleToImageView();
        QRectF brect = QRectF(0, 0, imgsize.width()  * downscale, imgsize.height() * downscale);
        return brect;
    }
    else
        return QRectF(0, 0, MAX_WIDTH, MAX_HEIGHT);
}

void CGIOverview::fadeIn()
{
    if (!isVisibleTo(parentItem()))
    {
        m_fadetimeline.stop();
        m_isfadingin = true;

        // begin at lowest opacity
        setOpacity(0.0 * m_maximumopacity);
        setVisible(true);

        m_fadetimeline.start();
    }
    else if (!m_dragviewstate)
    {
        // in short, only enable to auto-hide when we are not dragging
        // this function called by overview user to show it (and then expect to
        // auto-hide eventually) as such it get called when mouse pointer
        // position over image changed. Of course when we are dragging on
        // overview, which basically moving mouse, it will defeat the entire
        // purpose of NOT auto-hiding overview as long the use of this class
        // keep calling this; so we have to block that behaviour here
        updateHideCountdown();
    }
}

void CGIOverview::fadeOut()
{
    m_fadetimeline.stop();
    m_isfadingin = false;

    m_fadetimeline.start();
}

void CGIOverview::hoverEnterEvent(QGraphicsSceneHoverEvent* event)
{
    QGraphicsItem::hoverEnterEvent(event);

    updateHideCountdown();
    update();
}

void CGIOverview::hoverLeaveEvent(QGraphicsSceneHoverEvent* event)
{
    QGraphicsWidget::hoverLeaveEvent(event);

    updateHideCountdown();
    update();
}

void CGIOverview::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
    QGraphicsItem::mouseMoveEvent(event);

    if ((uchar) m_dragviewstate > OVDS_NONE)
    {
        // if user is actually dragging change the cursor
        if (((uchar) m_dragviewstate < OVDS_DRAGGING) && (QPointF(event->pos() - m_lastdragposition).manhattanLength() > DRAG_DISTANCE_TRESHOLD))
        {
            if (cursor().shape() != Qt::SizeAllCursor)
                setCursor(QCursor(Qt::SizeAllCursor));

            m_dragviewstate = OVDS_DRAGGING;
        }

        qreal downscale = scaleToImageView();
        QPointF dist(-(m_lastdragposition - event->pos()) / downscale);
        m_image->setViewTopLeft(m_image->viewTopLeft() + dist);
    }
    m_lastdragposition = event->pos();
}

void CGIOverview::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    QGraphicsItem::mousePressEvent(event);

    if (event->button() == Qt::LeftButton)
    {
        m_dragviewstate = OVDS_PREDRAG;
        m_lastdragposition = event->pos();

        // deduce the center of image from current mouse position against size
        // of viewport and the relative scale of this overview  against image
        // (and its scale too)
        qreal scaletoimg = scaleToImageView();
        QSizeF contsize =  parentItem()->boundingRect().size();
        QPointF topleft = (m_lastdragposition / scaletoimg ) -
                          (QPointF(contsize.width() / 2, contsize.height() / 2) / m_image->scale());

        if (!viewRect().contains(event->pos()))
        {
            // try to go to that position in image
            m_image->setViewTopLeft(topleft);
        }

        fadeIn();
        // disable autohide while dragging
        m_autohidetimer.stop();
    }
    event->accept();
}

void CGIOverview::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
    QGraphicsItem::mouseReleaseEvent(event);

    if (event->button() == Qt::LeftButton)
    {
        m_dragviewstate = OVDS_NONE;
        setCursor(QCursor(Qt::ArrowCursor));

        // start count down to hide this
        updateHideCountdown();
    }
    event->accept();
}

void CGIOverview::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    QRectF brect = boundingRect().toAlignedRect();


    QBrush pbrush(m_basebrush);
    if (isUnderMouse())
        pbrush.setColor(pbrush.color().darker(110));

    // draw edge twice, because somehow, sometimes, some edges renders visibly
    // different from the others; since the color opacity for the pen is ~50%
    // drawing it twice (obviously) will stack to ~100%
    painter->save();
    painter->setPen(m_basepen);
    painter->setBrush(pbrush);
    painter->drawRect(brect);
    painter->setBrush(Qt::NoBrush);
    painter->drawRect(brect);
    painter->restore();

    if (m_image)
    {
        QRectF viewpaintrect = viewRect();

        pbrush = m_viewbrush;
        if (isUnderMouse())
            pbrush.setColor(pbrush.color().lighter(130));

        painter->save();
        painter->setPen(m_viewpen);
        painter->setBrush(pbrush);
        painter->drawRect(viewpaintrect);
        painter->setBrush(Qt::NoBrush);
        painter->drawRect(viewpaintrect);
        painter->restore();
    }
}

void CGIOverview::updateHideCountdown()
{
    if (m_autohidetimer.isActive())
        m_autohidetimer.stop();

    m_autohidetimer.setSingleShot(true);
    m_autohidetimer.start(m_autohideinterval);
}

qreal CGIOverview::scaleToImageView() const
{
    Q_ASSERT(m_image);

    QRectF imgrect = m_image->boundingRect();
    QSizeF imgsize = imgrect.size();
    return qMin< qreal >(qBound< qreal >(0, MAX_WIDTH  / imgsize.width(),  1),
                         qBound< qreal >(0, MAX_HEIGHT / imgsize.height(), 1));
}

QRectF CGIOverview::viewRect()
{
    CGIImageView* container = m_image->container();
    Q_ASSERT(container);

    // basically calculating scale; image and viewing area's scale against
    // overview bounds
    QRectF imgrect = m_image->boundingRect();
    qreal imgscale = m_image->scale();
    QRectF contrect = container->geometry();
    QRectF downcontrect = QRectF(contrect.x() / imgscale, contrect.y() / imgscale,
                                 contrect.width() / imgscale, contrect.height() / imgscale).intersected(imgrect);

    qreal  scaletoimg = scaleToImageView();
    QPointF itopleft = m_image->viewTopLeft();
    QRectF result = QRectF(itopleft.x() * scaletoimg, itopleft.y() * scaletoimg,
                           downcontrect.width() * scaletoimg, downcontrect.height() * scaletoimg).toAlignedRect();

    return result;
}

QPainterPath CGIOverview::shape() const
{
    QPainterPath result;
    result.addRect(boundingRect());

    return result;
}

void CGIOverview::slotFadeTimeLineFinished()
{
    if (m_isfadingin)
        updateHideCountdown();
    else
    {
        setVisible(false);
        m_autohidetimer.stop();
    }
}

void CGIOverview::slotFadeTimeLineFrameChanged(int frame)
{
    Q_UNUSED(frame);

    if (m_isfadingin)
        setOpacity(m_maximumopacity * m_fadetimeline.currentValue());
    else
        setOpacity(m_maximumopacity * (1 - m_fadetimeline.currentValue()));
}
