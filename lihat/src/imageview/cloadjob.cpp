#include "cloadjob.h"

#include "../icoutils.h"
#include "cgiimage.h"
#include "cloadresult.h"

#include <QMovie>

using namespace Lihat;
using namespace Lihat::Image;


CLoadJob::CLoadJob(const CLoadJob& other):
    ImageLoad::CURLLoadJob(other),
    Requester(other.Requester)
{
    setRequestedSubImage(other.requestedSubImage());
}

CLoadJob::CLoadJob(CGIImage* requester, const QUrl& url):
    CURLLoadJob(url),
    Requester(requester),
    m_subimgidx(-1)
{

}

CLoadJob::~CLoadJob()
{

}

ImageLoad::CURLLoadJob::EImageLoadType CLoadJob::imageLoadType() const
{
    return ILT_IMAGEVIEW;
}

bool CLoadJob::load()
{
    dropResult();

    if (!URL.isLocalFile())
    {
        // for now we only load local files
        setResult(new CLoadResult(CLoadResult::ILRC_FAIL));

        return false;
    }

    return loadFile(URL.toLocalFile());
}

bool CLoadJob::loadFile(const QString& fname)
{
    // open and load the file
    QImageReader reader(fname);
    if (!reader.canRead())
    {
        reader.setDecideFormatFromContent(true);
        reader.setFileName(fname);
    }

    // see if we are dealing with animation file
    if (reader.supportsAnimation())
    {
        QMovie* mov = new QMovie(fname);
        mov->setCacheMode(QMovie::CacheAll);
        if (mov->isValid())
        {
            CLoadResult* result = new CLoadResult(CLoadResult::ILRC_OK);
            result->setLoadedMovie(mov);

            setResult(result);

            return true;
        }
        else
            delete mov;

        // unable to load as animation, continue to fall back to image
    }

    int subimgidx = -1;
    applySubImageToImageReader(&reader, &subimgidx);

    QImage* img = new QImage();
    if (reader.read(img))
    {
        CLoadResult* result = new CLoadResult(CLoadResult::ILRC_OK);
        result->setLoadedImage(img);
        result->SubImageIndex = subimgidx;

        setResult(result);

        return true;
    }
    else
    {
        delete img;

        setResult(new CLoadResult(CLoadResult::ILRC_FAIL));
    }

    return false;
}

bool CLoadJob::applySubImageToImageReader(QImageReader* ir, int* pindex)
{
    if (m_parambits & ImageLoad::ISubImageJob::InBit)
        return ir->jumpToImage(requestedSubImage());
    else
    {
        // no image index specified, look for the best looking
        if (QString(ir->format()) == Utils::ICO::FORMAT_STR)
            return Utils::ICO::locateNicerIcon(ir, Utils::ICO::MAX_DIMENSION, pindex) == Utils::ICO::NILR_SUCCESS;
    }

    return false;
}

int CLoadJob::requestedSubImage() const
{
    return m_subimgidx;
}

void CLoadJob::setRequestedSubImage(const int idx)
{
    m_subimgidx = idx;
    if (m_subimgidx >= 0)
        m_parambits |= ImageLoad::ISubImageJob::InBit;
}
