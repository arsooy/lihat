#ifndef CONFIG_LIHAT_H
#define CONFIG_LIHAT_H

#include "config_lihat_cmake.h"


#if (HAS_KDE4_LIBKONQ || HAS_WINDOWS)
    #ifdef HAS_KDE4_LIBKONQ
        #define FILEOPS_API_LIBKONQ
    #endif
    #ifdef HAS_WINDOWS
        #define FILEOPS_API_WINDOWS
    #endif
#else
    #define FILEOPS_API_NONE
#endif

#endif // CONFIG_LIHAT_H
