#include "cmimetyperesolver.h"

#include "cmimetyperesolver_p.h"

#ifdef HAS_LIBMAGIC
#include <magic.h>
#endif

#include <QDir>
#include <QDomDocument>
#include <QDomElement>
#include <QDomNodeList>
#include <QFileInfo>
#include <QtDebug>


using namespace Lihat;

enum ESuffixMatchedType
{
    SMT_INVALID = 0,
    SMT_PREFIX  = 1,
    SMT_SUFFIX  = 2,
    SMT_FULL    = 3
};

bool lessThanSMIMEType(SMIMEType* a, SMIMEType* b);

CMIMETypeResolver::CMIMETypeResolver():
    d(new CMIMETypeResolverPrivate(this))
{

}

CMIMETypeResolver::~CMIMETypeResolver()
{
    delete d;
}

SMIMEType* CMIMETypeResolver::fromFilename(const QString& fname)
{
    d->buildCache();
    return d->searchCacheByFilename(fname);
}

SMIMEType* CMIMETypeResolver::fromTypeName(const QString& tname)
{
    d->buildCache();
    return d->searchCacheByTypename(tname);
}

QString CMIMETypeResolver::iconNameFromTypeName(const QString& tname, bool generic)
{
    // This is to anticipate the use of libmagic MIME type name
    // (see searchCacheByFilename) passed to tname.
    SMIMEType* mimetype = fromTypeName(tname);
    if (mimetype)
        return mimetype->iconName(generic);
    else
    {
        // As it does not use CMIMETypeResolver internal cache so we must
        // provide a fallback.
        QString result = tname;
        int slashindex = tname.indexOf('/');
        if (slashindex != -1)
            result[slashindex] = '-';
        return result;
    }
}

QString CMIMETypeResolver::typeNameFromFilename(const QString& fname, bool baseclass)
{
    QString result;

    SMIMEType* mimetype = fromFilename(fname);
    if (mimetype)
    {
        if (baseclass)
            return mimetype->SubClassOf;
        else
            return mimetype->Type;
    }
    else
        return d->searchMagicDatabaseByFilename(fname);

    return QString();
}


SMIMEType::SMIMEType() :
    Type(),
    Comment(),
    GenericIconName(),
    SubClassOf(),
    MagicPriority(50)
{

}

bool SMIMEType::filenameGlobMatch(const QString& fname, bool casesensitive, QString* matchingpattern) const
{
    if (matchingpattern)
        matchingpattern->clear();

    if (GlobPatterns.isEmpty())
        return false;

    QString filename = casesensitive ? fname : fname.toLower();
    int filename_len = filename.length();

    for (int i = 0; i < GlobPatterns.count(); i++)
    {
        QString pattern = GlobPatterns.at(i);
        if (pattern.isEmpty())
            continue;
        pattern = casesensitive ? pattern : pattern.toLower();

        // basic glob matching routine, this will (try) to match fname to
        // patterns like '*.foo', 'foo.*', and 'literalfoo'

        int pattern_len = pattern.length();
        int stars = pattern.count('*');

        if ((pattern.at(0) == '*') && (stars == 1))
        {
            // 'foo.txt' must, at least, have more characters than '.txt'
            if (filename_len < pattern_len - 1)
                continue;

            // compare filename againts pattern by iterating each characters
            // in reverse (starting from the end) until pattern exhausted
            const QChar* pattern_cp = pattern.unicode() + pattern_len - 1;
            const QChar* fname_cp = filename.unicode() + filename_len - 1;

            int cnt = 1;
            while ((cnt < pattern_len) && (*pattern_cp-- == *fname_cp--))
                ++cnt;
            if (cnt == pattern_len)
            {
                if (matchingpattern)
                    *matchingpattern = pattern;
                return true;
            }
        }
        else if ((pattern.at(pattern_len - 1) == '*') && (stars == 1))
        {
            // 'README.NOW' must, at least, have more characters than 'README.'
            if (filename_len < pattern_len - 1)
                continue;

            // compare filename againts pattern by iterating each characters
            // in reverse until pattern exhausted
            const QChar* pattern_cp = pattern.unicode();
            const QChar* fname_cp = filename.unicode();

            int cnt = 1;
            while ((cnt < pattern_len) && (*pattern_cp++ == *fname_cp++))
                ++cnt;
            if (cnt == pattern_len)
            {
                if (matchingpattern)
                    *matchingpattern = pattern;
                return true;
            }
        }
        else if (stars == 0)
        {
            // literal (well, with respect to case sensitivity) comparison
            // between filename and pattern
            if (pattern == filename)
            {
                if (matchingpattern)
                    *matchingpattern = pattern;
                return true;
            }
        }
    }

    return false;
}

QString SMIMEType::iconName(bool generic) const
{
    if (generic)
    {
        if (GenericIconName.isEmpty())
        {
            QString result = Type;
            int slashindex = result.indexOf('/');
            if (slashindex != -1)
                result = result.left(slashindex);
            return result + "-x-generic";
        }
        else
            return GenericIconName;
    }
    else
    {
        QString result = Type;
        int slashindex = result.indexOf('/');
        if (slashindex != -1)
            result[slashindex] = '-';
        return result;
    }
}


CMIMETypeResolverPrivate::CMIMETypeResolverPrivate(CMIMETypeResolver* mtresolver):
    q(mtresolver),
    CacheBuilt(false)
{

}

CMIMETypeResolverPrivate::~CMIMETypeResolverPrivate()
{
    clearCache();
}

void CMIMETypeResolverPrivate::buildCache(bool rebuild)
{
    if (CacheBuilt && !rebuild)
        return;

    clearCache();
    QStringList mimefiles = getMIMEInfoFiles();
    for (int i = 0; i < mimefiles.count(); ++i)
    {
        QFile mimefile(mimefiles.at(i));
        if (mimefile.open(QFile::ReadOnly))
            parseMIMEInfoXML(&mimefile);
    }
    qSort(MIMETypes.begin(), MIMETypes.end(), lessThanSMIMEType);

    CacheBuilt = true;
}

void CMIMETypeResolverPrivate::clearCache()
{
    PatternLookup.clear();
    TypeNameLookup.clear();
    while (MIMETypes.count() > 0)
        delete MIMETypes.takeFirst();
    CacheBuilt = false;
}

QStringList CMIMETypeResolverPrivate::getMIMEInfoFiles() const
{
    QString home(getenv("HOME"));
    QStringList filters;
    filters << "*.xml";

    QStringList result;
    QStringList datadirs = QString(getenv("XDG_DATA_DIRS")).split(":", QString::SkipEmptyParts);
    if (datadirs.isEmpty())
    {
        datadirs << QString("/usr/local/share/");
        datadirs << QString("/usr/share/");
    }
    datadirs.removeDuplicates();

    for (int i = 0; i < datadirs.count(); ++i)
    {
        QString datadirstr = datadirs.at(i);
        if (datadirstr.startsWith("~"))
            datadirstr = home + datadirstr.mid(1);

        QDir mimepackagesdir(datadirstr + QString("/mime/packages"));
        if (!mimepackagesdir.exists())
            continue;

        QFileInfoList mimeinfofiles = mimepackagesdir.entryInfoList(filters, QDir::Files);
        for (int k = 0; k < mimeinfofiles.count(); ++k)
            result << mimeinfofiles.at(k).absoluteFilePath();
    }

    // TODO: Is it a good idea to embed freedesktop.org.xml into the program?
    //       We can use that if result came up empty.

    return result;
}

int CMIMETypeResolverPrivate::parseMIMEInfoXML(QIODevice* xml)
{
    int result = 0;

    // using (lazy) DOM for now..
    QDomDocument xmldom;
    if (!xmldom.setContent(xml))
        return 0;

    QDomElement rootelem = xmldom.documentElement();
    if (rootelem.nodeName() != "mime-info")
        return 0;

    QDomNodeList mimetypeelems = rootelem.elementsByTagName("mime-type");
    for (int i = 0; i < mimetypeelems.count(); ++i)
    {
        QDomElement mimetypeelem = mimetypeelems.at(i).toElement();
        SMIMEType* mimetype = parseMIMETypeElement(&mimetypeelem);
        if (mimetype)
        {
            MIMETypes << mimetype;
            ++result;
        }
    }

    return result;
}

SMIMEType* CMIMETypeResolverPrivate::parseMIMETypeElement(QDomElement* element) const
{
    if (!element->hasAttribute("type"))
        return 0;

    SMIMEType* result = new SMIMEType();
    result->Type = element->attribute("type");

    QDomNodeList commentelems = element->elementsByTagName("comment");
    for (int i = 0; i < commentelems.count(); ++i)
    {
        QDomElement curelem = commentelems.at(i).toElement();
        if (!curelem.hasAttribute("xml:lang"))
        {
            // skip localized comments
            result->Comment = curelem.text();
            break;
        }
    }

    QDomNodeList globelems = element->elementsByTagName("glob");
    for (int i = 0; i < globelems.count(); ++i)
    {
        QDomElement curelem = globelems.at(i).toElement();
        if (curelem.isElement() && curelem.hasAttribute("pattern"))
            result->GlobPatterns << curelem.attribute("pattern");
    }
    result->GlobPatterns.removeDuplicates();
    result->GlobPatterns.sort();

    QDomNodeList subclasselems = element->elementsByTagName("sub-class-of");
    if (subclasselems.count() > 0)
    {
        QDomElement curelem = subclasselems.at(0).toElement();
        if (curelem.hasAttribute("type"))
            result->SubClassOf = curelem.attribute("type");
    }

    QDomNodeList genericiconselems = element->elementsByTagName("generic-icon");
    if (subclasselems.count() > 0)
    {
        QDomElement curelem = genericiconselems.at(0).toElement();
        if (curelem.hasAttribute("name"))
            result->GenericIconName = curelem.attribute("name");
    }

    QDomNodeList magicelems = element->elementsByTagName("magic");
    if (magicelems.count() > 0)
    {
        QDomElement magicelem = magicelems.at(0).toElement();
        if (magicelem.hasAttribute("priority"))
        {
            bool priorityok = false;
            unsigned short priority = magicelem.attribute("priority").trimmed().toUShort(&priorityok);
            if (priorityok)
                result->MagicPriority = qBound< char >(0, priority, 100);
            else
                result->MagicPriority = 50;
        }
    }

    return result;
}

SMIMEType* CMIMETypeResolverPrivate::searchCacheByFilename(const QString& fname)
{
    QFileInfo finfo(fname);
    QString filename = finfo.fileName();

    QString part;
    // do cache lookup for '*.suffix'
    part = finfo.completeSuffix();
    if (!part.isEmpty())
    {
        if (GlobSuffixPatternLookup.contains(part))
            return MIMETypes.value(*GlobSuffixPatternLookup.object(part), 0);
    }
    part = finfo.suffix();
    if (!part.isEmpty())
    {
        if (GlobSuffixPatternLookup.contains(part))
            return MIMETypes.value(*GlobSuffixPatternLookup.object(part), 0);
    }

    int dotindex = filename.indexOf('.');
    if (dotindex != -1)
    {
        // do cache lookup for 'foo.*'
        part = filename.left(dotindex + 1);
        if (!part.isEmpty())
        {
            if (GlobPrefixPatternLookup.contains(part))
                return MIMETypes.value(*GlobPrefixPatternLookup.object(part), 0);
        }
    }
    else
    {
        // do cache lookup for 'foo'
        part = filename.toLower();
        if (!part.isEmpty())
        {
            if (PatternLookup.contains(part))
                return MIMETypes.value(*PatternLookup.object(part), 0);
        }
    }

    // lookup not available, find cache for its MIME type and match its
    // glob patterns.
    return searchByMatchingGlobPatterns(filename);
}

SMIMEType* CMIMETypeResolverPrivate::searchByMatchingGlobPatterns(const QString& filename, char* kind)
{
    for (int i = 0; i < MIMETypes.count(); ++i)
    {
        int* pmindex = 0;
        QString matchingpart;
        SMIMEType* mimetype = MIMETypes.at(i);
        if (mimetype->filenameGlobMatch(filename, false, &matchingpart))
        {
            if (!matchingpart.isEmpty())
            {
                int starscount = matchingpart.count('*');
                if ((matchingpart.at(0) == '*') && (starscount == 1) && (matchingpart.indexOf('[') == -1))
                {
                    matchingpart = matchingpart.mid(1);
                    pmindex = new int(i);
                    GlobSuffixPatternLookup.insert(matchingpart, pmindex);
                    if (kind)
                        *kind = SMT_SUFFIX;
                }
                else if ((matchingpart.at(matchingpart.length() - 1) == '*') && (starscount == 1) && (matchingpart.indexOf('[') == -1))
                {
                    matchingpart = matchingpart.left(matchingpart.indexOf('*'));
                    pmindex = new int(i);
                    GlobPrefixPatternLookup.insert(matchingpart, pmindex);
                    if (kind)
                        *kind = SMT_PREFIX;
                }
                else if (starscount == 0)
                {
                    pmindex = new int(i);
                    PatternLookup.insert(matchingpart, pmindex);
                    if (kind)
                        *kind = SMT_FULL;
                }
            }

            return mimetype;
        }
    }

    return 0;
}

SMIMEType* CMIMETypeResolverPrivate::searchCacheByTypename(const QString& tname)
{
    int* pmindex = 0;
    if (TypeNameLookup.contains(tname))
    {
        pmindex = TypeNameLookup.object(tname);
        if (pmindex)
            return MIMETypes.value(*pmindex, 0);
    }
    else
    {
        for (int i = 0; i < MIMETypes.count(); ++i)
        {
            SMIMEType* mimetype = MIMETypes.at(i);
            if (mimetype->Type == tname)
            {
                pmindex = new int(i);
                break;
            }
        }

        TypeNameLookup.insert(tname, pmindex);
        if (pmindex)
            return MIMETypes.value(*pmindex, 0);
    }

    return 0;
}

QString CMIMETypeResolverPrivate::searchMagicDatabaseByFilename(const QString& fname)
{
#ifdef HAS_LIBMAGIC
    magic_t magich = magic_open(MAGIC_MIME_TYPE | MAGIC_PRESERVE_ATIME);
    Q_ASSERT(magich);

    if (magic_load(magich, 0))
    {
        magic_close(magich);
        qCritical() << "CMIMETypeResolver::fromFilename:" <<
                       "Unable to load default magic database file!";
        return QString();
    }

    QByteArray path = fname.toLocal8Bit();
    QString result = QString(magic_file(magich, path.data()));
    magic_close(magich);

    return result;
#else
    Q_UNUSED(fname);
    return QString();
#endif
}


Q_GLOBAL_STATIC(CMIMETypeResolver, glMIMETypeResolver)

CMIMETypeResolver* Lihat::getMIMETypeResolver()
{
    return glMIMETypeResolver();
}


bool lessThanSMIMEType(SMIMEType* a, SMIMEType* b)
{
    bool result = false;
    if (a->MagicPriority == b->MagicPriority)
    {
        int len_a = a->Type.length();
        int len_b = b->Type.length();
        if (len_a == len_b)
            result = QString::compare(a->Type, b->Type, Qt::CaseInsensitive);
        else
            result = len_a < len_b;
    }
    else
        result = a->MagicPriority < b->MagicPriority;

    return !result;   // sort descending
}
