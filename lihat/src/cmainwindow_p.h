#ifndef CMAINWINDOW_P_H
#define CMAINWINDOW_P_H

#include "types_lihat.h"

#include <QKeySequence>
#include <QList>
#include <QMultiMap>
#include <QString>
#include <QUrl>


class QStackedWidget;

namespace Lihat
{
    namespace Image
    {

        class  CImageViewWidget;

    } // namespace Image

    namespace Thumbnails
    {
        class  CThumbnailViewWidget;
    } // namespace Thumbnails

    class  CMainWindow;

    enum EMainWindowMode
    {
        MWM_INVALID   = 0,
        MWM_THUMBNAIL = 1,
        MWM_IMAGE     = 2
    };


    struct SMainWindowStartContext
    {
        EMainWindowMode  Mode;
        QString          Path;
    };


    class CViewWidget;


    class CMainWindowPrivate
    {
    public:
        Q_DECLARE_PUBLIC(CMainWindow)
        CMainWindow* const     q_ptr;

        bool                                AdjustWindowToImageSizeRatio;
        bool                                EnableWindowModeToggling;
        QMultiMap< QString, QKeySequence >  KeySequences;
        CViewWidget*                        ViewWidget;

        CMainWindowPrivate(CMainWindow* mainwindow);
        ~CMainWindowPrivate();

        void deserialize();
        void serialize();

        void initActions();
        void initUserInterface();
        void applyActionKeySequences();

        QAction*  getAction(const QString& name, bool mustexists = true);
        void      updateActionsState();

        bool isInImageViewMode() const;
        bool isInThumbnailViewMode() const;

        void currentPositionChanged();
        bool setStartContext(SMainWindowStartContext* startctx);
        void urlToWindowTitle(const QUrl& url);
    };

} // namespace Lihat

#endif // CMAINWINDOW_P_H
