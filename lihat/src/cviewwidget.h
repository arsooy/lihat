#ifndef CVIEWWIDGET_H
#define CVIEWWIDGET_H

#include "imageview/cimageviewwidget.h"
#include "thumbnailview/cthumbnailviewwidget.h"
#include "types_lihat.h"

#include <QStackedWidget>


namespace Lihat
{

    class CViewWidget:
        public QStackedWidget
    {
        Q_OBJECT
    public:
        explicit CViewWidget(QWidget* parent = 0);
        virtual ~CViewWidget();

        Image::CImageViewWidget          ImageView;
        Thumbnails::CThumbnailViewWidget ThumbnailView;

        bool hasNext();
        bool hasPrevious();

        /**
         * @brief Override positioning flag for later use in hasNext/hasPrevious.
         *
         * @param pos The possition to set.
         */
        void setCurrentPos(ListPosition pos);

    signals:
        void activeWidgetChanged();
        void currentChanged();
        void pathChanged(const QUrl& url);
        void imageViewed(const QUrl& url);

    public slots:
        void next();
        void previous();
        void showImageView();
        void showThumbnailView();
        void reload();

        bool imageOpen(const QUrl& url);
        bool thumbnailOpen(const QUrl& url);

    private:
        bool         m_changepathonshowthumbnailview;
        bool         m_ignorepathchanged;
        bool         m_openonshowimageview;
        ListPosition m_currentpos;

        void currentChanged_();

    private slots:
        void slotImageViewImageLoaded(bool loaded, const QUrl& url);
        void slotThumbnailViewCurrentChanged(const QUrl& currenturl);
        void slotThumbnailViewPathChanged(const QUrl& pathurl);
        void slotThumbnailViewViewRequested(const QUrl& url);

    };

} // namespace Lihat

#endif // CVIEWWIDGET_H
