#include "cviewwidget.h"

#include "types_lihat.h"


using namespace Lihat;


CViewWidget::CViewWidget(QWidget* parent):
    QStackedWidget(parent),
    ImageView(this),
    ThumbnailView(this),
    m_changepathonshowthumbnailview(true),
    m_ignorepathchanged(false),
    m_openonshowimageview(true),
    m_currentpos(LP_INVALID)
{
    ImageView.setObjectName("ivMain");
    QObject::connect(&ImageView, SIGNAL(imageLoaded(bool, QUrl)),
                     this, SLOT(slotImageViewImageLoaded(bool, QUrl)));
    QObject::connect(&ImageView, SIGNAL(nextImageRequested()),
                     this, SLOT(next()));
    QObject::connect(&ImageView, SIGNAL(previousImageRequested()),
                     this, SLOT(previous()));
    QObject::connect(&ImageView, SIGNAL(toThumbnailViewRequested()),
                     this, SLOT(showThumbnailView()));

    // thumbnail view widget
    ThumbnailView.setObjectName("tvMain");
    QObject::connect(&ThumbnailView, SIGNAL(currentChanged(QUrl)),
                     this, SLOT(slotThumbnailViewCurrentChanged(QUrl)));
    QObject::connect(&ThumbnailView, SIGNAL(pathChanged(QUrl)),
                     this, SLOT(slotThumbnailViewPathChanged(QUrl)));
    QObject::connect(&ThumbnailView, SIGNAL(viewRequested(QUrl)),
                     this, SLOT(slotThumbnailViewViewRequested(QUrl)));

    ImageView.setThumbnailViewWidget(&ThumbnailView);
    ThumbnailView.setImageViewWidget(&ImageView);

    addWidget(&ImageView);
    addWidget(&ThumbnailView);
}

CViewWidget::~CViewWidget()
{

}

void CViewWidget::currentChanged_()
{
    emit currentChanged();
}

bool CViewWidget::hasNext()
{
    return (m_currentpos == LP_MIDDLE) || ThumbnailView.hasNextThumbnail();
}

bool CViewWidget::hasPrevious()
{
    return (m_currentpos == LP_MIDDLE) || ThumbnailView.hasPreviousThumbnail();
}

void CViewWidget::setCurrentPos(ListPosition pos)
{
    m_currentpos = pos;
}

void CViewWidget::next()
{
    // It is possible that the first nextThumbnail() call here to fail. In
    // one of the expected case is that user is viewing the image by including
    // the image file in the command line (ie. executed by the shell because
    // of the file association).
    //
    // Common-sense dictates Lihat not to enumerate ALL file(s) in that file's
    // directory (why? it might well be the user just want to see that one
    // file) when executed in this mode, it will just open that file and
    // continue its business. Now, what we have here is the user's wish to get
    // the next file to this. Since Lihat didn't enumerate its (the current
    // image) sibling files, therefore the nextThumbnail() (which in turn
    // CThumbnailView's adjustCurrentThumbnailByOffset() that requires a valid
    // QModelIndex) will return false.
    //
    // To handle this situation, we set the current thumbnail in the thumbnail
    // view to the current image, and THEN call the nextThumbnail().
    bool cango = ThumbnailView.hasNextThumbnail();

    if (ImageView.isVisible())
    {
        QUrl urlpath = ImageView.currentLoadedParentURL();

        cango = urlpath.isValid() && (urlpath == ThumbnailView.currentThumbnailParentURL());
        if (!cango)
        {
            QUrl imageurl = ImageView.currentLoadedURL();

            // setCurrentThumbnail() should change the path of the thumbnail
            // mode to the file path of current image.
            m_ignorepathchanged = true;
            cango = ThumbnailView.setCurrentThumbnail(imageurl) && ThumbnailView.hasNextThumbnail();
            m_ignorepathchanged = false;
        }
    }

    if (cango)
    {
        if (ThumbnailView.isVisible())
            ThumbnailView.nextThumbnail();
        else if (ImageView.isVisible())
        {
            // save current url
            QUrl lasturl = ThumbnailView.currentThumbnailURL();

            // keep going forward to the next loadable item
            while (true)
            {
                ThumbnailView.nextThumbnail();
                QUrl url = ThumbnailView.currentThumbnailURL();
                if (!imageOpen(url))
                {
                    cango = ThumbnailView.hasNextThumbnail();

                    // ok, so we cant go forward .. stop and go back to where we came from
                    if (!cango)
                    {
                        if (url != lasturl)
                            ThumbnailView.setCurrentThumbnail(lasturl);

                        // hint user that we dont have any more (loadable)
                        // item, put it here to override currentThumbnailChanged()
                        // called by setCurrentThumbnail.
                        m_currentpos = LP_LAST;
                        break;
                    }
                }
                else
                    break;
            }
        }
    }

    currentChanged_();
}

void CViewWidget::previous()
{
    // NOTE: See starting comment on goNext()
    bool cango = ThumbnailView.hasPreviousThumbnail();

    if (ImageView.isVisible())
    {
        QUrl urlpath = ImageView.currentLoadedParentURL();

        cango =  urlpath.isValid() && (urlpath == ThumbnailView.currentThumbnailParentURL());
        if (!cango)
        {
            QUrl imageurl = ImageView.currentLoadedURL();

            // setCurrentThumbnail() should change the path of the thumbnail
            // mode to the file path of current image.
            m_ignorepathchanged = true;
            cango = ThumbnailView.setCurrentThumbnail(imageurl) && ThumbnailView.hasPreviousThumbnail();
            m_ignorepathchanged = false;
        }
    }

    if (cango)
    {
        if (ThumbnailView.isVisible())
            ThumbnailView.previousThumbnail();
        else if (ImageView.isVisible())
        {
            // save current url
            QUrl lasturl = ThumbnailView.currentThumbnailURL();

            // keep going backward to the next loadable item
            while (true)
            {
                ThumbnailView.previousThumbnail();
                QUrl url = ThumbnailView.currentThumbnailURL();
                if (!imageOpen(url))
                {
                    cango = ThumbnailView.hasPreviousThumbnail();

                    if (!cango)
                    {
                        if (url != lasturl)
                            ThumbnailView.setCurrentThumbnail(lasturl);

                        // hint user that we dont have any previous (loadable)
                        // item, put it here to override currentPositionChanged()
                        // called by setCurrentThumbnail.
                        m_currentpos = LP_FIRST;
                        break;
                    }
                }
                else
                    break;
            }
        }
    }

    currentChanged_();
}

void CViewWidget::showImageView()
{
    bool fromthumbnailview = ThumbnailView.isVisible();
    if (m_openonshowimageview && fromthumbnailview)
    {
        QUrl imgurl = ThumbnailView.currentThumbnailURL();
        if (imgurl.isValid())
        {
            if (!ImageView.load(imgurl))
            {
                qDebug() << "CViewWidget::showImageView:" <<
                            "unable to load" << imgurl;
                return;
            }
        }
    }

    setCurrentWidget(&ImageView);
    emit activeWidgetChanged();
}

void CViewWidget::showThumbnailView()
{
    bool fromimageview = ImageView.isVisible();
    if (m_changepathonshowthumbnailview && fromimageview)
    {
        QUrl imgurl = ImageView.currentLoadedURL();
        if (imgurl.isValid())
        {
            if (!ThumbnailView.setCurrentThumbnail(imgurl))
            {
                qDebug() << "CViewWidget::showThumbnailView:" <<
                            "unable to set current thumbnail to" << imgurl;
                return;
            }
        }
    }

    setCurrentWidget(&ThumbnailView);
    emit activeWidgetChanged();
}

bool CViewWidget::imageOpen(const QUrl& url)
{
    if (ImageView.load(url))
    {
        m_openonshowimageview = false;
        showImageView();
        m_openonshowimageview = true;
        return true;
    }

    return false;
}

bool CViewWidget::thumbnailOpen(const QUrl& url)
{
    if (ThumbnailView.goToPath(url))
    {
        m_changepathonshowthumbnailview = false;
        showThumbnailView();
        m_changepathonshowthumbnailview = true;
        return true;
    }

    return false;
}

void CViewWidget::reload()
{
    if (ImageView.isVisible())
        ImageView.reload();
    else if (ThumbnailView.isVisible())
        ThumbnailView.reload();
}

void CViewWidget::slotImageViewImageLoaded(bool loaded, const QUrl& imgurl)
{
    if (!loaded)
        return;

    currentChanged_();
    emit imageViewed(imgurl);
}

void CViewWidget::slotThumbnailViewCurrentChanged(const QUrl& currenturl)
{
    Q_UNUSED(currenturl);

    // invalidate flag for position
    m_currentpos = LP_INVALID;

    // because this will (eventually) update it
    currentChanged_();
}

void CViewWidget::slotThumbnailViewPathChanged(const QUrl& pathurl)
{
    if (m_ignorepathchanged)
        return;

    currentChanged_();
    emit pathChanged(pathurl);
}

void CViewWidget::slotThumbnailViewViewRequested(const QUrl& imgurl)
{
    imageOpen(imgurl);
}
