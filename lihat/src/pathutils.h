#ifndef PATHUTILS_H
#define PATHUTILS_H

#include <QString>
#include <QStringList>
#include <QUrl>

namespace Lihat
{
    namespace Utils
    {
        namespace Path
        {
            QString      join(const QString& path, const QString& name);
            bool         parentOfPath(const QString& path, QString& outparent);
            bool         parentOfPath(const QUrl& path, QString& outparent);
            QString      toClean(const QString& path);
            QString      toNative(const QString& path);
            QStringList  urlsToStringList(const QList< QUrl >& urls);
        } // namespace Path
    } // namespace Utils
} // namespace Lihat

#endif // PATHUTILS_H
