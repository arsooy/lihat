#include "fileutils.h"

#include "config_lihat.h"
#include "pathutils.h"

#ifdef FILEOPS_API_NONE
#include "fileoperations/cfileoperationhub.h"
#endif

#ifdef FILEOPS_API_LIBKONQ
#include "fileoperations/copy/ckde4filecopyqueue.h"
#include "fileoperations/delete/ckde4filedeletequeue.h"
#include "fileoperations/move/ckde4filemovequeue.h"

#include <konqmimedata.h>
#endif

#ifdef FILEOPS_API_WINDOWS
#include "fileoperations/copy/cwindowsfilecopyqueue.h"
#include "fileoperations/delete/cwindowsfiledeletequeue.h"
#include "fileoperations/move/cwindowsfilemovequeue.h"

#include "windows/windowsutils.h"

static const unsigned long  WINDOWS_DROPEFFECT_MOVE = 2;
static const QString        WINDOWS_PREFERRED_DROPEFFECT("Preferred DropEffect");
#endif

#include <ctype.h>

#include <QApplication>
#include <QByteArray>
#include <QClipboard>
#include <QFile>
#include <QList>
#include <QMimeData>
#include <QString>
#include <QStringList>
#include <QUrl>
#include <QtDebug>


using namespace Lihat::Utils;
using namespace Lihat;

bool File::copyFilesTo(const QStringList& files, const QString& destination)
{
#ifdef FILEOPS_API_LIBKONQ
        qDebug() << "CFileOperationHub::createFileCopyOperation:" <<
                    "Using KDE4 API to copy" << files << " to " << destination;

        FileOps::KDE4::CKDE4FileCopyQueue* kde4result = new FileOps::KDE4::CKDE4FileCopyQueue(files, destination);

        // do not add this to m_operations, instead let it delete itself
        QObject::connect(kde4result, SIGNAL(finished()), kde4result, SLOT(deleteLater()));
        kde4result->start();

        return bool(kde4result);
#endif
#ifdef FILEOPS_API_WINDOWS
        qDebug() << "CFileOperationHub::createFileCopyOperation:" <<
                    "Using Windows API to copy" << files << " to " << copyto;

        CWindowsFileCopyQueue* winresult = new CWindowsFileCopyQueue(files, copyto);

        // do not add this to m_operations, instead let it delete itself
        connect(winresult, SIGNAL(finished()), winresult, SLOT(deleteLater()));

        if (autostart)
            winresult->start();

        return bool(winresult);
#endif
#ifdef FILEOPS_API_NONE
    return FileOps::getGlobalFileOperationHub()->createFileCopyOperation(files, destination, true) != 0;
#endif

    return false;
}

bool File::copyFilesToClipboard(const QStringList& files)
{
    QMimeData* mimedata = new QMimeData();
    QList< QUrl > urls;

    for (int i = 0; i < files.count(); ++i)
    {
        QUrl url = QUrl::fromLocalFile(files.at(i));
        urls << url;
    }
    mimedata->setUrls(urls);

    QApplication::clipboard()->setMimeData(mimedata);
    return true;
}

bool File::cutFilesToClipboard(const QStringList& files)
{
    QMimeData* mimedata = new QMimeData();
    QList< QUrl > urls;

    for (int i = 0; i < files.count(); ++i)
    {
        QUrl url = QUrl::fromLocalFile(files.at(i));
        urls << url;
    }
    mimedata->setUrls(urls);

#ifdef FILEOPS_API_LIBKONQ
        KonqMimeData::addIsCutSelection(mimedata, true);
        QApplication::clipboard()->setMimeData(mimedata);

        return true;
#endif
#ifdef FILEOPS_API_WINDOWS
        QByteArray dropeffect(sizeof(unsigned long), '\0');  // sizeof(DWORD a.k.a unsigned long)
        dropeffect[0] = WINDOWS_DROPEFFECT_MOVE;

        mimedata->setData(WINDOWS_PREFERRED_DROPEFFECT, dropeffect);
        QApplication::clipboard()->setMimeData(mimedata);

        return true;
#endif

    // if we got here then the lihat is not compiled with system-dependant file
    // cutting API, clean up and complain
    if (mimedata)
        delete mimedata;

    qWarning() << "Lihat::Utils::File::cutFilesToClipboard:" <<
                  "Unable to cut files to system clipboard because Lihat not compiled with code to do this.";
    // clean current clipboard because we do not want user to paste wrong document
    QApplication::clipboard()->clear();

    return false;
}

bool File::deleteFiles(const QStringList& files)
{
#ifdef FILEOPS_API_LIBKONQ
        qDebug() << "CFileOperationHub::createFileDeleteOperation:" <<
                    "Using KDE4 API to delete" << files;

        FileOps::KDE4::CKDE4FileDeleteQueue* kde4result = new FileOps::KDE4::CKDE4FileDeleteQueue(files);

        // do not add this to m_operations, instead let it delete itself
        QObject::connect(kde4result, SIGNAL(finished()), kde4result, SLOT(deleteLater()));
        kde4result->start();

        return bool(kde4result);
#endif
#ifdef FILEOPS_API_WINDOWS
        qDebug() << "CFileOperationHub::createFileDeleteOperation:" <<
                    "Using Windows API to delete" << files;

        CWindowsFileDeleteQueue* winresult = new CWindowsFileDeleteQueue(files);

        // do not add this to m_operations, instead let it delete itself
        connect(winresult, SIGNAL(finished()), winresult, SLOT(deleteLater()));

        if (autostart)
            winresult->start();

        return bool(winresult);
#endif
#ifdef FILEOPS_API_NONE
    return FileOps::getGlobalFileOperationHub()->createFileDeleteOperation(files, true) != 0;
#endif

    return false;
}

bool File::fileIsText(const QString& fname)
{
    QFile file(fname);
    if (!file.open(QIODevice::ReadOnly))
        return false;

    QByteArray buf = file.read(8 << 4);
    if (!buf.isEmpty())
    {
        // check for Byte Order Mark
        static const QByteArray ByteOrderMarks [] =
        {
            QByteArray("\xEF\xBB\xBF"),         // UTF-8 BOM
            QByteArray("\xFE\xFF"),             // UTF-16 BOM big endian
            QByteArray("\xFF\xFE"),             // UTF-16 BOM little endian
            QByteArray("\x00\x00\xFE\xFF", 4),  // UTF-32 BOM big endian
            QByteArray("\xFF\xFE\x00\x00", 4),  // UTF-32 BOM little endian
        };
        for (int i = 4; i >= 0; --i)
        {
            if (buf.startsWith(ByteOrderMarks[i]))
                return true;
        }

        // check available bytes for alpha, ascii, and whitespace ONLY
        for (int i = 0; i < buf.count(); ++i)
        {
            char ch = buf.at(i);
            if (!(isalpha(ch) || isascii(ch) || isblank(ch)))
                return false;
        }
        // if we got here, then the early bytes seems to indicate this file is
        // a text file
        return true;
    }

    return false;
}

QString File::fileSizeToString(quint64 filesize)
{
    int prec = 2;
    const quint64 a_kilobyte = 1024;
    const quint64 a_megabyte = a_kilobyte * 1024;
    const quint64 a_gigabyte = a_megabyte * 1024;
    const quint64 a_terabyte = a_gigabyte * 1024;

    if (filesize >= a_terabyte)
    {
        if (filesize % a_terabyte == 0)
            prec = 0;
        return QString("%1 TiB").arg(QString::number(qreal(filesize) / a_terabyte, 'f', prec));
    }
    else if (filesize >= a_gigabyte)
    {
        if (filesize % a_gigabyte == 0)
            prec = 0;
        return QString("%1 GiB").arg(QString::number(qreal(filesize) / a_gigabyte, 'f', prec));
    }
    else if (filesize >= a_megabyte)
    {
        if (filesize % a_megabyte == 0)
            prec = 0;
        return QString("%1 MiB").arg(QString::number(qreal(filesize) / a_megabyte, 'f', prec));
    }
    else if (filesize >= a_kilobyte)
    {
        if (filesize % a_kilobyte == 0)
            prec = 0;
        return QString("%1 KiB").arg(QString::number(qreal(filesize) / a_kilobyte, 'f', prec));
    }
    else
        return QString("%1 bytes").arg(QString::number(filesize));
}

bool File::moveFilesTo(const QStringList& files, const QString& destination)
{
#ifdef FILEOPS_API_LIBKONQ
        qDebug() << "CFileOperationHub::createFileCopyOperation:" <<
                    "Using KDE4 API to move" << files << " to " << destination;

        FileOps::KDE4::CKDE4FileMoveQueue* kde4result = new FileOps::KDE4::CKDE4FileMoveQueue(files, destination);

        // do not add this to m_operations, instead let it delete itself
        QObject::connect(kde4result, SIGNAL(finished()), kde4result, SLOT(deleteLater()));
        kde4result->start();

        return bool(kde4result);
#endif
#ifdef FILEOPS_API_WINDOWS
        qDebug() << "CFileOperationHub::createFileMoveOperation:" <<
                    "Using Windows API to move" << files << " to " << moveto;

        CWindowsFileMoveQueue* winresult = new CWindowsFileMoveQueue(files, moveto);

        // do not add this to m_operations, instead let it delete itself
        connect(winresult, SIGNAL(finished()), winresult, SLOT(deleteLater()));

        if (autostart)
            winresult->start();

        return bool(winresult);
#endif
#ifdef FILEOPS_API_NONE
    return FileOps::getGlobalFileOperationHub()->createFileMoveOperation(files, destination, true) != 0;
#endif

    return false;
}

bool File::pasteFilesFromClipboardTo(const QString& destination)
{
    const QMimeData* clipboardmimedata = QApplication::clipboard()->mimeData();
    if (!clipboardmimedata)
        return false;
    if (!clipboardmimedata->hasUrls())
        return false;

    QStringList files;
    files = Path::urlsToStringList(clipboardmimedata->urls());
    if (files.isEmpty())
        return false;

#ifdef FILEOPS_API_LIBKONQ
    files = Path::urlsToStringList(clipboardmimedata->urls());
    if (files.isEmpty())
        return false;

    bool iscut = KonqMimeData::decodeIsCutSelection(clipboardmimedata);
    if (iscut)
        return moveFilesTo(files, destination);
    else
        return copyFilesTo(files, destination);
#endif
#ifdef FILEOPS_API_WINDOWS
    bool ismove = false;
    if (clipboardmimedata->hasFormat(WINDOWS_PREFERRED_DROPEFFECT))
    {
        QByteArray dropeffect = clipboardmimedata->data(WINDOWS_PREFERRED_DROPEFFECT);
        if (dropeffect.size() != sizeof(unsigned long))
            return false;

        ismove = ((unsigned long) dropeffect[0] == WINDOWS_DROPEFFECT_MOVE);
    }

    if (ismove)
        return moveFilesTo(files, destination);
    else
        return copyFilesTo(files, destination);
#endif

    // if it got here then the system-specific bit was not executed, fall back
    // to generic copy-paste
    files = Path::urlsToStringList(clipboardmimedata->urls());
    qWarning() << "Lihat::Utils::File::pasteFilesFromClipboardTo:" <<
                  "Pasting" << files.count() << "files using generic copy assumption.";
    return copyFilesTo(files, destination);
}
