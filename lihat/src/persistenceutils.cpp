#include "persistenceutils.h"

#include <QSettings>


using namespace Lihat::Utils;

QVariant Persistence::get(const Persistence::EPersistenceType type, const QString& key, const QVariant& value)
{
    if (type == PT_CONFIG)
    {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, "lihat", "lihat.config");
        return settings.value(key, value);
    }
    else if (type == PT_STORAGE)
    {
        QSettings settings (QSettings::IniFormat, QSettings::UserScope, "lihat", "lihat.storage");
        return settings.value(key, value);
    }

    return QVariant();
}

QVariant Persistence::getConfig(const QString& key, const QVariant& value)
{
    return get(PT_CONFIG, key, value);
}

QVariant Persistence::getStorage(const QString& key, const QVariant& value)
{
    return get(PT_STORAGE, key, value);
}

bool Persistence::set(const Persistence::EPersistenceType type, const QString& key, const QVariant& value)
{
    if (type == PT_CONFIG)
    {
        QSettings settings(QSettings::IniFormat, QSettings::UserScope, "lihat", "lihat.config");
        settings.setValue(key, value);
        return true;
    }
    else if (type == PT_STORAGE)
    {
        QSettings settings (QSettings::IniFormat, QSettings::UserScope, "lihat", "lihat.storage");
        settings.setValue(key, value);
        return true;
    }

    return false;
}

QVariant Persistence::setConfig(const QString& key, const QVariant& value)
{
    return set(PT_CONFIG, key, value);
}

QVariant Persistence::setStorage(const QString& key, const QVariant& value)
{
    return set(PT_STORAGE, key, value);
}
