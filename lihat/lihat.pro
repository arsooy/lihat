#-------------------------------------------------
#
# Project created by QtCreator 2013-06-19T00:50:09
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = lihat
TEMPLATE = app


SOURCES += src/main.cpp \
    src/cmainwindow.cpp \
    src/cthumbnailviewmodel.cpp \
    src/cthumbnailview.cpp \
    src/cthumbnailviewdelegate.cpp \
    src/cthumbnailviewwidget.cpp \
    src/cimageloadthread.cpp \
    src/cthumbnailitemsprovider.cpp \
    src/cthumbnailitemsfilesystemprovider.cpp

HEADERS  += \
    src/cmainwindow.h \
    src/cthumbnailviewmodel.h \
    src/cthumbnailview.h \
    src/cthumbnailviewdelegate.h \
    src/cthumbnailviewwidget.h \
    src/cimageloadthread.h \
    src/thumbnailitem.h \
    src/cthumbnailitemsprovider.h \
    src/cthumbnailitemsfilesystemprovider.h

FORMS    += ui/mainwindow.ui
