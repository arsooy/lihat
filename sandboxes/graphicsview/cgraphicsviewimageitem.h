#ifndef CGRAPHICSVIEWIMAGEITEM_H
#define CGRAPHICSVIEWIMAGEITEM_H

#include <QGraphicsWidget>
#include <QPainterPath>
#include <QPointF>
#include <QRectF>
#include <QTime>
#include <QTimeLine>

class QAction;
class QGraphicsScene;
class QGraphicsSceneMouseEvent;
class QPixmap;
class QStyleOptionGraphicsItem;

class CGraphicsViewImageItem:
    public QGraphicsWidget
{
    Q_OBJECT
public:
    explicit CGraphicsViewImageItem(QGraphicsItem* parent = 0);
    virtual ~CGraphicsViewImageItem();

    // QGraphicsWidget
    virtual QRectF       boundingRect() const;
    virtual QPainterPath shape() const;

    QPointF paintSourceTopLeft() const;
    void    setPaintSourceTopLeft(QPointF ptopleft);
    void    updatePaintSourceTopLeft();

    QSizeF  pixmapSize() const;
    void    setPixmap(QPixmap *pixmap);
    void    setScale(qreal scale);
    void    updateTopLeft();
    void    zoomIn();
    void    zoomOut();
    void    zoomReset();

    bool IsFitToView;
    bool IsSmoothScaling;

signals:
    void updated();

protected:
    // QGraphicsWidget
    virtual void mouseMoveEvent(QGraphicsSceneMouseEvent* event);
    virtual void mousePressEvent(QGraphicsSceneMouseEvent* event);
    virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent* event);
    virtual void paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget);

private:
    struct SScrollCtx
    {
        QTime         LastTime;
        QKeySequence  LastKeySequence;
        QTimeLine     TimeLine;

        QPointF  Start;
        QPointF  Finish;
    } ScrollCtx;

    QPixmap* DrawPixmap;
    bool     IsDraggingView;
    QPointF  PaintSourceTopLeft;
    QPixmap* Pixmap;
    QPointF  TopLeft;

    QAction* getAction(const QString& name) const;
    void     scrollViewTo(const QPointF& newpos, bool scrolleffect);
    void     scrollViewToDirection(const QPointF& direction, bool scrolleffect);

private slots:
    void slotScrollTimeLineFinished();
    void slotScrollTimeLineFrameChanged(int frame);
    void slotScrollTriggered();
};

#endif  // CGRAPHICSVIEWIMAGEITEM_H
