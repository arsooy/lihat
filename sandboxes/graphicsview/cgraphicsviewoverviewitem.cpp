#include "cgraphicsviewoverviewitem.h"

#include "cgraphicsviewimageitem.h"

#include <QGraphicsScene>
#include <QPainter>
#include <QRectF>
#include <QtDebug>

static const unsigned char MAX_WIDTH  = 128;
static const unsigned char MAX_HEIGHT = 96;

CGraphicsViewOverviewItem::CGraphicsViewOverviewItem(QGraphicsItem *parent):
    QGraphicsWidget(parent),
    ImageItem(0)
{
    setFlag(ItemIgnoresParentOpacity, true);
}

CGraphicsViewOverviewItem::~CGraphicsViewOverviewItem()
{

}

QRectF CGraphicsViewOverviewItem::boundingRect() const
{
    if (ImageItem)
    {
        QSizeF isize = ImageItem->boundingRect().size() * ImageItem->scale();
        qreal mapscale = qBound< qreal >(0, qMin< qreal >(qreal(MAX_WIDTH)  / isize.width(),
                                                          qreal(MAX_HEIGHT) / isize.height()), 1.);

        QRectF maprect(QPointF(0, 0),
                       QSizeF(isize.width() * mapscale, isize.height() * mapscale));
        return maprect;
    }

    return QRectF(0, 0, MAX_WIDTH, MAX_HEIGHT);
}

void CGraphicsViewOverviewItem::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    painter->fillRect(boundingRect(), QColor(50, 50, 50, 100));

    if (ImageItem)
    {
        QSizeF isize = ImageItem->boundingRect().size() * ImageItem->scale();
        QPointF ptopleft = ImageItem->paintSourceTopLeft();
        QSizeF viewablesize = scene()->sceneRect().size();
        QSizeF scale = overviewScale();

        QSizeF validviewablesize(qMin< qreal >(viewablesize.width(), isize.width()),
                                 qMin< qreal >(viewablesize.height(), isize.height()));
        QRectF srect(QPointF(ptopleft.x() * scale.width(),
                             ptopleft.y() * scale.height()),
                     QSizeF(validviewablesize.width() * scale.width(),
                            validviewablesize.height() * scale.height()));

        painter->save();
        painter->setPen(QColor(255, 255, 255, 75));
        painter->setBrush(QBrush(QColor(90, 90, 90, 150)));
        painter->drawRect(srect);
        painter->restore();
    }
}

QSizeF CGraphicsViewOverviewItem::overviewScale() const
{
    if (ImageItem)
    {
        QSizeF isize = ImageItem->boundingRect().size() * ImageItem->scale();
        QSizeF bsize = boundingRect().size();
        return QSizeF(qBound< qreal >(0, bsize.width()  / isize.width(), 1),
                      qBound< qreal >(0, bsize.height() / isize.height(), 1.));
    }

    return QSizeF();
}

void CGraphicsViewOverviewItem::setImageItem(CGraphicsViewImageItem* image)
{
    ImageItem = image;
    connect(image, SIGNAL(updated()),
            this, SLOT(slotImageItemUpdated()));
}

void CGraphicsViewOverviewItem::slotImageItemUpdated()
{
    update();
}
