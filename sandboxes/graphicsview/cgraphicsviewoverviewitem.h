#ifndef CGRAPHICSVIEWOVERVIEWITEM_H
#define CGRAPHICSVIEWOVERVIEWITEM_H

#include <QGraphicsWidget>
#include <QSizeF>

class CGraphicsViewImageItem;

class CGraphicsViewOverviewItem:
    public QGraphicsWidget
{
    Q_OBJECT
public:
    explicit CGraphicsViewOverviewItem(QGraphicsItem *parent = 0);
    virtual ~CGraphicsViewOverviewItem();

    // QGraphicsItem
    virtual QRectF  boundingRect() const;
    virtual void    paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget);

    void setImageItem(CGraphicsViewImageItem* image);

protected:
    CGraphicsViewImageItem *ImageItem;

    QSizeF overviewScale() const;

private slots:
    void slotImageItemUpdated();
};

#endif // CGRAPHICSVIEWOVERVIEWITEM_H
