#include "cgraphicsviewmainwindow.h"

#include "cgraphicsviewimageitem.h"
#include "cgraphicsviewoverviewitem.h"

#include <QAction>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QMenu>
#include <QMenuBar>
#include <QPixmap>


CGraphicsViewMainWindow::CGraphicsViewMainWindow(QWidget *parent, Qt::WindowFlags flags):
    QMainWindow(parent, flags)
{
    GraphicsView = new QGraphicsView(this);
    setCentralWidget(GraphicsView);

    Scene = new QGraphicsScene(this);
    Scene->setBackgroundBrush(QBrush(QColor(75, 75, 75, 255)));

    GraphicsView->setScene(Scene);

    ImageViewItem = new CGraphicsViewImageItem();
    Scene->addItem(ImageViewItem);

    ImageViewMapItem = new CGraphicsViewOverviewItem();
    ImageViewMapItem->setImageItem(ImageViewItem);
    Scene->addItem(ImageViewMapItem);

    QPixmap *pix = new QPixmap(":/test.jpg");
    Q_ASSERT(!pix->isNull());

    ImageViewItem->setPixmap(pix);

    QAction *action = 0;
    QMenu *viewMenu = menuBar()->addMenu("&View");

    action = new QAction("Fit to Window", this);
    action->setShortcut(QKeySequence(Qt::Key_F));
    action->setCheckable(true);
    action->setChecked(ImageViewItem->IsFitToView);
    connect(action, SIGNAL(toggled(bool)), this, SLOT(slotFitToWindowToggled(bool)));
    viewMenu->addAction(action);

    action = new QAction("Smooth transformation", this);
    action->setShortcut(QKeySequence(Qt::Key_S));
    action->setCheckable(true);
    action->setChecked(ImageViewItem->IsSmoothScaling);
    connect(action, SIGNAL(toggled(bool)), this, SLOT(slotSmoothTransformation(bool)));
    viewMenu->addAction(action);

    viewMenu->addSeparator();

    action = new QAction("Zoom In", this);
    action->setShortcut(QKeySequence(Qt::Key_Plus));
    connect(action, SIGNAL(triggered(bool)), this, SLOT(slotZoomIn(bool)));
    viewMenu->addAction(action);

    action = new QAction("Zoom Out", this);
    action->setShortcut(QKeySequence(Qt::Key_Minus));
    connect(action, SIGNAL(triggered(bool)), this, SLOT(slotZoomOut(bool)));
    viewMenu->addAction(action);

    action = new QAction("Zoom Reset", this);
    action->setShortcut(QKeySequence(Qt::Key_Equal));
    connect(action, SIGNAL(triggered(bool)), this, SLOT(slotZoomReset(bool)));
    viewMenu->addAction(action);
}

CGraphicsViewMainWindow::~CGraphicsViewMainWindow()
{

}

void CGraphicsViewMainWindow::resizeEvent(QResizeEvent *event)
{
    QWidget::resizeEvent(event);

    if (ImageViewItem->IsFitToView)
        updateScale();

    sizeUpdated();
}

void CGraphicsViewMainWindow::showEvent(QShowEvent* event)
{
    QWidget::showEvent(event);

    sizeUpdated();
}

void CGraphicsViewMainWindow::sizeUpdated()
{
    QRect vrect = GraphicsView->viewport()->rect();
    QRect viewrect(0, 0, vrect.width(), vrect.height());

    Scene->setSceneRect(viewrect);

    // update map position
    QRectF maprect = ImageViewMapItem->sceneBoundingRect();
    maprect.moveBottomRight(viewrect.bottomRight() - QPoint(20, 20));
    ImageViewMapItem->setPos(maprect.topLeft());

    ImageViewItem->updateTopLeft();
}

void CGraphicsViewMainWindow::slotFitToWindowToggled(bool checked)
{
    ImageViewItem->IsFitToView = checked;
    GraphicsView->resetTransform();
    if (!checked)
        ImageViewItem->zoomReset();
    else
        updateScale();

    sizeUpdated();
    ImageViewItem->update();
}

void CGraphicsViewMainWindow::slotSmoothTransformation(bool checked)
{
    ImageViewItem->IsSmoothScaling = checked;
    GraphicsView->setRenderHint(QPainter::SmoothPixmapTransform, checked);

    ImageViewItem->update();
}

void CGraphicsViewMainWindow::slotZoomIn(bool checked)
{
    Q_UNUSED(checked);

    ImageViewItem->zoomIn();
    sizeUpdated();
}

void CGraphicsViewMainWindow::slotZoomOut(bool checked)
{
    Q_UNUSED(checked);

    ImageViewItem->zoomOut();
    sizeUpdated();
}

void CGraphicsViewMainWindow::slotZoomReset(bool checked)
{
    Q_UNUSED(checked);

    ImageViewItem->zoomReset();
    sizeUpdated();
}

void CGraphicsViewMainWindow::updateScale()
{
    QSizeF isize = ImageViewItem->boundingRect().size();
    QSize vsize = GraphicsView->viewport()->size();
    qreal scale = qBound< qreal >(0, qMin< qreal >(qreal(vsize.width()) / isize.width(),
                                                   qreal(vsize.height()) / isize.height()), 1.);
    ImageViewItem->setScale(scale);
}
