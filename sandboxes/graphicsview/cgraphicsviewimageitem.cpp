#include "cgraphicsviewimageitem.h"

#include <QAction>
#include <QGraphicsScene>
#include <QGraphicsSceneMouseEvent>
#include <QGraphicsView>
#include <QPainter>
#include <QPixmap>
#include <QStyleOptionGraphicsItem>
#include <QtDebug>

CGraphicsViewImageItem::CGraphicsViewImageItem(QGraphicsItem* parent):
    QGraphicsWidget(parent),
    IsFitToView(false),
    IsSmoothScaling(true),
    DrawPixmap(0),
    IsDraggingView(false),
    Pixmap(0),
    TopLeft(0, 0)
{
    ScrollCtx.TimeLine.setDuration(250);
    ScrollCtx.TimeLine.setFrameRange(0, 100);
    connect(&ScrollCtx.TimeLine, SIGNAL(frameChanged(int)),
            this, SLOT(slotScrollTimeLineFrameChanged(int)));
    connect(&ScrollCtx.TimeLine, SIGNAL(finished()),
            this, SLOT(slotScrollTimeLineFinished()));
    setCursor(Qt::OpenHandCursor);

    QAction* act;
    QList< QKeySequence > shortcuts;

    act = new QAction(tr("Scroll up"), this);
    act->setObjectName("acImageScrollUp");
    shortcuts.clear();
    shortcuts << QKeySequence(Qt::Key_Up);
    shortcuts << QKeySequence(Qt::Key_W);
    act->setShortcuts(shortcuts);
    connect(act, SIGNAL(triggered(bool)),
            this, SLOT(slotScrollTriggered()));
    addAction(act);

    act = new QAction(tr("Scroll right"), this);
    act->setObjectName("acImageScrollRight");
    shortcuts.clear();
    shortcuts << QKeySequence(Qt::Key_Right);
    shortcuts << QKeySequence(Qt::Key_D);
    act->setShortcuts(shortcuts);
    connect(act, SIGNAL(triggered(bool)),
            this, SLOT(slotScrollTriggered()));
    addAction(act);

    act = new QAction(tr("Scroll down"), this);
    act->setObjectName("acImageScrollDown");
    shortcuts.clear();
    shortcuts << QKeySequence(Qt::Key_Down);
    shortcuts << QKeySequence(Qt::Key_S);
    act->setShortcuts(shortcuts);
    connect(act, SIGNAL(triggered(bool)),
            this, SLOT(slotScrollTriggered()));
    addAction(act);

    act = new QAction(tr("Scroll left"), this);
    act->setObjectName("acImageScrollLeft");
    shortcuts.clear();
    shortcuts << QKeySequence(Qt::Key_Left);
    shortcuts << QKeySequence(Qt::Key_A);
    act->setShortcuts(shortcuts);
    connect(act, SIGNAL(triggered(bool)),
            this, SLOT(slotScrollTriggered()));
    addAction(act);

    act = new QAction(tr("Scroll page up"), this);
    act->setObjectName("acImageScrollPageUp");
    act->setShortcut(QKeySequence(Qt::Key_PageUp));
    connect(act, SIGNAL(triggered(bool)),
            this, SLOT(slotScrollTriggered()));
    addAction(act);

    act = new QAction(tr("Scroll page down"), this);
    act->setObjectName("acImageScrollPageDown");
    act->setShortcut(QKeySequence(Qt::Key_PageDown));
    connect(act, SIGNAL(triggered(bool)),
            this, SLOT(slotScrollTriggered()));
    addAction(act);

    act = new QAction(tr("Scroll page right"), this);
    act->setObjectName("acImageScrollPageRight");
    act->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_PageDown));
    connect(act, SIGNAL(triggered(bool)),
            this, SLOT(slotScrollTriggered()));
    addAction(act);

    act = new QAction(tr("Scroll page left"), this);
    act->setObjectName("acImageScrollPageLeft");
    act->setShortcut(QKeySequence(Qt::SHIFT + Qt::Key_PageUp));
    connect(act, SIGNAL(triggered(bool)),
            this, SLOT(slotScrollTriggered()));
    addAction(act);
}

CGraphicsViewImageItem::~CGraphicsViewImageItem()
{

}

QRectF CGraphicsViewImageItem::boundingRect() const
{
    if (!Pixmap)
        return QRectF();

    return QRectF(TopLeft, QSizeF(Pixmap->width(), Pixmap->height()));
}

void CGraphicsViewImageItem::mouseMoveEvent(QGraphicsSceneMouseEvent* event)
{
    QGraphicsWidget::mouseMoveEvent(event);

    if (IsDraggingView)
    {
        QPointF posdiff(event->lastPos() - event->pos());
        setPaintSourceTopLeft(paintSourceTopLeft() + posdiff);

        event->accept();
    }
}

void CGraphicsViewImageItem::mousePressEvent(QGraphicsSceneMouseEvent* event)
{
    QGraphicsWidget::mousePressEvent(event);

    if (event->button() == Qt::LeftButton)
    {
        IsDraggingView = event->button() == Qt::LeftButton;
        setCursor(Qt::ClosedHandCursor);

        event->accept();
    }
}

void CGraphicsViewImageItem::mouseReleaseEvent(QGraphicsSceneMouseEvent* event)
{
    QGraphicsWidget::mouseReleaseEvent(event);

    if (event->button() == Qt::LeftButton)
    {
        IsDraggingView = !(IsDraggingView && (event->button() == Qt::LeftButton));
        if (cursor().shape() == Qt::ClosedHandCursor)
            setCursor(Qt::OpenHandCursor);

        event->accept();
    }
}

QPointF CGraphicsViewImageItem::paintSourceTopLeft() const
{
    return PaintSourceTopLeft;
}

QSizeF CGraphicsViewImageItem::pixmapSize() const
{
    if (Pixmap)
        return Pixmap->size();

    return QSizeF();
}

void CGraphicsViewImageItem::paint(QPainter* painter, const QStyleOptionGraphicsItem* option, QWidget* widget)
{
    Q_UNUSED(option);
    Q_UNUSED(widget);

    if (!Pixmap)
        return;

    QRectF imgrect = boundingRect();
    imgrect.moveTopLeft(PaintSourceTopLeft);

    painter->drawPixmap(boundingRect(), *Pixmap, imgrect);
}

void CGraphicsViewImageItem::updatePaintSourceTopLeft()
{
    setPaintSourceTopLeft(paintSourceTopLeft());
}

void CGraphicsViewImageItem::setScale(qreal scale)
{
    QGraphicsWidget::setScale(scale);
    updatePaintSourceTopLeft();
}

void CGraphicsViewImageItem::setPixmap(QPixmap* pixmap)
{
    prepareGeometryChange();
    Pixmap = pixmap;
}

void CGraphicsViewImageItem::setPaintSourceTopLeft(QPointF ptopleft)
{
    QSizeF isize;
    if (Pixmap)
        isize = Pixmap->size() * scale();

    QSizeF ssize;
    if (scene())
        ssize = scene()->sceneRect().size();

    if (!(ssize.isValid() && isize.isValid()))
        return;

    // get paint topleft by taking consideration of viewable dimension (ie.
    // current scene size) boundaries:
    //  - no less than (0, 0) which is the most top-left, and ..
    //  - no more than current dimension - its viewable dimension
    // its harder to put it into words, but (I hope) the next code should be
    // self-explanatory
    QPointF newptopleft(qBound< qreal >(0, ptopleft.x(), isize.width() - ssize.width()),
                        qBound< qreal >(0, ptopleft.y(), isize.height() - ssize.height()));
    if (newptopleft != PaintSourceTopLeft)
    {
        PaintSourceTopLeft = newptopleft;

        update();
        emit updated();
    }
}

QPainterPath CGraphicsViewImageItem::shape() const
{
    QPainterPath ppath;
    ppath.addRect(boundingRect());

    return ppath;
}

void CGraphicsViewImageItem::updateTopLeft()
{
    if (!scene() || !Pixmap)
        return;

    QSizeF scenesize = scene()->sceneRect().size();
    QSizeF viewablesize = Pixmap->size() * scale();

    QPointF scenetopleft(qMax< qreal >(0, (scenesize.width() -  viewablesize.width()) / 2),
                         qMax< qreal >(0, (scenesize.height() - viewablesize.height()) / 2));

    TopLeft = mapFromScene(scenetopleft);
    updatePaintSourceTopLeft();
}

void CGraphicsViewImageItem::zoomIn()
{
    setScale(scale() + 0.25);
    updatePaintSourceTopLeft();
}

void CGraphicsViewImageItem::zoomOut()
{
    setScale(scale() - 0.25);
    updatePaintSourceTopLeft();
}

void CGraphicsViewImageItem::zoomReset()
{
    setScale(1.);
    updatePaintSourceTopLeft();
}

QAction * CGraphicsViewImageItem::getAction(const QString& name) const
{
    return findChild< QAction* >(name);
}

void CGraphicsViewImageItem::slotScrollTimeLineFinished()
{

}

void CGraphicsViewImageItem::slotScrollTimeLineFrameChanged(int /* frame */)
{
    QPointF distance = (ScrollCtx.Finish - ScrollCtx.Start) * ScrollCtx.TimeLine.currentValue();
    QPointF step = (ScrollCtx.Start + distance) - PaintSourceTopLeft;

    setPaintSourceTopLeft(PaintSourceTopLeft + step);
}

void CGraphicsViewImageItem::slotScrollTriggered()
{
    QAction* action = qobject_cast< QAction* >(sender());

    QPointF direction(0, 0);
    bool isright = (action == getAction("acImageScrollRight")) || (action == getAction("acImageScrollPageRight"));
    bool isdown  = (action == getAction("acImageScrollDown"))  || (action == getAction("acImageScrollPageDown"));
    bool isleft  = (action == getAction("acImageScrollLeft"))  || (action == getAction("acImageScrollPageLeft"));
    bool isup    = (action == getAction("acImageScrollUp"))    || (action == getAction("acImageScrollPageUp"));
    bool ispagescroll = (action == getAction("acImageScrollPageRight")) ||
                        (action == getAction("acImageScrollPageDown"))  ||
                        (action == getAction("acImageScrollPageLeft"))  ||
                        (action == getAction("acImageScrollPageUp"));

    qreal step = 0.25;
    if (ispagescroll)
        step = 1.0;

    if (isright || isleft)
        direction.setX(isright ? step : -step);
    else if (isdown || isup)
        direction.setY(isdown ? step : -step);
    else
        return;

    bool matcheslast = false;
    bool rapidkeystroke = (ScrollCtx.LastTime.elapsed() <= 250);
    if (rapidkeystroke)
        matcheslast = action->shortcut().matches(ScrollCtx.LastKeySequence);

    // do not do 'smooth' scrolling if user press the key in quick succession
    bool scrolleffect = !(rapidkeystroke && matcheslast);

    scrollViewToDirection(direction, scrolleffect);
    ScrollCtx.LastKeySequence = action->shortcut();
}

void CGraphicsViewImageItem::scrollViewTo(const QPointF& newpos, bool scrolleffect)
{
    if (!scrolleffect && (ScrollCtx.TimeLine.state() == QTimeLine::Running))
        ScrollCtx.TimeLine.stop();

    // set new view position
    if (scrolleffect)
    {
        ScrollCtx.TimeLine.stop();

        ScrollCtx.Start = PaintSourceTopLeft;
        ScrollCtx.Finish = newpos;
        ScrollCtx.TimeLine.start();
    }
    else
        setPaintSourceTopLeft(newpos);

    // update LastTime to detect quick scroll request
    ScrollCtx.LastTime.start();
}

void CGraphicsViewImageItem::scrollViewToDirection(const QPointF& direction, bool scrolleffect)
{
    QSizeF isize = boundingRect().size() * scale();
    QSizeF dsize = scene()->sceneRect().size();

    qreal viewingratio = 1.;
    qreal step = 0;

    if (direction.y() == 0)
    {
        // scrolling horizontally
        viewingratio = qMin(isize.width() / dsize.width(), 1.);
        step = (dsize.width()  * viewingratio) * direction.x();
    }
    else if (direction.x() == 0)
    {
        // scrolling vertically
        viewingratio = qMin(isize.height() / dsize.height(), 1.);
        step = (dsize.height()  * viewingratio) * direction.y();
    }
    else
    {
        // scrolling horizontally & vertically
        viewingratio = qMax< qreal >( qMin(isize.width()  / dsize.width(),  1.),
                                      qMin(isize.height() / dsize.height(), 1.) );
        step = qMax< qreal >((dsize.width()  * viewingratio) * direction.x(),
                             (dsize.height() * viewingratio) * direction.y());
    }

    QPointF currenttopleft = PaintSourceTopLeft;
    QPointF delta(0, 0);
    if (direction.x() != 0)
        delta.setX(step);
    if (direction.y() != 0)
        delta.setY(step);
    scrollViewTo(currenttopleft + delta, scrolleffect);
}
