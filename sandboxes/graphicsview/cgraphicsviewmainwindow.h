#ifndef CGRAPHICSVIEWMAINWINDOW_H
#define CGRAPHICSVIEWMAINWINDOW_H

#include <QMainWindow>

class CGraphicsViewOverviewItem;
class CGraphicsViewImageItem;
class QGraphicsScene;
class QGraphicsView;

class CGraphicsViewMainWindow:
    public QMainWindow
{
    Q_OBJECT
public:
    explicit CGraphicsViewMainWindow(QWidget *parent = 0, Qt::WindowFlags flags = 0);
    virtual ~CGraphicsViewMainWindow();

    // QMainWindow
    virtual void resizeEvent(QResizeEvent *event);
    virtual void showEvent(QShowEvent *event);

private:
    QGraphicsScene             *Scene;
    QGraphicsView              *GraphicsView;
    CGraphicsViewImageItem     *ImageViewItem;
    CGraphicsViewOverviewItem  *ImageViewMapItem;

    void  updateScale();
    void  sizeUpdated();

private slots:
    void  slotFitToWindowToggled(bool checked);
    void  slotSmoothTransformation(bool checked);
    void  slotZoomIn(bool checked);
    void  slotZoomOut(bool checked);
    void  slotZoomReset(bool checked);
};

#endif // CGRAPHICSVIEWMAINWINDOW_H
