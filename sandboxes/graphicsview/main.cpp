#include "cgraphicsviewmainwindow.h"

#include <QApplication>

int main(int argc, char** argv)
{
    QApplication app(argc, argv);
    CGraphicsViewMainWindow mwindow;
    mwindow.show();

    app.exec();
}
