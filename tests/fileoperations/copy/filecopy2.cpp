#include "fileoperations/copy/cfilecopyqueue.h"
#include "../shared.h"

#include "gtest/gtest.h"

#include <QApplication>
#include <QString>
#include <QStringList>

using namespace Lihat;
using namespace Lihat::FileOps;


int argc = 1;
char** argv = 0;
static QApplication app(argc, argv);

CFileCopyQueue* createTheadedFileCopy(const QStringList& files, const QString& destination)
{
    CFileCopyQueue* result = new CFileCopyQueue(files, destination, &app);
    return result;
}


TEST(filecopierthread_filecopy, copy1)
{
    QString destdir("1testdest");
    QStringList sourcefiles;
    if (touch("1foofile"))
        sourcefiles << "1foofile";

    CFileCopyQueue* copyqueue = createTheadedFileCopy(sourcefiles, destdir);
    copyqueue->start();

    // wait until it's done
    while (copyqueue->status() == CFileCopyQueue::QS_WORKING)
        app.processEvents();
    EXPECT_TRUE(copyqueue->status() == CFileCopyQueue::QS_FINISHED);
    EXPECT_TRUE(copyqueue->totalCopied() == 1);
    delete copyqueue;

    EXPECT_TRUE(purge(destdir));
    EXPECT_TRUE(purge(sourcefiles));
}

TEST(filecopierthread_filecopy, copy2)
{
    QString destdir("2testdest");
    QStringList sourcefiles;
    if (touch("2foofile"))
        sourcefiles << "2foofile";
    if (touch("2goofile"))
        sourcefiles << "2goofile";
    if (touch("2barfile"))
        sourcefiles << "2barfile";
    if (touch("2bazfile"))
        sourcefiles << "2bazfile";

    CFileCopyQueue* copyqueue = createTheadedFileCopy(sourcefiles, destdir);
    copyqueue->start();

    // wait until it's done
    while (copyqueue->status() == CFileCopyQueue::QS_WORKING)
        app.processEvents();
    EXPECT_TRUE(copyqueue->status() == CFileCopyQueue::QS_FINISHED);
    EXPECT_TRUE(copyqueue->totalCopied() == 4);
    delete copyqueue;

    EXPECT_TRUE(purge(destdir));
    EXPECT_TRUE(purge(sourcefiles));
}

TEST(filecopierthread_filecopy, copy3)
{
    QString destdir("3testdest");
    QStringList sourcefiles;
    QDir dir;

    /* Create a tree:
     * - 3foo
     *  |- abc
     *  |- def
     *  |- ghi
     * - 3bar
     *  |- rincianbiaya
     *  |- tembusan
     *  |- akomodasi
     *  | |- hotel
     *  | |- losmen
     *  |- peserta
     *  | |- grup_a
     *  | |- grup_b
     *  | |- grup_m
     * - 3baz
     *  |- spring.jpg
     *  |- summer.jpg
     *  |- fall.jpg
     *  |- winter.jpg
     * - 3fubar
     *
     * Items count: 20 (directories and files)
     */
    EXPECT_TRUE( dir.mkdir("3foo") && touch("3foo/abc")
                                   && touch("3foo/def")
                                   && touch("3foo/ghi") );
    sourcefiles << "3foo";

    EXPECT_TRUE( dir.mkdir("3bar") && touch("3bar/rincianbiaya")
                                   && touch("3bar/tembusan") );

    EXPECT_TRUE( dir.mkpath("3bar/akomodasi") && touch("3bar/akomodasi/hotel")
                                              && touch("3bar/akomodasi/losmen") );
    EXPECT_TRUE( dir.mkpath("3bar/peserta") && touch("3bar/peserta/grup_a")
                                            && touch("3bar/peserta/grup_b")
                                            && touch("3bar/peserta/grup_m") );
    sourcefiles << "3bar";

    EXPECT_TRUE( dir.mkdir("3baz") && touch("3baz/spring.jpg")
                                   && touch("3baz/summer.jpg")
                                   && touch("3baz/fall.jpg")
                                   && touch("3baz/winter.jpg") );
    sourcefiles << "3baz";

    EXPECT_TRUE( touch("3fubar") );
    sourcefiles << "3fubar";

    CFileCopyQueue* copyqueue = createTheadedFileCopy(sourcefiles, destdir);
    copyqueue->start();

    // wait until it's done
    while (copyqueue->status() == CFileCopyQueue::QS_WORKING)
        app.processEvents();
    EXPECT_TRUE(copyqueue->status() == CFileCopyQueue::QS_FINISHED);
    EXPECT_TRUE(copyqueue->totalCopied() == 20);
    delete copyqueue;

    EXPECT_TRUE(purge(destdir));
    EXPECT_TRUE(purge(sourcefiles));
}
