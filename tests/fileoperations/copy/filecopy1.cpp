#include "fileoperations/copy/cdirectorycopier.h"
#include "fileoperations/copy/cfilecopier.h"
#include "../shared.h"

#include "gtest/gtest.h"

#include <QString>
#include <QStringList>

using namespace Lihat;
using namespace Lihat::FileOps;

TEST(fileoperation_copy, filecopier1)
{
    QString destdir("1testdest");
    QStringList sourcefiles;
    if (touch("1foofile"))
        sourcefiles << "1foofile";

    int totalcopied = 0;
    for (int i = 0; i < sourcefiles.count(); ++i)
    {
        CFileCopier* copier = new CFileCopier(sourcefiles.at(i), destdir);
        copier->start();
        totalcopied += copier->transferredCount();
        copier->deleteLater();
    }

    EXPECT_TRUE(totalcopied == 1);
    EXPECT_TRUE(purge(destdir));
    EXPECT_TRUE(purge(sourcefiles));
}

TEST(fileoperation_copy, filecopier2)
{
    QString destdir("2testdest");
    QStringList sourcefiles;
    if (touch("2foofile"))
        sourcefiles << "2foofile";
    if (touch("2goofile"))
        sourcefiles << "2goofile";
    if (touch("2barfile"))
        sourcefiles << "2barfile";
    if (touch("2bazfile"))
        sourcefiles << "2bazfile";

    int totalcopied = 0;
    for (int i = 0; i < sourcefiles.count(); ++i)
    {
        CFileCopier* copier = new CFileCopier(sourcefiles.at(i), destdir);
        copier->start();
        totalcopied += copier->transferredCount();
        copier->deleteLater();
    }

    EXPECT_TRUE(totalcopied == 4);
    EXPECT_TRUE(purge(destdir));
    EXPECT_TRUE(purge(sourcefiles));
}

TEST(fileoperation_directorycopy, directorycopier1)
{
    QString destdir("3testdest");
    QStringList sourcefiles;
    QDir dir;

    EXPECT_TRUE( dir.mkdir("3foo") );
    sourcefiles << "3foo";

    int totalcopied = 0;
    for (int i = 0; i < sourcefiles.count(); ++i)
    {
        CDirectoryCopier* copier = new CDirectoryCopier(sourcefiles.at(i), destdir);
        copier->start();
        totalcopied += copier->transferredCount();
        copier->deleteLater();
    }

    EXPECT_TRUE(totalcopied == 1);
    EXPECT_TRUE(purge(destdir));
    EXPECT_TRUE(purge(sourcefiles));
}

TEST(fileoperation_directorycopy, directorycopier2)
{
    QString destdir("4testdest");
    QStringList sourcefiles;
    QDir dir;

    EXPECT_TRUE( dir.mkdir("4foo") && touch("4foo/abc")
                                   && touch("4foo/def")
                                   && touch("4foo/ghi") );
    sourcefiles << "4foo";

    int totalcopied = 0;
    for (int i = 0; i < sourcefiles.count(); ++i)
    {
        CDirectoryCopier* copier = new CDirectoryCopier(sourcefiles.at(i), destdir);
        copier->start();
        totalcopied += copier->transferredCount();
        copier->deleteLater();
    }

    EXPECT_TRUE(totalcopied == 4);
    EXPECT_TRUE(purge(destdir));
    EXPECT_TRUE(purge(sourcefiles));
}
