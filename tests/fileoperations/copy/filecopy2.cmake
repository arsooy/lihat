INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR})

SET(filecopy2_HEADERS
    ../../../lihat/src/pathutils.h
    ../../../lihat/src/fileoperations/cfileoperationitem.h
    ../../../lihat/src/fileoperations/copy/cbasecopier.h
    ../../../lihat/src/fileoperations/copy/cbasefilecopier.h
    ../../../lihat/src/fileoperations/copy/cdirectorycopier.h
    ../../../lihat/src/fileoperations/copy/cfilecopier.h
    ../../../lihat/src/fileoperations/copy/cfilecopyqueue.h)
SET(filecopy2_SOURCES
    filecopy2.cpp
    ../../../lihat/src/pathutils.cpp
    ../../../lihat/src/fileoperations/cfileoperationitem.cpp
    ../../../lihat/src/fileoperations/copy/cbasefilecopier.cpp
    ../../../lihat/src/fileoperations/copy/cdirectorycopier.cpp
    ../../../lihat/src/fileoperations/copy/cfilecopier.cpp
    ../../../lihat/src/fileoperations/copy/cfilecopyqueue.cpp)
    
IF(LIHAT_QT EQUAL 4)
    FIND_PACKAGE(Qt4  REQUIRED
                 QtCore QtGui)
    INCLUDE(${QT_USE_FILE})
    
    SET(filecopy2_QT_INCLUDES
        ${QT_QTCORE_INCLUDE_DIR}
        ${QT_QTGUI_INCLUDE_DIR})
    SET(filecopy2_QT_LIBRARIES
        ${QT_QTCORE_LIBRARY}
        ${QT_QTGUI_LIBRARY})

    QT4_WRAP_CPP(filecopy2_QTGENERATED_MOC ${filecopy2_HEADERS})
ELSEIF(LIHAT_QT EQUAL 5)
    FIND_PACKAGE(Qt5Core    REQUIRED)
    FIND_PACKAGE(Qt5Widgets REQUIRED)
    
    SET(filecopy2_QT_INCLUDES
        ${Qt5Core_INCLUDE_DIRS}
        ${Qt5Widgets_INCLUDE_DIRS})
    SET(filecopy2_QT_LIBRARIES
        ${Qt5Core_LIBRARIES}
        ${Qt5Widgets_LIBRARIES})

    QT5_WRAP_CPP(filecopy2_QTGENERATED_MOC ${filecopy2_HEADERS})
ENDIF()

INCLUDE_DIRECTORIES(${filecopy2_QT_INCLUDES})
ADD_EXECUTABLE(filecopy2
               ${filecopy2_HEADERS}
               ${filecopy2_SOURCES}
               ${filecopy2_QTGENERATED_MOC})
TARGET_LINK_LIBRARIES(filecopy2
                      ${filecopy2_QT_LIBRARIES})

IF(LIHAT_QT EQUAL 5)
    QT5_USE_MODULES(filecopy2
                    Core Widgets)
ENDIF()

ADD_DEPENDENCIES(filecopy2 googletest)
TARGET_LINK_LIBRARIES(filecopy2 pthread gtest gtest_main)

ADD_TEST(test_filecopierthread filecopy2)
