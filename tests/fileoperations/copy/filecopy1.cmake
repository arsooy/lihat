INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR})

SET(filecopy1_HEADERS
    ../../../lihat/src/pathutils.h
    ../../../lihat/src/fileoperations/copy/cbasecopier.h
    ../../../lihat/src/fileoperations/copy/cbasefilecopier.h
    ../../../lihat/src/fileoperations/copy/cdirectorycopier.h
    ../../../lihat/src/fileoperations/copy/cfilecopier.h)
SET(filecopy1_SOURCES
    filecopy1.cpp
    ../../../lihat/src/pathutils.cpp
    ../../../lihat/src/fileoperations/copy/cbasefilecopier.cpp
    ../../../lihat/src/fileoperations/copy/cdirectorycopier.cpp
    ../../../lihat/src/fileoperations/copy/cfilecopier.cpp)

IF(LIHAT_QT EQUAL 4)
    FIND_PACKAGE(Qt4 REQUIRED
                 QtCore)
    INCLUDE(${QT_USE_FILE})
    SET(filecopy1_QT_INCLUDES
        ${QT_QTCORE_INCLUDE_DIR})
    SET(filecopy1_QT_LIBRARIES
        ${QT_QTCORE_LIBRARY})

    QT4_WRAP_CPP(filecopy1_QTGENERATED_MOC ${filecopy1_HEADERS})
ELSEIF(LIHAT_QT EQUAL 5)
    FIND_PACKAGE(Qt5Core REQUIRED)
    SET(filecopy1_QT_INCLUDES
        ${Qt5Core_INCLUDE_DIRS})
    SET(filecopy1_QT_LIBRARIES
        ${QtCore_LIBRARIES})

    QT5_WRAP_CPP(filecopy1_QTGENERATED_MOC ${filecopy1_HEADERS})
ENDIF()

INCLUDE_DIRECTORIES(${filecopy1_QT_INCLUDES})
ADD_EXECUTABLE(filecopy1
               ${filecopy1_HEADERS}
               ${filecopy1_SOURCES}
               ${filecopy1_QTGENERATED_MOC})
TARGET_LINK_LIBRARIES(filecopy1
                      ${filecopy1_QT_LIBRARIES})

IF(LIHAT_QT EQUAL 5)
    QT5_USE_MODULES(filecopy1
                    Core)
ENDIF()

ADD_DEPENDENCIES(filecopy1 googletest)
TARGET_LINK_LIBRARIES(filecopy1 pthread gtest gtest_main)

ADD_TEST(test_filecopierthread filecopy1)
