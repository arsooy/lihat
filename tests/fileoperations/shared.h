#ifndef FILECOPY_SHARED_H
#define FILECOPY_SHARED_H

#include <QFile>
#include <QFileInfo>
#include <QDir>
#include <QDirIterator>
#include <QString>
#include <QStringList>

bool exists(const QStringList& paths);
bool exists(const QString& path);
bool purge(const QStringList& fsitems);
bool purge(const QString& fsitem);
bool touch(const QString& fname);


bool exists(const QStringList& paths)
{
    for (int i = 0; i < paths.count(); ++i)
    {
        if (!exists(paths.at(i)))
            return false;
    }

    return true;
}

bool exists(const QString& path)
{
    QFile foofile(path);
    return foofile.exists();
}

bool touch(const QString& fname)
{
    bool result = false;

    QFile foofile(fname);
    result = foofile.open(QIODevice::WriteOnly);
    foofile.close();

    return result;
}

bool purge(const QStringList& fsitems)
{
    for (int i = 0; i < fsitems.count(); ++i)
    {
        QFileInfo finfo(fsitems.at(i));
        if (finfo.isFile())
        {
            if (!QFile::remove(finfo.absoluteFilePath()))
                return false;
        }
        else if (finfo.isDir())
        {
            QDirIterator filesiter(finfo.absoluteFilePath(), QDir::AllEntries | QDir::NoDotAndDotDot);
            while (filesiter.hasNext())
            {
                filesiter.next();

                bool rmret = false;
                if (filesiter.fileInfo().isDir())
                    rmret = purge(filesiter.filePath());
                else
                    rmret = QFile::remove(filesiter.filePath());


                if (!rmret)
                    return false;
            }

            QDir dir(finfo.absoluteFilePath());
            if (!dir.rmdir(finfo.absoluteFilePath()))
                return false;
        }
    }

    return true;
}

bool purge(const QString& fsitem)
{
    QStringList items;
    items << fsitem;

    return purge(items);
}

#endif  // define FILECOPY_SHARED_H
