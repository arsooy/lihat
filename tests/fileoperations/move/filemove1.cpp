#include "fileoperations/move/cdirectorymover.h"
#include "fileoperations/move/cfilemover.h"
#include "../shared.h"

#include "gtest/gtest.h"

#include <QString>
#include <QStringList>


using namespace Lihat;
using namespace Lihat::FileOps;


bool allDoesNotExists(const QStringList& paths)
{
    for (int i = 0; i < paths.count(); ++i)
    {
        if (exists(paths.at(i)))
            return false;
    }

    return true;
}

TEST(fileoperation_move, 1_filemover1)
{
    QString destfile("1testdest");
    QStringList sourcefiles;
    if (touch("1foofile"))
        sourcefiles << "1foofile";

    int totalmoved = 0;
    CFileMover* mover = new CFileMover(sourcefiles.at(0), destfile);
    mover->start();
    totalmoved += mover->transferredCount();
    mover->deleteLater();

    EXPECT_TRUE(totalmoved == 1);
    EXPECT_TRUE(purge(destfile));
    EXPECT_TRUE(allDoesNotExists(sourcefiles));
}

TEST(fileoperation_move, 2_filemover2)
{
    QString destdir("2testdest");
    QStringList sourcefiles;
    QDir dir;

    if (touch("2foofile"))
        sourcefiles << "2foofile";
    if (touch("2goofile"))
        sourcefiles << "2goofile";
    if (touch("2barfile"))
        sourcefiles << "2barfile";
    if (touch("2bazfile"))
        sourcefiles << "2bazfile";


    int totalmoved = 0;
    for (int i = 0; i < sourcefiles.count(); ++i)
    {
        CFileMover* mover = new CFileMover(sourcefiles.at(i), destdir);
        mover->start();
        totalmoved += mover->transferredCount();
        mover->deleteLater();
    }

    EXPECT_TRUE(totalmoved == 4);
    EXPECT_TRUE(purge(destdir));
    EXPECT_TRUE(allDoesNotExists(sourcefiles));
}

TEST(fileoperation_move, 3_directorymover1)
{
    QString destdir("3testdest");
    QStringList sourcefiles;
    QDir dir;

    EXPECT_TRUE(dir.mkdir("3foo"));
    sourcefiles << "3foo";

    int totalmoved = 0;
    for (int i = 0; i < sourcefiles.count(); ++i)
    {
        CDirectoryMover* mover = new CDirectoryMover(sourcefiles.at(i), destdir);
        mover->start();
        totalmoved += mover->transferredCount();
        mover->deleteLater();
    }

    EXPECT_TRUE(totalmoved == 1);
    EXPECT_TRUE(purge(destdir));
    EXPECT_TRUE(allDoesNotExists(sourcefiles));
}

TEST(fileoperation_move, 4_directorymover2)
{
    QString destdir("4testdest");
    QStringList sourcefiles;
    QDir dir;

    EXPECT_TRUE( dir.mkdir("4foo") && touch("4foo/abc")
                                   && touch("4foo/def")
                                   && touch("4foo/ghi") );
    sourcefiles << "4foo";

    int totalmoved = 0;
    for (int i = 0; i < sourcefiles.count(); ++i)
    {
        CDirectoryMover* mover = new CDirectoryMover(sourcefiles.at(i), destdir);
        mover->start();
        totalmoved += mover->transferredCount();
        mover->deleteLater();
    }

    EXPECT_TRUE(totalmoved == 4);
    EXPECT_TRUE(purge(destdir));
    EXPECT_TRUE(allDoesNotExists(sourcefiles));
}
