INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR})

SET(filemove2_HEADERS
    ../../../lihat/src/pathutils.h
    ../../../lihat/src/fileoperations/cfileoperationitem.h
    ../../../lihat/src/fileoperations/copy/cbasecopier.h
    ../../../lihat/src/fileoperations/copy/cbasefilecopier.h
    ../../../lihat/src/fileoperations/copy/cdirectorycopier.h
    ../../../lihat/src/fileoperations/copy/cfilecopier.h
    ../../../lihat/src/fileoperations/copy/cfilecopyqueue.h
    ../../../lihat/src/fileoperations/move/cbasefilemover.h
    ../../../lihat/src/fileoperations/move/cbasemover.h
    ../../../lihat/src/fileoperations/move/cdirectorymover.h
    ../../../lihat/src/fileoperations/move/cfilemovequeue.h
    ../../../lihat/src/fileoperations/move/cfilemover.h)
SET(filemove2_SOURCES
    filemove2.cpp
    ../../../lihat/src/pathutils.cpp
    ../../../lihat/src/fileoperations/cfileoperationitem.cpp
    ../../../lihat/src/fileoperations/copy/cbasefilecopier.cpp
    ../../../lihat/src/fileoperations/copy/cdirectorycopier.cpp
    ../../../lihat/src/fileoperations/copy/cfilecopier.cpp
    ../../../lihat/src/fileoperations/copy/cfilecopyqueue.cpp
    ../../../lihat/src/fileoperations/move/cbasefilemover.cpp
    ../../../lihat/src/fileoperations/move/cdirectorymover.cpp
    ../../../lihat/src/fileoperations/move/cfilemover.cpp
    ../../../lihat/src/fileoperations/move/cfilemovequeue.cpp)

IF(LIHAT_QT EQUAL 4)
    FIND_PACKAGE(Qt4 REQUIRED
                 QtCore QtGui)
    INCLUDE(${QT_USE_FILE})

    SET(filemove2_QT_INCLUDES
        ${QT_QTCORE_INCLUDE_DIR}
        ${QT_QTGUI_INCLUDE_DIR})
    SET(filemove2_QT_LIBRARIES
        ${QT_QTCORE_LIBRARY}
        ${QT_QTGUI_LIBRARY})

    QT4_WRAP_CPP(filemove2_QTGENERATED_MOC ${filemove2_HEADERS})
ELSEIF(LIHAT_QT EQUAL 5)
    FIND_PACKAGE(Qt5Core    REQUIRED)
    FIND_PACKAGE(Qt5Widgets REQUIRED)

    SET(filemove2_QT_INCLUDES
        ${Qt5Core_INCLUDE_DIRS}
        ${Qt5Widgets_INCLUDE_DIRS})
    SET(filemove2_QT_LIBRARIES
        ${Qt5Core_LIBRARIES}
        ${Qt5Widgets_LIBRARIES})

    QT5_WRAP_CPP(filemove2_QTGENERATED_MOC ${filemove2_HEADERS})
ENDIF()

INCLUDE_DIRECTORIES(${filemove2_QT_INCLUDES})
ADD_EXECUTABLE(filemove2
               ${filemove2_HEADERS}
               ${filemove2_SOURCES}
               ${filemove2_QTGENERATED_MOC})
TARGET_LINK_LIBRARIES(filemove2
                      ${filemove2_QT_LIBRARIES})

IF(LIHAT_QT EQUAL 5)
    QT5_USE_MODULES(filemove2
                    Core Widgets)
ENDIF()

ADD_DEPENDENCIES(filemove2 googletest)
TARGET_LINK_LIBRARIES(filemove2 pthread gtest gtest_main)

ADD_TEST(test_filecopierthread filemove2)
