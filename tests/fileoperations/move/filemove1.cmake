INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR})

SET(filemove1_HEADERS
    ../../../lihat/src/pathutils.h
    ../../../lihat/src/fileoperations/copy/cbasecopier.h
    ../../../lihat/src/fileoperations/copy/cbasefilecopier.h
    ../../../lihat/src/fileoperations/move/cbasefilemover.h
    ../../../lihat/src/fileoperations/move/cbasemover.h
    ../../../lihat/src/fileoperations/move/cdirectorymover.h
    ../../../lihat/src/fileoperations/move/cfilemover.h)
SET(filemove1_SOURCES
    filemove1.cpp
    ../../../lihat/src/pathutils.cpp
    ../../../lihat/src/fileoperations/copy/cbasefilecopier.cpp
    ../../../lihat/src/fileoperations/move/cbasefilemover.cpp
    ../../../lihat/src/fileoperations/move/cdirectorymover.cpp
    ../../../lihat/src/fileoperations/move/cfilemover.cpp)

IF(LIHAT_QT EQUAL 4)
    FIND_PACKAGE(Qt4 REQUIRED
                 QtCore)
    INCLUDE(${QT_USE_FILE})
    SET(filemove1_QT_INCLUDES
        ${QT_QTCORE_INCLUDE_DIR})
    SET(filemove1_QT_LIBRARIES
        ${QT_QTCORE_LIBRARY})

    QT4_WRAP_CPP(filemove1_QTGENERATED_MOC ${filemove1_HEADERS})
ELSEIF(LIHAT_QT EQUAL 5)
    FIND_PACKAGE(Qt5Core REQUIRED)

    SET(filemove1_QT_INCLUDES
        ${Qt5Core_INCLUDE_DIRS})
    SET(filemove1_QT_LIBRARIES
        ${QtCore_LIBRARIES})

    QT5_WRAP_CPP(filemove1_QTGENERATED_MOC ${filemove1_HEADERS})
ENDIF()

INCLUDE_DIRECTORIES(${filemove1_QT_INCLUDES})
ADD_EXECUTABLE(filemove1
               ${filemove1_HEADERS}
               ${filemove1_SOURCES}
               ${filemove1_QTGENERATED_MOC})
TARGET_LINK_LIBRARIES(filemove1
                      ${filemove1_QT_LIBRARIES})

IF(LIHAT_QT EQUAL 5)
    QT5_USE_MODULES(filemove1
                    Core)
ENDIF()

ADD_DEPENDENCIES(filemove1 googletest)
TARGET_LINK_LIBRARIES(filemove1 pthread gtest gtest_main)

ADD_TEST(test_filecopierthread filemove1)
