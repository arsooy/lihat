INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR})

SET(filedelete2_HEADERS
    ../../../lihat/src/pathutils.h
    ../../../lihat/src/fileoperations/cfileoperationitem.h
    ../../../lihat/src/fileoperations/delete/cbasefiledeleter.h
    ../../../lihat/src/fileoperations/delete/cdirectorydeleter.h
    ../../../lihat/src/fileoperations/delete/cfiledeleter.h
    ../../../lihat/src/fileoperations/delete/cfiledeletequeue.h)
SET(filedelete2_SOURCES
    filedelete2.cpp
    ../../../lihat/src/pathutils.cpp
    ../../../lihat/src/fileoperations/cfileoperationitem.cpp
    ../../../lihat/src/fileoperations/delete/cbasefiledeleter.cpp
    ../../../lihat/src/fileoperations/delete/cdirectorydeleter.cpp
    ../../../lihat/src/fileoperations/delete/cfiledeleter.cpp
    ../../../lihat/src/fileoperations/delete/cfiledeletequeue.cpp)

IF(LIHAT_QT EQUAL 4)
    FIND_PACKAGE(Qt4 REQUIRED
                 QtCore QtGui)
    INCLUDE(${QT_USE_FILE})
    
    SET(filedelete2_QT_INCLUDES
        ${QT_QTCORE_INCLUDE_DIR}
        ${QT_QTGUI_INCLUDE_DIR})
    SET(filedelete2_QT_LIBRARIES
        ${QT_QTCORE_LIBRARY}
        ${QT_QTGUI_LIBRARY})

    QT4_WRAP_CPP(filedelete2_QTGENERATED_MOC ${filedelete2_HEADERS})
ELSEIF(LIHAT_QT EQUAL 5)
    FIND_PACKAGE(Qt5Core    REQUIRED)
    FIND_PACKAGE(Qt5Widgets REQUIRED)
    
    SET(filedelete2_QT_INCLUDES
        ${Qt5Core_INCLUDE_DIRS}
        ${Qt5Widgets_INCLUDE_DIRS})
    SET(filedelete2_QT_LIBRARIES
        ${Qt5Core_LIBRARIES}
        ${Qt5Widgets_LIBRARIES})

    QT5_WRAP_CPP(filedelete2_QTGENERATED_MOC ${filedelete2_HEADERS})
ENDIF()

INCLUDE_DIRECTORIES(${filedelete2_QT_INCLUDES})
ADD_EXECUTABLE(filedelete2
               ${filedelete2_HEADERS}
               ${filedelete2_SOURCES}
               ${filedelete2_QTGENERATED_MOC})
TARGET_LINK_LIBRARIES(filedelete2
                      ${filedelete2_QT_LIBRARIES})

IF(LIHAT_QT EQUAL 5)
    QT5_USE_MODULES(filedelete2
                    Core Widgets)
ENDIF()

ADD_DEPENDENCIES(filedelete2 googletest)
TARGET_LINK_LIBRARIES(filedelete2 pthread gtest gtest_main)

ADD_TEST(test_filecopierthread filedelete2)
