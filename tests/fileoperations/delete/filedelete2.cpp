#include "fileoperations/delete/cfiledeletequeue.h"
#include "../shared.h"

#include "gtest/gtest.h"

#include <QApplication>
#include <QString>
#include <QStringList>

using namespace Lihat;
using namespace Lihat::FileOps;


int argc = 1;
char** argv = 0;
static QApplication app(argc, argv);

CFileDeleteQueue* createTheadedFileDelete(const QStringList& files)
{
    CFileDeleteQueue* result = new CFileDeleteQueue(files, &app);
    return result;
}


TEST(filecopierthread_filedelete, delete1)
{
    QStringList sourcefiles;
    if (touch("1foofile"))
        sourcefiles << "1foofile";

    CFileDeleteQueue* deletequeue = createTheadedFileDelete(sourcefiles);
    deletequeue->start();

    // wait until it's done
    while (deletequeue->status() == CFileDeleteQueue::QS_WORKING)
        app.processEvents();
    EXPECT_TRUE(deletequeue->status() == CFileDeleteQueue::QS_FINISHED);
    EXPECT_TRUE(deletequeue->totalDeleted() == 1);
    delete deletequeue;

    EXPECT_FALSE(exists(sourcefiles));
}

TEST(filecopierthread_filedelete, delete2)
{
    QStringList sourcefiles;
    if (touch("2foofile"))
        sourcefiles << "2foofile";
    if (touch("2goofile"))
        sourcefiles << "2goofile";
    if (touch("2barfile"))
        sourcefiles << "2barfile";
    if (touch("2bazfile"))
        sourcefiles << "2bazfile";

    CFileDeleteQueue* deletequeue = createTheadedFileDelete(sourcefiles);
    deletequeue->start();

    // wait until it's done
    while (deletequeue->status() == CFileDeleteQueue::QS_WORKING)
        app.processEvents();
    EXPECT_TRUE(deletequeue->status() == CFileDeleteQueue::QS_FINISHED);
    EXPECT_TRUE(deletequeue->totalDeleted() == 4);
    delete deletequeue;

    EXPECT_FALSE(exists(sourcefiles));
}

TEST(filecopierthread_filedelete, delete3)
{
    QStringList sourcefiles;
    QDir dir;

    /* Create a tree:
     * - 3foo
     *  |- abc
     *  |- def
     *  |- ghi
     * - 3bar
     *  |- rincianbiaya
     *  |- tembusan
     *  |- akomodasi
     *  | |- hotel
     *  | |- losmen
     *  |- peserta
     *  | |- grup_a
     *  | |- grup_b
     *  | |- grup_m
     * - 3baz
     *  |- spring.jpg
     *  |- summer.jpg
     *  |- fall.jpg
     *  |- winter.jpg
     * - 3fubar
     *
     * Items count: 20 (directories and files)
     */
    EXPECT_TRUE( dir.mkdir("3foo") && touch("3foo/abc")
                                   && touch("3foo/def")
                                   && touch("3foo/ghi") );
    sourcefiles << "3foo";

    EXPECT_TRUE( dir.mkdir("3bar") && touch("3bar/rincianbiaya")
                                   && touch("3bar/tembusan") );

    EXPECT_TRUE( dir.mkpath("3bar/akomodasi") && touch("3bar/akomodasi/hotel")
                                              && touch("3bar/akomodasi/losmen") );
    EXPECT_TRUE( dir.mkpath("3bar/peserta") && touch("3bar/peserta/grup_a")
                                            && touch("3bar/peserta/grup_b")
                                            && touch("3bar/peserta/grup_m") );
    sourcefiles << "3bar";

    EXPECT_TRUE( dir.mkdir("3baz") && touch("3baz/spring.jpg")
                                   && touch("3baz/summer.jpg")
                                   && touch("3baz/fall.jpg")
                                   && touch("3baz/winter.jpg") );
    sourcefiles << "3baz";

    EXPECT_TRUE( touch("3fubar") );
    sourcefiles << "3fubar";

    CFileDeleteQueue* deletequeue = createTheadedFileDelete(sourcefiles);
    deletequeue->start();

    // wait until it's done
    while (deletequeue->status() == CFileDeleteQueue::QS_WORKING)
        app.processEvents();
    EXPECT_TRUE(deletequeue->status() == CFileDeleteQueue::QS_FINISHED);
    EXPECT_TRUE(deletequeue->totalDeleted() == 20);
    delete deletequeue;

    EXPECT_FALSE(exists(sourcefiles));
}
