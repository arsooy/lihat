INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR})

SET(filedelete1_HEADERS
    ../../../lihat/src/pathutils.h
    ../../../lihat/src/fileoperations/delete/cbasefiledeleter.h
    ../../../lihat/src/fileoperations/delete/cdirectorydeleter.h
    ../../../lihat/src/fileoperations/delete/cfiledeleter.h)
SET(filedelete1_SOURCES
    filedelete1.cpp
    ../../../lihat/src/pathutils.cpp
    ../../../lihat/src/fileoperations/delete/cbasefiledeleter.cpp
    ../../../lihat/src/fileoperations/delete/cdirectorydeleter.cpp
    ../../../lihat/src/fileoperations/delete/cfiledeleter.cpp)

IF(LIHAT_QT EQUAL 4)
    FIND_PACKAGE(Qt4 REQUIRED
                 QtCore)
    INCLUDE(${QT_USE_FILE})
    SET(filedelete1_QT_INCLUDES
        ${QT_QTCORE_INCLUDE_DIR})
    SET(filedelete1_QT_LIBRARIES
        ${QT_QTCORE_LIBRARY})

    QT4_WRAP_CPP(filedelete1_QTGENERATED_MOC ${filedelete1_HEADERS})
ELSEIF(LIHAT_QT EQUAL 5)
    FIND_PACKAGE(Qt5Core REQUIRED)

    SET(filedelete1_QT_INCLUDES
        ${Qt5Core_INCLUDE_DIRS})
    SET(filedelete1_QT_LIBRARIES
        ${QtCore_LIBRARIES})

    QT5_WRAP_CPP(filedelete1_QTGENERATED_MOC ${filedelete1_HEADERS})
ENDIF()

INCLUDE_DIRECTORIES(${filedelete1_QT_INCLUDES})
ADD_EXECUTABLE(filedelete1
               ${filedelete1_HEADERS}
               ${filedelete1_SOURCES}
               ${filedelete1_QTGENERATED_MOC})
TARGET_LINK_LIBRARIES(filedelete1
                      ${filedelete1_QT_LIBRARIES})

IF(LIHAT_QT EQUAL 5)
    QT5_USE_MODULES(filedelete1
                    Core)
ENDIF()

ADD_DEPENDENCIES(filedelete1 googletest)
TARGET_LINK_LIBRARIES(filedelete1 pthread gtest gtest_main)

ADD_TEST(test_filecopierthread filedelete1)
