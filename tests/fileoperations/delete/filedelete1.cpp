#include "fileoperations/delete/cdirectorydeleter.h"
#include "fileoperations/delete/cfiledeleter.h"
#include "../shared.h"

#include "gtest/gtest.h"

#include <QString>
#include <QStringList>

using namespace Lihat;
using namespace Lihat::FileOps;

TEST(fileoperation_copy, filedeleter1)
{
    QStringList sourcefiles;
    if (touch("1foofile"))
        sourcefiles << "1foofile";

    int totaldeleted = 0;
    for (int i = 0; i < sourcefiles.count(); ++i)
    {
        CFileDeleter* deleter = new CFileDeleter(sourcefiles.at(i));
        deleter->start();
        totaldeleted += deleter->deletedCount();
        deleter->deleteLater();
    }

    EXPECT_TRUE(totaldeleted == 1);
    EXPECT_FALSE(exists(sourcefiles));
}

TEST(fileoperation_copy, filedeleter2)
{
    QStringList sourcefiles;
    if (touch("2foofile"))
        sourcefiles << "2foofile";
    if (touch("2goofile"))
        sourcefiles << "2goofile";
    if (touch("2barfile"))
        sourcefiles << "2barfile";
    if (touch("2bazfile"))
        sourcefiles << "2bazfile";

    int totaldeleted = 0;
    for (int i = 0; i < sourcefiles.count(); ++i)
    {
        CFileDeleter* deleter = new CFileDeleter(sourcefiles.at(i));
        deleter->start();
        totaldeleted += deleter->deletedCount();
        deleter->deleteLater();
    }

    EXPECT_TRUE(totaldeleted == 4);
    EXPECT_FALSE(exists(sourcefiles));
}

TEST(fileoperation_directorydelete, directorydeleter1)
{
    QStringList sourcefiles;
    QDir dir;

    EXPECT_TRUE( dir.mkdir("3foo") );
    sourcefiles << "3foo";

    int totaldeleted = 0;
    for (int i = 0; i < sourcefiles.count(); ++i)
    {
        CDirectoryDeleter* deleter = new CDirectoryDeleter(sourcefiles.at(i));
        deleter->start();
        totaldeleted += deleter->deletedCount();
        deleter->deleteLater();
    }

    EXPECT_TRUE(totaldeleted == 1);
    EXPECT_FALSE(exists(sourcefiles));
}

TEST(fileoperation_directorydelete, directorydeleter2)
{
    QStringList sourcefiles;
    QDir dir;

    EXPECT_TRUE( dir.mkdir("4foo") && touch("4foo/abc")
                                   && touch("4foo/def")
                                   && touch("4foo/ghi") );
    sourcefiles << "4foo";

    int totaldeleted = 0;
    for (int i = 0; i < sourcefiles.count(); ++i)
    {
        CDirectoryDeleter* deleter = new CDirectoryDeleter(sourcefiles.at(i));
        deleter->start();
        totaldeleted += deleter->deletedCount();
        deleter->deleteLater();
    }

    EXPECT_TRUE(totaldeleted == 4);
    EXPECT_FALSE(exists(sourcefiles));
}
