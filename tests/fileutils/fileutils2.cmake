INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR})

IF(LIHAT_QT EQUAL 4)
    FIND_PACKAGE(Qt4  REQUIRED
                 QtCore)
    INCLUDE(${QT_USE_FILE})

    SET(fileutils2_QT_INCLUDES
        ${QT_QTCORE_INCLUDE_DIR})
ELSEIF(LIHAT_QT EQUAL 5)
    FIND_PACKAGE(Qt5Core REQUIRED)

    SET(fileutils2_QT_INCLUDES
        ${Qt5Core_INCLUDE_DIRS})
ENDIF()

SET(fileutils2_HEADERS)
SET(fileutils2_SOURCES
    fileutils2.cpp)

SET(fileutils2_INCLUDES
    ${lihat_QT_INCLUDES})
SET(fileutils2_LIBRARIES
    lihatlib
    ${lihat_QT_LIBRARIES})

INCLUDE_DIRECTORIES(${fileutils2_INCLUDES})
ADD_EXECUTABLE(fileutils2
               ${fileutils2_HEADERS}
               ${fileutils2_SOURCES})
TARGET_LINK_LIBRARIES(fileutils2 ${fileutils2_LIBRARIES})

IF(LIHAT_QT EQUAL 5)
    QT5_USE_MODULES(fileutils2
                    Core)
ENDIF()

ADD_DEPENDENCIES(fileutils2 googletest)
TARGET_LINK_LIBRARIES(fileutils2 pthread gtest gtest_main)

ADD_TEST(test_filecopierthread fileutils2)
