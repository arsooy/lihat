INCLUDE_DIRECTORIES(${CMAKE_CURRENT_BINARY_DIR})

IF(LIHAT_QT EQUAL 4)
    FIND_PACKAGE(Qt4  REQUIRED
                 QtCore)
    INCLUDE(${QT_USE_FILE})

    SET(fileutils1_QT_INCLUDES
        ${QT_QTCORE_INCLUDE_DIR})
ELSEIF(LIHAT_QT EQUAL 5)
    FIND_PACKAGE(Qt5Core REQUIRED)

    SET(fileutils1_QT_INCLUDES
        ${Qt5Core_INCLUDE_DIRS})
ENDIF()

SET(fileutils1_HEADERS)
SET(fileutils1_SOURCES
    fileutils1.cpp)

SET(fileutils1_INCLUDES
    ${lihat_QT_INCLUDES})
SET(fileutils1_LIBRARIES
    lihatlib
    ${lihat_QT_LIBRARIES})

INCLUDE_DIRECTORIES(${fileutils1_INCLUDES})
ADD_EXECUTABLE(fileutils1
               ${fileutils1_HEADERS}
               ${fileutils1_SOURCES})
TARGET_LINK_LIBRARIES(fileutils1 ${fileutils1_LIBRARIES})

IF(LIHAT_QT EQUAL 5)
    QT5_USE_MODULES(fileutils1
                    Core)
ENDIF()

ADD_DEPENDENCIES(fileutils1 googletest)
TARGET_LINK_LIBRARIES(fileutils1 pthread gtest gtest_main)

ADD_TEST(test_filecopierthread fileutils1)
