#include <QFile>

#include "fileutils.h"
#include "gtest/gtest.h"

using namespace Lihat;

TEST(fileutils_1, fileIsText_1)
{
    QFile file("testfile.foo");
    file.open(QIODevice::WriteOnly | QIODevice::Truncate);
    file.write("This is a text file.");
    file.close();

    EXPECT_TRUE(Utils::File::fileIsText("testfile.foo"));
    file.remove();

    file.open(QIODevice::WriteOnly | QIODevice::Truncate);
    file.write("\xEF\xBB\xBF\x48\x65\x6C\x6C\x6F\x20\x57\x6F\x72\x6C\x64\x21");  // (UTF-8 BOM)Hello World!
    file.close();

    EXPECT_TRUE(Utils::File::fileIsText("testfile.foo"));
    file.remove();

    file.open(QIODevice::WriteOnly | QIODevice::Truncate);
    file.write("1\tTepung terigu (800gr).\n"
               "2\tTelur (6 butir)\n"
               "3\tGula (120gr)\n"
               "4\tSusu cair (300ml)\n"
               "5\tKayu Manis (2 buah)");
    file.close();

    EXPECT_TRUE(Utils::File::fileIsText("testfile.foo"));
    file.remove();
}

TEST(fileutils_1, fileIsText_2)
{
    QFile file("testfile.foo");
    file.open(QIODevice::WriteOnly | QIODevice::Truncate);
    file.write("\xDE\xAD\xBE\xEF\x15\xDE\xAD");
    file.close();

    EXPECT_FALSE(Utils::File::fileIsText("testfile.foo"));
    file.remove();

    file.open(QIODevice::WriteOnly | QIODevice::Truncate);
    file.write("FZX\x03\xFE\xACIZ\x00\x00\xC7\x83\x84\x88\x00\x00");
    file.close();

    EXPECT_FALSE(Utils::File::fileIsText("testfile.foo"));
    file.remove();
}
