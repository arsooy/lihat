#include <QFile>

#include "fileutils.h"
#include "gtest/gtest.h"

using namespace Lihat;

TEST(fileutils_2, fileSizeToString)
{
    QString test;

    test = Utils::File::fileSizeToString(1024);
    EXPECT_TRUE(test == "1 KiB");

    test = Utils::File::fileSizeToString(354282);
    EXPECT_TRUE(test == "345.98 KiB");

    test = Utils::File::fileSizeToString(1024 * 1024);
    EXPECT_TRUE(test == "1 MiB");

    test = Utils::File::fileSizeToString(1724623);
    EXPECT_TRUE(test == "1.64 MiB");

    test = Utils::File::fileSizeToString(1024 * 1024 * 1024);
    EXPECT_TRUE(test == "1 GiB");

    test = Utils::File::fileSizeToString(4123168605);
    EXPECT_TRUE(test == "3.84 GiB");
}
