#include <icoutils.h>

#include <QImage>
#include <QImageReader>
#include <QSize>
#include <QtDebug>

#include "gtest/gtest.h"


using namespace Lihat;

TEST(icoutils_1, loadicon)
{
    {
        QImageReader reader("32x32_4.ico");
        reader.read();

        EXPECT_TRUE(Utils::ICO::locateNicerIcon(&reader, QSize(32, 32)) == Utils::ICO::NILR_SUCCESS);
        QImage img;
        EXPECT_TRUE(reader.read(&img));

        EXPECT_TRUE(img.size() == QSize(32, 32));
    }

    {
        QImageReader reader("32x32_8.ico");
        reader.read();

        EXPECT_TRUE(Utils::ICO::locateNicerIcon(&reader, QSize(16, 16)) == Utils::ICO::NILR_SUCCESS);
        QImage img;
        EXPECT_TRUE(reader.read(&img));

        EXPECT_TRUE(img.size() == QSize(32, 32));
    }
}

TEST(icoutils_2, icons)
{
    {
        QImageReader reader("composite_2.ico");
        reader.read();

        EXPECT_TRUE(Utils::ICO::locateNicerIcon(&reader, QSize(16, 16)) == Utils::ICO::NILR_SUCCESS);
        QImage img;
        EXPECT_TRUE(reader.read(&img));

        EXPECT_TRUE(img.size() == QSize(16, 16));
    }

    {
        QImageReader reader("composite_2.ico");
        reader.read();

        EXPECT_TRUE(Utils::ICO::locateNicerIcon(&reader, QSize(48, 48)) == Utils::ICO::NILR_SUCCESS);
        QImage img;
        EXPECT_TRUE(reader.read(&img));

        EXPECT_TRUE(img.size() == QSize(48, 48));
    }
}

TEST(icoutils_3, pngicons)
{
    // composite_3.ico contains 16x16, 24x24, 32x32, 48x48, 64x64 and vista's
    // 256x256 png

    {
        QImageReader reader("composite_3.ico");
        reader.read();

        // asking for moderate size, this should be closer to the (smaller) 64x64 icon
        EXPECT_TRUE(Utils::ICO::locateNicerIcon(&reader, QSize(96, 96)) == Utils::ICO::NILR_SUCCESS);
        QImage img;
        EXPECT_TRUE(reader.read(&img));

        EXPECT_TRUE(img.size() == QSize(64, 64));
    }

    {
        QImageReader reader("composite_3.ico");
        reader.read();

        // asking for rather large size, this should return the Vista png icon
        EXPECT_TRUE(Utils::ICO::locateNicerIcon(&reader, QSize(190, 190)) == Utils::ICO::NILR_SUCCESS);
        QImage img;
        EXPECT_TRUE(reader.read(&img));

        EXPECT_TRUE(img.size() == QSize(256, 256));
    }
}
