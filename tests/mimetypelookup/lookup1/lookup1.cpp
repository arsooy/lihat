#include "cmimetyperesolver.h"
#include "gtest/gtest.h"


using namespace Lihat;

TEST(mimetypelookup_1, 00_has_libmagic_check)
{
#ifdef HAS_LIBMAGIC
    //  lookup1 is about _not_ using libmagic, so this part of code should
    //  never be included in compilation
    EXPECT_TRUE(false);
#endif
}

TEST(mimetypelookup_1, create)
{
    QString mimetype;
    CMIMETypeResolver resolver;

    mimetype = resolver.typeNameFromFilename("foo.tar.gz");
    EXPECT_TRUE(mimetype == QString("application/x-compressed-tar"));

    // second lookup should be faster ...
    mimetype = resolver.typeNameFromFilename("bar.zip");
    EXPECT_TRUE(mimetype == QString("application/zip"));
}
