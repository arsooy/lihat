#include "cmimetyperesolver.h"
#include "gtest/gtest.h"


using namespace Lihat;

TEST(MIMETypeLookup, 00_has_libmagic_check)
{
#ifdef HAS_LIBMAGIC
    EXPECT_TRUE(true);
#endif
}

TEST(MIMETypeLookup, create)
{
    CMIMETypeResolver resolver;
    QString result = resolver.typeNameFromFilename("/usr/share/mime/packages/freedesktop.org.xml");
    EXPECT_TRUE(result == QString("application/xml"));
}

TEST(MIMETypeLookup, lookup_suffix)
{
    CMIMETypeResolver resolver;
    QString result;

    // '*foo' pattern matching
    result = resolver.typeNameFromFilename("Bar.cmake");
    EXPECT_TRUE(result == QString("text/x-cmake"));
}

TEST(MIMETypeLookup, lookup_prefix)
{
    CMIMETypeResolver resolver;
    QString result;

    // 'foo*' pattern matching
    result = resolver.typeNameFromFilename("README.NOW");
    EXPECT_TRUE(result == QString("text/x-readme"));
}

TEST(MIMETypeLookup, lookup_fullname)
{
    CMIMETypeResolver resolver;
    QString result;

    // 'foo' pattern matching
    result = resolver.typeNameFromFilename("AUTHORS");
    EXPECT_TRUE(result == QString("text/x-authors"));
}

TEST(MIMETypeLookup, type_matching)
{
    CMIMETypeResolver resolver;
    QString result;

    // 'baz.tar.xz' should return 'application/x-xz-compressed-tar' instead
    // of 'application/x-xz'
    result = resolver.typeNameFromFilename("baz.tar.xz");
    EXPECT_TRUE(result == QString("application/x-xz-compressed-tar"));
    // this should return 'application/x-xz'
    result = resolver.typeNameFromFilename("baz.xz");
    EXPECT_TRUE(result == QString("application/x-xz"));

    // for MIME type that provides sub-class-of, setting baseclass parameter
    // to true should return 'application/x-xz' instead of
    // 'application/x-xz-compressed-tar'
    result = resolver.typeNameFromFilename("baz.tar.xz", true);
    EXPECT_TRUE(result == QString("application/x-xz"));
}
